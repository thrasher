#!/usr/bin/perl

# While throwing around XML fragments results in some ugly source
# code, it seem safer and more correct than trying to do anything
# else fancy.

use Test::More 'no_plan';
use Test::Deep;

use strict;
use warnings;

use Data::Dumper;

BEGIN {
    use_ok 'Thrasher', qw(:all);
    use_ok 'Thrasher::Component';
    use_ok 'Thrasher::Constants', qw(:all);
    use_ok 'Thrasher::Protocol';
    use_ok 'Thrasher::Protocol::Test';
    use_ok 'Thrasher::Backend';
    use_ok 'Thrasher::Backend::Test';
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::Roster', qw(:constants);
    use_ok 'Thrasher::ConnectionManager', qw(:all);
}

# This test was written before the connection manager, and really
# doesn't depend on it.
$Thrasher::Component::USE_CONNECTION_MANAGER = 0;

my $JID = transport_jid;

# Test the connection and binding process.
CONNECTION_AND_BINDING: {
    my $comp = new_component;

    is(ref($comp->{protocol}), 'Thrasher::Protocol::Test',
       'component has a test protocol in it');

    # Send in the initial stream tag, check the resulting handshake
    # for correctness
    send_server_stream($comp);
}

# Test that we correct detect bad handshake errors.
BAD_HANDSHAKE: {
    my $comp = new_component;

    no strict qw(refs);
    no warnings qw(redefine);
    local *{'Thrasher::Component::reconnect_stream'} = sub {
        die('calling reconnect_stream()');
    };

    send_server_stream($comp);
    #$comp->xml_in('<stream:error>Invalid Handshake</stream:error>'
    #.'</stream:stream>');
    dies {
        $comp->xml_in('<stream:error>Invalid Handshake</stream:error>'
                      .'</stream:stream>');
    } 'calling reconnect_stream()',
    'bad handshakes correctly detected';
}

IQ_HANDLING: {
    my $comp = connected_component;
    my $test_ns = 'xxx';

    my $test_ns_handler_fired = 0;
    my $test_ns_handler = sub {
        $test_ns_handler_fired = 1;
    };
    my $plugin_data = {
        'component_iq_handlers' => {
            $test_ns => { 'get' => $test_ns_handler },
        },
    };
    Thrasher::Plugin::register_plugin($plugin_data);

    my $expect_id = 'id' . $Thrasher::Component::id;
    my $result_handler_fired = 0;
    $comp->iq_query([[ $NS_COMPONENT, 'iq' ],
                     {
                         to => 'nowhere',
                         from => $comp->{'component_name'},
                         type => 'get',
                     },
                     [[[ $test_ns, 'query' ], {}, []]]],
                    sub { $result_handler_fired = 1; });

        $comp->xml_in(<<IQ);
<iq type='get'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='${expect_id}'>
    <query xmlns='${test_ns}' />
</iq>
IQ
    ok((! $result_handler_fired),
       'handler expecting a result must not be confused with requests');
    ok($test_ns_handler_fired,
       'handler for test NS result was fired instead');
    Thrasher::Plugin::unregister_plugin($plugin_data);

    $result_handler_fired = 0;
    $comp->xml_in(<<IQ);
<iq type='get'
    from='nowhere'
    to='$JID'
    id='${expect_id}'>
    <query xmlns='${test_ns}' />
</iq>
IQ
    ok((! $result_handler_fired),
       'non-result IQs do not fire result handler');

    $result_handler_fired = 0;
    $comp->xml_in(<<IQ);
<iq type='result'
    from='nowhere'
    to='$JID'
    id='${expect_id}'>
    <query xmlns='${test_ns}' />
</iq>
IQ
    ok($result_handler_fired,
       'result IQs do fire the result handler');

    $result_handler_fired = 0;
    $expect_id = 'id' . $Thrasher::Component::id;
    $comp->iq_query([[ $NS_COMPONENT, 'iq' ],
                     {
                         to => 'nowhere',
                         from => $comp->{'component_name'},
                         type => 'get',
                     },
                     [[[ $test_ns, 'query' ], {}, []]]],
                    sub { $result_handler_fired = 1; });
    $comp->xml_in(<<IQ);
<iq type='error'
    from='nowhere'
    to='$JID'
    id='${expect_id}'>
    <query xmlns='${test_ns}' />
</iq>
IQ
    ok($result_handler_fired,
       'error IQs do fire the result handler');
}

# Test the two types of discovery, using the discovery and the
# agents protocol. Section 4.1 of the protocol.
DISCOVERY: {
    COMPONENT_DISCOVERY: {
        my $comp = connected_component;
        $comp->xml_in(<<DISCO);
<iq type='get'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='disco1'>
    <query xmlns='http://jabber.org/protocol/disco#info'
           node='http://testnode/#test' />
</iq>
DISCO

        my $disco_result = clean_xml(<<DISCO_RESULT);
<iq 
  from='test.transport' 
  id='disco1' 
  to='romeo\@montague.lit/orchard' 
  type='result'>
    <query node='http://testnode/#test' 
           xmlns='http://jabber.org/protocol/disco#info'>
       <identity category='gateway' name='Test Gateway' type='aim'/>
       <feature var='http://jabber.org/protocol/chatstates'/>
       <feature var='http://jabber.org/protocol/disco#info'/>
       <feature var='http://jabber.org/protocol/disco#items'/>
       <feature var='jabber:iq:register'/>
       <feature var='jabber:iq:time'/>
       <feature var='jabber:iq:version'/>
    </query>
</iq>
DISCO_RESULT

        is(output, $disco_result, 'proper discovery reply');

        $comp->xml_in(<<DISCO);
<iq type='get'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='disco1'>
    <query xmlns='http://jabber.org/protocol/disco#items' />
</iq>
DISCO
        my $disco_items_result = clean_xml(<<DISCO_RESULT);
<iq 
  from='test.transport' 
  id='disco1' 
  to='romeo\@montague.lit/orchard' 
  type='result'>
    <query xmlns='http://jabber.org/protocol/disco#items'/>
</iq>
DISCO_RESULT

        is(output, $disco_items_result, 
           'proper discovery items reply');
    }

    # Test a discovery request fired against the virtual client
    # that a legacy user represents.
    # We're cheating a bit here, in that we'll answer the same
    # to all queries, regardless of whether there's an actual 
    # user behind the given username.
    CLIENT_DISCOVERY: {
        my $comp = connected_component;
        $comp->xml_in(<<DISCO);
<iq type='get'
    from='romeo\@montague.lit/orchard'
    to='juliet\@$JID'
    id='disco1'>
    <query xmlns='http://jabber.org/protocol/disco#info'
           node='http://testnode/#test' />
</iq>
DISCO

        my $disco_result = clean_xml(<<DISCO_RESULT);
<iq 
  from='juliet\@$JID' 
  id='disco1' 
  to='romeo\@montague.lit/orchard' 
  type='result'>
    <query node='http://testnode/#test' 
           xmlns='http://jabber.org/protocol/disco#info'>
       <identity category='gateway' name='Test Gateway' type='aim'/>
       <feature var='http://jabber.org/protocol/disco#info'/>
       <feature var='http://jabber.org/protocol/disco#items'/>
       <feature var='jabber:iq:register'/>
       <feature var='jabber:iq:time'/>
       <feature var='jabber:iq:version'/>
    </query>
</iq>
DISCO_RESULT

        is(output(),
           $disco_result,
           'proper discovery reply for the virtual clients');

        $comp->xml_in(<<DISCO);
<iq type='get'
    from='romeo\@montague.lit/orchard'
    to='juliet\@$JID'
    id='disco1'>
    <query xmlns='http://jabber.org/protocol/disco#items' />
</iq>
DISCO
        my $disco_items_result = clean_xml(<<DISCO_RESULT);
<iq 
  from='juliet\@$JID' 
  id='disco1' 
  to='romeo\@montague.lit/orchard' 
  type='result'>
    <query xmlns='http://jabber.org/protocol/disco#items'/>
</iq>
DISCO_RESULT

        is(output, $disco_items_result, 
           'proper discovery items reply from the virtual client');

    }
}

REGISTRATION: {
    CORRECT_REGISTRATION: {
        my $comp = connected_component;

        my $registration_query = <<REGISTRATION_QUERY;
<iq type='get' 
    from='romeo\@montague.lit/orchard' 
    to='$JID'
    id='reg1'>
  <query xmlns='$NS_REGISTER' />
</iq>
REGISTRATION_QUERY

        $comp->xml_in($registration_query);

        my $register_query_result = clean_xml(<<DISCO_RESULT);
<iq from='test.transport' 
    id='reg1' 
    to='romeo\@montague.lit/orchard' 
    type='result'>
    <query xmlns='jabber:iq:register'>
        <instructions>Please provide your username and password for Test</instructions>
        <username/>
        <password/>
    </query>
</iq>
DISCO_RESULT

        clear;

        # Send back a good username and password
        $comp->xml_in(<<GOOD_PASSWORD);
<iq type='set'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='reg2'>
    <query xmlns='jabber:iq:register'>
        <username>RomeoMyRomeo</username>
        <password>ILoveJuliet</password>
    </query>
</iq>
GOOD_PASSWORD

        cmp_deeply($comp->{protocol}->{backend}->{registered},
                   {'romeo@montague.lit' => {username => 'RomeoMyRomeo', 
                     password => 'ILoveJuliet'}});
        my $expected = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='reg2'
    to='romeo\@montague.lit/orchard' 
    type='result'/>
<presence from='$JID' to='romeo\@montague.lit' type='subscribe'/>
<presence from='$JID' to='romeo\@montague.lit' type='probe'/>
EXPECTED
        is(output, $expected,
           'correctly replies that registration is successful');

        # Verify the registration info is correct.
        $comp->xml_in($registration_query);
        $register_query_result = clean_xml(<<REGISTRATION_QUERY_RESULT);
<iq from='test.transport' 
    id='reg1' 
    to='romeo\@montague.lit/orchard' 
    type='result'>
    <query xmlns='jabber:iq:register'>
        <instructions>Please provide your username and password for Test</instructions>
        <registered/>
        <username>RomeoMyRomeo</username>
        <password>ILoveJuliet</password>
    </query>
</iq>
REGISTRATION_QUERY_RESULT

        is(output, $register_query_result,
           'correctly returns username and password info');
        is($comp->{protocol}->{backend}->{registered}->{'romeo@montague.lit/orchard'}, 
           undef, 'protocol doesn\'t register with jid');
        cmp_deeply($comp->{protocol}->{backend}->{registered}->{'romeo@montague.lit'},
                   {username => 'RomeoMyRomeo',
                    password => 'ILoveJuliet'},
                   'protocol correctly registers with the backend');

        # And now, I can unregister
        $comp->xml_in(<<UNREGISTER);
<iq type='get' 
    from='romeo\@montague.lit/orchard' 
    to='$JID'
    id='reg3'>
  <query xmlns='$NS_REGISTER'>
    <remove/>
  </query>
</iq>
UNREGISTER

        $expected = clean_xml(<<UNREGISTRATION);
<iq from='test.transport' 
    id='reg3' 
    to='romeo\@montague.lit/orchard' 
    type='result'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unsubscribe'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unsubscribed'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unavailable'/>
UNREGISTRATION
        is(output, $expected,
           "unregistering from the transport gets correct XML");
        is(undef,
           $comp->{protocol}->{backend}->{registered}->{'romeo@montague.lit'}, 
           'protocol unregistered');
    }

    # This actually covers all the failure code paths in the spec
    # for registration; it may document them separately but they
    # are the same path
    FAILED_REGISTRATION: {
        my $comp = connected_component;

        # Note we don't actually have to discover the registration
        # requirements, we can just know them
        $comp->xml_in(<<BAD_PASSWORD);
<iq type='set'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='reg2'>
    <query xmlns='jabber:iq:register'>
        <username>fail</username>
        <password>forbidden</password>
    </query>
</iq>
BAD_PASSWORD

        my $expected_response = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='reg2' 
    to='romeo\@montague.lit/orchard' 
    type='error'>
    <query xmlns='jabber:iq:register'>
        <username>fail</username>
        <password>forbidden</password>
    </query>
    <error code='403' type='auth'><forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/></error>
</iq>
EXPECTED
        is(clean_xml(output), $expected_response,
           'component can handle protocol registration failures');
    }

    # A real bug encountered by our client; if you send in too
    # much information the registration fails miserably.
    TOO_DETAILED_REGISTRATION: {
        my $comp = connected_component;

        # Note we don't actually have to discover the registration
        # requirements, we can just know them
        $comp->xml_in(<<TMI);
<iq type='set'
    from='romeo\@montague.lit/orchard'
    to='$JID'
    id='reg2'>
    <query xmlns='jabber:iq:register'>
        <username>RomeoMyRomeo</username>
        <nick>Nickname</nick>
        <password>ILoveJuliet</password>
    </query>
</iq>
TMI

        my $expected_response = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='reg2'
    to='romeo\@montague.lit/orchard' 
    type='result'/>
<presence from='$JID' to='romeo\@montague.lit' type='subscribe'/>
<presence from='$JID' to='romeo\@montague.lit' type='probe'/>
EXPECTED

        is(output, $expected_response,
           'successfully registered with transport with TMI');
        my $registration = $comp->{protocol}->{backend}->{registered}->{'romeo@montague.lit'};
        cmp_deeply($registration,
                   {username => 'RomeoMyRomeo',
                    nick => 'Nickname',
                    password => 'ILoveJuliet'},
                   'protocol correctly registers with the backend');
    }
}

LOG_IN: {
    # This tests the simple case, where we are directly logging in
    # by telling the component we are present.
    INITIAL: {
        my $comp = get_registered_comp;
        $comp->xml_in(<<LOGIN);
<presence from='romeo\@montague.lit/orchard'
          to='$JID'/>
LOGIN
        is(scalar(@{$comp->{protocol}->{logged_in}}), 1, 
                  'have a logged in user');
        my $expected_response = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='id1' 
    to='romeo\@montague.lit/orchard'
    type='get'>
    <query xmlns='http://jabber.org/protocol/disco#info'/>
</iq>
<presence from='test.transport' to='romeo\@montague.lit'/>
EXPECTED
        is(output, $expected_response,
           'gateway indicated it is online and fired disco query.');
    }

    BAD_LOGIN: {
        my $comp = get_registered_comp;
        $comp->xml_in(<<LOGIN);
<presence from='juliet\@capulet.lit' to='$JID'/>
LOGIN
        is($comp->{protocol}->{logged_in}, undef, 
                  'log in failed');
        my $expected_response = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='juliet\@capulet.lit'
          type='error'>
    <error code='504' type='wait'>
        <remote-server-timeout xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
</presence>
EXPECTED

        is(output, $expected_response,
           'gateway returned error as expected');

        $comp = connected_component;
        # BAD LOGIN due to corruption in the backend resulting
        # in not having all the required pieces. Hack away the
        # username for this user.
        $comp->{protocol}->registration('romeo@montague.lit',
                                        {password => 'ILoveJuliet'});
        $comp->xml_in(<<LOGIN);
<presence from='romeo\@montague.lit' to='$JID'/>
LOGIN

        is($comp->{protocol}->{logged_in}, undef, 'log in failed');
        $expected_response = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='romeo\@montague.lit'
          type='error'>
    <error code='407' type='auth'>
        <registration-required xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
</presence>
EXPECTED

        is(output, $expected_response,
           'spontaneously unregistered user with bad reg data');
    }
}

LOG_OUT_AND_UNREGISTRATION: {
    NORMAL_LOG_OUT: {
        # The normal log out procedure
        my $comp = logged_in_comp;
        $comp->xml_in(<<LOGOUT);
<presence from='romeo\@montague.lit/orchard'
          to='$JID'
          type='unavailable'/>
LOGOUT

        my $expected = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unavailable'/>
EXPECTED
        is(output, $expected, 
           "upon logging out, get expected presence unavailable");
        is($comp->session_for('romeo@montague.lit'),
           undef, "session has been removed for the user");
    }

    UNREGISTER_NOT_LOGGED_IN: {
        my $comp = get_registered_comp;
        $comp->xml_in(<<UNREGISTER);
<iq type='get' 
    from='romeo\@montague.lit/orchard' 
    to='$JID'
    id='reg3'>
  <query xmlns='$NS_REGISTER'>
    <remove/>
  </query>
</iq>
UNREGISTER

        my $expected = clean_xml(<<UNREGISTER);
<iq from='test.transport' 
    id='reg3' 
    to='romeo\@montague.lit/orchard' 
    type='result'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unsubscribe'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unsubscribed'/>
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unavailable'/>
UNREGISTER
        is(output, $expected,
           "unregistering while not logged in works as expected");
    }

    # Unregistering while logged in is covered above, in the
    # registration sequence

    # An error case, basically: The user is unregistering, 
    # but they aren't registered in the first place.
    UNREGISTER_NOT_REGISTERED: {
        my $comp = get_registered_comp;
        $comp->xml_in(<<UNREGISTER);
<iq type='get' 
    from='balthasar\@montague.lit/tomb' 
    to='$JID'
    id='reg3'>
  <query xmlns='$NS_REGISTER'>
    <remove/>
  </query>
</iq>
UNREGISTER

        my $expected = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='reg3' 
    to='balthasar\@montague.lit/tomb' 
    type='error'>
    <query xmlns='jabber:iq:register'>
        <remove/>
    </query>
    <error code='407' 
           type='auth'>
        <registration-required xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
</iq>
EXPECTED
        is(clean_xml(output), $expected,
           'correct error for unregistering a not-registered user')
    }
}

SUBSCRIBING: {
    SUCCESS: {
        my $comp = logged_in_comp;

        my $get_roster = sub {
            $comp->{protocol}->{backend}->get_roster('romeo@montague.lit')
        };
        # Verify the initial roster is empty.
        cmp_deeply($get_roster->(), {},
                   'initial roster for romeo is confirmed empty');

        my $subscribe_xml = <<SUBSCRIBE;
<presence type='subscribe'
          from='romeo\@montague.lit'
          to='CapuletNurse\@$JID'/>
SUBSCRIBE
        $comp->xml_in($subscribe_xml);

        my $expected = clean_xml(<<EXPECTED);
<presence from='CapuletNurse\@$JID' 
          to='romeo\@montague.lit' 
          type='subscribed'/>
<presence from='CapuletNurse\@$JID' 
          to='romeo\@montague.lit' 
          type='subscribe'/>
EXPECTED
        is(output, $expected,
           'proper response to successful subscriptions');
        cmp_deeply($get_roster->(), 
                   {CapuletNurse => subscribed},
                   'correctly subscribed to CapuletNurse');

        # Unsubscribing.
        
        my $unsubscribe_xml = <<UNSUBSCRIBE;
<presence type='unsubscribe'
          from='romeo\@montague.lit'
          to='CapuletNurse\@$JID'/>
UNSUBSCRIBE
        $comp->xml_in($unsubscribe_xml);

        $expected = clean_xml(<<EXPECTED);
<presence from='romeo\@montague.lit' 
          to='CapuletNurse\@test.transport'
          type='unsubscribe'/>
<presence from='romeo\@montague.lit'
          to='CapuletNurse\@test.transport'
          type='unsubscribed'/>
<presence from='CapuletNurse\@test.transport' 
          to='romeo\@montague.lit'
          type='unavailable'/>
EXPECTED
        is(output, $expected,
           'proper response to unsubscriptions');
        cmp_deeply($get_roster->(), {},
                   'and once again unsubscribed from CapuletNurse');
    }
      
    FAILURE: {
        my $comp = logged_in_comp;
        my $subscribe_xml = <<SUBSCRIBE;
<presence type='subscribe'
          from='romeo\@montague.lit'
          to='fail\@$JID'/>
SUBSCRIBE
        $comp->xml_in($subscribe_xml);
        is(output, 
           "<presence from='fail\@$JID' to='romeo\@montague.lit' "
           ."type='unsubscribed'/>",
           "correctly responds to failing to subscribe");
    }
}

MESSAGES: {
    # This should also test an "out-of-the-blue" sending, we try to 
    # guess the remote transport id even though we don't really
    # know. This really shouldn't come up; even if the client
    # supports "out-of-the-blue" sending on a transport, it
    # should still use the gateway protocol.

    my $comp = logged_in_comp;

    # FIXME: Address above, think about the next line
    $comp->legacy_name_to_xmpp('romeo@montague.lit', 'juliet');

    my $message = <<MESSAGE;
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'>
  <body>Neither, fair saint, if either thee dislike.</body>
</message>
MESSAGE

    $comp->xml_in($message);

    my $messages = \($comp->session_for('romeo@montague.lit/orchard')->{messages});
    cmp_deeply($$messages,
               [["juliet", 'Neither, fair saint, if either '
                .'thee dislike.', 'chat']]);
    @$$messages = ();

    # BAD INPUT: Message bodies with more tags.
    # This shouldn't be possible, so the main goal is to
    # not crash.
    $message = <<MESSAGE;
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'>
  <body>Neither, <b>fair</b> saint, if either thee dislike.</body>
</message>
MESSAGE
    $comp->xml_in($message);
    cmp_deeply($$messages,
               [["juliet", 'Neither,  saint, if either thee dislike.',
                 'chat']],
               "don't crash on bad message bodies");

    clear_log;
    # BAD INPUT: An otherwise-correct message with no body.
    $message = <<MESSAGE;
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'/>
MESSAGE
    $comp->xml_in($message);
    logged('Message without usable child',
           "doesn't try to process message without body");

    # diag("Beginning message type tests");
    clear_log();
    output();
    @{${$messages}} = ();
    my $random_type = rand_string(6, [ 'A'..'Z' ]);
    $comp->xml_in(<<XML);
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         >
  <body>no type</body>
</message>
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='$random_type'>
  <body>type is a random string</body>
</message>
XML
    cmp_deeply(${$messages}->[0],
               [ 'juliet', 'no type', 'chat' ],
               'no message type => chat');
    cmp_deeply(${$messages}->[1],
               [ 'juliet', 'type is a random string', $random_type ],
               'message type attribute pulled out correctly');
    clear_log();
    output();
    @{${$messages}} = ();
    # diag("End of message type tests");

    diag("Beginning chatstates parsing tests");
    clear_log();
    output();
    @{${$messages}} = ();
    my $chatstates
      = $comp->session_for('romeo@montague.lit/orchard')->{chatstates}
      = [];
    $comp->xml_in(<<XML);
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         >
  <body>no chatstates</body>
</message>
XML
    cmp_deeply(${$messages}->[0],
               [ 'juliet', 'no chatstates', 'chat' ],
               'message w/no chatstates: body parsed correctly');
    ok(scalar(@{$chatstates}) == 0,
       'message w/no chatstates: no chatstate passed');
    $comp->xml_in(<<XML);
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'>
  <active xmlns='http://jabber.org/protocol/chatstates'/>
  <body>active chatstate and body</body>
</message>
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'>
  <composing xmlns='http://jabber.org/protocol/chatstates'/>
</message>
XML
    cmp_deeply(${$messages}->[1],
               [ 'juliet', 'active chatstate and body', 'chat' ],
               'message w/body and chatstate: body parsed correctly');
    is($chatstates->[0],
       'active',
       'message w/body and chatstate: chatstate parsed correctly');
    ok(! exists(${$messages}->[2]),
       'message w/chatstate but no body: no message sent');
    is($chatstates->[1],
       'composing',
       'message w/chatstate but no body: chatstate handled');
    clear_log();
    output();
    @{${$messages}} = ();
    delete($comp->session_for('romeo@montague.lit/orchard')->{chatstates});
    diag("End of chatstates parsing tests");

    # Fake up an error to see that we handle that correctly
    $message = <<MESSAGE;
<message from='romeo\@montague.lit/orchard'
         to='juliet\@$JID'
         type='chat'>
  <body>Error: item_not_found</body>
</message>
MESSAGE

    $comp->xml_in($message);
    my $result = clean_xml(<<RESULT);
<message from='juliet\@test.transport' 
         to='romeo\@montague.lit/orchard' 
         type='error'>
    <error code='404' type='cancel'>
        <item-not-found xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
</message>
RESULT
    is(output, $result,
       "message errors correctly sends out error packets");

    # BAD INPUT: Sending a message when the component doesn't
    # know who you are sends back an error message
    $message = <<MESSAGE;
<message from='steel\@flenser.tine' 
         to='juliet\@test.transport' 
         type='chat'>
    <body>Hey, what are you doing in my reality?</body>
</message>
MESSAGE
    $comp->xml_in($message);

    $result = clean_xml(<<RESULT);
<message from='juliet\@test.transport' 
         to='steel\@flenser.tine' 
         type='error'>
    <error code='407' type='auth'>
        <registration-required xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
    <body>Hey, what are you doing in my reality?</body>
</message>
RESULT
    is(output, $result, 
       'error message when sending messages and not registered is correct');

}

PROTOCOL_TESTS: 
{
    # Main flow of a subscription request: Permit it
    SUBSCRIPTION_PERMITTED: {
        my $comp = logged_in_comp;
        my $get_roster = sub {
            $comp->{protocol}->{backend}->get_roster('romeo@montague.lit')
        };
        my $protocol = $comp->{protocol};
        $protocol->adding_contact('juliet', 'romeo@montague.lit');
        is(output, 
           "<presence from='juliet\@$JID' to='romeo\@montague.lit' type='subscribe'/>",
           "correct reflects legacy subscription requests to user");

        cmp_deeply($get_roster->(), 
                   {'juliet@test.transport' => want_subscribe},
                   'correctly recorded that juliet wants a subscription');

        my $session =
            $comp->session_for('romeo@montague.lit/orchard');
        is($session->{subscribed}->{'juliet'}, undef);
        my $subscribed = <<SUBSCRIBED;
<presence from='romeo\@montague.lit/orchard'
          to='juliet\@$JID'
          type='subscribed'/>
SUBSCRIBED
        $comp->xml_in($subscribed);
        is($session->{subscribed}->{'juliet'}, 'subscribed',
           'correctly handles subscription acceptance requests');
        print Dumper($get_roster->());
        cmp_deeply($get_roster->(),
                   {'juliet@test.transport' => subscribed},
                   'correctly notes juliet as subscribed');

	my $expected_subscription = clean_xml(<<EXPECTED);
<presence from='juliet\@$JID'
          to='romeo\@montague.lit'
          type='subscribed'/>
<presence from='juliet\@$JID'
          to='romeo\@montague.lit'>
    <show>Online</show>
</presence>
EXPECTED

        is(output, $expected_subscription,
	   'gateway correctly says the other user is '
	   .'subscribed');
    }

    # Test 5.1.2, where the foreign user requests a subscription
    # and the XMPP user says no
    SUBSCRIPTION_REJECTED: {
	my $comp = logged_in_comp;
        my $get_roster = sub {
            $comp->{protocol}->{backend}->get_roster('romeo@montague.lit')
        };
	my $protocol = $comp->{protocol};

	$protocol->adding_contact('juliet', 'romeo@montague.lit');
	clear;

        print Dumper($get_roster->());
	cmp_deeply($get_roster->(), {'juliet@test.transport' => want_subscribe});

	my $unsubscribed = <<UNSUBSCRIBED;
<presence from='romeo\@montague.lit/orchard'
          to='juliet\@$JID'
          type='unsubscribed'/>
UNSUBSCRIBED
        $comp->xml_in($unsubscribed);

        my $session =
            $comp->session_for('romeo@montague.lit/orchard');

        print Dumper($get_roster->());
        cmp_deeply($get_roster->(), {}, 
                   'rejected subscriptions go back to not subscribed');

        is($session->{subscribed}->{'juliet'}, 'unsubscribed',
           'correctly rejects subscription requests');
    }

    LEGACY_UNSUBSCRIPTION: {
	my $comp = logged_in_comp;
	my $protocol = $comp->{protocol};
	my $session = $comp->session_for('romeo@montague.lit/orchard');
	
	# Bit of a cheat here; we aren't actually "subscribed"
	$protocol->deleting_contact('juliet', 'RomeoMyRomeo');
	my $expected = clean_xml(<<EXPECTED);
<presence from='juliet\@$JID'
          to='romeo\@montague.lit'
          type='unsubscribe'/>
<presence from='juliet\@$JID'
          to='romeo\@montague.lit'
          type='unsubscribed'/>
<presence from='juliet\@$JID'
          to='romeo\@montague.lit'
          type='unavailable'/>
EXPECTED
        is(output, $expected, 
	   'correct XML emitted when a user unsubscribes.');
    }

    # oh, yeah, right, did we want to receive legacy user's messages?
    MESSAGE: {
	my $comp = logged_in_comp;
	my $protocol = $comp->{protocol};
	$protocol->sending_message('juliet', 'RomeoMyRomeo',
				   'sucks!');
	my $expected = clean_xml(<<EXPECTED);
<message from='juliet\@$JID' to='romeo\@montague.lit' type='chat'>
    <body>sucks!</body>
    <active xmlns='http://jabber.org/protocol/chatstates'/>
</message>
EXPECTED
        is(output, $expected, 
	   'correctly sends a message when we receive one');
    }
}

GATEWAY_PROTOCOL: {
    my $comp = logged_in_comp;
    my $protocol = $comp->{protocol};

    my $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='one'>
  <query xmlns='$NS_GATEWAY'/>
</iq>
QUERY

    $comp->xml_in($query);
    my $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='one' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>Prompt en</prompt>
  </query>
</iq>
EXPECTED

    is(output, $expected);

    $protocol->{gateway_desc} = 'Desc';
    $comp->xml_in($query);
    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='one' to='romeo\@montague.lit' type='result'>

  <query xmlns='$NS_GATEWAY'>
    <prompt>Prompt en</prompt>
    <desc>Desc</desc>
  </query>
</iq>
EXPECTED
    is(output, $expected);

    clear;
    # I think this is the right place for the XML lang
    my $lang_query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='one' xml:lang='fr'>
  <query xmlns='$NS_GATEWAY'/>
</iq>
QUERY
    $comp->xml_in($lang_query);
    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='one' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>Prompt fr</prompt>
    <desc>Desc</desc>
  </query>
</iq>
EXPECTED
    is(output, $expected, 'langs handled as expected');

    # Test the translation
    $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>marklar\@schmoo.hut</prompt>
  </query>
</iq>
QUERY
    $comp->xml_in($query);

    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='two' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_GATEWAY'>
    <jid>marklar\%schmoo.hut\@test.transport</jid>
    <prompt>marklar\%schmoo.hut\@test.transport</prompt>
  </query>
</iq>
EXPECTED
    is(output, $expected);

    is($comp->{protocol}->{backend}->{jid_to_legacy}
       ->{'romeo@montague.lit'}->{'marklar%schmoo.hut@test.transport'},
       'marklar@schmoo.hut');
    is($comp->{protocol}->{backend}->{legacy_to_jid}
       ->{'romeo@montague.lit'}->{'marklar@schmoo.hut'},
       'marklar%schmoo.hut@test.transport');

    clear;

    # Check whether JID dupes get handled correctly
    $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>marklar\%schmoo.hut</prompt>
  </query>
</iq>
QUERY
    $comp->xml_in($query);
    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='two' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_GATEWAY'>
    <jid>marklar\%schmoo.hut2\@test.transport</jid>
    <prompt>marklar\%schmoo.hut2\@test.transport</prompt>
  </query>
</iq>
EXPECTED
    is(output, $expected);

    # Check that the JIDs stay stable
    $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>marklar\%schmoo.hut</prompt>
  </query>
</iq>
QUERY
    $comp->xml_in($query);
    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='two' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_GATEWAY'>
    <jid>marklar\%schmoo.hut2\@test.transport</jid>
    <prompt>marklar\%schmoo.hut2\@test.transport</prompt>
  </query>
</iq>
EXPECTED
    is(output, $expected,
       'gateway protocol-selected JIDs stay stable');


    # Bad input: Incorrect query with no prompt gets error
    my $bad_query = <<BAD;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <bangleford/>
  </query>
</iq>
BAD

    $comp->xml_in($bad_query);
    $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='two' to='romeo\@montague.lit' type='error'>
  <query xmlns='$NS_GATEWAY'>
    <bangleford/>
  </query>
  <error code='400' type='modify'>
      <bad-request xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
  </error>
</iq>
EXPECTED
    is(clean_xml(output), $expected,
       'properly handles errors');
}

# Now that the gateway protocol is tested, verify that we can use
# it to make subscriptions to foreign users.
WITH_CONTACTS: {
    SUBSCRIBE_WHILE_NOT_LOGGED_IN: {
        my $comp = get_registered_comp;
        # Get the ID for the subscription
        my $initial_gateway_query = <<GATEWAY_QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>marklar\@schmoo.hut</prompt>
  </query>
</iq>
GATEWAY_QUERY
        $comp->xml_in($initial_gateway_query);

        # We already tested this above, assume we got
        # marklar%schmoo.hut .
        clear;

        my $add_subscription = <<SUBSCRIPTION;
<presence from='romeo\@montague.lit' 
          to='marklar\%schmoo.hut\@$JID'
          type='subscribed'/>
SUBSCRIPTION
        $comp->xml_in($add_subscription);

        my $expected = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='error'>
    <error code='401' type='auth'>
        <not-authorized xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
    </error>
</presence>
EXPECTED

        # currently disabled in Thrasher::Component:
        # "This gets sent out after logging off..."
        is(output, $expected, 'correct error when trying to '
           .'subscribe without being logged in');
    }

    SUBSCRIBE_WHILE_LOGGED_IN: {
        my $comp = logged_in_comp;
        # Get the ID for the subscription
        my $initial_gateway_query = <<GATEWAY_QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='two'>
  <query xmlns='$NS_GATEWAY'>
    <prompt>marklar\@schmoo.hut</prompt>
  </query>
</iq>
GATEWAY_QUERY
        $comp->xml_in($initial_gateway_query);

        # We already tested this above, assume we got
        # marklar%schmoo.hut .
        clear;
        
        my $add_subscription = <<SUBSCRIPTION;
<presence from='romeo\@montague.lit' 
          to='marklar\%schmoo.hut\@$JID'
          type='subscribed'/>
SUBSCRIPTION
        $comp->xml_in($add_subscription);

        # Note that the "online" comes from the Test 
        # protocol itself.
        my $expected = clean_xml(<<EXPECTED);
<presence from='marklar\%schmoo.hut\@test.transport' 
          to='romeo\@montague.lit' 
          type='subscribed'/>
<presence from='marklar\%schmoo.hut\@test.transport' 
          to='romeo\@montague.lit'>
    <show>Online</show>
</presence>
EXPECTED
        is(output, $expected, "got correct response for subscription");
    }

    UNSUBSCRIBE_WHILE_UNKNOWN: {
        my $comp = get_registered_comp;
        # Unsubscribe from somebody I'm not subscribed to.
        # This is an error condition, but I'm trying to handle it.
        my $unsubscribe = <<UNSUBSCRIBE;
<presence from='romeo\@montague.lit'
          to='nobody\@$JID'
          type='unsubscribe'/>
UNSUBSCRIBE
        clear;
        $comp->xml_in($unsubscribe);
        my $expected = clean_xml(<<EXPECTED);
<presence from='nobody\@$JID'
          to='romeo\@montague.lit'
          type='unsubscribed'/>
EXPECTED
        is(output, $expected, 
           "can correctly unsubscribe people I don't even know about");
    }
}

VARIOUS_ERRORS: {
    my $comp = logged_in_comp;

    # Unexpected iq - get error
    my $unexpected = <<UNEXPECTED;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='a'>
  <query xmlns='complete:gibberish'/>
</iq>
UNEXPECTED

    $comp->xml_in($unexpected);
    my $expected = <<EXPECTED;
<iq from='$JID' id='a' to='romeo\@montague.lit' type='error'>
  <ns1:query xmlns:ns1='complete:gibberish'/>
  <error code='503' type='cancel'>
    <service-unavailable xmlns='$NS_ERROR'/>
  </error>
</iq>
EXPECTED
    is(clean_xml(output),
       clean_xml($expected),
       "properly handles IQs of a namespace I don't understand");
}

OTHER_XEP_SUPPORT: {
    XEP_0090: {
        # Time support
        my $comp = logged_in_comp;
        my $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='a'>
  <query xmlns='$NS_TIME'/>
</iq>
QUERY
        $comp->xml_in($query);
        my $response = output;
        ok($response =~ m|\<iq from='$JID' id='a' to='romeo\@montague.lit' type='result'\>\<query xmlns='$NS_TIME'\>\<utc\>\d{8}T\d\d:\d\d:\d\d\<\/utc\>\<\/query\>\<\/iq\>|,
           'got a time as expected');
    }

    XEP_0092: {
        # Version support
        my $comp = logged_in_comp;
        my $query = <<QUERY;
<iq from='romeo\@montague.lit' to='$JID' type='get' id='a'>
  <query xmlns='$NS_VERSION'/>
</iq>
QUERY

        $comp->xml_in($query);
        my $expected = clean_xml(<<EXPECTED);
<iq from='$JID' id='a' to='romeo\@montague.lit' type='result'>
  <query xmlns='$NS_VERSION'>
    <name>Thrasher - $comp->{component_name}</name>
    <version>$Thrasher::VERSION</version>
  </query>
</iq>
EXPECTED
        is(output, $expected,
           'correctly responds to version requests');
    }
}
  
# Simulate what happens when the protocol disconnects from us; verify
# the transport immediately tries to re-login if we're connected,
# but doesn't try to re-login if we're not.
PROTOCOL_DISCONNECTED: {
    my $comp = logged_in_comp;
    my $session = $comp->session_for('romeo@montague.lit');

    is($session->{protocol_state}, 'online',
       "we're showing as online as I expect");
    $comp->{protocol}->disconnecting($session);
    is($session->{protocol_state}, 'online', 
       "still online after a second connection");
    logged("unexpectedly dropped, scheduling re-connection",
           "correctly re-established the connection after "
           ."unexpected droppage");
}

# Checking sending messages, especially with XHTML-ish on.
SENDING_MESSAGES: {
    my $comp = logged_in_comp;
    
    $comp->send_message('romeo@montague.lit',
                        'juliet@capulet.lit',
                        "look, to hell with this shakespeare stuff "
                        ."lets screw",
                        {nick => 'romeo'});
    my $expected = clean_xml(<<EXPECTED);
<message from='romeo\@montague.lit' 
         to='juliet\@capulet.lit' 
         type='chat'>
    <body>look, to hell with this shakespeare stuff lets screw</body>
    <nick xmlns='http://jabber.org/protocol/nick'>romeo</nick>
</message>
EXPECTED

    is(output, $expected, "message without xhtml works");

    $comp->send_message('juliet@capulet.lit',
                        'romeo@montague.lit',
                        "<h1>hell yeah</h1>\n\n<p>been waiting to "
                        ."hear you say that for hundreds of years",
                        {is_xhtml_ish => 1});
    $expected = clean_xml(<<EXPECTED);
<message from='juliet\@capulet.lit'
         to='romeo\@montague.lit'
         type='chat'>
    <body>hell yeah

been waiting to hear you say that for hundreds of years</body>
    <html xmlns='http://jabber.org/protocol/xhtml-im'>
        <body xmlns='http://www.w3.org/1999/xhtml'>
            <h1>hell yeah</h1>

            <p>been waiting to hear you say that for hundreds of years</p>
        </body>
    </html>
</message>
EXPECTED

     is(clean_xml(output), $expected,
        'xhtml output works as expected');

}
