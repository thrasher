#!/usr/bin/perl

use strict;
use warnings;
use Test::More tests => 1;

is (run_grep('incomplete_init', 'Purple core is uninitialized', '2>&1'),
    1,
    'Test login_thrasher for core validation');



# Returns the number of times the given regex is found when running the cmd
# Allows for bash redirection via bash_redir arg
sub run_grep {
    my $cmd = shift;
    my $re  = shift;
    my $bash_redir = shift;

    if (-x $cmd) {
        $cmd .= " $bash_redir" if $bash_redir;
        return  scalar grep {/$re/} `./$cmd`;
    }

    # Stop hard on bad commands
    diag("Command \"$cmd\" is not executable!\n");
    exit 0;
}
