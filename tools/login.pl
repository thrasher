#!/usr/bin/perl

use strict;
use warnings;
use lib q(perl/lib);
use THPPW;
use Glib qw(G_PRIORITY_DEFAULT);

my $loop = Glib::MainLoop->new();
my $DEBUG = 0;

my $user  = shift;
my $pass  = shift;
my $proto = shift;
my $debug = shift || 0;

# Simple error handling
if (!$user or !$pass or !$proto) {
    print "Syntax: $0 username password protocol\n";
    exit 0;
}


# Initialize the wrapper
# Do this BEFORE initializing the libpurple internals!
THPPW::thrasher_wrapper_init(\&timeout_add,
                             \&input_add,
                             \&source_remove,
                             \&incoming_msg,
                             \&presence_in,
                             \&subscription_add,
                             \&connection_error);

THPPW::thrasher_purple_debug($debug);

# Initialize the beast
THPPW::thrasher_init();

# Login
THPPW::thrasher_action_login('n/a', $user, $pass, $proto);

# Let the eventloop fly!
$loop->run();


# Thrasher Bird subrefs
sub incoming_msg {
}

sub presence_in {
}

sub subscription_add {
}

sub connection_error {
}



# Subrefs for which to satiate the libpurple monster
sub timeout_add {
    my $interval = shift;
    my $code     = shift;
    my $trigger  = shift;

    debug_out("perl::timeout_add called\n");

    debug_out("\tinterval = $interval\n") if $interval;
    debug_out("\tcode     = $code\n")     if $code;
    debug_out("\ttrigger  = $trigger\n")  if $trigger;

    my $ret =Glib::Timeout->add($interval,
                             ($code, $trigger),
                             G_PRIORITY_DEFAULT);

    debug_out("Glib::Timeout->add returned [$ret]\n");
    return $ret;
};


sub source_remove {
    debug_out("perl::timeout_remove called with $_[0]\n");

    return Glib::Source->remove($_[0]);
    return 1;
}

sub input_add {
    my $fd       = shift;
    my $cond     = shift;
    my $code     = shift;
    my $trigger  = shift;

    my $i = 0;
    foreach (@_) {
        debug_out("\t$i = $_\n");
        $i++;
    }


    debug_out("\tfd       = $fd\n")      if $fd;
    debug_out("\tcond     = $cond\n")    if $cond;
    debug_out("\tcode     = $code\n")    if $code;
    debug_out("\ttrigger  = $trigger\n") if $trigger;

    $cond = ($cond == 1) ? 'G_IO_IN' : 'G_IO_OUT';

    my $ret = Glib::IO->add_watch($fd,
                                  $cond,
                                  $code,
                                  $trigger,
                                  G_PRIORITY_DEFAULT);

    debug_out("Glib::IO->add_watch returned [$ret]\n");

    return $ret;
}


sub debug_out {
    my $msg = shift;

    if ($DEBUG) {
        print $msg;
    }
}
