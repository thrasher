package Thrasher::XHTML_IM_Normalize;
use strict;
use warnings;

use Thrasher::HTMLNormalize qw(normalize
                               tagremains_to_atts_hash);

=pod

=head1 NAME

Thrasher::XHTMLNormalize - provides utility functions for creating
XHTML-IM (XEP-0071) messages out of some HTML-like stuff

=head1 SYNOPSIS

 use Thrasher::XHTMLNormalize qw(xhtml xhtml_and_text);

 my $test_string = "<html><body><p><i>Almost <b>XHTML</i></b>";

 # Will return something suitable for insertion as XML.
 # In this case, '<p><i>Almost <b>XHTML</b></i></p>'.
 my $xhtml = xhtml($test_string);

 # Produces the same $xhtml as the previous statement, but
 # $text will have just the pure-text part of the string.
 my ($text, $xhtml) = xhtml_and_text($test_string);

=head1 DESCRIPTION

This utilizes Thrasher::HTMLNormalize to create two functions, one
which takes vaguely HTML-ish stuff and outputs XHTML-IM/XEP-0071
complaint text suitable for inclusion in an XML stream, and
one which additionally returns a pure-text version.

The RECOMMENDEDs, SHOULDs, and SHOULD NOTs are resolved
as follows:

=over

=item *

Per section 7.1 of XEP-0071, we ignore C<html>, C<head>, and 
C<title>, stripping them out, and we ignore any C<body> tag,
replacing it with our own, on the theory that if we need this
normalization in the first place it's probably because we're
not actually processing XHTML-IM.

=item *

Section 7.6.1's recommend styles is upgraded from RECOMMENDED to
REQUIRED; no other styles will be permitted to pass through.

=item *

Section 7.7.1 NOT RECOMMENDs class, id, title, and xml:lang.
These are always discarded, in accordance with the given logic.
Even if you had some crazy XMPP app that actually expected to
be able to push out stylesheets, things on the other end of
a transport have no chance of using these correctly.

Section 7.7.2 NOT RECOMMENDEDs are also tossed out.

=item *

There is no support for xml:lang. I don't think anything is
emitted localized messages with multiple bodies in a way that works
with this correctly.

=item *

Section 8, bullet point 9 recommends \n => <br/>; this will
do that, it also replaces \n\n => <p/>.

=back

=cut

use base 'Exporter';

our @EXPORT_OK = qw(xhtml xhtml_and_text text);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

my %ACCEPTABLE_STYLES = map { $_ => 1 }
    qw(background-color color font-family font-size font-style
       font-weight margin-left margin-right text-align 
       text-decoration);

sub clean_style {
    my $style = shift;

    my @style_chunks = split /;/, $style;

    my @permitted_chunks;
    for my $chunk (@style_chunks) {
        my ($name, $value) = split /\s*:\s*/, $chunk;
        if (!defined($value)) {
            # No actual value
            next;
        }

        $name = lc $name; # force lowercase, which it should be anyhow
        if (!$ACCEPTABLE_STYLES{$name}) {
            next;
        }

        push @permitted_chunks, "$name\:$value";
    }

    if (@permitted_chunks) {
        return join '; ', @permitted_chunks;
    } else {
        return 0;
    }
}

my $standard_atts = {style => \&clean_style};
my $with_cite = {style => \&clean_style, cite => 1};

my $xhtml_im_permissions = 
{
    # XEP-0071 section 6.1 - all rejected, see perldocs

    # section 6.2, as described
    abbr => $standard_atts,
    acronym => $standard_atts,
    address => $standard_atts,
    br => $standard_atts,
    cite => $standard_atts,
    code => $standard_atts,
    dfn => $standard_atts,
    div => $standard_atts,
    em => $standard_atts,
    h1 => $standard_atts,
    h2 => $standard_atts,
    h3 => $standard_atts,
    h4 => $standard_atts,
    h5 => $standard_atts,
    h6 => $standard_atts,
    kdb => $standard_atts,
    p => $standard_atts,
    pre => $standard_atts,
    q => $standard_atts,
    samp => $standard_atts,
    span => $standard_atts,
    strong => $standard_atts,
    var => $standard_atts,

    blockquote => $with_cite,
    q => $with_cite,

    # Section 6.3, modded by 7.7.2
    a => {style => \&clean_style, type => 1, 
          href => \&Thrasher::HTMLNormalize::fixURL},

    # Section 6.4
    dl => $standard_atts,
    dt => $standard_atts,
    dd => $standard_atts,
    ol => $standard_atts,
    ul => $standard_atts,
    li => $standard_atts,

    # Section 6.5, modded by 7.7.2
    img => {style => \&clean_style, 
            alt => 1, height => 1, src => 1, width => 1},
};

# $tag_translations translate certain HTML tags to equivalent XHTML(-IM) tags.
#
# throw_a_spanner($text_decoration): Return a tag translator to modify
# a tokenizing $groups to be a <span> with the given text-decoration style.
sub throw_a_spanner {
    my ($text_decoration) = @_;
    return sub {
        my ($groups) = @_;
        $groups->{'tagname'} = 'span';
        if (! $groups->{'slash'}) {
            $groups->{'tagremains'}
              = qq|style="text-decoration: $text_decoration;"|;
        };
    }
}
my $tag_translations = {
    'b' => 'strong',
    'i' => 'em',
    'u' => throw_a_spanner('underline'),
    's' => throw_a_spanner('line-through'),
    'strike' => throw_a_spanner('line-through'),

    'font' => sub {
        my ($groups) = @_;

        my $attrs = tagremains_to_atts_hash($groups->{'tagremains'});
        my %style = ();
        if ($attrs->{'color'}) {
            $style{'color'} = $attrs->{'color'};
        }
        if ($attrs->{'back'}) {
            $style{'background-color'} = $attrs->{'back'};
        }
        if (scalar(keys(%style)) > 0) {
            $groups->{'tagname'} = 'span';
            if (! $groups->{'slash'}) {
                my $style_attribute = '';
                while (my ($prop, $value) = each(%style)) {
                    $style_attribute .= "${prop}: ${value};";
                }
                $groups->{'tagremains'} = qq|style="${style_attribute}"|;
            }
        }
        # Drop other font attributes (by overwriting here or
        # eventually dropping the entire tag).
    },
};

# No tags for you!
my $plain_text_permissions = {};

sub xhtml {
    my $original_text = shift;
    my $normalized = normalize($original_text, $xhtml_im_permissions, 0,
                     1, $tag_translations);
    # Strip out the first <p>, since it tends to get in our way
    # in this application.
    $normalized =~ s/^\<p[^>]*\>(.*?)\<\/p[^>]*\>/$1/;
    return $normalized;
}

sub xhtml_and_text {
    my $original_text = shift;

    my $xhtml = xhtml($original_text);
    my $text = text($original_text);

    return ($xhtml, $text);
}

sub text {
    my $original_text = shift;
    my $text = normalize($original_text, $plain_text_permissions, 0,
                         1, undef, 1);
    return $text;
}
