package Thrasher::Test;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Test - repeated code in the test cases

=head1 DESCRIPTION

Thrasher::Test contains code that repeatedly came up in the test code.
This module is never use'd in a production install.

=cut

use Test::More;
use Test::Deep;
use Data::Dumper;
use MIME::Base64 qw(decode_base64 encode_base64);
use Digest::SHA1 qw(sha1_hex);
use Image::Magick;

use Thrasher::Log;

BEGIN {
    use_ok 'Thrasher::Protocol::Test';
    use_ok 'Thrasher::Backend::Test';
}

$Thrasher::TESTING = 1;

use Digest::SHA1 qw(sha1_hex);

use base qw(Exporter);
our @ISA = qw(Exporter);

our @EXPORT_OK = qw(clear output clear_log logged transport_jid
                    new_component send_server_stream show_log
                    accepted_handshake connected_component
                    clean_xml get_registered_comp logged_in_comp 
                    dies get_mysql_username_password
                    rand_string

                    $small_png $small_png_base64
                    $small_gif $small_gif_base64
                    $small_gif_png_base64
                    $small_gif_png_sha1
                    $small_gif_png_len
                    $binary_garbage
                    );
our %EXPORT_TAGS = (all => \@EXPORT_OK);

my $JID = 'test.transport';

sub transport_jid { $JID }

my @accum;
my $output = sub {
    push @accum, @_;
};

sub clear {
    @accum = ();
}

sub output {
    my $output = join '', @accum;
    clear;
    return $output;
}

my @log_entries;
sub logger {
    push @log_entries, @_;
}
$Thrasher::Log::logger_sub = \&logger;

sub clear_log { 
    @log_entries = (); 
}

sub logged {
    my $test = shift;
    my $name = shift;
    for my $log_entry (@log_entries) {
        if ($log_entry =~ /$test/) {
            pass($name);
            clear_log;
            return;
        }
    }
    fail($name);
    clear_log;
}

sub show_log {
    print Dumper(\@log_entries);
}

#END {
#    show_log();
#}

sub new_component {
    my $preregistered_accounts = shift || {};

    clear;

    my $test_backend = 
        new Thrasher::Backend::Test({registered => $preregistered_accounts});
    my $test_protocol = new Thrasher::Protocol::Test({}, $test_backend);
    my $comp = new Thrasher::Component
        ($test_protocol, $output, 'secret', $JID);

    is(ref($comp), 'Thrasher::Component', 
       'get a component as expected');

    $comp->output_initial_stream_tag();
    $test_protocol->{component} = $comp;

    cmp_deeply(\@accum,
               ["<stream:stream to=\'$JID\' xmlns=\'jabber:component:accept\' xmlns:stream=\'http://etherx.jabber.org/streams\'>"],
               'stream open tag accumulated');

    clear;
    
    return $comp;
}

sub send_server_stream {
    my $comp = shift;
    $comp->xml_in(<<STREAM);
<?xml version='1.0'?>
<stream:stream 
  xmlns:stream='http://etherx.jabber.org/streams' 
  xmlns='jabber:component:accept' 
  id='4152762083' 
  from='ni.cuda.ims'>
STREAM

    cmp_deeply(\@accum,
               ['<handshake>', sha1_hex('4152762083'.'secret'),
                '</handshake>'],
               'hands all properly shaken');

    @accum = ();
}

sub accepted_handshake {
    my $comp = shift;
    $comp->xml_in('<handshake/>');
}

# This is a connected component that is ready to process actual
# events.
sub connected_component {
    my $comp = new_component;
    send_server_stream($comp);
    accepted_handshake($comp);

    is($comp->{state}, 'connected', 'component connected');
    
    return $comp;
}

# This allows me to write nicely formatted XML in this file,
# and strips it down to what we expect in output.
sub clean_xml {
    my $s = shift;
    $s =~ s/ +$//gm;
    $s =~ s/^ +//gm;
    $s =~ s/\n/ /gm;

    # Don't separate tags with spaces. Hacky, but OK for testing.
    $s =~ s/\> +\</></gm;
    # Clean up the last newline which went to a space
    $s =~ s/ +$//s;
    return $s;
}

# FIXME: Test that first connection after registration!

sub get_registered_comp {
    # Reset counters used for generating unique IDs.
    $Thrasher::Component::id = 1;
    $Thrasher::XMPPStreamOut::namespace_index = 0;

    my $comp = connected_component;
    
    # Romeo gets registered.
    $comp->{protocol}->registration('romeo@montague.lit',
                                    {username => 'RomeoMyRomeo',
                                     password => 'ILoveJuliet'});
    
    # A user that will error out if they login
    # HACK HACK HACK for testing
    Thrasher::Protocol::registration
        ($comp->{protocol}, 'juliet@capulet.lit',
         {username => 'fail',
          password => 'remote_server_timeout'});
    return $comp;
}

# Return a component with a logged in Romeo.
sub logged_in_comp {
    my $comp = get_registered_comp;
    $comp->xml_in(<<LOGIN);
<presence from='romeo\@montague.lit/orchard'
          to='$JID'/>
LOGIN

    # this is Psi's disco reply
    my $expected_id = $Thrasher::Component::id - 1;
    clear;
    $comp->xml_in(<<CLIENT_DISCO_REPLY);
<iq from='romeo\@montague.lit' 
    to='$JID' 
    id='id$expected_id' 
    type='result'>
    <query xmlns='http://jabber.org/protocol/disco#info'>
        <identity category='pubsub' type='pep'/>
        <feature var='vcard-temp'/>
        <feature var='http://jabber.org/protocol/commands'/>
    </query>
</iq>
CLIENT_DISCO_REPLY

    my $session = $comp->session_for('romeo@montague.lit');
    cmp_deeply($session->{client_identities},
               [['pep', 'pubsub', undef]]);
    cmp_deeply($session->{client_features},
               {'vcard-temp' => 1,
                'http://jabber.org/protocol/commands' => 1},
               'discovery worked correctly');

    return $comp;
}

sub dies (&;$$) {
    eval 'use Test::More;';
    my $code = shift;
    my $check = '';
    if (@_ == 2) {
        $check = shift;
    }
    my $name = shift;

    { 
        local $@ = '';
        eval { $code->(); };

        if (!$@) {
            fail($name);
            return;
        }

        my $die_message = $@ . '';
        if ($check) {
            if (ref($check) eq 'Regexp') {
                if ($die_message !~ /$check/) {
                    fail($name . " (regex $check not in '$die_message')");
                }
            } else {
                if (index($die_message, $check) == -1) {
                    fail($name . " (string $check not in $die_message)");
                }
            }
        }
    }

    pass($name);
}            

# This opens a file (implicitly in the test directory) to get the
# username and password to connect to MySQL with. This file is
# .gitignored so you shouldn't accidentally commit it.
# This assumes localhost for testing; if that's a problem, add
# more lines in for port, etc.
sub get_mysql_username_password {
    my $fh;
    if (!(open $fh, '<', 'mysql_innodb_password')) {
        if (!(open $fh, '<', '../mysql_innodb_password')) {
            die "Can't locate mysql_innodb_password in this directory "
                ."or parent. Create a file called "
                ."'mysql_innodb_password', where the first line is "
                ."the user account, and the second is the password";
        }
    }
    my $username = <$fh>;
    chomp($username);
    my $password = <$fh>;
    chomp($password);
    close $fh;
    return ($username, $password);
}

### TEST DATA

our $small_png_base64 = <<'SMALL_PNG';
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsOAAALDgFAvuFBAAAA
B3RJTUUH0wkJFSIsp+GiggAAAAZiS0dEAP8A/wD/oL2nkwAAAnJJREFUOI11Ut1LU2EYP1chEUF3
dRNB/8HZzoee7bhW0ZSmBl5ERaE0qagkEYwVIhHUhR9DC7KbkMIiCIpCzD6uKtCtqZllNkPc2Zru
aHZcsvTQr/c5OreZPfDjPe9znt/z9f44bgMbtkkFg7zofM+LVSFeqA7yYvErsXDzRrHriVs+2KTr
n3lxXrPLmDxUicjR49AEGR+Zb8AmdvZJys7/kbeP2KSxeKkXvx48RFqLYdkwLCwldcw/fwHt8DGE
eSHZo6iuPPIQL1LlMd13CqnRT0hPTW2IxYlviPkvUxK9W92zYy0Bm7c5VlqGn2/eYXF8HCarmgqH
85DxL4RCiLKx3trlTp/Pt4ljSyqg+fRAB+Z6+7A0OwsyOulOMPqDyJjRPwDt1m2M8OJCwFOyixI4
J2UHprvuYuZet4XfiYQVTOePvpdrZP3xE6RLvIDiQpQt+WmR8wglqJpi7cda2hAPtCPefsNCOqoh
14yOm1isqAQcbgYXkpKCXkVtpATVsX0eJM7XYbr+Imb8jTBqTiN9qTHLDg8CTncWLElScqDHUdzE
kUiiQiGWDxxcg1nfkE/OTaLuBVz7LW08crpOcqQwWqLp8cJko5gN/iz53IUV0mqSP0PDMD2sAItl
S0xdLa+wWc/IFNaqMfJczRksjX2xgo3mNuueQa6fYl9LRffZM65ogclzW8guRSbP1iLedAX6nS7r
XA/yU0zQLkWvlVUolg4y9kxRd4cEeeIrU+P3lgASrfkgH/0LCnK0zVteyshbufVG8iSFDaruhdET
VYjU1lmgb+ZLUdurlf8lZ4zaIoWRSOid6alo27QwmjmvbWZ/AQkpPzEuxY62AAAAAElFTkSuQmCC
SMALL_PNG

our $small_png = decode_base64($small_png_base64);

our $small_gif_base64 = <<'SMALL_GIF';
R0lGODdhLAA3AMQAAP///wgICBAQEBgYGCEhISkpKTExMTk5OUJCQkpKSlJSUlpaWmNjY2tra3Nz
c3t7e4SEhIyMjJSUlJycnKWlpa2trbW1tb29vcbGxs7OztbW1t7e3ufn5+/v7/f39wAAACwAAAAA
LAA3AAAF/yAgjmRpniIXLQzloXAcV8Nnf8cm73sW3LfDi0c0LYDASnEp8hCQtwZz6RFAbdJpMXH9
TLTFyw8pBBcxCWtg0OhoK46HfA55QO6QCGbo2ehEGxlMEl1XAwgLDnUOCwcCAYJLFAWFlUgFQ0Ue
GBAJNZaFD2YeGRMNCQRjoB8CfzEUEW4wfRcSqpUOOweMDhISCQmuJYSgA7IoGwcAHhYTFxsMBQ8W
FZkiBqsUMQ5ZJRoWBgQmFqsKMBIIHCgcAUolHpSWA9YiGQwQxyYKyicOq5EjKBC4MOODBhTkQH0Z
seFDgw0YUGgQIAFGQ1CiRvyy8ODQhAkMHqjbMIBBjCqgTP+e6BBBggZmDSAU6IbCw6dKCTBQ+Pix
Qs8KPmEdAkqBQoWD7xAUKECgqdMBUAcoYLDLASMB3BosYNMgDoGpDKiaA/NAAU8IAyR8hEBA7UcC
D3ZSWEBzBNC7QC3otXDhWYQC3BwwENCgsAKuYQfQLVwAgokMqxaYGuHBAMMEJA4caxDBxITIFcYC
6IBJBAYEIzoYOLbAHYkGqxigoWxAHYAMqJsUOIaAYAlsGJORqF0v92h+14QBYLdKQgcCLBYsEDB1
gaewDKYXbsCAAL0Kqz5cgBdhAgUJ0tQ6KOBLwgMCEeI7MEDvwSoB6hC4SoA0A2YRGojmnwlcgGIZ
AAlYMEL/Tqb9B8AFDs71jhVIGHDLB/8toI0IDAIwmwgXiBZBXRhAIYAGTwAxVkwetJiACx5cgMAL
HkgoglXDQCFKgTeohNYBQApQAJAGCIDAAUoNkAACniw0whFAGAOAfUDkAsAKfmyQQAQabEDBARpk
oEFZGZSJgGsipHhDRQCAB4RjAID05IYfXmklgr4BeMkQF60pggWiMcBmnRBYWZkwFCCxoQjx2ECn
gw505qGDcuiWD2w30EcClDa4s4EBYmqQyAYafOkHB11xwAEGZYwAnA0KkhABEBGNNoABuEKFKwEC
GLCUALcyZdxyqjg4wgVARHJoiw840CKELXrAWYsTLECCvps21EpCB2ME4AoCSDULooMtiCABTVR+
YO0JB9gQwDEKxFooiKJpKIK4IyDgLlImMGCDAJm0Zq6VGNTrG2epUajSCcSUJoJWRVFVAQUPGHCX
AdNUcOax/9p2QokfHCiCTNKFIx0Cikl3iAKHaUtlRihw+8GwEVhZgUoXrItgJGAu+IGUMTxhrJwP
joWBzulcw1s7OzgQwKIAgAPUAwVMbYBeFAxAgQWTZBImER6nwADLCkhHNjAKJMByAneaEQIAOw==
SMALL_GIF

our $small_gif = decode_base64($small_gif_base64);

# We have to compute this, since different Image::Magicks may
# create different PNGs.
my $small_gif_image = new Image::Magick;
$small_gif_image->BlobToImage(decode_base64($small_gif_base64));
$small_gif_image->Set(magick => 'PNG');

our $small_gif_png = $small_gif_image->ImageToBlob;

our $small_gif_png_base64 = encode_base64($small_gif_png);
our $small_gif_png_len = length($small_gif_png);

our $small_gif_png_sha1 = sha1_hex($small_gif_png);

our $binary_garbage = <<'XML_HUNK';
<presence from='test.transport'
          to='test\@test.com'>
    <c hash='sha-1'
       node='http://developer.berlios.de/projects/thrasher/'
       ver='AgYmsFJ/8ZcNIfFSst43VBp9Qec'
       xmlns='http://jabber.org/protocol/caps'/>
o</presence>
XML_HUNK

sub rand_string {
    my ($length, $chars) = @_;

    my $s = '';
    for (1 .. $length) {
        $s .= $chars->[int(rand(scalar(@{$chars})))];
    }
    return $s;
}

1;
