#!/usr/bin/perl

# Test that Thrasher::XMPPStreamOut works as expected.
use Test::More 'no_plan';
use Test::Deep;

use strict;
use warnings;

use Data::Dumper;

BEGIN {
    use_ok 'Thrasher::XMPPStreamOut';
    use_ok 'Thrasher::Constants', qw(:all);
}

# We deliberately store this as a list so we can examine that
# the chunks are correct.
my @accum;
my $output = sub {
    push @accum, @_;
};

sub clear {
    @accum = ();
}

BASIC_TESTING: {
    my $out = new Thrasher::XMPPStreamOut($output);
    my $initial_stream = [[$NS_STREAM, 'stream'],
                          {'{}version' => '1.0',
                           "{$NS_CLIENT}to" => 'example.com',
                           '{fleem}moo' => 'floo',
                           '{}moo' => 'test'},
                          []];

    $out->output_tag_and_children($initial_stream, 1);

    cmp_deeply(\@accum, ["<stream:stream moo='test' ns1:moo='floo' "
                         ."to='example.com' version='1.0' "
                         ."xmlns='jabber:client' "
                         ."xmlns:ns1='fleem' "
                         ."xmlns:stream='http://etherx.jabber.org/streams'>"],
               "basic stream output works.");

    cmp_deeply($out->{prefixes_to_namespace},
               {'' => [$NS_CLIENT],
                'ns1' => ['fleem'],
                'stream' => [$NS_STREAM]},
               'correctly remembers the root-level namespaces');

    clear;

    my $ugly_sample = [[$NS_CLIENT, 'hello'],
                       {'{}from' => 'test',
                        '{}to' => 'test2',
                        '{jabber:fake}to' => 'test'},
                       [
                        [[$NS_CLIENT, 'moo'],
                         {'{}aimlessly' => 'true',
                          '{jabber:fake}oo' => 'oo'},
                         ["Hi!\x00"]]
                        ]];
    $out->output($ugly_sample);
    cmp_deeply(\@accum, ["<hello from='test' ns2:to='test' to='test2' xmlns:ns2='jabber:fake'>",
                         "<moo aimlessly='true' ns2:oo='oo'>",
                         "Hi!",
                         "</moo>",
                         "</hello>"]);

    cmp_deeply($out->{prefixes_to_namespace},
               {'' => [$NS_CLIENT],
                'ns1' => ['fleem'],
                'stream' => [$NS_STREAM]},
               'correctly forgets the tag-specific namespaces');

    clear;

    $out->output([[$NS_CLIENT, 'test'], {}, []]);
    cmp_deeply(\@accum, ['<test/>'], 'self-closed tags work');
    
    clear;
    
    my $test_string = 'I choose <p>to</p> disregard you.';
    $out->output(\$test_string);
    cmp_deeply(\@accum, [$test_string], 'scalar refs work');
    clear;

    $test_string = "\x1fI have\x00 illegal\x08ness.";
    $out->output(\$test_string);
    cmp_deeply(\@accum, ["I have illegalness."],
               'sending out scalar strings still strips PCDATA-'
               .'illegal strings out.');
}

ENCODING: {
    my $test = "\xf3";
    is(Thrasher::XMPPStreamOut::pcdata_process($test),
       "?", "pcdata_process paranoia-ly strips out bad characters");
}
