package Thrasher::Plugin::Basic;
use strict;
use warnings;

use Thrasher::Plugin qw(:all);
use Thrasher::Constants qw(:all);
use Thrasher::XML qw(:all);
use Thrasher::XMPPStreamIn qw(:all);
use Thrasher::Log qw(:all);

=pod

=head1 NAME

Thrasher::Plugin::Basic - the basic plugins for the component.

=head1 DESCRIPTION

This provides the basic plugins that the component must always
support, or where the support is implemented in pure perl with
no reference to the protocol (such as "time"). This is 
always used by the component.

=cut

sub component_identity_info {
    my $component = shift;

    # Capability-ready info: CATEGORY, TYPE, LANG, NAME
    return ('gateway', lc $component->{protocol}->identifier, '',
            $component->{protocol}->name() . ' Gateway');
}

sub client_identity_info {
    component_identity_info(@_);
}

# Section 4.1: Registration

# Section 4.1.1

# TODO: Handle the old agent information protocol (example 2 and 4)

# Handle the true discovery protocol
register_plugin({component_iq_handlers => 
                     {$NS_DISCO_INFO =>
                          {get => \&handle_disco_info}},
                 client_iq_handlers =>
                     {$NS_DISCO_INFO => 
                          {get => \&handle_disco_info}},
                 features => [$NS_DISCO_INFO]});
sub handle_disco_info {
    my $component = shift;
    my $iq_params = shift;
    my $iq_tag = shift;

    my @identity_info = component_identity_info($component);
    my @supported_features = supported_features;

    my $node = $iq_params->{query}->[1]->{'{}node'};

    my ($category, $type, $lang, $name) = @identity_info;

    # We get here with queries corresponding to Example 1
    my $reply = [[$NS_DISCO_INFO, 'query'], {($node ? (node => $node) : ())},
                 [
                  [[$NS_DISCO_INFO, 'identity'],
                   {category => $category,
                    type => $type,
                    name => $name},
                   []],
                  map { feature($_) } @supported_features,
                  ]];
    # Reply with Example 3
    $component->iq_reply($iq_params, $reply);
}

register_plugin({component_iq_handlers => 
                 {$NS_DISCO_ITEMS =>
                      {get => \&handle_disco_items}},
                 client_iq_handlers => 
                 {$NS_DISCO_ITEMS =>
                      {get => \&handle_disco_items}},
                 features => [$NS_DISCO_ITEMS]});
sub handle_disco_items {
    my $self = shift;
    my $iq_params = shift;

    # We have no items.
    my $reply = [[$NS_DISCO_ITEMS, 'query'], {}, []];
    $self->iq_reply($iq_params, $reply);
}

# 4.1.1 3 and 4 - handle In-Band Registration
register_plugin({component_iq_handlers => 
                 {$NS_REGISTER => {set => \&handle_register,
                                   get => \&handle_register}},
                 features => [$NS_REGISTER]});
sub handle_register {
    my $self = shift;
    my $iq_params = shift;

    my $base_from = strip_resource($iq_params->{from});

    # This handles the mechanics of unregistering.
    my $actually_unregister = sub {
        $self->{protocol}->remove($base_from);
        $self->iq_reply($iq_params);
        $self->send_presence_xml($iq_params->{from}, 'unsubscribe');
        $self->send_presence_xml($iq_params->{from}, 'unsubscribed');
        $self->send_presence_xml($iq_params->{from}, 'unavailable');

        # Hack; do we need to do something more
        # for optional callbacks?
        if (my $callback =
            $Thrasher::Component::UNREGISTER_CALLBACK)
        {
            log "Calling callback\n";
            local $@;
            eval { $callback->($base_from); };
        } else {
            log "Callback not found.\n";
        }
    };

    # And this performs the bookkeeping around it.
    my $unregister = sub {
        my $session = $self->session_for($iq_params->{from});

        # If I'm currently "logged in", I want to finish
        # disconnecting before I report to the user
        # that they are disconnected; if they are not 
        # "logged in", we can immediately unregister.
        if (defined($session)) {
            $self->logout($session, $actually_unregister);
        } else {
            if ($self->registration_info($base_from)) {
                $actually_unregister->();
            } else {
                # What do you mean, "unregister"? I 
                # don't even know you! A little odd
                # to fire "registration required" 
                # to unregister, but it fits best...
                $self->iq_error($iq_params, 
                                'registration_required');
            }
        }
    };

    multi_extract($iq_params->{query},

                  # Example 5 - query registration,
                  # reply with Example 6
                  [[$NS_REGISTER, 'query'], {}, []] =>
                  sub {
                      $self->iq_reply($iq_params,
                                      [[$NS_REGISTER, 'query'],
                                       {},
                                       $self->{protocol}->registration_xml(strip_resource($iq_params->{from}))]);
                  },

                  # Unregistering - Section 4.3
                  [[$NS_REGISTER, 'query'], {}, 
                   save_match([[$NS_REGISTER, 'remove'], undef,
                   undef])] => 
                  sub {
                      $unregister->();
                  },

                  # User is registering - 4.1.1 #5 and 4.2.1 #3
                  [[$NS_REGISTER, 'query'], undef, 
                   save_sub('children', 
                            sub { ref($_[0]) eq 'ARRAY' ? $_[0] : undef})] =>
                  sub {
                      my $children = $_[0]->{children};

                      my $registration_info = {};
                      
                      # Process the children
                      for my $child (@$children) {
                          my $subchildren = $child->[2];
                          if (has_subtags($subchildren)) {
                              $self->iq_error($iq_params, 'bad_request');
                              return;
                          }
                          my $tag_name = $child->[0]->[1];
                          $registration_info->{$tag_name} =
                              join '', @$subchildren;
                      }

                      # add in the defaults as needed
                      my $protocol = $self->{protocol};
                      my $backend = $protocol->{backend};
                      my $registration_defaults =
                          $protocol->registration_defaults;

                      for my $item (keys %$registration_defaults) {
                          if (!defined($registration_info->{$item})) {
                              # If we added a field in a new version, hopefully it
                              # has a new default to go with it. If so, apply it.
                              if (defined($registration_defaults->{$item})) {
                                  $registration_info->{$item} =
                                      $registration_defaults->{$item};
                              } 
                          }
                      }

                      my $current_registration = 
                          $backend->registered
                          (strip_resource($iq_params->{from}));
                      if ($current_registration) {
                          # If the user is already registered, see if
                          # they changed anything. If not, ignore it.
                          # If so, start by unregistering.
                          if (hash_equals($current_registration,
                                          $registration_info)) {
                              $self->iq_reply($iq_params);
                              return;
                          } else {
                              # Perhaps surprisingly, the entire
                              # unregistration process is synchronous.
                              $unregister->();
                          }
                      }

                      # HACK: Allow unregistration by registering
                      # an empty username. While this is not 
                      # compliant with the XEP-0100 protocol, it
                      # is a reasonable digression, IMHO, and many 
                      # clients make it either impossible to 
                      # correctly unregister, or make it a lot less
                      # obvious than just toasting your credentials.
                      # I think this is the best interpretation of 
                      # this action by far.
                      if ($registration_info->{username} eq '') {
                          $unregister->();
                          return;
                      }

                      # This handles 4.1.1 and 4.2.1 registration
                      # in the protocol interface, by requiring
                      # protocol implementations to return correctly
                      # formatted registration replies OR errors.
                      my ($successful, $error) = 
                          $self->{protocol}->registration(strip_resource($iq_params->{from}), $registration_info);
                      if ($successful) {
                          $self->iq_reply($iq_params);
                          $self->send_presence_xml($iq_params->{from}, 'subscribe');
                          $self->send_presence_xml($iq_params->{from}, 'probe');
                      } else {
                          # Section 4.1.2 if the info fails to 
                          # verify.
                          $self->iq_error($iq_params, $error ||
                                                      'bad_request');
                      }
                  },

                  $self->no_match("in iq for $NS_REGISTER", $iq_params));
}

register_plugin({component_iq_handlers => {$NS_GATEWAY => 
                                           {set => \&handle_gateway,
                                            get => \&handle_gateway}}});
sub handle_gateway {
    my $self = shift;
    my $iq_params = shift;
    my $iq_packet = shift;

    # FIXME: Did the lang matching work? Use that.
    my $lang = $iq_packet->[1]->{"{$NS_XML}lang"} || 'en';

    my $children = extract([undef, undef, 
			    save_sub('children',
				     sub { ref($_[0]) ? $_[0] : undef },
				     1)],
			   $iq_params->{query});

    # User made a query.
    if (@{$children->{children}}) {
        my ($prompt) = grep { $_->[0]->[1] eq 'prompt' } @{$children->{children}};

        if (!$prompt) {
            # Had subtags, but no prompt? Error.
            $self->iq_error($iq_params, 'bad_request');
            return;
        }

        my $prompt_strings = 
            extract([undef, undef,
                     save_sub("legacy_name", \&text_extractor)],
                    $prompt);
        my $legacy_name = join '', @{$prompt_strings->{legacy_name}};
        my $jid = $self->legacy_name_to_xmpp
            (strip_resource($iq_params->{from}), $legacy_name,
             $self->{component_name}, $lang);
        
        my $reply = [[$NS_GATEWAY, 'query'], {},
                     [[[$NS_GATEWAY, 'jid'], {}, [$jid]],
                      # PSI at the very least, possibly others,
                      # expect the answer in the 'prompt', bleh
                      [[$NS_GATEWAY, 'prompt'], {}, [$jid]]]];
        $self->iq_reply($iq_params, $reply);
        return;
    }

    
    my $protocol = $self->{protocol};
    my $desc = $protocol->gateway_desc($lang);
    my $prompt = $protocol->gateway_prompt($lang);

    my $reply = [[$NS_GATEWAY, 'query'], {},
                 [[[$NS_GATEWAY, 'prompt'], {}, [$prompt]],
                  (defined($desc) ? 
                   ([[$NS_GATEWAY, 'desc'], {}, [$desc]])
                   : ())
                  ]];
    $self->iq_reply($iq_params, $reply);
}

register_plugin({component_iq_handlers => {$NS_TIME => {get => \&handle_time}},
                 client_iq_handlers => {$NS_TIME => {get => \&handle_time}},
                 features => [$NS_TIME]});
sub handle_time {
    my $self = shift;
    my $iq_params = shift;
    my $iq_packet = shift;

    # "gmtime" gets the UTC time
    my ($sec, $min, $hour, $mday, $mon, $year) = gmtime;
    $year += 1900;
    my $utc_time = sprintf("%04d%02d%02dT%02d:%02d:%02d",
                           $year, $mon + 1, $mday, $hour, $min, $sec);
    
    # I choose to decline to send the other elements because they are
    # worthless; a client should convert the utc time into local time
    # if they care, and XEP-0090 fails to specify an *unambigiuous*
    # timezone format.
    my $reply = [[$NS_TIME, 'query'], {},
                 [[[$NS_TIME, 'utc'], {}, [$utc_time]]]];
    $self->iq_reply($iq_params, $reply);
}

register_plugin({component_iq_handlers => {$NS_VERSION => {get => \&handle_version}},
                 client_iq_handlers => {$NS_VERSION => {get => \&handle_version}},
                 features => [$NS_VERSION]});
sub handle_version {
    my $self = shift;
    my $iq_params = shift;
    
    my $reply = [[$NS_VERSION, 'query'], {},
                 [[[$NS_VERSION, 'name'], {},
                   ['Thrasher - ' . $self->{component_name}]],
                  [[$NS_VERSION, 'version'], {},
                   [$Thrasher::VERSION]]]];
    $self->iq_reply($iq_params, $reply);
}

# Ignore some namespaces we don't support and don't want to see error
# messages for
register_plugin({component_iq_handlers => {$NS_LAST => {get => "ignore"}}});

# hate depending on a library for this
sub hash_equals {
    my $a = shift;
    my $b = shift;

    my @a_keys = keys %$a;
    my @b_keys = keys %$b;

    if (scalar(@a_keys) != scalar(@b_keys)) {
        return 0;
    }

    for my $a_key (@a_keys) {
        if (!exists($b->{$a_key}) ||
            $a->{$a_key} ne $b->{$a_key}) {
            return 0;
        }
    }

    return 1;
}

1;
