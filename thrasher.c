/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>

#include "purple.h"
#include "thrasher.h"
#include "thblist.h"
#include "thconnection.h"
#include "thperl.h"
#include "thft.h"
#include "threquest.h"
#include "thconversations.h"

GHashTable *account_to_connection = NULL;
GHashTable *jid_to_account = NULL;

void debug_print(PurpleDebugLevel level, const char *category, const char *arg_s);
gboolean is_enabled(PurpleDebugLevel level, const char *category);

static void thrasher_signing_off_cb(PurpleConnection *gc);

static PurpleEventLoopUiOps glib_eventloops =
{
    (guint(*)(guint, GSourceFunc, gpointer)) thrasher_wrapper_set_timeout_add,
    (gboolean(*)(guint)) thrasher_wrapper_call_source_remove,
    ((guint(*)(int, PurpleInputCondition, PurpleInputFunction, gpointer))
     thrasher_wrapper_set_input_add),
    (gboolean(*)(guint))thrasher_wrapper_call_source_remove,
    NULL,
    NULL,

    /* padding */
    NULL,
    NULL,
    NULL
};
static PurpleCoreUiOps thrasher_core_uiops =
{
    NULL,
    NULL,
    NULL,
    NULL,

    /* padding */
    NULL,
    NULL,
    NULL,
    NULL
};


static PurpleConnectionUiOps thrasher_conn_uiops =
{
    // connect progress
    thrasher_connection_update_progress,
    // connected,
    thrasher_connection,
    // disconnected
    NULL,
    // connection-specific notices, effectively unused
    NULL,
    // report disconnect
    NULL,
    // computer network connected
    NULL,
    // computer network disconnected
    NULL,
    // report disconnect reason
    NULL,
    // reserved 1
    NULL,
    // reserved 2
    NULL,
    // reserved 3
    NULL
};

void thrasher_init()
{
     purple_core_set_ui_ops(&thrasher_core_uiops);
     purple_eventloop_set_ui_ops(&glib_eventloops);
     purple_connections_set_ui_ops(&thrasher_conn_uiops);

     if (!purple_core_init(UI_ID))
          error(EXIT_FAILURE, 0, "libpurple init failed");

     thrasher_blist_init();
     thrasher_connection_init();
     thrasher_xfer_init();
     thrasher_perl_init();
     thrasher_request_init();
     thrasher_conversations_init();

     // This hash table has no ownership on the pointers stored in it
     account_to_connection = g_hash_table_new(g_direct_hash,
                                              g_direct_equal);
     // Automatically cleans up keys, does not own stored pointers
     jid_to_account = g_hash_table_new_full(g_str_hash, g_str_equal,
                                            g_free, NULL);

     purple_set_blist(purple_blist_new());

     purple_signal_connect(purple_connections_get_handle(),
                           "signing-off",
                           thrasher_connection_get_handle(),
                           PURPLE_CALLBACK(thrasher_signing_off_cb),
                           NULL);
}

void thrasher_whoami (PurpleAccount * pa)
{
    if (pa)
        purple_debug_info("thrasher",
                          "You are user [%s]\n",
                          pa->username);
}

static void
thrasher_signing_off_cb(PurpleConnection *gc) {
    g_return_if_fail(gc);

    PurpleAccount* pa = purple_connection_get_account(gc);
    g_return_if_fail(pa);

    if (g_hash_table_lookup(account_to_connection, pa)) {
        char *jid = thrasher_account_get_jid(pa);
        g_hash_table_remove(jid_to_account, jid);
        g_hash_table_remove(account_to_connection, pa);
    }
}

void
thrasher_logout (PurpleAccount *pa)
{
    g_return_if_fail(pa);

    purple_account_set_enabled(pa, UI_ID, FALSE);

    // Only disconnect if we haven't already
    if (g_hash_table_lookup(account_to_connection, pa)) {
        purple_account_disconnect(pa);
    } else {
        purple_debug_info("thrasher", 
                          "Declined to log out unconnected account\n");
    }

    if (pa->ui_data) {
        Thrasher_PurpleAccount_UI_Data* ui_data = pa->ui_data;
        pa->ui_data = NULL;
        g_hash_table_destroy(ui_data->pending_send_file);
        g_free(ui_data);
    }
}

PurpleAccount * thrasher_get_account_by_jid(const char* jid) {
    g_return_val_if_fail(jid, NULL);
    return g_hash_table_lookup(jid_to_account, jid);
}

PurpleConnection * thrasher_get_connection_by_account
    (const PurpleAccount* account) {
    g_return_val_if_fail(account, NULL);
    return g_hash_table_lookup(account_to_connection, account);
}

// An error can occur during the login, when the protocol decides
// there's going to be a connection error before it even returns the
// connection to us. In that case, our user data won't exist on the
// connection, but the connection error still needs to propagate back
// to the user. In that case, we use this value. This rather ties
// us to a single thread.

static char *current_login_jid = NULL;
static uint got_error = 0;

char * get_current_login_jid () {
    return current_login_jid;
}

void set_got_error (uint new_val) {
    got_error = new_val;
}


static void
thrasher_prefs_init() {
    purple_prefs_load();

    /* Disable all logging to disk because it might leave files under
     * USER_ROOT_DIR open.
     */
    purple_prefs_set_bool("/purple/logging/log_ims", FALSE);
    purple_prefs_set_bool("/purple/logging/log_chats", FALSE);
    purple_prefs_set_bool("/purple/logging/log_system", FALSE);
}

PurpleAccount *
thrasher_login (char *service, char *username, char *password, char *jid,
                GHashTable *other_args)
{
    got_error = 0;
    current_login_jid = jid;
    gchar *user_dir;

    if (purple_get_core() == 0)
        error(EXIT_FAILURE, 0, "Purple core is uninitialized");

    purple_debug_info("thrasher",
                      "Thrasher login initiated\n");

    /* Test service for validity */

    /* Test user for validity */
    /* We need to verify there is NO path redirection in the user name! */
    /* We also need to verify username is NOT longer than MAX_NAME_LEN */

    /* Test password for validity */


    /* Validate the root_dir and build us a filename */
    user_dir = g_build_filename(USER_ROOT_DIR, service, username, NULL);

    purple_util_set_user_dir(user_dir);

    g_free(user_dir);

    PurpleAccount *account;
    PurpleSavedStatus *status;

    /* Setup the account and throw it back */
    account = purple_account_new(username, service);
    g_hash_table_insert(jid_to_account, g_strdup(jid), account);
    purple_account_set_password(account, password);

    Thrasher_PurpleAccount_UI_Data* ui_data
        = g_new(Thrasher_PurpleAccount_UI_Data, 1);
    ui_data->jid = g_strdup(jid); /* Store the JID with the account */
    ui_data->pending_send_file = g_hash_table_new(g_str_hash,
                                                  g_str_equal);
    account->ui_data = ui_data;

    if (other_args) {
        GHashTableIter iter;
        gpointer key, value;
        char *char_key, *char_value;

        g_hash_table_iter_init(&iter, other_args);
        while (g_hash_table_iter_next(&iter, &key, &value)) {
            char_value = (char *)value;
            char_key = (char *)key;
            if (!strncmp(key, "int_", 4))
            {
                char_key += 4;
                int int_value = atoi(char_value);
                purple_debug_info("thrasher",
                                  "Setting account #: %s -> %d\n",
                                  char_key, int_value);
                purple_account_set_int(account, char_key, atoi(char_value));
            } else if (!strncmp(key, "bool_", 5)) {
                char_key += 5;
                int value = (*char_value == '0' ||
                             *char_value == 0) ? FALSE: TRUE;
                purple_debug_info("thrasher",
                                  "Setting account bool: %s -> %d\n",
                                  char_key, value);
                purple_account_set_bool(account, char_key, value);
            } else {
                purple_debug_info("thrasher",
                                  "Setting account string: %s -> %s\n",
                                  char_key, char_value);
                purple_account_set_string(account, char_key, 
                                          char_value);
            }
        }
    }

    purple_account_set_enabled(account, UI_ID, TRUE);
    status = purple_savedstatus_new(NULL, PURPLE_STATUS_AVAILABLE);
    purple_savedstatus_activate(status);

    current_login_jid = NULL; 

    if (got_error) {
        purple_debug_info("thrasher",
                          "got an error during initial login\n");
        return NULL;
    }

#ifdef TH_DEBUG
    purple_debug_info("thrasher",
                      "Using libpurple v%s\n",
                      purple_core_get_version());
    purple_debug_info("thrasher",
                      "Supported protocols\n");
    GList *iter; int i;
    iter = purple_plugins_get_protocols();
    for (i = 0; iter; iter = iter->next) {
        PurplePlugin *plugin = iter->data;
        PurplePluginInfo *info = plugin->info;
        if (info && info->name) {
            purple_debug_info("thrasher",
                              "\t%d: %s\n",
                              i++,
                              info->name);
        }
    }
#endif


/* Not sure how I got in this state, but write a catch nonetheless */
    if (!purple_plugins_get_protocols())
        error(EXIT_FAILURE, 0, "libpurple reports NO supported protocols, this is a BAD thing");

    return account;
}

/*
 * How to send message to remote user
 *
 * pa      = PurpleAccount ptr
 * name    = recipient name char*
 * message = message char*
 *
 * If the conversation data can be created this returns TRUE, else it returns FALSE
 *
 * Get the conversation if it already exists
 * (Per the code flow, we may be able to set type to NULL and let it figure it out.)
*/

gboolean
thrasher_send_msg (PurpleAccount *pa, const char *name, const char *message)
{
    PurpleConversation *conv = thrasher_find_conversation_with_account(pa,
                                                                       name);
    PurpleConvIm       *im;

    /* Get the IM specific data */
    im = purple_conversation_get_im_data(conv);

    if (im) {
        purple_conv_im_send(im, message);
        return TRUE;
    }

    purple_debug_info("thrasher",
                      "Failed in purple_conv_im_send\n");

    return FALSE;
}

void thrasher_connection (PurpleConnection *conn)
{
     PurpleAccount *account = purple_connection_get_account(conn);

     g_hash_table_insert(account_to_connection, account, conn);
     
     thrasher_wrapper_connection(thrasher_account_get_jid(account));
}

PurpleConnection* thrasher_connection_for_account(PurpleAccount
                                                  *account) {
    g_return_val_if_fail(account, NULL);

    return g_hash_table_lookup(account_to_connection, account);
}

/*
 * thrasher_account_get_jid
 */
gchar*
thrasher_account_get_jid(PurpleAccount *account)
{
    g_return_val_if_fail(account, NULL);

    Thrasher_PurpleAccount_UI_Data* ui_data = account->ui_data;
    return ui_data->jid;
}

/*
 * thrasher_account_get_pending_send_file
 */
GHashTable*
thrasher_account_get_pending_send_file(PurpleAccount *account) {
    g_return_val_if_fail(account, NULL);

    Thrasher_PurpleAccount_UI_Data* ui_data = account->ui_data;
    g_return_val_if_fail(ui_data, NULL);
    return ui_data->pending_send_file;
}

gboolean
thrasher_account_buddy_is_authorized(PurpleBuddy *buddy) {
    /* FIXME: There's no cross-prpl call for this? Get tricky. */
    g_return_val_if_fail(buddy, 1);
    PurpleAccount* account = purple_buddy_get_account(buddy);
    g_return_val_if_fail(account, 1);

    const char *prpl_id = purple_account_get_protocol_id(account);
    g_return_val_if_fail(prpl_id, 1);
    PurplePlugin *prpl = purple_find_prpl(prpl_id);
    g_return_val_if_fail(prpl, 1);

    PurplePluginProtocolInfo *prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(prpl);
    g_return_val_if_fail(prpl_info, 1);

    if (prpl_info->list_emblem) {
	const char *emblem_name = prpl_info->list_emblem(buddy);
        if (emblem_name && 0 == strcmp(emblem_name, "not-authorized")) {
            return 0;
        }
    }

    return 1;
}
