/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#ifndef THRASHER_H
#define THRASHER_H

#include <error.h>
#include "purple.h"

#define THRASHER_DEBUG      "thrasher"
#define THRASHER_PORT       31415
#define USER_ROOT_DIR       "/tmp/purpletest"
#define MAX_NAME_LEN        128
#define CUSTOM_PLUGIN_PATH  ""
#define PLUGIN_SAVE_PREF    "/purple/thrasher/plugins/saved"
#define UI_ID               "thrasher"

#define PURPLE_GLIB_READ_COND  (G_IO_IN  | G_IO_HUP | G_IO_ERR)
#define PURPLE_GLIB_WRITE_COND (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)


/* Errors */
#define NO_PROTOCOLS        1


typedef struct _PurpleGLibIOClosure {
    PurpleInputFunction function;
    guint result;
    gpointer data;
} PurpleGLibIOClosure;


char * get_current_login_jid(void);
void set_got_error(uint);
PurpleAccount *
thrasher_login (char *service, char *username, char *password, char *jid,
                GHashTable *other_args);
void thrasher_init(void);
gboolean thrasher_send_msg (PurpleAccount *pa, const char *name, const char *message);
void thrasher_logout (PurpleAccount *pa);
void thrasher_whoami (PurpleAccount *pa);
PurpleAccount* thrasher_get_account_by_jid(const char* jid);
PurpleConnection* thrasher_get_connection_by_account(const PurpleAccount*);

void thrasher_connection(PurpleConnection *);
PurpleConnection* thrasher_connection_for_account(PurpleAccount *);

typedef struct _Thrasher_PurpleAccount_UI_Data {
    gchar *jid;          /* not owned */
    GHashTable *pending_send_file; /* char* who => Thrasher_Xfer_UI_Data* */
} Thrasher_PurpleAccount_UI_Data;

gchar* thrasher_account_get_jid(PurpleAccount *account);
GHashTable* thrasher_account_get_pending_send_file(PurpleAccount *account);
gboolean thrasher_account_buddy_is_authorized(PurpleBuddy *buddy);

#endif /* THRASHER_H */
