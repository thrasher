#!/usr/bin/perl

use Test::More tests => 4;

BEGIN {
    use_ok('Thrasher::Log', qw(logger debug dies));
    $Thrasher::Log::SILENT = 1;
};

my $rand_num =  int rand 100000;
my $test_msg = "test message $rand_num";

my ($fn, $line, $msg) = logger($test_msg);

is ("$fn:$line $msg",
    "$0:$line $test_msg",
    "Valid message call");

is (debug("$test_msg"),
    0,
    "Silent debug call");

$Thrasher::Log::DEBUG = 1;

$test_msg .= int rand 1000000;
($fn, $line, $msg) = logger($test_msg);

is ("$fn: $msg",
    "$0: $test_msg",
    "Loud debug call");

# TODO: flip dies subroutine to use Error and test on try/throw

