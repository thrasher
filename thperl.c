/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "thperl.h"
#include "thblist.h"
#include "blist.h"
#include "thrasher.h"
#include "thpresence.h"
#include "thconversations.h"
#include "thft.h"
#include "unistd.h"

/* Perl */
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

/* Internal prototypes */
int func_call(gpointer func);
void destroy_record_key (gpointer key);
void validate_callback (SV *cb);
void thrasher_wrapper_destructor (void);
SV *clean_newSVpv(const char *inp);
void thrasher_remove_account(const char *jid);

perl_callbacks callback;
perl_ft_callbacks ft_callback;


char *trigger_timeout_func_name = "THPPW::thrasher_wrapper_trigger_timeout_func";
char *trigger_input_func_name   = "THPPW::thrasher_wrapper_trigger_input_func";


GHashTable *accts = NULL;
GHashTable *callback_funcs = NULL;
GHashTable *perl_key_lookup = NULL;

void thrasher_purple_debug (int tf)
{
    /* Flip on libpurple debugging info */
    if (tf > 0) {
        purple_debug_set_enabled(1);
    }
    if (tf > 1) {
        purple_debug_set_verbose(1);
    }
}


void thrasher_perl_init()
{
    accts = g_hash_table_new(g_str_hash, g_str_equal);
}

/**
 * @brief create a PurpleAccount and start connecting it.
 * @param hash_args of arguments for thrasher_login().
 * @return 0 for success. 1 if thrasher_login() fails. 2 if the PurpleAccount already exists.
 */
int thrasher_action_login (GHashTable *hash_args)
{
    int err = 0;

#ifdef TH_DEBUG
    purple_debug_info("thrasher",
                      "Login request:\n");
    GList *keys = g_hash_table_get_keys(hash_args);
    GList *values = g_hash_table_get_values(hash_args);
    while (keys) 
    {
        purple_debug_info("thrasher",
                          "    %s -> %s\n",
                          (char *)keys->data,
                          (char *)values->data);
        keys = keys->next;
        values = values->next;
    }

    g_list_free(keys);
    g_list_free(values);
#endif /* TH_DEBUG */

    // some parameters we always have
    char *jid = (char *)g_strdup(g_hash_table_lookup(hash_args, "jid"));
    char *username = (char *)g_strdup(g_hash_table_lookup(hash_args, "username"));
    char *password = (char *)g_strdup(g_hash_table_lookup(hash_args, "password"));
    char *proto = (char *)g_strdup(g_hash_table_lookup(hash_args, "proto"));

    g_return_val_if_fail(jid, 0);
    g_return_val_if_fail(username, 0);
    g_return_val_if_fail(password, 0);
    g_return_val_if_fail(proto, 0);

    g_hash_table_remove(hash_args, "jid");
    g_hash_table_remove(hash_args, "username");
    g_hash_table_remove(hash_args, "password");
    g_hash_table_remove(hash_args, "proto");

    /* Verify user is not already logged in */
    if (g_hash_table_lookup(accts, jid) == NULL)
    {
        PurpleAccount *account = thrasher_login(proto, username, password,
                                                jid, hash_args);

        // In the event of an error, connection_error has already been
        // called.
        if (account == NULL) {
            return 1;
        }
        
        char *new_jid = g_strdup(jid);

        /* Add user to hash */
        g_hash_table_insert(
                accts,
                new_jid, /* Should eventually be the protocol + username */
                account
                );

        /* Success! */
        err = 0;
    }
    else
    {
        // At least indicate something bad has happened.
        purple_debug_info("thrasher",
                          "login request for [%s] fail because they are apparently already logged in\n",
                          jid);
        err = 2;
    }

    g_free(jid);
    g_free(username);
    g_free(password);
    g_free(proto);
    g_hash_table_destroy(hash_args);

    return err;
}


/**
 * @brief Get a list of all JIDs with active accounts.
 */
GList_String*
thrasher_action_get_logged_in_jids() {
    GList* jids = g_hash_table_get_keys(accts);
    return jids;
}



/**
 * @brief add buddy (@p buddy_name) for given @p jid buddy list
 * @param jid releational key to bound to account
 * @param buddy_name name of buddy
 * @return 1 if there is a problem, 0 otherwise (C)
 */
int thrasher_action_buddy_add (char *jid, char *buddy_name)
{
    PurpleAccount *account = NULL;
    PurpleBuddy   *buddy = NULL;
    PurpleGroup   *group = NULL;

    purple_debug_info("thrasher", "Buddy_add request\n");

    account = g_hash_table_lookup(accts, jid);
    g_return_val_if_fail(account != NULL, 1);

    buddy = purple_find_buddy(account, buddy_name);

    /* Re-requesting authorization addresses cases in which a buddy is
     * implicitly added somewhere else but never actually sent an
     * authorization request. Such a state was sometimes possible with
     * Yahoo but consistently reproducible with as follows in ICQ:
     *
     * 1. Register user1 and user2 with the ICQ transport, each on the
     *    other's roster.
     * 2. Remove user1's ICQ contact from user2's roster.
     * 3. Remove user2's ICQ contact from user1's roster.
     * 4. Add user2's ICQ contact to user1's roster.
     * 5. thrasher_buddy_request_authorize() receives and handles an
     *    authorization request from user1 on behalf of user2. It
     *    adds user1 to user2's buddy list.
     * 6. user1 is now fully authorized with user2.
     * 7. user2, however, had user1 added as an *un*authorized buddy.
     *
     * This also means Thrasher tries to do something sensible with
     * XMPP client operations like "re-request authorization from
     * contact". Otherwise, they'd always be handled as no-ops because
     * the libpurple buddy is already present.
     */
    gboolean need_auth;
    if (buddy) {
        /* Re-add: check if (re-)auth is needed. */
        need_auth = ! thrasher_account_buddy_is_authorized(buddy);
    }
    else {
        /* Not yet on list: assume an auth request is needed. */
        need_auth = 1;
    }
    /* FIXME: There's no cross-prpl calls for checking/sending
     * authorization like oscar.c:purple_auth_request()? Could try to
     * parse for the menu items added by each prpl we know requires
     * authorization. :(
     */
    if (need_auth) {
        purple_debug_info("thrasher",
                          "%s needs authorization from %s\n",
                          jid, buddy_name);
    }

    // FIXME: Figure out if we need this buddy list stuff
    // at all. We don't actually care, as long as we get
    // the messages and presence to send out.

    /* We're done if buddy already exists and is authorized.
     *
     * libpurple says prpl should interpret a re-add of a existent
     * buddy as an attempt to (re)send an authorization request if
     * applicable.
     */
    if (buddy == NULL || need_auth)
    {
        char *default_group = "General";

        /* Check to see if default group exists */
        group = purple_find_group(default_group);

        /* If not, create default group */
        if (group == NULL)
            group = purple_group_new(default_group);

        purple_blist_add_group(group, NULL);

        /* Add buddy to default group (NULL should be alias...) */
        buddy = purple_buddy_new(account, buddy_name, NULL);
        purple_blist_add_buddy(buddy, NULL, group, NULL);
        purple_account_add_buddy(account, buddy);

        purple_debug_info("thrasher",
                          "finished adding %s to %s's buddy list\n",
                          buddy_name, jid);

        return 0;
    } else {
        purple_debug_info("thrasher",
                          "%s already on %s's buddy list\n",
                          buddy_name, jid);
    }

    /* Buddy already exists */
    return 1;
}

/**
 * @brief remove buddy (@p buddy_name) from given @p jid buddy list
 * @param jid releational key to bound to account
 * @param buddy_name name of buddy
 * @return 1 if there is a problem, 0 otherwise (C)
 */
int thrasher_action_buddy_remove (char *jid, char *buddy_name)
{
    PurpleGroup   *group = NULL;
    PurpleBuddy   *buddy = NULL;
    PurpleAccount *account = NULL;

    purple_debug_info("thrasher", "Buddy remove\n");

    account = g_hash_table_lookup(accts, jid);
    g_return_val_if_fail(account != NULL, 1);

    // Don't try and remove a buddy if the account
    // isn't connected
    if (purple_account_is_connected(account)) {

        purple_debug_info("thrasher", "find_buddy...\n");
        buddy = purple_find_buddy(account, buddy_name);

        /* Only remove a buddy if they exist */
        if (buddy != NULL)
        {
            purple_debug_info("thrasher", "get_group...\n");
            /* Give up us the group */
            group = purple_buddy_get_group(buddy);

            purple_debug_info("thrasher", "account_remove_buddy...\n");
            /* Remove server-side buddy entry */
            purple_account_remove_buddy(account, buddy, group);

            purple_debug_info("thrasher", "blist_remove_buddy...\n");
            /* Also need to remove buddy entry from memory */
            purple_blist_remove_buddy(buddy);

            return 0;
        }
    }

    /* Account isn't connected or buddy is not found */
    return 1;
}

void thrasher_action_buddy_authorize(char *jid, char *legacy_username)
{
    thrasher_action_buddy_add(jid, legacy_username);
}

void thrasher_action_buddy_deauthorize(char *jid, char *legacy_username) {
    thrasher_action_buddy_remove(jid, legacy_username);
}

/**
 * @brief update presence status for given @p jid
 * @param jid releational key to bound to account
 * @param status integer representation of PurpleStatusPrimitive
 * @param message a status message (may be NULL)
 * @return 1 if there is a problem, 0 otherwise (C)
 */
int thrasher_action_presence (char *jid, int status, char *message)
{
    PurpleAccount *account;

    g_return_val_if_fail(jid != NULL, 0);

    account = g_hash_table_lookup(accts, jid);

    if (account == NULL)
        return 1;

    thrasher_set_presence(account, status, message);

    return 0;
}


/**
 * @brief Send @p message from the user given by @p jid to the user @p recipient
 * @param jid
 * @param recipient
 * @param message
 * @return 1 on success 0 on failure
 */
int thrasher_action_outgoing_msg (char *jid, char *recipient, char *message)
{
    int ret = 0;
    PurpleAccount *account = NULL;

    g_return_val_if_fail(jid != NULL, 0);
    g_return_val_if_fail(recipient != NULL, 0);
    g_return_val_if_fail(message != NULL, 0);

    purple_debug_info("thrasher", "Send message request\n");
    /* Pull in user if they exist */
    account = g_hash_table_lookup(accts, jid);

    /* User does not exist (may not be logged in) */
    if (account == NULL) {
        return 0;
    }

    /* Try to send message (message might not be sent) */
    if (thrasher_send_msg(account,recipient,message) == TRUE)
        ret = 1;

    return ret;
}


/**
 * @brief Send @p chatstate from the user given by @p jid to the @p recipient
 * @param jid
 * @param recipient
 * @param chatstate
 */
void
thrasher_action_outgoing_chatstate(char *jid,
                                   char *recipient,
                                   PurpleTypingState chatstate) {
    g_return_if_fail(jid != NULL);
    g_return_if_fail(recipient != NULL);

    PurpleAccount *pa = NULL;
    pa = g_hash_table_lookup(accts, jid);
    if (pa == NULL) {
        /* User does not exist (may not be logged in) */
        return;
    }

    /* Try to send chatstate */
    thrasher_outgoing_chatstate(pa, recipient, chatstate);
}


/**
 * @brief Log the user out with the given jid
 * @param jid releational key to bind to account
 * @return 1 if logout was successful, 0 if it was not
 *
 * NOTE:  As we're going to be running a process for each protocol,
 *        there's no need to worry about the protocol used.
 */
int thrasher_action_logout (char *jid)
{
    int ret = 0;
    PurpleAccount *account;

    g_return_val_if_fail(jid != NULL, 0);

    purple_debug_info("thrasher", "Logout requested for %s\n", jid);

    /* Verify user is logged in */
    account =g_hash_table_lookup(accts, jid);
    if (account != NULL)
    {
        thrasher_logout(account);
        thrasher_remove_account(jid);
        ret = 1;
    }

    return ret;
}


void _thrasher_action_debug_logged_in_accts_printer(gpointer key, gpointer value, gpointer user_data);

/**
 * @brief Dump active account name lists to STDERR from libpurple and thperl.
 */
void
thrasher_action_debug_logged_in() {
    fprintf(stderr, "purple_accounts_get_all_active:\n");
    GList* list = purple_accounts_get_all_active();
    if (list) {
        GList *l = list;
        while (l) {
            PurpleAccount *pa = (PurpleAccount*)l->data;
            fprintf(stderr, "\t%s = %p\n", purple_account_get_username(pa), pa);
            l = l->next;
        }
        g_list_free(list);
    }

    if (accts) {
        fprintf(stderr, "thperl accts:\n");
        g_hash_table_foreach(accts,
                             _thrasher_action_debug_logged_in_accts_printer,
                             NULL);
    }
    else {
        fprintf(stderr, "No thperl accts?!!\n");
    }
}

void
_thrasher_action_debug_logged_in_accts_printer(gpointer key,
                                               gpointer value,
                                               gpointer user_data) {
    const char *jid = (char*)key;
    PurpleAccount *pa = (PurpleAccount*)value;
    fprintf(stderr, "\t%s = %s = %p\n",
            jid, purple_account_get_username(pa), pa);
}


/**
 * @brief Destruction function for our internal function hash
 * @param key data element
 */
void destroy_record_key (gpointer key)
{
    free(key);
}


/**
 * @brief Bizarro passthrough to allow perl calling of dynamically-set C events
 * @param key a guint value corresponding to the pointer of a callback.
 * @return boolean as int (0 || 1) on success.  Returns 0 on failure as well.
 */
int thrasher_wrapper_trigger_timeout_func(long key)
{
    /* @exception key cannot be NULL */
    g_return_val_if_fail(key != TNULL,TFALSE);

    char *real_key = LONG_TO_PTR(key);

    return func_call(real_key);
}

/**
 * @brief Bizarro passthrough (two) to allow perl calling of dynamically-set C events
 * @param file descriptor
 * @param condition trigger was called on
 * @param key a long value corresponding to the pointer of a callback.
 * @return boolean as int (0 || 1) on success.  Returns 0 on failure as well.
 */
int thrasher_wrapper_trigger_input_func(long fd, SV *cond, long key)
{
    /* @exception fd cannot be zero */
    g_return_val_if_fail(fd != 0, 0);
    /* @exception cond cannot be zero */
    g_return_val_if_fail(cond != 0, 0);
    /* @exception key cannot be zero */
    g_return_val_if_fail(key != 0, 0);

//    GIOCondition real_cond = cond;
    char *real_key = LONG_TO_PTR(key);

    /* FIXME: We need to pass cond and key to this call... */
    return func_call(real_key);
}

/**
 * @brief Simple verifcation for callbacks
 * @param subref to callback
 *
 */
void validate_callback (SV *cb)
{
    if (!SvROK(cb) || SvTYPE(SvRV(cb)) != SVt_PVCV)
    {
        croak("Not a callback!");
        exit(0);
    }
}


/**
 * @brief  Initialize our wrapper
 * @param  subref (SV *) to call for timeout_add
 * @param  subref (SV *) to call for input_add
 * @param  subref (SV *) to call for source_remove
 * @param  subref (SV *) to call on incoming messages
 * @param  subref (SV *) to call on incoming presence information
 * @param  subref (SV *) to call on subscription requests
 * @param  subref (SV *) to call on subscription requests from legacy users
 * @param  subref (SV *) to call on connection errors
 * @param  subref (SV *) to call when connections have succeeded
 * @param  subref (SV *) to call on incoming chatstate (optional)
 */
void thrasher_wrapper_init (SV *timeout_add_cb,
                            SV *input_add_cb,
                            SV *source_remove_cb,
                            SV *incoming_msg_cb,
                            SV *presence_in_cb,
                            SV *subscription_add_cb,
                            SV *legacy_user_add_user_cb,
                            SV *connection_error_cb,
                            SV *connection_cb,
                            ...)
{
    va_list argp;
    va_start(argp, connection_cb);
    SV* incoming_chatstate_cb = va_arg(argp, SV*);

    g_return_if_fail(timeout_add_cb);
    g_return_if_fail(input_add_cb);
    g_return_if_fail(source_remove_cb);
    g_return_if_fail(incoming_msg_cb);
    g_return_if_fail(presence_in_cb);
    g_return_if_fail(subscription_add_cb);
    g_return_if_fail(legacy_user_add_user_cb);
    g_return_if_fail(connection_error_cb);
    g_return_if_fail(connection_cb);

    /* Set the bindings for our function table */
    callback_funcs = g_hash_table_new_full(g_direct_hash,
                                           g_direct_equal,
                                           destroy_record_key,
                                           NULL);

    /* Create the lookup table so we know what to do when Perl triggers a remove */
    perl_key_lookup = g_hash_table_new(g_int_hash, g_int_equal);

    /* Validate callback, this burns up if it fails so no
     * need to check for errors!
     */
    validate_callback(timeout_add_cb);
    validate_callback(input_add_cb);
    validate_callback(source_remove_cb);
    validate_callback(incoming_msg_cb);
    validate_callback(presence_in_cb);
    validate_callback(subscription_add_cb);
    validate_callback(legacy_user_add_user_cb);
    validate_callback(connection_error_cb);
    validate_callback(connection_cb);
    if (incoming_chatstate_cb) {
        validate_callback(incoming_chatstate_cb);
    }

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(SP);
    PUTBACK;

    callback.timeout_add   = newSVsv(timeout_add_cb);
    callback.input_add     = newSVsv(input_add_cb);
    callback.source_remove = newSVsv(source_remove_cb);
    callback.incoming_msg  = newSVsv(incoming_msg_cb);
    callback.presence_in   = newSVsv(presence_in_cb);
    callback.subscription_add = newSVsv(subscription_add_cb);
    callback.legacy_user_add_user = newSVsv(legacy_user_add_user_cb);
    callback.connection_error = newSVsv(connection_error_cb);
    callback.connection = newSVsv(connection_cb);
    if (incoming_chatstate_cb) {
        callback.incoming_chatstate = newSVsv(incoming_chatstate_cb);
    }
    else {
        callback.incoming_chatstate = 0;
    }

    SPAGAIN;
    PUTBACK;

    FREETMPS;
    LEAVE;
}

/**
 * @brief  Initialize our file transfer wrapper
 * @param  subref (SV *) to call for recv_accept
 * @param  subref (SV *) to call for recv_start
 * @param  subref (SV *) to call for recv_cancel
 * @param  subref (SV *) to call for recv_complete
 * @param  subref (SV *) to call for send_accept
 * @param  subref (SV *) to call for send_start
 * @param  subref (SV *) to call for send_cancel
 * @param  subref (SV *) to call for send_complete
 * @param  subref (SV *) to call for ui_read. Must return (buffer, size).
 * @param  subref (SV *) to call for ui_write
 * @param  subref (SV *) to call for ui_data_not_sent
 */
void thrasher_wrapper_ft_init(SV *ft_recv_request_cb,
                              SV *ft_recv_accept_cb,
                              SV *ft_recv_start_cb,
                              SV *ft_recv_cancel_cb,
                              SV *ft_recv_complete_cb,
                              SV *ft_send_accept_cb,
                              SV *ft_send_start_cb,
                              SV *ft_send_cancel_cb,
                              SV *ft_send_complete_cb,
                              SV *ft_read_cb,
                              SV *ft_write_cb,
                              SV *ft_data_not_sent_cb)
{
    g_return_if_fail(ft_recv_request_cb);
    g_return_if_fail(ft_recv_accept_cb);
    g_return_if_fail(ft_recv_start_cb);
    g_return_if_fail(ft_recv_cancel_cb);
    g_return_if_fail(ft_recv_complete_cb);
    g_return_if_fail(ft_send_accept_cb);
    g_return_if_fail(ft_send_start_cb);
    g_return_if_fail(ft_send_cancel_cb);
    g_return_if_fail(ft_send_complete_cb);
    g_return_if_fail(ft_read_cb);
    g_return_if_fail(ft_write_cb);
    g_return_if_fail(ft_data_not_sent_cb);

    // This assumes thrasher_wrapper_init was already called.
    g_return_if_fail(callback_funcs);

    validate_callback(ft_recv_request_cb);
    validate_callback(ft_recv_accept_cb);
    validate_callback(ft_recv_start_cb);
    validate_callback(ft_recv_cancel_cb);
    validate_callback(ft_recv_complete_cb);
    validate_callback(ft_send_accept_cb);
    validate_callback(ft_send_start_cb);
    validate_callback(ft_send_cancel_cb);
    validate_callback(ft_send_complete_cb);
    validate_callback(ft_read_cb);
    validate_callback(ft_write_cb);
    validate_callback(ft_data_not_sent_cb);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(SP);
    PUTBACK;

    ft_callback.ft_recv_request_cb = newSVsv(ft_recv_request_cb);
    ft_callback.ft_recv_accept_cb = newSVsv(ft_recv_accept_cb);
    ft_callback.ft_recv_start_cb = newSVsv(ft_recv_start_cb);
    ft_callback.ft_recv_cancel_cb = newSVsv(ft_recv_cancel_cb);
    ft_callback.ft_recv_complete_cb = newSVsv(ft_recv_complete_cb);
    ft_callback.ft_send_accept_cb = newSVsv(ft_send_accept_cb);
    ft_callback.ft_send_start_cb = newSVsv(ft_send_start_cb);
    ft_callback.ft_send_cancel_cb = newSVsv(ft_send_cancel_cb);
    ft_callback.ft_send_complete_cb = newSVsv(ft_send_complete_cb);
    ft_callback.ft_read_cb = newSVsv(ft_read_cb);
    ft_callback.ft_write_cb = newSVsv(ft_write_cb);
    ft_callback.ft_data_not_sent_cb = newSVsv(ft_data_not_sent_cb);

    SPAGAIN;
    PUTBACK;

    FREETMPS;
    LEAVE;
}



/**
 * @brief Validate then call the given callback pointer.
 * @param key a hash key for the local function callback table.
 * @return boolean as int (0 || 1) on success
 */
int func_call(gpointer key)
{
    /* @exception key cannot be NULL */
    g_return_val_if_fail(key != NULL,TFALSE);

    gpointer orig_key = NULL;
    hash_record *record = NULL;
    int (*timeout_add_caller)(gpointer);
    int (*input_add_caller)(gpointer, gint, PurpleInputCondition);
    int ret = 0;

    /* Crazy voodoo madness!
     * We're using direct pointers as keys and a struct at the
     * end of the pointer, so no need to get any values.
     */
    if (g_hash_table_lookup_extended(callback_funcs, key, &orig_key, NULL))
    {
        record = orig_key;

        if (record->type == TIMEOUT_ADD)
        {
            timeout_add_caller = record->function;
            ret = (timeout_add_caller)(record->data);
        }

        else if (record->type == INPUT_ADD)
        {
            input_add_caller = record->function;
            (input_add_caller)(record->data, record->fd,
                               record->cond);
            // these functions are void
            ret = TRUE; 
        }

        if (ret == FALSE)
        {
            /* We're never gonna be calling this again, so get rid of it */
            g_hash_table_remove(callback_funcs, record);
        }

        return (ret);
    }

    else
    {
        purple_debug_info("thrasher", "Callback for [%p] does not exist!\n", key);
    }

    return(TFALSE);
}


/**
 * @brief Wraps Glib::Source->remove
 * @param key to return to the Perl end
 * @return Success of call, including the success of the callback
 */
gboolean thrasher_wrapper_call_source_remove (long key)
{
    long ret = FALSE;

    /* @exception key cannot be NULL */
    g_return_val_if_fail(callback.source_remove, 0);

    /* @exception callback.source_remove cannot be NULL */
    g_return_val_if_fail(key != TNULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the key onto the stack */
    XPUSHs(sv_2mortal(newSViv(key)));

    PUTBACK;

    call_sv(callback.source_remove, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    /* Remove call from lookup table */
    g_hash_table_remove(perl_key_lookup, (gpointer)&key);

    return ret;
}


/**
 * PurpleInputCondition is a bitwise OR of read/write events
 * PurpleInputFunction is a callback to a guint function with the args gpointer, gint, and PurpleInoutCondition
 *
 *
 * @brief Wraps Glib::IO->add_watch
 * @param File descriptor
 * @param Condition to trigger on
 * @param Nested callback
 * @param Data to pass to callback
 * @return Hash key
 */
long thrasher_wrapper_set_input_add (guint fd, PurpleInputCondition cond, PurpleInputFunction function, gpointer data)
{
    g_return_val_if_fail(callback.input_add, 0);
    g_return_val_if_fail(function != NULL, 0);

    long key = 0;

    hash_record *record = (hash_record *)malloc(sizeof(hash_record));

    record->type     = INPUT_ADD;
    record->function = function;
    record->data     = data;
    record->cond     = cond;
    record->fd       = fd;

    g_hash_table_insert(callback_funcs, record, NULL);

    key = PTR_TO_LONG(record);
    g_return_val_if_fail(key != 0, 0);

    dSP;

    ENTER;
    SAVETMPS;


    SV *pc      = NULL;
    CV *temp_cv = NULL;
    SV *svkey   = NULL;


    svkey = newSViv(key);
    temp_cv = get_cv(trigger_input_func_name, FALSE);

    if (temp_cv)
    {
        pc = newRV((SV*)temp_cv);

        /* Ok, this is hairy, first we push the vars we want to send to the
         * timeout_add Perl callback.  Next we push the var we want to give
         * back to ourselves via trigger_input_func.
         */

        PUSHMARK(sp);

        XPUSHs(sv_2mortal(newSViv(fd)));
        XPUSHs(sv_2mortal(newSViv(cond)));
        XPUSHs(sv_2mortal(pc));
        XPUSHs(sv_2mortal(svkey));

        PUTBACK;

        call_sv(callback.input_add, G_EVAL | G_SCALAR);
        SPAGAIN;

        /* Tweak our internal key to an external key */
        key = POPl;
    }
    else
    {
        croak("We've got serious problems, cannot create CV* to [%s]", trigger_input_func_name);
    }

    g_hash_table_insert(perl_key_lookup, (gpointer)&key, record);

    return key;
}


/**
 * @brief Wraps Glib::Timeout->add
 * @param interval in milliseconds
 * @param callback
 * @param data to pass to the callback
 * @return Key for the internal timeout_add function hash.  (Should this really be returning anything?!)
 */
long thrasher_wrapper_set_timeout_add (guint interval, GSourceFunc function, gpointer data)
{
    /* @exception callback.timeout_add cannot be NULL */
    g_return_val_if_fail(callback.timeout_add, 0);
    g_return_val_if_fail(function != NULL, TFALSE);

    long key = 0;

    hash_record *record = (hash_record *)malloc(sizeof(hash_record *));

    record->type     = TIMEOUT_ADD;
    record->function = function;
    record->data     = data;

    g_hash_table_insert(callback_funcs, record, NULL);

    key = PTR_TO_LONG(record);
    g_return_val_if_fail(key != 0, 0);

    dSP;

    ENTER;
    SAVETMPS;


    SV *pc      = NULL;
    CV *temp_cv = NULL;
    SV *svkey   = NULL;

    svkey = newSViv(key);
    temp_cv = get_cv(trigger_timeout_func_name, FALSE);

    if (temp_cv)
    {
        pc = newRV((SV*)temp_cv);

        /* Ok, this is hairy, first we push the vars we want to send to the
         * timeout_add Perl callback.  Next we push the var we want to give
         * back to ourselves via trigger_input_func.
         */

        PUSHMARK(sp);

        XPUSHs(sv_2mortal(newSViv(interval)));
        XPUSHs(sv_2mortal(pc));
        XPUSHs(sv_2mortal(svkey));

        PUTBACK;

        call_sv(callback.timeout_add, G_EVAL | G_SCALAR);

        SPAGAIN;

        /* Tweak our internal key to an external key */
        key = POPl;
    }
    else
    {
        croak("We've got serious problems, cannot create CV* to [%s]", trigger_input_func_name);
    }

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return key;
}

/**
 * @brief Send a @p message out via callback.incoming_msg
 * @param jid user the @p message is being sent to
 * @param sender user name of the @p message sender
 * @param alias alternate user name of the @p sender (may be NULL)
 * @param message string being sent to @p jid
 * @return 1 on success, 0 on failure
 */
int thrasher_wrapper_incoming_msg (const char *jid,
                                   const char *sender,
                                   const char *alias,
                                   const char *message,
                                   PurpleMessageFlags flags)
{
    int ret = 0;

    /* @exception jid, sender, and message cannot be NULL */
    g_return_val_if_fail(jid != NULL, 0);
    g_return_val_if_fail(sender != NULL, 0);
    g_return_val_if_fail(message != NULL, 0);

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(callback.incoming_msg != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSVpvn(jid, strlen(jid))));
    XPUSHs(sv_2mortal(newSVpvn(sender, strlen(sender))));
    XPUSHs(sv_2mortal(newSVpvn(alias, strlen(alias))));
    XPUSHs(sv_2mortal(newSVpvn(message, strlen(message))));
    XPUSHs(sv_2mortal(newSViv(flags)));

    PUTBACK;

    call_sv(callback.incoming_msg, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;
}

/**
 * @brief Forwards the 'connection' event, that authentication has
 *   completed
 * @param jid user who has successfully connected
 */
int thrasher_wrapper_connection(const char *jid)
{
    int ret = 0;

    /* @exception jid, sender, and message cannot be NULL */
    g_return_val_if_fail(jid != NULL, 0);

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(callback.connection != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSVpvn(jid, strlen(jid))));

    PUTBACK;

    call_sv(callback.connection, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;    
}

SV *clean_newSVpv(const char *inp)
{
    if (inp)
        return sv_2mortal(newSVpv(inp, 0));
    else
        return sv_2mortal(&PL_sv_undef);
}


/**
 * @brief Forward presence information via callback.presence_in
 * @param jid user the @p status is being sent to
 * @param sender user name of the @p status sender
 * @param alias alternate user name of the @p sender (may be NULL)
 * @param group which the @p sender belongs to
 * @param status PurpleStatusPrimitive (guint) being sent to @p jid
 * @param message string being sent to @p jid (may be NULL)
 * @return 1 on success, 0 on failure (wrapped Perl)
 */
int thrasher_wrapper_presence_in(const char *jid,
                                 const char *sender,
                                 const char *alias,
                                 const char *group,
                                 const PurpleStatusPrimitive status,
                                 const char *message)
{
    int ret = 0;

    /* @exception jid, sender, and group cannot be NULL */
    g_return_val_if_fail(jid != NULL, 0);
    g_return_val_if_fail(sender != NULL, 0);
    g_return_val_if_fail(group != NULL, 0);

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(callback.presence_in != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(clean_newSVpv(jid));
    XPUSHs(clean_newSVpv(sender));
    XPUSHs(clean_newSVpv(alias));
    XPUSHs(clean_newSVpv(group));
    XPUSHs(sv_2mortal(newSViv(status)));
    XPUSHs(clean_newSVpv(message));

    PUTBACK;

    call_sv(callback.presence_in, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;
}

int thrasher_wrapper_legacy_user_add_user(const char *jid,
                                          const char *sender) {
    int ret = 0;

    g_return_val_if_fail(jid, 0);
    g_return_val_if_fail(sender, 0);

    g_return_val_if_fail(callback.legacy_user_add_user, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    XPUSHs(clean_newSVpv(jid));
    XPUSHs(clean_newSVpv(sender));

    PUTBACK;

    call_sv(callback.legacy_user_add_user, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;
}

/**
 * @brief Forward subscription request information via callback.subscription_add
 * @param jid user the @p status is being sent to
 * @param sender user name of the requester
 * @param status PurpleStatusPrimitive (guint) being sent to @p jid
 * @return 1 on success, 0 on failure (wrapped Perl)
 */
int thrasher_wrapper_subscription_add(const char *jid,
                                      const char *sender,
                                      guint status)
{
    int ret = 0;

    /* @exception jid, sender, and group cannot be NULL */
    g_return_val_if_fail(jid != NULL, 0);
    g_return_val_if_fail(sender != NULL, 0);

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(callback.subscription_add != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(clean_newSVpv(jid));
    XPUSHs(clean_newSVpv(sender));
    XPUSHs(sv_2mortal(newSViv(status)));

    PUTBACK;

    call_sv(callback.subscription_add, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;
}

/**
 * @brief Forward connection error messages via callback.connection_error
 * @param jid user the @p error_code is being sent to
 * @param error_code PurpleConnectionError (guint) being sent to @p jid
 * @param message a error message (may be NULL)
 * @return 1 on success, 0 on failure (wrapped Perl)
 */
int thrasher_wrapper_connection_error(const char *jid,
                                      const guint error_code,
                                      const char *message)
{
    int ret = 0;

    // If the JID is null, pick it up off of
    // thrasher.c:current_login_jid, per the comment in that file.
    if (jid == NULL) {
        purple_debug_info("thrasher", "had to retrieve the jid from mem\n");
        jid = get_current_login_jid();
    }

    // If we're in the middle of a connection, pick up that we have an error.
    set_got_error(1);

    g_return_val_if_fail(jid, 0);


    /* @exception error_code cannot be less than zero or greater than PURPLE_CONNECTION_ERROR_OTHER_ERROR */
    g_return_val_if_fail(error_code >= 0, ret);
    g_return_val_if_fail(error_code <= PURPLE_CONNECTION_ERROR_OTHER_ERROR, ret);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(clean_newSVpv(jid));
    XPUSHs(sv_2mortal(newSViv(error_code)));
    XPUSHs(clean_newSVpv(message));

    PUTBACK;

    call_sv(callback.connection_error, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    // Remove the account from our current active set
    thrasher_remove_account(jid);

    return ret;
}

void thrasher_remove_account (const char *jid) 
{
    PurpleAccount *account;
  
    g_return_if_fail(jid != NULL);

    purple_debug_info("thrasher", "Removing active account for %s\n",
                      jid);

    account = g_hash_table_lookup(accts, jid);
    if (account != NULL) 
    {
        g_hash_table_remove(accts, jid);
    }
}

/**
 * @brief Forwards typing notification state changes from libpurple buddies.
 * @param pa Account receiving the notification
 * @param who Buddy name sending the notification
 * @param state
 */
void thrasher_wrapper_incoming_chatstate(PurpleAccount* pa,
                                         const char* who,
                                         PurpleTypingState state) {
    /* @exception pa and who cannot be NULL */
    g_return_if_fail(pa != NULL);
    g_return_if_fail(who != NULL);

    /* bail if Perl-side doesn't implement this */
    if (! callback.incoming_chatstate) {
        return;
    }

    gchar* jid = thrasher_account_get_jid(pa);
    /* @exception bail if account has no JID */
    g_return_if_fail(jid != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(clean_newSVpv(jid));
    XPUSHs(clean_newSVpv(who));
    XPUSHs(sv_2mortal(newSViv(state)));

    PUTBACK;

    call_sv(callback.incoming_chatstate, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;
}

/**
 * @begin External trigger to flush the internal function tables
 */
void thrasher_wrapper_destructor ()
{
    g_hash_table_destroy(callback_funcs);
    g_hash_table_destroy(perl_key_lookup);
}

size_t
thrasher_wrapper_send_file(const char *jid,
                           const char *who,
                           char *filename,
                           size_t size,
                           char *desc) {
    g_return_val_if_fail(jid, 0);
    g_return_val_if_fail(who, 0);

    purple_debug_info("thrasher ft",
                      "Sending file %s -> %s\n",
                      jid, who);

    PurpleAccount *account = g_hash_table_lookup(accts, jid);

    g_return_val_if_fail(account, 0);

    return thrasher_send_file(account, who, filename, size, desc);
}

void
thrasher_action_ft_ui_ready(size_t id) {
    PurpleXfer* xfer = get_xfer_by_id(id);
    g_return_if_fail(xfer);
    purple_xfer_ui_ready(xfer);
    /* HACK: READY_PRPL is usually not set (or was cleared) right now,
     * and some prpls don't appear to set it correctly on repeats.
     * Spam prpl_ready to force a do_transfer anyway.
     *
     * Using a separate watch on the remote xfer->fd would avoid some
     * EAGAINs. I'd be happy to be proven wrong, but testing found no
     * significant benefit to Thrasher in that.
     */
    purple_xfer_prpl_ready(xfer);
}

/**
 * @brief Called on libpurple file-send-start signal
 * @param id of Thrasher file transfer structure.
 */
int
thrasher_wrapper_ft_send_start(guint id) {
    int ret = 0;

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(ft_callback.ft_send_start_cb != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));

    PUTBACK;

    call_sv(ft_callback.ft_send_start_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    ret = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return ret;
}

/**
 * @brief Called on libpurple file-send-cancel signal
 * @param id of Thrasher file transfer structure.
 */
void
thrasher_wrapper_ft_send_cancel(guint id) {
    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_send_cancel_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));

    PUTBACK;

    call_sv(ft_callback.ft_send_cancel_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

/**
 * @brief Called on libpurple file-send-complete signal
 * @param id of Thrasher file transfer structure.
 */
void
thrasher_wrapper_ft_send_complete(guint id) {
    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_send_complete_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));

    PUTBACK;

    call_sv(ft_callback.ft_send_complete_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

gssize
thrasher_wrapper_ft_read(guint id,
                         guchar **buffer,
                         gssize size) {
    int read_sz = -1;
    int count = 0;

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(ft_callback.ft_read_cb != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));
    XPUSHs(sv_2mortal(newSViv(size)));

    PUTBACK;

    count = call_sv(ft_callback.ft_read_cb, G_EVAL | G_ARRAY);

    SPAGAIN;

    if (count == 2) {
        read_sz = POPl;
        void* read = POPpbytex;
        *buffer = malloc(read_sz);
        /* This buffer is free'd by libpurple. */
        memcpy(*buffer, read, read_sz);
    }
    else {
        *buffer = NULL;
        read_sz = -1;
    }

    FREETMPS;
    PUTBACK;

    LEAVE;

    return read_sz;
}

void
thrasher_wrapper_ft_data_not_sent(guint id,
                                  const char *buffer,
                                  gsize size) {
    /* @exception buffer cannot be NULL */
    g_return_if_fail(buffer != NULL);

    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_data_not_sent_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));
    /* buffer is not null-terminated. */
    XPUSHs(sv_2mortal(newSVpvn(buffer, size)));

    PUTBACK;

    call_sv(ft_callback.ft_data_not_sent_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

/*
 * @brief ask user's permission for ft and set up bytestream
 * @param xfer a filled-in PURPLE_XFER_RECEIVE PurpleXfer.
 * @param filename suggested local filename
 * @return nothing; use thrasher_action_ft_recv_request_respond when ready
 */
void
thrasher_wrapper_ft_recv_request(PurpleXfer *xfer,
                                 const char* filename) {
    /* @exception xfer cannot be NULL */
    g_return_if_fail(xfer != NULL);

    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_recv_request_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    guint id = get_id_by_xfer(xfer);
    XPUSHs(sv_2mortal(newSViv(id)));
    PurpleAccount* pa = purple_xfer_get_account(xfer);
    gchar* jid = thrasher_account_get_jid(pa);
    XPUSHs(clean_newSVpv(jid));
    const char* who = purple_xfer_get_remote_user(xfer);
    XPUSHs(clean_newSVpv(who));
    XPUSHs(clean_newSVpv(filename));
    size_t size = purple_xfer_get_size(xfer);
    XPUSHs(sv_2mortal(newSViv(size)));

    PUTBACK;

    call_sv(ft_callback.ft_recv_request_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

void
thrasher_action_ft_recv_request_respond(size_t id,
                                        unsigned int accept) {
    thrasher_xfer_recv_request_responder(id, accept);
}

gssize
thrasher_wrapper_ft_write(guint id,
                          const char *buffer,
                          gssize size) {
    int written_sz = -1;

    /* @exception bail if our callback doesn't exist */
    g_return_val_if_fail(ft_callback.ft_write_cb != NULL, 0);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));
    /* buffer is not null-terminated. */
    XPUSHs(sv_2mortal(newSVpvn(buffer, size)));

    PUTBACK;

    call_sv(ft_callback.ft_write_cb, G_EVAL | G_ARRAY);

    SPAGAIN;

    written_sz = POPl;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return written_sz;
}

/**
 * @brief Called on libpurple file-recv-cancel signal
 * @param id of Thrasher file transfer structure.
 */
void
thrasher_wrapper_ft_recv_cancel(guint id) {
    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_recv_cancel_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));

    PUTBACK;

    call_sv(ft_callback.ft_recv_cancel_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

/**
 * @brief Called on libpurple file-recv-complete signal
 * @param id of Thrasher file transfer structure.
 */
void
thrasher_wrapper_ft_recv_complete(guint id) {
    /* @exception bail if our callback doesn't exist */
    g_return_if_fail(ft_callback.ft_recv_complete_cb != NULL);

    dSP;

    ENTER;
    SAVETMPS;

    PUSHMARK(sp);

    /* Push the args onto the stack */
    XPUSHs(sv_2mortal(newSViv(id)));

    PUTBACK;

    call_sv(ft_callback.ft_recv_complete_cb, G_EVAL | G_SCALAR);

    SPAGAIN;

    FREETMPS;
    PUTBACK;

    LEAVE;

    return;
}

void
thrasher_action_ft_cancel_local(size_t id) {
    PurpleXfer* xfer = get_xfer_by_id(id);
    purple_xfer_cancel_local(xfer);
}

#ifdef TH_DEBUG
/*********************************************************
 *
 *  Only debugging stuff beyond this point!
 *
 *********************************************************/
gboolean foo_callback (void *data);
gboolean foo_callback2 (void *data);
void foo_callback3 (gpointer data, gint fd, PurpleInputCondition cond);
long thrasher_wrapper_trigger_timeout_add (void);
long thrasher_wrapper_trigger_timeout_add2 (void);
long thrasher_wrapper_trigger_timeout_add3 (void);
long thrasher_wrapper_trigger_input_add (void);
void thrasher_wrapper_dump_calls (void);
void spit_func_info (gpointer key, gpointer value, gpointer user_data);


gboolean
foo_callback (void *data)
{
    printf("!!!!!!Calling a callback w/ data [%s]!!!!\n", (char *)data);
    return TRUE;
}


int cntr = 9;


gboolean
foo_callback2 (void *data)
{
    printf("will die in [%d] cycles\n", cntr);

    if (cntr--)
        return TRUE;
    else
        return FALSE;
}

void foo_callback3 (gpointer data, gint fd, PurpleInputCondition cond)
{
    printf("data [%p]\tfd [%d]\tcond [%d]\n", data, fd, cond);
}

/* Test functions to allow an external caller to trigger internal actions */
long thrasher_wrapper_trigger_timeout_add ()
{
    printf("trigger_timeout_add called\n");
    return thrasher_wrapper_set_timeout_add(1234, foo_callback, "YAY! test data");
}


long thrasher_wrapper_trigger_timeout_add2 ()
{
    printf("trigger_timeout_add2 called\n");
    return thrasher_wrapper_set_timeout_add(1867, foo_callback, "test data says what?");
}

long thrasher_wrapper_trigger_timeout_add3 ()
{
    printf("trigger_timeout_add3 called\n");
    return thrasher_wrapper_set_timeout_add(200, foo_callback2, "this shoudl fail shortly");
}

long thrasher_wrapper_trigger_input_add ()
{
    printf("trigger_input_add called\n");

    int fd = open("/tmp/testfoo", O_RDWR);
    PurpleInputCondition cond;
    cond = PURPLE_INPUT_WRITE;
    long face = 0;
    printf("HAMMERTIME\n");
    face = thrasher_wrapper_set_input_add(fd, cond, foo_callback3, "double plus win ungood");

    return face;
}

int thrasher_wrapper_trigger_timeout_remove (long key)
{
    printf("trigger_timeout_remove called\n");
    /* We currently have no way to correlate external keys to internal keys... */
    return !thrasher_wrapper_call_source_remove(key);
}


void spit_func_info (gpointer key, gpointer value, gpointer user_data)
{
    hash_record *record;
    printf("key [%p]\tvalue [%p]\n", key, value);

    record = key;

    if (record->type == INPUT_ADD)
    {
        printf("\tINPUT_ADD\n\tfunction [%p]\n\tdata [%p]\n\tcond [%d]\n\tfd [%d]\n", record->function, record->data, record->cond, record->fd);
    }

    else if (record->type == TIMEOUT_ADD)
    {

        printf("\tTIMEOUT_ADD\n\tfunction [%p]\n\tdata [%p]\n", record->function, record->data);
    }

    else
    {
        printf("record type [%d] is unrecognized!\n", record->type);
    }
}

void thrasher_wrapper_dump_calls ()
{
    g_hash_table_foreach(callback_funcs, spit_func_info, NULL);
}

#endif /* TH_DEBUG */
