/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#ifndef THFT_H
#define THFT_H

typedef struct _Thrasher_Xfer_UI_Data {
    size_t id;
    char* filename;             /* filename to send on network. owned. */
    guint size;
    char* desc;   /* description/message to send with offer. owned. */
} Thrasher_Xfer_UI_Data;

void thrasher_xfer_init(void);

// Thrasher bookkeeping
void free_id (gpointer id);
size_t get_next_file_transfer_id(void);
PurpleXfer* get_xfer_by_id(guint id);
guint get_id_by_xfer(PurpleXfer* xfer);
void thrasher_remove_xfer(PurpleXfer *xfer);
void thrasher_xfer_destroy(guint id);
int store_xfer(PurpleXfer *xfer);

// Callbacks
void thrasher_xfer_recv_request_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_recv_accept_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_recv_start_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_recv_cancel_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_recv_complete_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_send_accept_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_send_start_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_send_cancel_cb(PurpleXfer *xfer, gpointer data);
void thrasher_xfer_send_complete_cb(PurpleXfer *xfer, gpointer data);

// UI Ops
void thrasher_new_xfer(PurpleXfer *xfer);
void thrasher_destroy_xfer(PurpleXfer *xfer);
void thrasher_add_xfer(PurpleXfer *xfer);
void thrasher_update_xfer_progress(PurpleXfer *xfer, double percent);
void thrasher_xfer_cancel_local(PurpleXfer *xfer);
void thrasher_xfer_cancel_remote(PurpleXfer *xfer);
gssize thrasher_ui_write(PurpleXfer *xfer, const guchar *buffer, gssize size);
gssize thrasher_ui_read(PurpleXfer *xfer, guchar **buffer, gssize size);
void thrasher_data_not_sent(PurpleXfer *xfer, const guchar *buffer, gsize size);


// Request UI ops
void *thrasher_request_file(const char *title, const char *filename,
                            gboolean savedialog, GCallback ok_cb,
                            GCallback cancel_cb,
                            PurpleAccount *account,
                            const char *who, 
                            PurpleConversation *conv,
                            void *user_data);

// Perl hooks
size_t thrasher_send_file(PurpleAccount *account,
                          const char *who,
                          char *filename,
                          size_t size,
                          char *desc);
void thrasher_xfer_recv_request_responder(guint id, guint accept);

#endif
