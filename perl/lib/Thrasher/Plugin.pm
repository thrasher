package Thrasher::Plugin;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Plugin - manage the plugins to the component

=head1 DESCRIPTION

Thrasher::Plugin implements the code to manage the plugins to 
Thrasher::Component. Plugins are loaded by "use"ing the corresponding
module, which will call the appropriate "register_plugin" method 
within it.

A Plugin adds a given capability to the component, adding support for
the appropriate IQs and and other necessary codes. Protocols then 
declare which plugins they support, and the Component will load the
appropriate plugins. Protocols indicate plugins by their name in
the Plugin directory.

If plugins need other plugins (such as when the Avator code needs the
PEP plugin), you can simple "use" the prerequisites in your module.

Plugins call the following function in their module:

=cut

use Thrasher::Log qw(log);

use Carp qw(confess);

use Thrasher::Callbacks qw(:all);
use Thrasher::Constants;

use base 'Exporter';

our @EXPORT_OK = qw(register_plugin unregister_plugin
                    method_for_iq_namespace
                    supported_features
                    callbacks);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

# This is filled out as we go, to stay in sync with the spec
my %QUERY_FUNCTIONS;

sub method_for_iq_namespace { $QUERY_FUNCTIONS{$_[0]}->{$_[1]}->{$_[2]}; }

# This is a list of all features we support. This may be changed by
# 'use'ing various additional bits of functionality.
my %SUPPORTED_FEATURES;
my @SUPPORTED_FEATURES_SORTED;

# Features always supported:
register_plugin({features => [
    $Thrasher::Constants::NS_CHATSTATES,
]});

register_callback('plugins_changed', 'default', \&update_supported_features);

sub update_supported_features {
    @SUPPORTED_FEATURES_SORTED = sort keys %SUPPORTED_FEATURES;
}

sub supported_features { @SUPPORTED_FEATURES_SORTED; }

sub register_plugin {
    my $plugin_data = shift;

    # A plugin can define:
    # A set of IQ handlers by namespace
    # A set of features that this plugin enables
    
    if ($plugin_data->{client_iq_handlers}) {
        while (my ($namespace, $type_more) =
               each %{$plugin_data->{client_iq_handlers}}) {
            while (my ($type, $handler) = each %$type_more) {
                $QUERY_FUNCTIONS{'client'}->{$type}->{$namespace} = $handler;
            }
        }
    }

    if ($plugin_data->{component_iq_handlers}) {
        while (my ($namespace, $type_more) =
               each %{$plugin_data->{component_iq_handlers}}) {
            while (my ($type, $handler) = each %$type_more) {
                $QUERY_FUNCTIONS{'component'}->{$type}->{$namespace} = $handler;
            }
        }
    }

    if ($plugin_data->{features}) {
        for my $feature (@{$plugin_data->{features}}) {
            $SUPPORTED_FEATURES{$feature} = 1;
        }
    }

    if ($plugin_data->{callbacks}) {
        while (my ($type, $callbacks) = 
               each %{$plugin_data->{callbacks}}) {
            while (my ($name, $callback) = each %$callbacks) {
                register_callback($type, $name, $callback);
            }
        }
    }

    callbacks('plugins_changed');
}

# This is to make testing easier, so we can pop features in and out.
# This should never be called in running code.
sub unregister_plugin {
    my $plugin_data = shift;

    if ($plugin_data->{client_iq_handlers}) {
        while (my ($namespace, $handler) =
               each %{$plugin_data->{client_iq_handlers}}) {
            delete $QUERY_FUNCTIONS{client}->{$namespace};
        }
    }

    if ($plugin_data->{component_iq_handlers}) {
        while (my ($namespace, $handler) =
               each %{$plugin_data->{component_iq_handlers}}) {
            delete $QUERY_FUNCTIONS{component}->{$namespace};
        }
    }

    if ($plugin_data->{features}) {
        for my $feature (@{$plugin_data->{features}}) {
            delete $SUPPORTED_FEATURES{$feature};
        }
    }

    if ($plugin_data->{callbacks}) {
        while (my ($type, $callbacks) = 
               each %{$plugin_data->{callbacks}}) {
            while (my ($name, $callback) = each %$callbacks) {
                unregister_callbacks($type, $name);
            }
        }
    }

    callbacks('plugins_changed');
}

# Plugins are designed to load themselves via "use" and make
# themselves Just Work (TM), so you should not need to use this
# method in real code. However, the test protocol would sometimes like
# to use a certain plugin for testing purposes, or behave differently
# when the plugin is loaded, without actually "use"ing the plugin
# itself, and possibly affecting other test scripts not expecting that
# plugin. Thus, this method will tell the test protocol whether the
# given plugin is loaded. Hopefully this makes it clear why real code
# should never use this.
sub _has_plugin {
    my $plugin_name = shift; # the part after "Thrasher::Plugin::"
    return $INC{'Thrasher/Plugin/' . $plugin_name . '.pm'};
}

1;
