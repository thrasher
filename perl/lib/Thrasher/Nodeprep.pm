package Thrasher::Nodeprep;
use strict;
use warnings;

# This module uses the Unicode::Stringprep module, combined with the
# specification in RFC3920, to define "nodeprep" and "stringprep"
# functions that can be used for converting strings of unknown safety
# into JIDs.

use Unicode::Stringprep;
use Unicode::Stringprep::Mapping;
use Unicode::Stringprep::Prohibited;

use base 'Exporter';

use Data::Dumper;

our @EXPORT_OK = qw(nodeprep stringprep);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

*nodeprep = Unicode::Stringprep->new
    (3.2,

     [
      # In accordance with RFC3920 section A.3
      @Unicode::Stringprep::Mapping::B1,
      @Unicode::Stringprep::Mapping::B2,

      # The standard specifies these as banned in A.5, but I'm going to map
      # them to blank strings instead, so they are eliminated instead
      # of causing "die"s.
      (map { defined($_) ? ($_, '') : () }
        @Unicode::Stringprep::Prohibited::C11,
        @Unicode::Stringprep::Prohibited::C12,
        @Unicode::Stringprep::Prohibited::C21,
        @Unicode::Stringprep::Prohibited::C22,
        @Unicode::Stringprep::Prohibited::C3,
        @Unicode::Stringprep::Prohibited::C4,
        @Unicode::Stringprep::Prohibited::C5,
        @Unicode::Stringprep::Prohibited::C6,
        @Unicode::Stringprep::Prohibited::C7,
        @Unicode::Stringprep::Prohibited::C8,
        @Unicode::Stringprep::Prohibited::C9,

       (
        0x22, # double-quote
        0x26, # ampersand
        0x27, # apostrophe
        0x2f, # backslash,
        0x3a, # colon
        0x3c, # less than
        0x3e, # greater than
       )),

      ord('@') => '%'  # I don't really like this but it's traditional
     ],

     # Section A.4 specifies normalization form KC
     'KC', 
     
     # Prohibited characters are replaced with blanks
     [
     ],

     # Section A.6 specifies handling bi-directional strings.
     1
    );

1;

*resource_prep = Unicode::Stringprep->new
    (3.2,

     # Section B.3
     [
      @Unicode::Stringprep::Mapping::B1,
      (map { defined($_) ? ($_, '') : () }
       @Unicode::Stringprep::Prohibited::C12,
       @Unicode::Stringprep::Prohibited::C21,
       @Unicode::Stringprep::Prohibited::C22,
       @Unicode::Stringprep::Prohibited::C3,
       @Unicode::Stringprep::Prohibited::C4,
       @Unicode::Stringprep::Prohibited::C5,
       @Unicode::Stringprep::Prohibited::C6,
       @Unicode::Stringprep::Prohibited::C7,
       @Unicode::Stringprep::Prohibited::C8,
       @Unicode::Stringprep::Prohibited::C9
       )
      ],

     # Section B.4
     'KC',

     [
     ],

     # Section B.6
     1);

      
	 

1;
