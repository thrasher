#!/usr/bin/perl

use strict;
use warnings;

# This tests the MySQL InnoDB backend by directly manipulating it.
# Be aware that this will involve the random creation and 
# destruction of a MySQL database called
# "dbi_backend_mysql_innodb_test", which is given a klunky name
# on purpose. If this test runs to completion, it will clean
# up after itself, but if it crashes it won't. This is of
# course done so the last state of the DB can be examined.

use Test::More 'no_plan';
use Data::Dumper;
use IPC::Run qw(run);
use Test::Deep;

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::Backend::DBI';
}

my ($username, $password) = get_mysql_username_password;

my $db_name = 'dbi_backend_mysql_innodb_test';

my $args_hash = 
    {dbi_data_source => "dbi:mysql:$db_name",
     username => $username, password => $password,
     db_driver => 'mysql_innodb',
     database_name => $db_name,
     transport_name => 'test'};

VERIFY_ALL_REQUIRED_PARAMS: {
    for my $key (keys %$args_hash) {
        my %hash_copy = %$args_hash;
        delete $hash_copy{$key};
        dies {
            new Thrasher::Backend::DBI(\%hash_copy);
        } 'missing', "required param $key is required";
    }
}

DESTROY_DATABASE: {
    my $out;
    run(["mysql", 
         $username ? ("--user=$username") : (),
         $password ? ("--password=$password") : (),
         "-e", "DROP DATABASE $db_name"],
        '>&', \$out);

    if ($out =~ /exist/) {
        pass("$db_name didn't exist");
    } elsif (!$out) {
        pass("$db_name existed and was dropped");
    } else {
        die "$db_name was neither seen to be nonexistant, nor "
            ."dropped. Out: $out";
    }
}

my $db;

REAL_DB_LOAD: {
    $db = new Thrasher::Backend::DBI($args_hash);

    # Let's verify all the tables exist.
    for my $table (@Thrasher::Backend::DBI::tables) {
        my $sql = $db->sql('detect_table');
        # Have to manually do this since 'tablename' doesn't
        # work, which is what ? normally would expand to
        $sql =~ s/\?/$table/;
        my ($val) = $db->{dbh}->selectrow_array($sql);
        if (!$val) {
            # die, since there's not much point continuing on.
            die "Table $table was not created correctly.";
        }
        pass("Database table $table created");
    }
}

my $jid = "romeo\@montague.lit";

REGISTRATION: {
    my $jid_id = $db->jid_id($jid);
    is($jid_id, undef, 'user initially not found');

    my $registration_info = $db->registered($jid);
    is($registration_info, undef, 
       'user initially not registered');
    $db->register('romeo@montague.lit',
                  {username => 'romeo@montague.lit',
                   password => 'starcrossed'});
    $registration_info = $db->registered($jid);
    cmp_deeply($registration_info,
               {username => 'romeo@montague.lit',
                password => 'starcrossed'},
               'properly registered romeo');

    $jid_id = $db->jid_id($jid);
    isnt($jid_id, undef, 'romeo has an id');

    $db->register('romeo@montague.lit',
                  {username => 'romeo@montague.lit',
                   password => 'moo'});
    $registration_info = $db->registered($jid);
    cmp_deeply($registration_info,
               {username => 'romeo@montague.lit',
                password => 'moo'},
               'new registration information used');

    my $new_jid_id = $db->jid_id($jid);
    is($new_jid_id, $jid_id, 'same JID id used');

    # Verify that registering with too much info works;
    # we had a real problem with this with our client.
    ok($db->register($jid,
                     {username => 'romeo@montague.lit',
                      password => 'ILoveJuliet',
                      nick => "RomeoMyRomeo"}));
    $registration_info = $db->registered($jid);
    cmp_deeply($registration_info,
               {username => 'romeo@montague.lit',
                password => 'ILoveJuliet'},
               "fields the DB doesn't understand are correctly "
               ."filtered out, instead of killing everything");
}

NAME_MAPPING: {
    # Verify the three basic name-mapping cases:
    # * Incoming new legacy name
    # * Correctly-mapped legacy to JID
    # * Blindly-mapped JID to legacy.

    my $jid1 = $db->legacy_name_to_jid($jid, 'A@B', 
                                       'aim.transport');
    is($jid1, 'a%b@aim.transport', 
       'correctly translates legacy names as expected');

    $jid1 = $db->legacy_name_to_jid($jid, 'A@B',
                                    'aim.transport');
    is($jid1, 'a%b@aim.transport',
       'correctly looks up legacy names');

    my $legacy = $db->jid_to_legacy_name($jid, 'a%b@aim.transport');
    is($legacy, 'A@B', 'jid_to_legacy_name can correctly look things up');

    # Something we haven't seen yet
    my $legacy2 = $db->jid_to_legacy_name($jid,
                                          'me%ga@aim.transport');
    is($legacy2, 'me%ga', 'correctly forged a legacy name');

    # For somebody else, we get a%b
    $legacy = $db->jid_to_legacy_name('bill@montague.lit', 
                                      'a%b@aim.transport');
    is($legacy, 'a%b', 'correct separation by JID');
}

AVATARS: {
    my $avatar1 = "EBCDIC";
    my $avatar2 = "ASCII";

    is($db->get_avatar($jid, 'a@b'), undef,
       'no avatar initially');

    $db->set_avatar($jid, 'a@b', $avatar1);
    is($db->get_avatar($jid, 'a@b'), $avatar1,
       'can correctly store and retrieve an avatar');
    
    $db->set_avatar($jid, 'a@b', $avatar2);
    is($db->get_avatar($jid, 'a@b'), $avatar2,
       'correctly retrieves the changed avatar');

    $db->set_avatar($jid, 'a@b', undef);
    is($db->get_avatar($jid, 'a@b'), undef,
       'can delete avatar');

    $db->set_avatar($jid, 'a@b', $avatar2);
}

ROSTER: {
    my $roster = $db->get_roster($jid);

    cmp_deeply($roster, {}, 'has no initial roster');
    
    $db->set_roster_user_state($jid, 'a@b', $db->want_subscribe);
    $db->set_roster_user_state($jid, 'b@b', $db->subscribed);

    $roster = $db->get_roster($jid);
    cmp_deeply($roster, {'A@B' => $db->want_subscribe,
                         'b@b' => $db->subscribed},
               'can get roster');
    
    $db->set_roster_user_state($jid, 'b@b', $db->unsubscribed);
    $roster = $db->get_roster($jid);
    cmp_deeply($roster, {'A@B' => $db->want_subscribe},
               'setting people to unsubscribed deletes that entry');
}

MISC: {
    is($db->get_misc($jid, 'test'), undef,
       'no value yet');
    $db->set_misc($jid, 'test', 'moo');
    is($db->get_misc($jid, 'test'), 'moo',
       'can store misc values');
}

ALL_OF_VARIOUS_THINGS: {
    cmp_deeply($db->all_jids, [$jid],
               'all_jids works correctly');
    cmp_deeply($db->all_mappings('romeo@montague.lit'),
               {'me%ga' => 'me%ga@aim.transport',
                'A@B' => 'a%b@aim.transport'},
               'can get all name mappings for a user');
    cmp_deeply($db->all_avatars('romeo@montague.lit'),
               {'A@B' => 'ASCII'},
               'can retrieve all avatars');
    cmp_deeply($db->all_misc('romeo@montague.lit'),
               {test => 'moo'}, 'can retrieve all misc');
}

UNREGISTER: {
    ok($db->registered('romeo@montague.lit'),
       'romeo is currently registered.');
    $db->remove('romeo@montague.lit');
    is($db->registered('romeo@montague.lit'), undef,
       'romeo is no longer registered.');
}

DROP_DATABASE: {
    last;
    my $out;
    run(["mysql", 
         $username ? ("--user=$username") : (),
         $password ? ("--password=$password") : (),
         "-e", "DROP DATABASE $db_name"],
        '>&', \$out);
}
