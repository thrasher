package Thrasher::Backend::DBI;
use strict;
use warnings;

use base 'Thrasher::Backend';

use Carp qw(confess);

=pod

=head1 NAME

Thrasher::Backend::DBI - store the information about the transports
in an DBI-accessible database

=head1 DESCRIPTION

Thrasher::Backend::DBI stores the transport's information in a
DBI-accessible database.

Because of the individual characteristics of databases, this can't
be guaranteed to work across all databases, especially when it
comes to creating the necessary databases and tables. However,
the requisite SQL for reading and writing is relatively simple,
so it shouldn't be too bad.

This provides full read and write capability for Thrasher.

The following tables are built and used:

=over 4

=item *

C<jid>: Maps a JID to a unique integer ID. (I don't like natural
keys, especially when they are nearly-arbitrarily-large strings.)

C<legacy>: Maps a legacy username to a unique ID. This is the 
original name on the legacy service, not a translated name.

C<transport_name>: Maps a transport name to a simple numeric
unique ID. This allows these tables to be simultaneously used
by multiple Thrasher instances with otherwise-identical 
connection information.

C<registration>: Stores the registration information for a given
JID. Because this can be more than just username and password
in theory, a numeric field is used for each row to indicate
which thing the row is. Username is defined as 1, Password
is defined as 2, and the rest up to 255 are available for
any protocol than needs it.

C<name_maps>: For each JID, stores the mapping of JID
to legacy service name, in accordance with the gateway
protocol.

C<avatars>: For a given JID/legacy account, stores the avatar
for that user, if any. Note that the avatar's size is only
guaranteed up to 64KB, which really ought to be enough...

C<roster>: For a given JID, stores which legacy users are
currently on the roster for that user, and the state of
that roster entry.

C<misc>: For a given JID, allows the final backup of a 
string-based key/value storage. Hopefully this won't
be used, but just in case you need it, this will be here.

=back

Full definitions of the table can be seen in the source code.

Currently, only MySQL with InnoDB is supported, but other databases
should be easily added by adding an entry into the hash that 
stores all the SQL for the database.

Joins are avoiding by caching active ID information in 
Perl's process space, in the session object.

=cut

use Data::Dumper;

use Thrasher::Log qw(log debug);

# This currently does no in-memory caching, so it nails the database
# pretty hard in some cases. It is easily fixed, but we've left
# it out for debugging.

# It is checked that whatever database you have selected, that
# it has definitions for all the same SQL statements as the database
# below. This prevents us from adding SQL statements to MySQL,
# then having people run the new transport on some other database
# when they don't have SQL fragments for some operation.
my $privileged_database = 'mysql_innodb';

# Note that 3071 is the maximum length of a JID; each of the
# three components can be 1023 in size, + 2 chars for the separators.
# Therefore, don't use a char field that can only store 255 chars,
# despite the temptation.

# Database misc:
# * We turn off transactions if we can, because as it turns out
#   with the exception of assigning IDs (which DBs typically do
#   atomically anyhow), any given row will have only one single-
#   threaded process accessing it at any given time, even if
#   you are running multiple Thrasher Bird instances. Note the care taken
#   at the few places they could overlap, the creation of a legacy
#   id or jid row.
# * I don't like natural keys that take the form of potentially
#   multi-kilobyte texts, and neither does the database that I use.

my $database_support = 
{
    mysql_innodb => 
    {
        # This should be a sequence of table creation calls that 
        # can be run harmlessly even if the tables exist, and
        # which will create the tables in a fresh DB.
        create_tables => <<CREATE_TABLES,
CREATE TABLE IF NOT EXISTS `jid` (
  `id` bigint unsigned not null auto_increment,
  `jid` text not null,
  PRIMARY KEY (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `legacy` (
  `id` bigint unsigned not null auto_increment,
  `legacy` varchar(255) not null,
  KEY `legacy` (`legacy`),
  PRIMARY KEY (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `transport` (
  `id` tinyint unsigned not null auto_increment,
  `transport` varchar(255),
  PRIMARY KEY (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `registration` (
  `id` bigint unsigned not null auto_increment,
  `jid_id` bigint unsigned not null,
  `transport_id` tinyint unsigned not null,
  `key` tinyint not null,
  `value` text not null,
  PRIMARY KEY(`id`),
  KEY `jid_id` (`jid_id`),
  KEY `transport_id` (`transport_id`),
  FOREIGN KEY (`jid_id`) REFERENCES `jid` (`id`),
  FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `name_maps` (
  `id` bigint unsigned not null auto_increment,
  `jid_id` bigint unsigned not null,
  `legacy_id` bigint unsigned not null,
  `transport_id` tinyint unsigned not null,
  `mapped_jid` varchar(255) not null,
  PRIMARY KEY (`id`),
  KEY `jid_id` (`jid_id`),
  KEY `legacy_id` (`legacy_id`),
  KEY `transport_id` (`transport_id`),
  KEY `mapped_jid` (`mapped_jid`),
  FOREIGN KEY (`jid_id`) REFERENCES `jid` (`id`),
  FOREIGN KEY (`legacy_id`) REFERENCES `legacy` (`id`),
  FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `avatars` (
  `id` bigint unsigned not null auto_increment,
  `jid_id` bigint unsigned not null,
  `transport_id` tinyint unsigned not null,
  `legacy_id` bigint unsigned not null,
  `avatar` text not null,
  PRIMARY KEY (`id`),
  KEY `jid_id` (`jid_id`),
  KEY `legacy_id` (`legacy_id`),
  KEY `transport_id` (`transport_id`),
  UNIQUE `avatar_key` (`jid_id`, `transport_id`, `legacy_id`),
  FOREIGN KEY (`jid_id`) REFERENCES `jid` (`id`),
  FOREIGN KEY (`legacy_id`) REFERENCES `legacy` (`id`),
  FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `roster` (
  `id` bigint unsigned not null auto_increment,
  `jid_id` bigint unsigned not null,
  `legacy_id` bigint unsigned not null,
  `transport_id` tinyint unsigned not null,
  `state` tinyint unsigned not null,
  PRIMARY KEY (`id`),
  KEY `jid_id` (`jid_id`),
  KEY `legacy_id` (`legacy_id`),
  KEY `transport_id` (`transport_id`),
  UNIQUE `roster_key` (`jid_id`, `transport_id`, `legacy_id`),
  FOREIGN KEY (`jid_id`) REFERENCES `jid` (`id`),
  FOREIGN KEY (`legacy_id`) REFERENCES `legacy` (`id`),
  FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) TYPE=InnoDB CHARACTER SET utf8

CREATE TABLE IF NOT EXISTS `misc` (
  `id` bigint unsigned not null auto_increment,
  `jid_id` bigint unsigned not null,
  `transport_id` tinyint unsigned not null,
  `key` varchar(255) not null,
  `value` blob not null,
  PRIMARY KEY(`id`),
  KEY `jid_id` (`jid_id`),
  KEY `transport_id` (`transport_id`),
  UNIQUE `misc_key` (`jid_id`, `transport_id`, `key`),
  FOREIGN KEY (`jid_id`) REFERENCES `jid` (`id`),
  FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`)
) TYPE=InnoDB CHARACTER SET utf8
CREATE_TABLES

    # This is used by test scripts; it should return something,
    # anything, if the ? table exists, and error or return 
    # nothing if it doesn't.
    detect_table => 'SHOW CREATE TABLE ?',

    # a fragment of what is returned when the database is not found
    table_not_found_message => 'Unknown database',

    empty_table => 'DELETE * FROM ?',

    retrieve_transport_id => 
        'SELECT id FROM transport WHERE transport = ?',
    create_transport_id =>
        'INSERT INTO transport (transport) VALUES (?)',

    registration =>
        ('SELECT `key`, `value` FROM registration WHERE '
         .'transport_id = ? AND jid_id = ?'),
    add_registration_value => 
        ('REPLACE INTO registration '
         .'(transport_id, jid_id, `key`, `value`) VALUES '
         .'(?     , ?           , ?  , ?    )'),
    clear_registration =>
        ('DELETE FROM registration WHERE '
         .'transport_id = ? AND jid_id = ?'),

    jid_id =>
        'SELECT id FROM jid WHERE jid = ?',
    create_jid_id =>
        'INSERT INTO jid (jid) VALUES (?)',
    legacy_id =>
        'SELECT id FROM legacy WHERE legacy = ?',
    create_legacy_id =>
        'INSERT INTO legacy (legacy) VALUES (?)',

    retrieve_name_map =>
        ('SELECT mapped_jid FROM name_maps WHERE '
         .'transport_id = ? AND jid_id = ? AND '
         .'legacy_id = ?'),
    retrieve_jid_from_legacy =>
        ('SELECT legacy.legacy FROM name_maps '
         .'INNER JOIN legacy ON name_maps.legacy_id = legacy.id '
         .'WHERE transport_id = ? AND jid_id = ? AND '
         .'mapped_jid = ?'),
    store_username_mapping =>
        ('INSERT INTO name_maps (transport_id, jid_id, '
         .'legacy_id, mapped_jid) VALUES (?, ?, ?, ?)'),

    set_avatar =>
        ('REPLACE INTO avatars (transport_id, jid_id, '
         .'legacy_id, avatar) VALUES (?, ?, ?, ?)'),
    get_avatar =>
        ('SELECT avatar FROM avatars WHERE '
         .'transport_id = ? AND jid_id = ? AND '
         .'legacy_id = ?'),
    delete_avatar =>
        ('DELETE FROM avatars WHERE transport_id = ? '
         .'AND jid_id = ? AND legacy_id = ?'),
    all_avatars =>
        ('SELECT legacy.legacy, avatars.avatar FROM '
         .'avatars INNER JOIN legacy ON avatars.legacy_id = '
         .'legacy.id WHERE avatars.transport_id = ? '
         .'AND avatars.jid_id = ?'),

    get_roster =>
        ('SELECT legacy.legacy AS legacy, roster.state AS state FROM '
         .'roster INNER JOIN legacy ON roster.legacy_id = '
         .'legacy.id WHERE roster.transport_id = ? '
         .'AND roster.jid_id = ?'),
    set_roster_state =>
        ('REPLACE INTO roster (transport_id, jid_id, '
         .'legacy_id, state) VALUES (?, ?, ?, ?)'),
    remove_roster_state =>
        ('DELETE FROM roster WHERE transport_id = ? '
         .'AND jid_id = ? AND legacy_id = ?'),
    get_roster_state =>
        ('SELECT state FROM roster WHERE transport_id = ? '
         .'AND jid_id = ? AND legacy_id = ?'),
    clear_roster_for_jid =>
        ('DELETE FROM roster WHERE transport_id = ? '
         .'AND jid_id = ?'),

    all_jids => 'SELECT DISTINCT jid.jid FROM registration INNER JOIN jid ON registration.jid_id = jid.id WHERE registration.transport_id = ?',
    all_mappings =>
        ('SELECT legacy.legacy AS legacy, name_maps.mapped_jid AS '
         .'mapped_jid FROM name_maps '
         .'INNER JOIN legacy ON name_maps.legacy_id = legacy.id '
         .'WHERE transport_id = ? AND jid_id = ?'),
    all_misc =>
        ('SELECT `key`, value FROM misc WHERE transport_id = ? '
         .'AND jid_id = ?'),
    set_misc =>
        ('REPLACE INTO misc (transport_id, jid_id, `key`, value) '
         .'VALUES (?, ?, ?, ?)'),
    get_misc => 
        ('SELECT value FROM misc WHERE transport_id = ? '
         .'AND jid_id = ? AND `key` = ?'),

    unregister_roster =>
        ('DELETE FROM roster WHERE transport_id = ? AND jid_id = ?'),
    unregister_registration =>
        ('DELETE FROM registration WHERE transport_id = ? AND jid_id = ?'),
    unregister_namemaps =>
        ('DELETE FROM name_maps WHERE transport_id = ? AND jid_id = ?'),
    unregister_misc =>
        ('DELETE FROM misc WHERE transport_id = ? AND jid_id = ?'),
    unregister_avatars =>
        ('DELETE FROM avatars WHERE transport_id = ? AND jid_id = ?'),
    unregister_registration =>
        ('DELETE FROM registration WHERE transport_id = ? AND jid_id = ?'),

    # actually a sub to create the database
    create_database => sub {
        my $username = shift;
        my $password = shift;
        my $database_name = shift;
        return system("mysql", 
                      $username ? ("--user=$username") : (),
                      $password ? ("--password=$password") : (),
                      # so... don't name the database something 
                      # stupid, OK? You SQL-inject yourself, it's
                      # your own fault
                      "-e", "CREATE DATABASE `$database_name`");
    },

    # EVIL, does exactly what it says on the tin
    destroy_database => sub {
        my $username = shift;
        my $password = shift;
        my $database_name = shift;
        return system("mysql",
                      $username ? ("--user=$username") : (),
                      $password ? ("--password=$password") : (),
                      "-e", "DROP DATABASE `$database_name`");
    },

    turn_off_transactions => ''
    }
};

our @tables = qw(jid legacy transport registration name_maps
                 avatars roster misc);

use DBI;

sub new {
    my $class = shift;
    my $parameters = shift;

    my %clone = %$parameters;
    
    if (ref($parameters) ne 'HASH') {
        die "Thrasher::Backend::DBI requires parameters to set it up; "
            ."see the documentation for it.";
    }

    bless \%clone, $class;
    my $self = \%clone;

    my @missing_params;
    for my $param qw(dbi_data_source username password db_driver
                     database_name transport_name) {
        if (!defined($self->{$param})) {
            push @missing_params, $param;
        }
    }

    if (@missing_params) {
        die "Thrasher::Backend::DBI is missing the following "
            ."parameter(s): " . join(", ", @missing_params)
            .". Please provide them in $0 and restart.";
    }

    my $retry = 0;
    my $dbh = $self->connect_to_db;
    
    ### DEBUGING
    #$self->clear_backend;

    log("Connected to $self->{dbi_data_source}");

    $self->{dbh} = $dbh;

    $self->verify_tables;

    $self->{id} = $self->get_transport_id;
    
    return $self;
}

sub verify_tables {
    my $self = shift;
    my $dbh = $self->{dbh};

    # Check all the tables
    my $create_table = $self->sql('create_tables');
    my @table_creates = split/\n\n/, $create_table;
    for my $table_create (@table_creates) {
        if (!defined($dbh->do($table_create))) {
            die "SQL failure while creating table with statement: $table_create";
        }
    }
    log("Database tables verified as existing.");
}

sub get_transport_id {
    my $self = shift;

    debug("Getting the transport id");
    my $transport_id_sth = $self->call('retrieve_transport_id',
                                       $self->{transport_name});
    my ($transport_id) = $transport_id_sth->fetchrow_array;
    debug("Got transport id: " . ($transport_id||''));

    if (!defined($transport_id)) {
        debug("Transport id was undefined, making new one.");
        $self->call('create_transport_id', $self->{transport_name});
        return $self->get_transport_id;
    }

    return $transport_id;
}

sub connect_to_db {
    my $self = shift;
    my $is_retry = shift;

    log("Attempting to connect to $self->{dbi_data_source}");
    my $dbh;
    local $@;
    eval {
        $dbh = DBI->connect($self->{dbi_data_source},
                            $self->{username},
                            $self->{password},
                            {RaiseError => 1,
                             PrintError => 0});
    };
    if ($@) {
        log("Failed to connect to database: $@");
        # If it's because the DB didn't exist, try creating it and
        # try again. Otherwise, time to bail.
        my $not_found_message =
            $self->sql('table_not_found_message');
        if (!$is_retry && $@ =~ /$not_found_message/i) {
            my $create_database = $self->sql('create_database');
            log("Failed to locate $self->{dbi_data_source}, attempting to create.");
            $create_database->($self->{username},
                               $self->{password},
                               $self->{database_name});
            return $self->connect_to_db(1);
        }
        
        die "While Thrasher::Backend::DBI was trying to connect to the "
            ."database, DBI gave the following error: $@";
    }
    return $dbh;
}

sub sql {
    my $self = shift;
    my $sql_frag = shift;

    return $database_support->{$self->{db_driver}}->{$sql_frag};
}

sub call {
    my $self = shift;
    my $sql_fragment_name = shift;
    my @args = @_;
    
    my $sth = $self->{sths}->{$sql_fragment_name};
    if (!$sth) {
        my $sql = $self->sql($sql_fragment_name);
        if (!$sql) {
            die "In Thrasher::Backend::DBI, no sql fragment named "
                ."'$sql_fragment_name' for $self->{db_driver}.";
        }
        $sth = $self->{sths}->{$sql_fragment_name} = 
            $self->{dbh}->prepare($sql);
    }

    #debug(sub {
    #    my $sql = $self->sql($sql_fragment_name);
    #    return "SQL: $sql, args: " . join(' ', @args);
    #});
    
    {
        local $@;
        eval {
            $sth->execute(@args);
        };

        if ($@) {
            if ($@ =~ /gone away/) {
                log "MySQL server has gone away, attempting to reconnect";
                delete $self->{sths};
                $self->{dbh}->disconnect;
                my $new_dbh;
                $@ = '';
                eval {
                    log("Connecting...");
                    $new_dbh = $self->connect_to_db;
                    log("Got past connection attempt");
                };
                if ($@ || !$new_dbh) {
                    log "Reconnection failed, terminating component: $@";
                    $self->{component}->terminate(no_db => 1);
                    return;
                }
                log("Reconnection seems to be a success...");
                $self->{dbh} = $new_dbh;
                # Success!
                return $self->call($sql_fragment_name, @args);
            }
        }
    }
    return $sth;
}

sub jid_id {
    my $self = shift;
    my $jid = shift;
    my $create = shift || 0;

    #debug("JID id for $jid (force-create: $create)");

    my $sth = $self->call('jid_id', $jid);
    my ($jid_id) = $sth->fetchrow_array();
    $sth->finish;

    if (!defined($jid_id) && $create) {
        #debug("JID not found, attempting to create");
        $self->call('create_jid_id', $jid);
        #debug("Recursively calling jid_id");
        return $self->jid_id($jid);
    }

    #debug("returning " . ($jid_id||"no jid") . " for $jid");
    return $jid_id;
}

sub legacy_id {
    my $self = shift;
    my $legacy = shift;

    my $sth = $self->call('legacy_id', $legacy);
    my ($legacy_id) = $sth->fetchrow_array;
    $sth->finish;

    if (!defined($legacy_id)) {
        $self->call('create_legacy_id', $legacy);
        return $self->legacy_id($legacy);
    }

    return $legacy_id;
}

my $db_to_field = {};
my $field_to_db = {};

sub register_protocol {
    my $self = shift;
    my $protocol = shift;

    my @registration_items = $protocol->registration_items;
    my $i = 1;

    for my $item (@registration_items) {
        $db_to_field->{$i} = $item;
        $field_to_db->{$item} = $i;
        $i++;
    }

    $self->{registration_defaults} = $protocol->registration_defaults;
}

sub registered {
    my $self = shift;
    my $jid = shift;

    my $jid_id = $self->jid_id($jid);
    if (!defined($jid_id)) {
        return undef;
    }

    my $sth = $self->call('registration', $self->{id},
                          $jid_id);
    my $registration;
    my @values;

    while (@values = $sth->fetchrow_array) {
        $registration->{
            $db_to_field->{$values[0]} || $values[0]
        } = $values[1];
    }

    $sth->finish;

    return $registration;
}

sub register {
    my $self = shift;
    my $jid = shift;
    my $registration = shift;

    #debug("Attempting to register: $jid, " . Dumper($registration));

    my $jid_id = $self->jid_id($jid, 1);
    $self->call('clear_registration', $self->{id},
                $jid_id);

    while (my ($key, $value) = each %$registration) {
        my $db_key = $field_to_db->{$key};
        if (!defined($db_key)) {
            log("While trying to register $jid, tried "
                ."to convert $key to a DB value but couldn't.");
            next;
        }

        $self->call('add_registration_value', $self->{id},
                    $jid_id, $db_key, $value);
    }
    #debug("Successfully registered $jid: " . Dumper($registration));
    return 1;
}

sub retrieve_legacy_name_to_jid {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;

    my $jid_id = $self->jid_id($user_jid);
    my $legacy_id = $self->legacy_id($legacy_username);

    my ($legacy_jid) = $self->call('retrieve_name_map', 
                                   $self->{id}, $jid_id,
                                   $legacy_id)->
                              fetchrow_array;
    return $legacy_jid;
}

sub jid_to_legacy_name {
    my $self = shift;
    my $user_jid = shift;
    my $target_jid = shift;

    my $jid_id = $self->jid_id($user_jid, 1);

    my ($legacy_name) = $self->call('retrieve_jid_from_legacy',
                                    $self->{id}, $jid_id,
                                    $target_jid)->
                               fetchrow_array;

    if (!$legacy_name) {
        $legacy_name = $self->fake_up_a_legacy_name($user_jid, $target_jid);
    }
    return $legacy_name;
}

sub jid_has_legacy_name {
    my $self = shift;
    my $user_jid = shift;
    my $target_jid = shift;

    my $jid_id = $self->jid_id($user_jid);

    my ($legacy_name) = $self->call('retrieve_jid_from_legacy',
                                    $self->{id}, $jid_id,
                                    $target_jid)->
                               fetchrow_array;

    return !!$legacy_name;
}

sub store_username_mapping {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    my $mapped_jid = shift;

    debug("store_username_mapping($user_jid, $legacy_username, $mapped_jid)\n");

    my $jid_id = $self->jid_id($user_jid, 1);
    my $legacy_id = $self->legacy_id($legacy_username);

    $self->call('store_username_mapping',
                $self->{id}, $jid_id, $legacy_id,
                $mapped_jid);
}

sub set_avatar {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    my $avatar_png_base_64 = shift;

    my $jid_id = $self->jid_id($user_jid);
    my $legacy_id = $self->legacy_id($legacy_username);

    if ($avatar_png_base_64) {
        $self->call('set_avatar', $self->{id},
                    $jid_id, $legacy_id, $avatar_png_base_64);
    } else {
        $self->call('delete_avatar', $self->{id},
                    $jid_id, $legacy_id);
    }
}

sub get_avatar {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;

    my $jid_id = $self->jid_id($user_jid);
    my $legacy_id = $self->legacy_id($legacy_username);

    my ($avatar) = $self->call('get_avatar', $self->{id},
                               $jid_id, $legacy_id)->
                          fetchrow_array;
    return $avatar;
}

sub get_roster {
    my $self = shift;
    my $user_jid = shift;

    my $jid_id = $self->jid_id($user_jid);

    my $roster = {};
    my $sth = $self->call('get_roster', $self->{id}, $jid_id);

    while (my $roster_entry = $sth->fetchrow_hashref) {
        $roster->{$roster_entry->{legacy}} = $roster_entry->{state};
    }

    return $roster;
}

sub set_roster {
    my $self = shift;
    my $user_jid = shift;
    my $new_roster = shift;

    my $jid_id = $self->jid_id($user_jid);
    $self->call('clear_roster_for_jid', $self->{id}, $jid_id);

    while (my ($legacy_name, $sub_value) = each %$new_roster) {
        $self->set_roster_user_state($user_jid, $legacy_name, $sub_value);
    }
}

sub set_roster_user_state {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    my $state = shift;

    debug("set_roster_user_state($user_jid, $legacy_username, $state)");

    my $jid_id = $self->jid_id($user_jid);
    my $legacy_id = $self->legacy_id($legacy_username);
    
    if ($state == $self->unsubscribed) {
        $self->call('remove_roster_state',
                    $self->{id}, $jid_id, $legacy_id);
    } else {
        $self->call('set_roster_state', $self->{id},
                    $jid_id, $legacy_id, $state);
    }
}

sub get_roster_user_state {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;

    my $jid_id = $self->jid_id($user_jid);
    my $legacy_id = $self->legacy_id($legacy_username);

    my $sth = $self->call('get_roster_state', $self->{id},
                          $jid_id, $legacy_id);
    my ($state) = $sth->fetchrow_array();
    $sth->finish();
    $state ||= $self->unsubscribed;
    return $state;
}

sub all_jids {
    my $self = shift;

    my $sql = $self->sql("all_jids");
    my $jids = $self->{dbh}->selectcol_arrayref($sql, undef, $self->{id});
    return $jids;
}

sub all_mappings {
    my $self = shift;
    my $jid = shift;

    my $jid_id = $self->jid_id($jid);
    my $name_mappings = {};

    my $sth = $self->call('all_mappings', $self->{id}, $jid_id);
    while (my ($key, $value) = $sth->fetchrow_array) {
        $name_mappings->{$key} = $value;
    }

    return $name_mappings;
}

sub all_misc {
    my $self = shift;
    my $jid = shift;

    my $jid_id = $self->jid_id($jid);
    my $misc = {};
    
    my $sth = $self->call('all_misc', $self->{id}, $jid_id);
    while (my ($key, $value) = $sth->fetchrow_array) {
        $misc->{$key} = $value;
    }

    return $misc;
}

sub all_avatars {
    my $self = shift;
    my $jid = shift;

    my $jid_id = $self->jid_id($jid);
    my $avatars = {};

    my $sth = $self->call('all_avatars', $self->{id}, $jid_id);
    while (my ($legacy, $avatar) = $sth->fetchrow_array) {
        $avatars->{$legacy} = $avatar;
    }
    $sth->finish();

    return $avatars;
}

sub set_misc {
    my $self = shift;
    my $jid = shift;
    my $key = shift;
    my $value = shift;

    my $jid_id = $self->jid_id($jid);
    $self->call('set_misc', $self->{id}, $jid_id, $key, $value);
}

sub get_misc {
    my $self = shift;
    my $jid = shift;
    my $key = shift;
    
    my $jid_id = $self->jid_id($jid);
    my $sth = $self->call('get_misc', $self->{id}, $jid_id, $key);
    my ($value) = $sth->fetchrow_array;
    $sth->finish();
    return $value;
}

sub clear_backend {
    my $self = shift;
    my $destroy_database = $self->sql('destroy_database');
    $destroy_database->($self->{username},
                        $self->{password},
                        $self->{database_name});
    log("Destroyed the $self->{database_name} database.");
    $self->{dbh} = $self->connect_to_db;
    $self->verify_tables;
}

sub remove {
    my $self = shift;
    my $jid = shift;
    my $transport_id = $self->{id};
    my $jid_id = $self->jid_id($jid);

    log("Unregistering $jid all from $self->{transport_name}");
    for my $unregistration_type qw(roster registration namemaps misc
                                   avatars registration) { 
        $self->call("unregister_$unregistration_type",
                    $transport_id, $jid_id);
    }
}

1;
