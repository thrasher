package Thrasher::EventLoop::Glib;
use strict;
use warnings;

=head1 NAME

Thrasher::EventLoop::Glib - implement the Thrasher event loop with
Glib's event loop

=head1 DESCRIPTION

As the NAME says. Glib's event loop is used to implement the necessary
event loop primitives.

Note that even if you don't need libpurple, Glib is a viable event
loop for you to use, if you have the necessary glib and modules 
installed.

=cut

use Glib qw(FALSE G_PRIORITY_DEFAULT G_PRIORITY_HIGH);

use base 'Thrasher::EventLoop';
use Thrasher::Log qw(:all);

sub new {
    my $class = shift;

    my $self = $class->SUPER::new(@_);

    $self->{mainloop_ref} = Glib::MainLoop->new(undef, FALSE);

    return $self;
}

sub go {
    my $self = shift;

    $self->{mainloop_ref}->run();

    delete $self->{mainloop_ref};
}

sub schedule {
    my $self = shift;
    my $closure = shift;
    my $timeout = shift;

    my ($package, $filename, $line) = caller;
    my $name = "Task scheduled at $filename\:$line";
    my $new_closure = Thrasher::error_wrap($name, $closure);

    my $token = Glib::Timeout->add($timeout, $new_closure);
    debug("added scheduled event: $package\:$line: $token");
    return $token;
}

sub cancel_scheduled_event {
    my $self = shift;
    my $opaque_token = shift;
    Glib::Source->remove($opaque_token);
}

sub add_fd_watch {
    my $self = shift;
    my $fd = shift;
    my $directions = shift;
    my $closure = shift;

    my $condition = 0;
    if ($directions & $Thrasher::EventLoop::IN) {
        $condition = 'G_IO_IN';
    } elsif ($directions & $Thrasher::EventLoop::OUT) {
        $condition = 'G_IO_OUT';
    }

    debug("Adding watch: $fd, $condition, $closure\n");

    my ($package, $filename, $line) = caller;
    my $name = "FD watch created at $filename\:$line";
    my $new_closure = Thrasher::error_wrap($name, $closure);

    my $token = Glib::IO->add_watch($fd, $condition, $new_closure);
    debug("added fd watch: ($package\:$line) $token\n");
    return $token;
}

sub remove_fd_watch {
    my $self = shift;
    my $opaque_token = shift;

    debug("Removing fd watch: $opaque_token\n");

    Glib::Source->remove($opaque_token);
}

sub execute_on_idle {
    my $self = shift;
    my $closure = shift;

    my ($package, $filename, $line) = caller;
    my $name = "Idle execution created at $filename\:$line";
    my $new_closure = Thrasher::error_wrap($name, $closure);

    Glib::Idle->add($new_closure);
}

sub quit {
    my $self = shift;
    $self->{mainloop_ref}->quit;
}

1;
