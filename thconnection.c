/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include <signal.h>
#include <stdio.h>

#include "connection.h"
#include "thrasher.h"
#include "thconnection.h"
#include "thperl.h"

void thrasher_connection_error (PurpleConnection *gc,
                                PurpleConnectionError err,
                                const gchar *desc);


void thrasher_connection_error (PurpleConnection *gc,
                                PurpleConnectionError err,
                                const gchar *desc)
{
    PurpleAccount *account;

    /* Bail if we can't get what the error is for. */
    g_return_if_fail(gc != NULL);

    /* Set account and verify */
    account = purple_connection_get_account(gc);
    g_return_if_fail(account != NULL);

    thrasher_wrapper_connection_error(thrasher_account_get_jid(account),
                                      err,
                                      g_strdup(desc));
}

gpointer thrasher_connection_get_handle ()
{
    static int handle;
    return &handle;
}

void
thrasher_connection_update_progress(PurpleConnection *gc,
                                    const char *text,
                                    size_t step,
                                    size_t step_count) {
    g_return_if_fail(gc != NULL);
    g_return_if_fail(text != NULL);
    PurpleAccount *account = purple_connection_get_account(gc);
    purple_debug_info("thrasher connection",
                      "%s: %s (step %d of %d)\n",
                      thrasher_account_get_jid(account),
                      text,
                      step + 1,
                      step_count);
}

void thrasher_connection_init ()
{
    purple_signal_connect_priority
      (purple_connections_get_handle(),
       "connection-error",
       thrasher_connection_get_handle(),
       PURPLE_CALLBACK(thrasher_connection_error),
       NULL,
       10); // We want this to be called after the one in account.c,
          // which has a priority of 0 (PURPLE_SIGNAL_PRIORITY_DEFAULT)
    
}
