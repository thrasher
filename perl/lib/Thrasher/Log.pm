package Thrasher::Log;
use strict;
use warnings;

use Encode qw(encode);
use POSIX qw(strftime);

=pod

=head1 NAME

Thrasher::Log - contains logging functions that control log output

=head1 SYNOPSIS

 use Thrasher::Log qw(log);
 log("Error in sample code for POD documentation for Thrasher::Log.");

=head1 DESCRIPTION

Obviously, this is the logging function.

This was pulled out into a separate function due to load order issues;
it turns out this really needs to be in a separate module.

=cut

use base 'Exporter';

# Ugly, but we need to consolidate the two debugging modules...
our @EXPORT_OK = qw(log logger debug dies);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

our $DEBUG = 0;
our $SILENT = 0; # Really only useful for testing this module
our $logger_sub;

binmode STDERR, ':utf8';

sub log ($;$) {
    my $msg = shift;
    my $depth = shift;

    my ($package, $filename, $line);
    if (!$depth) {
        ($package, $filename, $line) = caller;
    } else {
        ($package, $filename, $line) = caller $depth;
    }

    my $time = strftime("%F %H:%M:%S", localtime());
    my $outp = "($time) $package\:$line - $msg\n";

    if (defined($logger_sub)) {
        $logger_sub->($outp);
        return;
    }

    #my $out_octets = encode("utf-8", $outp);

    print STDERR $outp if not $SILENT;
    return ($filename, $line, $msg);
}

sub debug ($;$) {
    my $s = shift;
    my $level = shift || 1;

    if ($level > $DEBUG) { 
        return 0;
    }

    if ($DEBUG) {
        if (ref($s) eq 'CODE') {
            local $@;
            my $result;
            eval {
                $result = $s->();
            };
            if ($@) {
                return Thrasher::Log::log("While trying to print debug message "
                            ."based on a code ref, got an error: $@", 1);
            } else {
                return Thrasher::Log::log($result, 1);
            }
        } else {
            return Thrasher::Log::log($s, 1);
        }
    }

    return 0;
}

# logger is a wrapper for calls to old Debug module.  We should
# antiquate "log" in favor of something else.  Perhaps this?
#
sub logger {
    my $s = shift;

    return Thrasher::Log::log($s, 1);
}


=item *

C<dies>($subroutine, $check, $name) - Evaluates $subroutine and fails if
$@ is not populated.

If $check is defined, it is tested for truth.  This may also be a reference
to a regex.  In which case the regex is called and tested for truth.

$name simply defines what is initially displayed on pass or fail.

=cut

sub dies (&;$$) {
    eval 'use Test::More;';
    my $code = shift;
    my $check = '';
    if (@_ == 2) {
        $check = shift;
    }
    my $name = shift;

    {
        local $@ = '';
        eval { $code->(); };

        if (!$@) {
            fail($name);
            return;
        }

        my $die_message = $@ . '';
        if ($check) {
            if (ref($check) eq 'Regexp') {
                if ($die_message !~ /$check/) {
                    fail($name . " (regex $check not in '$die_message')");
                }
            } else {
                if (index($die_message, $check) == -1) {
                    fail($name . " (string $check not in $die_message)");
                }
            }
        }
    }

    pass($name);
}
1;
