### "config" options
PERL = /usr/bin/perl
GLIB_PREFIX = /usr
PURPLE_PREFIX = /usr
### end of build config

PERL_CFLAGS := $(shell PERL5LIB= $(PERL) -MExtUtils::Embed -e ccopts)
PERL_CFLAGS := $(subst -Wdeclaration-after-statement,,$(PERL_CFLAGS))

ifeq ($(PERL_CFLAGS),)
$(error "Cannot find PERL_CFLAGS. Do you have ExtUtils::Embed?")
endif

PERL_LIBS := \
	$(shell PERL5LIB= $(PERL) -MExtUtils::Embed -e ldopts) \
	$(shell PERL5LIB= $(PERL) -MConfig -e'print "-L$$Config::Config{archlibexp}/CORE";')

GLIB_CFLAGS = $(shell pkg-config --cflags glib-2.0) \
              -I$(PURPLE_PREFIX)/include/libpurple \
              -DPURPLE_DISABLE_DEPRECATED \
              $(PERL_CFLAGS)

GLIB_LIBS  :=  -L$(GLIB_PREFIX)/lib \
               -lpurple \
               -lgobject-2.0 \
               -lgmodule-2.0 \
               -lgthread-2.0 \
               -lglib-2.0 \
               -lrt

CFLAGS       = -Wall \
               -Waggregate-return \
               -Wcast-align \
               -Werror-implicit-function-declaration \
               -Wmissing-declarations \
               -Wmissing-prototypes \
               -Wnested-externs \
               -Wpointer-arith \
               -Wundef \
               -O2

DEBUG_CFLAGS = -DTH_DEBUG \
               -DERROR_DEBUG

DEBUG_GLIB   = -g \
               -ggdb

DEPDIR = .deps
OBJS = thperl.o \
       thblist.o \
       thrasher.o \
       thpresence.o \
       thconnection.o \
       threquest.o \
       thconversations.o \
       thft.o

COMPILE = gcc -DHAVE_CONFIG_H -I. $(GLIB_CFLAGS) $(DEBUG_CFLAGS) $(CFLAGS)
WRAPPER    = THPPW
WRAPPER_PATH = perl/lib

ifeq ($(shell uname -m), x86_64)
COMPILE := $(COMPILE) -fPIC
endif

.PHONY: clean again

all: $(DEPDIR) $(OBJS) thperl_wrap.c wrapper

wrapper: $(WRAPPER_PATH)/$(WRAPPER).pm thperl_wrap.o $(WRAPPER_PATH)/$(WRAPPER).so

thperl_wrap.o: thperl_wrap.c
	$(CC) $(DEBUG_CFLAGS) $(DEBUG_GLIB) -fPIC -c thperl.c thperl_wrap.c $(GLIB_CFLAGS)

$(WRAPPER_PATH)/$(WRAPPER).so: $(OBJS) thperl_wrap.o
	$(CC) -shared $(GLIB_LIBS) $(PERL_LIBS) $(OBJS) thperl_wrap.o -o $@

thperl_wrap.c $(WRAPPER_PATH)/$(WRAPPER).pm: thperl.h
	swig -I$(PURPLE_PREFIX)/include $(DEBUG_CFLAGS) -perl -outdir $(WRAPPER_PATH) thperl.h

$(DEPDIR):
	if test ! -d $(DEPDIR); then mkdir $(DEPDIR); fi

.c.o:
	$(COMPILE) -MT $@ -MD -MP -MF $(DEPDIR)/$*.Tpo -c -o $@ $<
	mv -f $(DEPDIR)/$*.Tpo $(DEPDIR)/$*.Po

again: clean all

clean:
	$(RM) -f $(OBJS)
	$(RM) -rf $(DEPDIR)
	$(RM) -f thperl_wrap.*
	$(RM) -f $(WRAPPER_PATH)/$(WRAPPER).* $(WRAPPER).*
