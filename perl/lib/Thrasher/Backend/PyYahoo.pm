package Thrasher::Backend::PyYahoo;
use strict;
use warnings;

use base 'Thrasher::Backend';
use IPC::Run qw(run);

=pod

=head1 NAME

Thrasher::Backend::PyYahoo - Read-only backend for migration from
PyYIMt.

=head1 DESCRIPTION

PyYIMt ( http://xmpppy.sourceforge.net/yahoo/ ) stores its backend 
information in a GDBM file. This extract the useful information from
that GDBM file so that the migration script can use it as a 
source to populate Thrasher's database with the proper values.

This translator will only get your username and password, since
that appears to be all it stores.

This just barely implements enough to allow migration out of this
format.

=cut

sub new {
    my $class = shift;
    my $parameters = shift;

    my $self = bless $parameters, $class;

    my @missing;
    for my $required qw(location) {
        if (!defined($self->{$required})) {
            push @missing, $required;
        }
    }
    if (@missing) {
        die "$class required the following parameters that weren't "
            ."provided: " . join(', ', @missing);
    }

    if (!-e $self->{location}) {
        die "For PyYahoo, could not find the shelve file $self->{location}.";
    }

    my $python_program = <<PYTHON;
import shelve

# technically, this will still fail if you have three quotes in
# your shelve file name, in which case all I can say is that
# what the heck were you thinking?
s = shelve.open("""/mail/jabber/spool/yahoouser.dbm""")

def encode(s):
    return "".join(['\%'+str(ord(x)) for x in s])

user_data = [(encode(x), encode(s[x]['username']), 
              encode(s[x]['password'])) for x in s.keys()]

for jid, username, password in user_data:
    print jid, username, password
#print "end"

s.close()

PYTHON

    my $out;
    run ['python'], \$python_program, \$out;
    print $out;

    my @lines = split (/\r?\n/m, $out);
    use Data::Dumper;
    print Dumper(\@lines);
    
    for my $line (@lines) {
        my ($jid, $username, $password) = map { decode($_) } split(/ /, $line);
        $self->{registrations}->{$jid} = {username => $username,
                                          password => $password};
    }

    return $self;
}

# The inverse of that python encode function up there
sub decode {
    my $s = shift;
    $s =~ s/%(\d+)/chr($1)/ge;
    return $s;
}

sub all_jids {
    my $self = shift;
    my @jids = keys %{$self->{registrations}};
    return \@jids;
}

sub registered {
    my $self = shift;
    my $jid = shift;

    return $self->{registrations}->{$jid};
}

sub all_mappings { return {} }
sub get_roster { return {} }
sub all_avatars { return {} }
1;
