package Thrasher::HTMLNormalize;
use strict;
use warnings;

use Thrasher::HTMLNormalize::Entities qw(html_entity);

use base 'Exporter';

our @EXPORT_OK = qw(&escape $CONSERVATIVE_PERMISSIONS &normalize
                    tagremains_to_atts_hash
                    &escape_quote $CONSERVATIVE_PERMISSIONS_NO_LINKS);

our %EXPORT_TAGS = (all => \@EXPORT_OK);

my $VALIDATE_AS_XML = 0;

###
### PLEASE NOTE: This module has unit tests. Changing this module
###              without verifying them is a bad idea.
###

# Also note that this file isn't as dirty as it looks; that's
# just the natural result of sticking an entire compiler in one 
# file. 

###
### CONSTANT AND DATA DECLARATIONS
###
### Recall "my" is actually file scoped, not package scoped.
### use constant, however, *is* package scoped, so it's darned inconvenient.

# A set-like hash indicating which entities are allowed
# to exist as text-named entities, and not converted into numeric
# entities.
my %LEGAL_ENTITIES = map { $_ => 1 } qw(amp lt gt quot apos);

my %ESCAPE_ENTITIES = ('&' => '&amp;',
                       '>' => '&gt;',
                       '<' => '&lt;' );
my %QUOTE_ENTITIES = (%ESCAPE_ENTITIES,
                      "'" => '&apos;',
                      '"' => '&quot;');

# Tags that sort of close themselves
my %SELF_CLOSERS = map { $_ => 1 } qw(li p);

# Token types
my $TEXT          = 1;
my $TAG           = 2;
my $TAGCLOSE      = 3;
my $SELFCLOSEDTAG = 4;
my $ENTITY        = 5;
my $NEWLINE       = 6;
my $DOUBLENEWLINE = 7;
my $CDATA         = 8;

# Tag types. These are used for formatting the resulting text.
# A block tag, as in CSS
my $BLOCK         = 10;
# An inline tag, as in CSS
my $INLINE        = 11;
# A 'block' tag that should be converted to a self-closing
# tag if an empty instance is found (i.e., <br></br> => <br />)
my $BLOCK_SELF_CLOSE = 12;

my %TAG_TYPES = 
    (
     p => $BLOCK,
     div => $BLOCK,
     span => $INLINE,
     br => $BLOCK_SELF_CLOSE,
     a => $INLINE,
     li => $BLOCK,
     dl => $BLOCK,
     dt => $BLOCK,
     dd => $BLOCK,
     ul => $BLOCK,
     font => $INLINE,
     abbr => $INLINE,
     acronym => $INLINE,
     b => $INLINE,
     bgsound => $INLINE,
     big => $INLINE,
     caption => $INLINE,
     img => $INLINE,
     blockquote => $BLOCK,
     h1 => $BLOCK,
     h2 => $BLOCK,
     h3 => $BLOCK,
     h4 => $BLOCK,
     h5 => $BLOCK,
     h6 => $BLOCK,
     em => $INLINE,
     strong => $INLINE,
     dfn => $INLINE,
     code => $INLINE,
     samp => $INLINE,
     kbd => $INLINE,
     var => $INLINE,
     cite => $INLINE,
     hr => $BLOCK_SELF_CLOSE,
     i => $INLINE,
     ol => $BLOCK,
     pre => $BLOCK,
     small => $INLINE,
     s => $INLINE,
     strike => $INLINE,
     'sub' => $INLINE,
     sup => $INLINE,
     tt => $INLINE,
     table => $BLOCK,
     tr => $BLOCK,
     td => $BLOCK,
     u => $INLINE,
     html => $BLOCK,
     body => $BLOCK
     );

# A reasonable pre-defined behavior for URLs. If it has a protocol
# in the front, it *must* be http or https. Anything else gets
# destroyed. If it doesn't have a protocol in the front, we'll
# at least take a shot and prepend http://.
sub fixURL {
    my $url = shift;
    if ($url !~ /^https?:\/\//) {
        # has non-http protocol. Evil. Die now. 
        # or maybe an incorrectly-specified port. Die now anyhow.
        if ($url =~ /^[^:]+:/) {
            return undef;
        }

        return 'http://' . $url;
    }
    return $url;
}

# A reasonable pre-defined conservative set. Allows a reasonable
# range of simple formatting tags, but the only attributes allowed
# are URLS (and those only with http or https protocols), and
# titles for abbr and acronyms. Certainly no code execution allowed.
our $CONSERVATIVE_PERMISSIONS = {
    p => {},
    br => {},
    a => {href => \&fixURL},
    ul => {},
    li => {},
    abbr => {title => 1},
    acronym => {title => 1},
    b => {},
    big => {},
    blockquote => {},
    em => {},
    strong => {},
    dfn => {},
    code => {},
    samp => {},
    kbd => {},
    var => {},
    cite => {},
    hr => {},
    i => {},
    ol => {},
    pre => {},
    small => {},
    strike => {},
    sub => {},
    sup => {},
    tt => {},
    u => {}
};

my %CONSERVATIVE_PERMISSIONS_NO_LINKS = %$CONSERVATIVE_PERMISSIONS;
our $CONSERVATIVE_PERMISSIONS_NO_LINKS = \%CONSERVATIVE_PERMISSIONS_NO_LINKS;
delete $CONSERVATIVE_PERMISSIONS_NO_LINKS->{a};

###
### UTILITY FUNCTIONS
###

# Return whether $s is the name of a valid entity
sub is_valid_entity {
    my $s = shift;
    return !!($s =~ /\#\d{1,5}/) || $LEGAL_ENTITIES{$s};
}

# Given an entity name, either return that entity or an empty
# string to elide it.
sub make_entity {
    my $s = shift;
    if (is_valid_entity($s)) {
        return "&$s;";
    } else {
        return '';
    }
}

sub escape {
    my $s = shift;
    $s =~ s/([&<>])/$ESCAPE_ENTITIES{$1}/ge;
    return $s;
}
sub escape_quote {
    my $s = shift;
    $s =~ s/([&<>'"])/$QUOTE_ENTITIES{$1}/ge; #'])/;
    return $s;
}

# Nicer interface to create tokens; just specify the last bit.
sub token {
    my $token_type = shift;
    $token_type = 'Thrasher::HTMLNormalize::Token::' . $token_type;
    return $token_type->new(@_);
}

# Tokenize a tag or entity. We know it's one of them because of
# how it was parsed. (Pulled out for simple testing.)
sub tokenize_tag_or_entity {
    my $tag_or_entity = shift;
    my $allow_taglike = shift;
    my $tag_translation = shift;

    # \# is to convince emacs syntax highlighting that # is not a comment
    if ($tag_or_entity !~ /((<(\/)?(\w+)([^>]*)>)|(&(\#?\w+);))/) {
        die "tokenize_tag_or_entity must be passed a tag or entity: "
            .$tag_or_entity;
    }

    # Convert that mass of matches into something comprehensible
    my $groups = {tag => $2,    # the whole of the tag
                  slash => $3,  # if it has a slash to close the tag
                  tagname => $4,# the tag name
                  tagremains => $5, # the rest of the tag (atts)
                  entity => $6, # the whole of the entity
                  entityname => $7 };

    $groups->{tagname} = lc $groups->{tagname};

    if (defined(my $tag_translator = 
                    $tag_translation->{lc $groups->{tagname}})) {
        # use Data::Dumper;
        #print "Initial: " . Dumper($groups);
        if (ref($tag_translator) eq '') {
            $groups->{tagname} = $tag_translator;
        } elsif (ref($tag_translator) eq 'CODE') {
            $tag_translator->($groups);
        }
        #print "Post-translation: " . Dumper($groups);
    }

    if ($groups->{tag} && $allow_taglike && 
        !$TAG_TYPES{lc $groups->{tagname}}) {
        return token('Text', $tag_or_entity);
    } elsif ($groups->{tag}) {
        # handle tokenizing a tag
        if ($groups->{slash}) {
            return token('TagClose', $groups->{tagname});
        }

        my $token_type = 'Tag';

        my $atts = $groups->{tagremains};
        if ($atts && substr($atts, -1) eq '/') {
            $token_type = 'SelfClosedTag';
            $atts = substr($atts, 0, length($atts) - 1); # pop off /
        }

        my $atts_hash = tagremains_to_atts_hash($atts);
        return token($token_type, $groups->{tagname}, $atts_hash);
    } elsif ($groups->{entity}) {
        return token('Entity', $groups->{entityname});
    }

    # shouldn't be able to get here.
    die "tokenize_tag_or_entity didn't get anything it expected.";
}

sub tagremains_to_atts_hash {
    my ($atts) = @_;
    my $atts_hash = {};

    # Pull out the attributes
    while ($atts =~ /([a-zA-Z]\w+)(?:=(?:(?:\"([^\">]*)\")|(?:'([^'>]*)\')|(?:([^ \n\t>]*))))?/g) {
        my $name = $1;
        # one value delimited by ", the other by ',
        # the final by whitespace, the final case
        # covers those few atts in HTML that can
        # show up with no value at all
        my $value = $2 || $3 || $4 || 1;

        $atts_hash->{lc $name} = $value;
    }

    return $atts_hash;
}

# Given something known to be character-based text with no
# (X)HTML in it, tokenize it.
sub tokenize_text {
    my $s = shift;
    my @tokens;

    my @texthunks = split(/(\n+)/, $s);
    my $i = 0;

    for my $fragment (@texthunks) {
        if ($i %2) {
            my $newlines = length($fragment); # all newlines
            if ($newlines == 1) {
                push @tokens, token('Newline');
            } else {
                push @tokens, token('DoubleNewline');
            }
        } else { 
            if ($fragment) { # skip empty matches
                push @tokens, token('Text', $fragment);
            }
        }

        $i++;
    }

    return \@tokens;
}

# Given a tag and our validation criteria, verify the tag is
# allowed. If it is not allowed, this returns 0. It may also
# destructively manipulate the tag's attributes to make them valid.
sub validate_tag {
    my $tag = shift;
    my $allowed_elements = shift;

    # It's all allowed.
    if (!defined($allowed_elements)) {
        return $tag;
    }

    my $legal_attributes = $allowed_elements->{$tag->{name}};

    # Is this allowed at all?
    if (!$legal_attributes) {
        return 0;
    }

    # If this is a close tag, pass it through.
    if ($tag->{type} == $TAGCLOSE) {
        return $tag;
    }

    # Now, actually validate the attributes;
    while (my ($name, $value) = each %{$tag->{atts}}) {
        my $attribute_validation = $legal_attributes->{$name};
        
        # Is the attribute even allowed?
        if (!$attribute_validation) {
            # Nope, toast it.
            delete($tag->{atts}->{$name});
            next;
        }

        if ($attribute_validation eq '1') { # note string compare
            # everything is permitted here, pass through
            next;
        }

        my $new_value;
        if (ref($attribute_validation) eq 'CODE') {
            $new_value = $attribute_validation->($value);
            if (!defined($new_value)) {
                # This attribute is so evil that the entire tag
                # has to go away!
                return 0;
            }
            
            if (!$new_value) { # either false or empty string
                delete($tag->{atts}->{$name});
                next;
            }
        } elsif (ref($attribute_validation) eq 'Regexp') {
            my $match = $value =~ /$attribute_validation/;
            if (!$match) {
                return 0;
            }
            # Otherwise pass through; for better control, use a sub
            $new_value = $value;
        } elsif ($attribute_validation) { # any true value
            $new_value = $value;
        } else { 
            # any false value; redundent since unspecified = false
            delete($tag->{atts}->{$name});
            next;
        }

        $tag->{atts}->{$name} = $new_value;
    }

    return $tag;
}

# The money function at last. 
sub normalize {
    my $html = shift;
    my $allowed_elements = shift; # default - allow everything
    my $allow_cdata = shift; # default - no
    my $allow_taglike = shift;
    my $tag_translation = shift || {};

    my $text_only = shift; # don't automatically slap in <p>s

    # Whitespace trimmer
    {
        # Apparently, applying this to something that consists
        # entirely of whitespace gets a warning about using
        # an uninitialized value. Probably just the implementation
        # poking out and doesn't seem to be worth worrying about.
        no warnings 'uninitialized';
        $html =~ s/(^\s+)|(\s+$ )//gx;
    }
    
    # FIXME: Verify in utf-8 text encoding

    # If we elimated the HTML in that step, return nothing
    if (!$html) {
        return '';
    }

    my @tokens;

    if ($allow_cdata) {
        my @cdata_fragments = split(/<!\[CDATA\[(.*?)\]\]>/,
                                    $html);
        my $i = 0;

        for my $fragment (@cdata_fragments) {
            if ($i % 2) {
                push @tokens, token('CData', $fragment);
            } else {
                tokenize_fragment(\@tokens, $fragment,
                                  $allowed_elements, $allow_taglike,
                                  $tag_translation);
            }
            $i++;
        }
    } else {
        tokenize_fragment(\@tokens, $html, $allowed_elements, 
                          $allow_taglike, $tag_translation);
    }

    # Push a p on front if the first token is not a block 
    # tag. Note that <html> is a block tag, so complete
    # documents don't get this done, only fragments. This work
    # because the TagStack will normalize the p correctly.
    if (!$text_only &&
        @tokens && ($tokens[0]->{type} != $TAG ||
                    ($TAG_TYPES{$tokens[0]->{name}} || $BLOCK)
                        != $BLOCK)) {
        unshift @tokens, token('Tag', 'p');
    }

    my @new_tokens;
    # we abusively share modification between this func
    # and the tag stack :(
    my $tag_stack = new Thrasher::HTMLNormalize::TagStack(\@new_tokens);

    for (my $i = 0; $i < @tokens; $i++) {
        my $token = $tokens[$i];

        my $type = $token->{type};
        if ($type == $TAG) {
            $tag_stack->push($token);
        }
        if ($type == $TAGCLOSE) {
            $tag_stack->pop($token);
        }
        if ($type == $TEXT || $type == $ENTITY || 
            $type == $SELFCLOSEDTAG || $type == $CDATA) {
            push @new_tokens, $token
        }

        # If a newline is already preceded by a self-closed 
        # br, pass it through, otherwise, prepend a self-closed br
        if ($type == $NEWLINE) {
            # Well, actually, if we're in a "pre", skip all fancy
            # processing
            if ($tag_stack->in_tag('pre')) {
                push @new_tokens, token('Text', "\n");
                next;
            }

            if (!@new_tokens) {
                next;
            }

            my $previous_token = $tokens[$i - 1];
            if ($previous_token->{type} == $SELFCLOSEDTAG &&
                $previous_token->{name} eq 'br') {
                push @new_tokens, $token;
            } else {
                # add a br somehowe
                
                # we're not at the end of the tokens.
                if ($i + 1 < @tokens) {
                    my $next_token = $tokens[$i + 1];
                    if ($next_token->{type} == $TAG) {
                        my $tag_type = $TAG_TYPES{$next_token->{name}}
                                       || $BLOCK;
                        if ($tag_type != $BLOCK && 
                            $tag_type != $BLOCK_SELF_CLOSE) {
                            # this automatically gets eaten if there
                            # was no previous <p>
                            if (!$text_only) {
                                push @new_tokens,
                                     token('SelfClosedTag', 'br');
                            }
                            push @new_tokens, $token;
                        } else {
                            push @new_tokens, $token;
                        }
                    } else {
                        if (!$text_only) {
                            push @new_tokens, token('SelfClosedTag', 'br');
                        }
                        push @new_tokens, $token;
                    }
                }
            }
        }

        if ($type == $DOUBLENEWLINE) {
            if ($tag_stack->in_tag('pre')) {
                push @new_tokens, token('Text', "\n\n");
                next;
            }

            # Always assume we're closing a <p>. It gets eaten later
            # by the TagStack if this turns out to be false.
            if (!$text_only) {
                $tag_stack->pop(token('TagClose', 'p'));
            }

            # If this is followed by a non-block tag, add a <p>.
            if ($i < @tokens - 1) {
                my $next_token = $tokens[$i + 1];
                if ($next_token->{type} == $TAG) {
                    my $tag_type = $TAG_TYPES{$next_token->{name}} || $BLOCK;
                    if ($tag_type != $BLOCK &&
                        $tag_type != $BLOCK_SELF_CLOSE) {
                        push @new_tokens, $token;
                        if (!$text_only) {
                            $tag_stack->push(token('Tag', 'p'));
                        }
                    } else {
                        push @new_tokens, $token;
                    }
                } else {
                    push @new_tokens, $token;
                    if (!$text_only) {
                        $tag_stack->push(token('Tag', 'p'));
                    }
                }
            }
        }
    }

    $tag_stack->terminate();

    my $new_html = join '', map { $_->render } @new_tokens;

    if ($VALIDATE_AS_XML) {
        # Do a simple validation pass.
        die "XML validation not implemented yet.";
    }

    return $new_html;
}

# Performs low-level tokenization on a bit of text known not to have
# (valid) CDATA in it.
sub tokenize_fragment {
    my $tokens = shift;
    my $html = shift;
    my $allowed_elements = shift;
    my $allow_taglike = shift;
    my $tag_translation = shift;

    # Same as the validation re in tokenize_tag_or_entity, but
    # with all but the outer parens turned into non-capturing.
    my @fragments = split (/((?:<(?:\/)?(?:\w+)(?:[^>]*)>)|(?:&(?:\#?\w+);))/,
                           $html);

    for (my $i = 0; $i < @fragments; $i++) {
        my $text = $fragments[$i];

        if ($i % 2) {
            my $token = tokenize_tag_or_entity($text, $allow_taglike,
                                               $tag_translation);
            if ($token->{type} == $TAG) {
                $token = validate_tag($token, $allowed_elements);
            }
            if ($token) {
                push @$tokens, $token;
            }
        } else {
            push @$tokens, @{tokenize_text($text)};
        }
    }
}

###
### TOKEN CLASSES
###

package Thrasher::HTMLNormalize::Token;
# A base class for tokens, used for 'isa' checks.

# This is only used by children; do not call directly.
sub _new {
    my $class = shift;
    my %values = @_;
    my $self = \%values;
    bless $self, $class;
    return $self;
}


package Thrasher::HTMLNormalize::Token::Text;
use base 'Thrasher::HTMLNormalize::Token';

sub new { shift()->SUPER::_new(text => shift(), type => $TEXT) }

sub render {
    return Thrasher::HTMLNormalize::escape($_[0]->{text});
}

package Thrasher::HTMLNormalize::TagBase;
use base 'Thrasher::HTMLNormalize::Token';
# Contains the similarities between Tags and SelfClosedTags.

# process an attribute list into proper HTML; this assumes that
# the attributes has been checked for valid attributes already,
# but this also checks for permitted entities. Returns an arrayref
# containing strings ready-to-be-output.
sub att_list {
    my $self = shift;
    my @atts;
    while (my ($name, $value) = each %{$self->{atts}}) {
        my @fragments = split (/&(\w+);/, $value);
        my $i = 0;
        my @new_value;
        for my $text (@fragments) {
            if ($i % 2) {
                # This is an entity body.
                push @new_value, Thrasher::HTMLNormalize::make_entity($text);
            } else {
                # normal text
                push(@new_value, 
                     Thrasher::HTMLNormalize::escape_quote($text));
            }
            $i ++;
        }
        my $value = join '', @new_value;
        # note $name was checked for validity implicitly by the 
        # regex that pulled it out in the first place.
        push @atts, "$name='$value'";
    }
    return \@atts;
}

package Thrasher::HTMLNormalize::Token::Tag;
use base 'Thrasher::HTMLNormalize::TagBase';
# Represents a Tag, which may have attributes.
# Takes: A name, and a hashref of atts.
# Note that name is checked implicitly by the regex that pulls it out.

use Carp qw(confess);

sub new { 
    my $self = shift()->SUPER::_new(name => shift(),
                                    atts => shift() || {},
                                    type => $TAG); 
    confess "Hash" if ref($self->{name}) eq 'HASH';
    return $self;
}

sub render {
    my $self = shift;
    my $atts = $self->att_list;

    if (@$atts) {
        return "<$self->{name} " . join(' ', @$atts) . ">";
    } else {
        return "<$self->{name}>";
    }
}

package Thrasher::HTMLNormalize::Token::SelfClosedTag;
use base 'Thrasher::HTMLNormalize::TagBase';
# Represents a self-closing tag, which may have attributes.

sub new { shift()->SUPER::_new(name => shift(),
                               atts => shift() || {},
                               type => $SELFCLOSEDTAG); }

sub render {
    my $self = shift;
    my $atts = $self->att_list;

    if (@$atts) {
        return "<$self->{name} " . join (' ', @$atts) . " />";
    } else {
        return "<$self->{name} />";
    }
}

package Thrasher::HTMLNormalize::Token::TagClose;
use base 'Thrasher::HTMLNormalize::Token';
# Represents a close tag. Has only a name.

sub new { shift()->SUPER::_new(name => shift(),
                               type => $TAGCLOSE); }

sub render {
    return "</$_[0]->{name}>";
}

package Thrasher::HTMLNormalize::Token::Entity;
use base 'Thrasher::HTMLNormalize::Token';
use Thrasher::HTMLNormalize::Entities qw(html_entity);
# Represents an entity. Has only a name. Performs numberization of
# HTML entities

sub new {
    my $class = shift;
    
    my $name = shift;
    if (!$LEGAL_ENTITIES{$name} && (my $number = html_entity($name))) {
        $name = '#' . $number;
    }
    # make_entity takes care of nulling out illegal entities
    # that pass this test

    return $class->SUPER::_new(name => $name, type => $ENTITY);
}

sub render {
    return Thrasher::HTMLNormalize::make_entity($_[0]->{name});
}

package Thrasher::HTMLNormalize::Token::Newline;
use base 'Thrasher::HTMLNormalize::Token';
# Represents a single newline, suitable for <br />

sub new { shift()->SUPER::_new(type => $NEWLINE); }

sub render { "\n"; }

package Thrasher::HTMLNormalize::Token::DoubleNewline;
use base 'Thrasher::HTMLNormalize::Token';
# Double new line, suitable for <p>-ification

sub new { shift()->SUPER::_new(type => $DOUBLENEWLINE); }

sub render { "\n\n"; }

package Thrasher::HTMLNormalize::Token::CData;
use base 'Thrasher::HTMLNormalize::Token';

sub new { shift()->SUPER::_new(rawtext => shift(), type => $CDATA); }

sub render { "<![CDATA[" . $_[0]->{rawtext} . "]]>"; }

###
### TAG STACK
###

package Thrasher::HTMLNormalize::TagStack;
use strict;
use warnings;

use Data::Dumper;

*{token} = *Thrasher::HTMLNormalize::token;

# This is what it says it is; the "stack" of tags that has led
# us to this point in the HTML. It is a "smart" stack; it can
# silently refuse to accept tag tokens, resulting in the removal
# of that tag from the resulting HTML. If you need to implement
# the real nesting rules for (X)HTML tags, this is where you'd
# put that logic.

sub new {
    my $class = shift;
    my $self = {stack => [],
                # we abusively modify this in the normalize function :(
                tokens => shift};
    bless $self, $class;
    return $self;
}

# last element on the stack
sub laststack { $_[0]->{stack}->[-1]; }
sub lasttoken { $_[0]->{tokens}->[-1]; }

# Push a new tag token onto the stack.
sub push {
    my $self = shift;
    my $tag = shift;

    # Assume certain tags close themselves if they appear immediately
    # after themselves, i.e., <li>stuff<li>morestuff. 
    # In particular, this if statement is asking "Is the previous
    # element the same as this element and a SELF_CLOSER like p?"
    if (@{$self->{stack}} && $self->laststack->{name} eq $tag->{name}
        && $SELF_CLOSERS{$tag->{name}}) {
        # If so, 'pop' the tag, which causes the emission of the
        # appropriate end tag (see pop)
        $self->pop(token('TagClose', $tag->{name}));
    }

    # If this tag should always self close, convert it to such
    # unconditionally
    if ($TAG_TYPES{$tag->{name}} == $BLOCK_SELF_CLOSE) {
        push @{$self->{tokens}}, token('SelfClosedTag',
                                       $tag->{name},
                                       $tag->{atts});
        return;
    }

    # otherwise, a normal push
    push @{$self->{stack}}, $tag;
    push @{$self->{tokens}}, $tag;
}

# A lot of the normalization magic takes place here.
sub pop {
    my $self = shift;
    my $tag = shift;

    # We want a chance to manipulate the tokens we push back on
    # a bit, before losing track of the tokens this func 
    # generated by tossing them onto the pile
    my @tokens;

    # If a newline or doublenewline is currently at the end of the
    # stack, pop it off for a moment and put it back later. This has
    # the effect of binding close paragraph tags to the paragraph they
    # were used in. Without this,
    #
    #  <p>Hello.
    #
    #  <p>There.
    #
    # (complete with those newlines) gets converted to
    #
    #  <p>Hello.
    #
    #  </p><p>There.</p>
    #
    # which really isn't the intent, even if it is valid HTML.
    my $popped_newline;
    if (@{$self->{tokens}} && 
          ($self->lasttoken->{type} == $NEWLINE ||
           $self->lasttoken->{type} == $DOUBLENEWLINE)) {
        $popped_newline = pop @{$self->{tokens}};
    }
    
    # Scan backwards for the matching start tag, emit
    # any missing close tags.
    my $i = scalar(@{$self->{stack}}) - 1;
    while ($i >= 0) {
        my $open_tag = $self->{stack}->[$i];
        if ($open_tag->{name} eq $tag->{name}) {
            while ($i + 1 < @{$self->{stack}}) {
                my $tag_to_close = pop @{$self->{stack}};
                CORE::push(@tokens,
                           token('TagClose', $tag_to_close->{name}));
            }
            CORE::pop @{$self->{stack}};
            CORE::push @tokens, token('TagClose', $tag->{name});
            last;
        }
        $i--;
    }

    # If we're closing off the previous *token* (not stack
    # element), and there is therefore no content for that
    # token, either elide it if it is an empty block token,
    # or convert it to a self-closed tag
    if (@tokens && $self->lasttoken->{type} == $TAG &&
        $self->lasttoken->{name} eq $tokens[0]->{name}) {
        my $name = $tokens[0]->{name};
        my $tag_type = $TAG_TYPES{$name};
        if ($tag_type == $BLOCK) {
            # elide the token
            pop @tokens;
            pop @{$self->{tokens}};
        }
        if ($tag_type == $BLOCK_SELF_CLOSE) {
            pop @tokens;
            my $old_tag = pop @{$self->tokens};
            unshift @tokens, token('SelfClosedTag',
                                   $old_tag->{name},
                                   $old_tag->{atts});
        }
    }

    # If there was no corresponding open tag, just discard it by
    # doing nothing on purpose.

    # do_nothing();

    # If we popped off a newline, now is when it goes back on
    CORE::push @tokens, $popped_newline if $popped_newline;

    CORE::push @{$self->{tokens}}, @tokens;
}

# Returns true if we are "in" the given tag, false otherwise. This
# is used to implement special treatment for <pre>, for instance
sub in_tag {
    my $self = shift;
    my $tag_name = shift;

    for my $tag (@{$self->{stack}}) {
        return 1 if $tag->{name} eq $tag_name;
    }
    return 0;
}

# Terminates the TagStack by popping all current elements. Used
# at the end of the input string. Despite the name, not the same
# as a DESTROY function.
sub terminate {
    my $self = shift;
    
    while (@{$self->{stack}}) {
        my $tag_still_open = CORE::pop @{$self->{stack}};
        CORE::push @{$self->{tokens}}, token('TagClose',
                                             $tag_still_open->{name});
    }
}

1;
__END__

=head1 NAME

Thrasher::HTMLNormalize - a module for normalizing (X)HTML for safe
display

=head1 SYNOPSIS

 use Thrasher::HTMLNormalize qw(:all);

 # Take arbitrary HTML and make it syntactically correct
 my $normalizedHTML = normalize($html);

 # Take arbitrary maybe-HTML and make it correct, and
 # use a chosen sub-set of HTML suitable for simple rich-text-like
 # HTML.
 my $comment_like_html = normalize($html, $CONSERVATIVE_PERMISSIONS);

 # Take arbitrary maybe-HTML and make it correct, use a 
 # a chosen sub-set of HTML suitable for simple rich-text-like 
 # HTML, and if you see a "tag" that isn't actually HTML,
 # escape it as if it was text.
 my $message_log_like_html = normalize($html,
                                       $CONSERVATIVE_PERMISSIONS,
                                       undef, 1);


=head1 MOTIVATION

This predated HTML::Normalize on CPAN. I just had it lying around, and
I understand it, so I used it. I recommend checking out
HTML::Normalize if you think you might want to use this. (On the
other hand, I've had to implement features here that it doesn't have.)

This is a utility module; Thrasher::XHTMLNormalize specifically
defines the normalization for XHTML-IM.

This is a utility module; Thrasher::XHTMLNormalize specifically
defines the normalization for XHTML-IM.

=cut
