package Net::REPL::Client::Queue;
use strict;
use warnings;

use base qw(Net::REPL::Client);

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(
        'input_queue' => [],
        'output_queue' => [],
        @_,
    );
    return $self;
}

sub queue_input {
    my ($self, $input) = @_;
    push(@{$self->{'input_queue'}}, $input);
}

sub setup_io {
    my ($self) = @_;

    $self->{'out_fh'} = \*STDOUT;
}

sub read {
    my ($self) = @_;
    return shift(@{$self->{'input_queue'}})
}

sub print {
    my ($self, @output) = @_;

    push(@{$self->{'output_queue'}}, @output);
}

1;
