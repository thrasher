package Thrasher::Protocol::Purple::MSN;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Purple::MSN - protocol for connection to Windows
Live Messenger, or whatever they're calling it this week.

=cut

use base "Thrasher::Protocol::Purple";

use Thrasher::Log qw(log debug);
use Thrasher::Protocol::Purple::Vars qw(%purple_presence);

sub prpl {
    return "prpl-msn";
}

sub process_outgoing_purple_status {
    my ($self, $session, $type, $show, $message, $purple_status) = @_;

    if ($purple_status == $purple_presence{'xaway'}) {
        # MSN prpl lacks a status type for the EXTENDED_AWAY primitive.
        $purple_status = $purple_presence{'away'};
    }

    return ($message, $purple_status);
}

sub fake_up_a_legacy_name {
    my ($self, $user_jid, $jid, $legacy_guess) = @_;

    if ($legacy_guess !~ /@/) {
        # Avoid discombobulation: msn_normalize adds @hotmail.com, but
        # only after some things are set up w/o it (e.g. name_maps).
        $legacy_guess .= '@hotmail.com';
    }

    return $legacy_guess;
}

sub name { "MSN" }
sub identifier { "msn" }
sub gateway_prompt { "Enter MSN Username:" }
sub gateway_desc { "Enter MSN Username:" }

sub ft_local_ready {
    my ($self, $id) = @_;
    # no op--MSN prpl uses its own loop which ft.c do_transfer interferes with.
    return 0;                   # don't repeat this notification.
}

1;
