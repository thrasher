package Thrasher::Protocol::Purple::Vars;

use strict;
use warnings;

use Exporter;
our @ISA = qw(Exporter);
our @ERROR_CODES = qw(
                      $ERROR_NETWORK_ERROR
                      $ERROR_INVALID_USERNAME
                      $ERROR_AUTHENTICATION_FAILED
                      $ERROR_AUTHENTICATION_IMPOSSIBLE
                      $ERROR_NO_SSL_SUPPORT
                      $ERROR_ENCRYPTION_ERROR
                      $ERROR_NAME_IN_USE
                      $ERROR_INVALID_SETTINGS
                      $ERROR_CERT_NOT_PROVIDED
                      $ERROR_CERT_UNTRUSTED
                      $ERROR_CERT_EXPIRED
                      $ERROR_CERT_NOT_ACTIVATED
                      $ERROR_CERT_HOSTNAME_MISMATCH
                      $ERROR_CERT_FINGERPRINT_MISMATCH
                      $ERROR_CERT_SELF_SIGNED
                      $ERROR_CERT_OTHER_ERROR
                      $ERROR_OTHER_ERROR
                      );
our @EXPORT_OK = (qw(%purple_presence %purple_presence_to_xmpp), @ERROR_CODES);
our %EXPORT_TAGS = (all => \@EXPORT_OK,
                    error_codes => \@ERROR_CODES);

# mirror server.h enum
our $ERROR      = 0;
our $LOGIN      = 1;
our $LOGOUT     = 2;
our $SEND_MSG   = 3;
our $RECV_MSG   = 4;
our $PRESENCE   = 5;
our $SUBSCR_ADD = 6;
our $SUBSCR_REM = 7;
our $BUDDY_ADD  = 8;
our $BUDDY_REM  = 9;

# Mirror of PurpleConnectionError from connection.h
our $ERROR_NETWORK_ERROR             = 0;
our $ERROR_INVALID_USERNAME          = 1;
our $ERROR_AUTHENTICATION_FAILED     = 2;
our $ERROR_AUTHENTICATION_IMPOSSIBLE = 3;
our $ERROR_NO_SSL_SUPPORT            = 4;
our $ERROR_ENCRYPTION_ERROR          = 5;
our $ERROR_NAME_IN_USE               = 6;
our $ERROR_INVALID_SETTINGS          = 7;
our $ERROR_CERT_NOT_PROVIDED         = 8;
our $ERROR_CERT_UNTRUSTED            = 9;
our $ERROR_CERT_EXPIRED              = 10;
our $ERROR_CERT_NOT_ACTIVATED        = 11;
our $ERROR_CERT_HOSTNAME_MISMATCH    = 12;
our $ERROR_CERT_FINGERPRINT_MISMATCH = 13;
our $ERROR_CERT_SELF_SIGNED          = 14;
our $ERROR_CERT_OTHER_ERROR          = 15;
our $ERROR_OTHER_ERROR               = 16;


# mirror libpurple/status.h PurpleStatusPrimitive enum
our $PURPLE_STATUS_UNSET          = 0;
our $PURPLE_STATUS_OFFLINE        = 1;
our $PURPLE_STATUS_AVAILABLE      = 2;
our $PURPLE_STATUS_UNAVAILABLE    = 3;
our $PURPLE_STATUS_INVISIBLE      = 4;
our $PURPLE_STATUS_AWAY           = 5;
our $PURPLE_STATUS_EXTENDED_AWAY  = 6;
our $PURPLE_STATUS_MOBILE         = 7;
our $PURPLE_STATUS_TUNE           = 8;
our $PURPLE_STATUS_NUM_PRIMITIVES = 9;

# mirror tlv_struct enum in server.h
our $JID       = 1;
our $USERNAME  = 2;
our $PASSWORD  = 3;
our $MESSAGE   = 4;
our $RECIPIENT = 5;
our $SENDER    = 6;
our $STATUS    = 7;
our $ALIAS     = 8;
our $GROUP     = 9;
our $ERROR_CODE= 10;

# TLV lookup
my %tlv_type = (
    JID       => $JID,
    USERNAME  => $USERNAME,
    PASSWORD  => $PASSWORD,
    MESSAGE   => $MESSAGE,
    RECIPIENT => $RECIPIENT,
    SENDER    => $SENDER,
    STATUS    => $STATUS,
    ALIAS     => $ALIAS,
    GROUP     => $GROUP,
    ERROR_CODE=> $ERROR_CODE,
);

# TLV reverse lookup
my %tlv_reverse_lookup = reverse %tlv_type;

# presence lookup
our %purple_presence = (
    unset       => $PURPLE_STATUS_UNSET,
    offline     => $PURPLE_STATUS_OFFLINE,
    available   => $PURPLE_STATUS_AVAILABLE,
    unavailable => $PURPLE_STATUS_UNAVAILABLE,
    invisible   => $PURPLE_STATUS_INVISIBLE,
    away        => $PURPLE_STATUS_AWAY,
    xaway       => $PURPLE_STATUS_EXTENDED_AWAY,
    mobile      => $PURPLE_STATUS_MOBILE,
    tune        => $PURPLE_STATUS_TUNE,
    num_prim    => $PURPLE_STATUS_NUM_PRIMITIVES,
);

our %purple_presence_to_xmpp = (
    $PURPLE_STATUS_OFFLINE => [ 'unavailable', '', ],
    $PURPLE_STATUS_AVAILABLE => [ '', '', ],
    $PURPLE_STATUS_UNAVAILABLE => [ '', 'dnd' ],
    $PURPLE_STATUS_INVISIBLE => [ 'unavailable', '', ],
    $PURPLE_STATUS_AWAY => [ '', 'away', ],
    $PURPLE_STATUS_EXTENDED_AWAY => [ '', 'xa', ],
);

# action lookup
my %actions = (
    'error'      => $ERROR,
    'login'      => $LOGIN,
    'logout'     => $LOGOUT,
    'send_msg'   => $SEND_MSG,
    'recv_msg'   => $RECV_MSG,
    'presence'   => $PRESENCE,
    'subscr_add' => $SUBSCR_ADD,
    'subscr_rem' => $SUBSCR_REM,
    'buddy_add'  => $BUDDY_ADD,
    'buddy_rem'  => $BUDDY_REM,
);

1;
