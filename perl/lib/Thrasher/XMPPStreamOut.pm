package Thrasher::XMPPStreamOut;
use strict;
use warnings;

# This package is responsible for the XML output. It outputs by
# calling a closure you give it, which makes it easy to test.
# This is highly tuned for XMPP streams and the messages it
# sends. It is intended to do the "right thing" if you send it
# the right data as input, but it does not validate the XML
# for things like legal tag names.

use CGI qw(escapeHTML); # doubling as XML escaping

use Thrasher::Constants qw(:all);
use Thrasher::Log qw(:all);
use Encode;

use Carp qw(confess);

use Data::Dumper;

# This runs an escaping process over the data, and also strips out
# invalid 
sub pcdata_process {
    my $s = shift;

    if (!Encode::is_utf8($s)) {
        $s = Encode::decode("UTF-8", $s);
    }
    $s = escapeHTML($s);
    $s = remove_invalid_chars($s);
    return $s;
}

sub remove_invalid_chars {
    my $s = shift;
    # \x{fffd} is the substitution character, which ends up
    # coming out evil if we don't do something here for
    # some reason.
    $s =~ s/\x{fffd}/?/g;
    $s =~ s/[\x00-\x08\x0b\x0c\x0e-\x1f]+//g;
    $s =~ s/([\x80-\xff])/'&#' . ord($1) . ';'/ge;
    return $s;
}

# This is a set of conventional prefixes, so the output stream
# looks like people expect.
my %CONVENTIONAL_PREFIXES =
    ($NS_STREAM => 'stream',
     $NS_CLIENT => '',
     $NS_COMPONENT => '',
     $NS_DISCO_INFO => '',
     $NS_DISCO_ITEMS => '',
     $NS_REGISTER => '',
     $NS_ERROR => '',
     $NS_GATEWAY => '',
     $NS_TIME => '',
     $NS_VERSION => '',
     $NS_CAPS => '',
     $NS_PUBSUB => '',
     $NS_TUNE => '',
     $NS_PEP_AVATAR => '',
     $NS_PEP_AVATAR_METADATA => '',
     $NS_LAST => '',
     $NS_VCARD => '',
     $NS_VCARD_UPDATE => '',
     $NS_ROSTER_EXCHANGE => '',
     $NS_XHTML_IM => '',
     $NS_XHTML => '',
     $NS_NICK => '',
     $NS_BYTESTREAMS => '',
     $NS_FILE_TRANSFER => '',
     $NS_FEATURE_NEG => '',
     $NS_DATA => '',
     $NS_STREAM_INITIATION => '',
     $NS_FILE_TRANSFER => '',
     $NS_CHATSTATES => '',

     $NS_THRASHER_PRESENCE => 'thrasher');

my %XML_NAMESPACES = 
    ($NS_XMLNS => 'xmlns', $NS_XML => 'xml');

sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{output} = shift;
    if (ref($self->{output}) ne 'CODE') {
        die "Thrasher::XMPPStreamOut needs an output method.";
    }
    $self->{namespace_to_prefixes} = {};
    $self->{prefix_to_namespace} = {};

    return $self;
}

# This outputs everything but the closing tag, so it
# can be used to start "stream"
sub output_tag_and_children {
    my $self = shift;
    my $fragment = shift;
    my $is_root_element = shift; 
    my $namespace_declarations = {};
    my ($element, $atts, $children) = @$fragment;

    if (!defined($children)) {
        confess "Undefined \$children in output_tag_and_children.";
    }

    my $element_text = $self->xml_element_text($element,
                                               $namespace_declarations);
    my $outputtable_atts = $self->outputtable_atts($atts, $namespace_declarations);
    $self->output_tag($element_text, $outputtable_atts,
                      # immediately close? not root, no children
                      !$is_root_element && !@$children);
    if ($children) {
        for my $child (@$children) {
            $self->output($child);
        }
    }    

    # If this is the root element, we didn't close the tag,
    # and any initial namespace declarations are still in effect,
    # so don't clear them!
    if (!$is_root_element) {
        for my $key (values %$namespace_declarations) {
            my $prefix = pop @{$self->{namespace_to_prefixes}->{$key}};
            if (!@{$self->{namespace_to_prefixes}->{$key}}) {
                delete $self->{namespace_to_prefixes}->{$key};
            } 
            
            pop @{$self->{prefixes_to_namespace}->{$prefix}};
            if (!@{$self->{prefixes_to_namespace}->{$prefix}}) {
                delete $self->{prefixes_to_namespace}->{$prefix};
            }
        }

        # If this isn't a root element, we want to close the tag.
        if (@$children) {
            $self->{output}->('</' . $element_text . '>');
        }
    }

    

    return $element_text;
}

# Core routine: Output the XML bits as they come. This
# outputs closing tags too, so it can't be used for the
# initial tag.
sub output {
    my $self = shift;

    my $output = $self->{output};

    for my $fragment (@_) {
        if (ref($fragment) eq '') {
            $output->(pcdata_process($fragment));
        } elsif (ref($fragment) eq 'ARRAY') {
            my $element_text =
                $self->output_tag_and_children($fragment);
        } elsif (ref($fragment) eq 'SCALAR') {
            $output->(remove_invalid_chars($$fragment));
        } else {
            log("Error: outputting something to the XML "
                ."stream which is invalid (ignored): "
                .Dumper($fragment));
        }
    }
}

# This may add a namespace declaration to the atts, which is why
# it has to be passed in.
sub xml_element_text {
    my $self = shift;
    if (!ref($_[0])) {
        confess "Not a ref in xml_element_text.";
    }
    my ($namespace, $element) = @{shift()};
    my $namespace_declarations = shift;

    my $prefix = $self->prefix_for_namespace($namespace,
                                             $namespace_declarations);
    if ($prefix eq '') {
        return $element;
    } else {
        return "$prefix\:$element";
    }
}

our $namespace_index = 0;

# Get the prefix for the given namespace. May add a prefix if
# it needs to.
sub prefix_for_namespace {
    my $self = shift;
    my $namespace = shift;
    my $namespace_declarations = shift;

    if (!defined($namespace)) {
        confess "No defined namespace.";
    }

    if (my $xml_prefix = $XML_NAMESPACES{$namespace}) {
        return $xml_prefix;
    }

    my $prefixes = $self->{namespace_to_prefixes}->{$namespace};
    if (!defined($prefixes)) {
        # need to create a new one.
        my $conventional_prefix = $CONVENTIONAL_PREFIXES{$namespace};
        if (defined($conventional_prefix)) {
            if ($conventional_prefix eq '') {
                $namespace_declarations->{'xmlns'} = $namespace;
                $self->declare_prefix('', $namespace);
                return '';
            } else {
                $namespace_declarations->{"xmlns:$conventional_prefix"} =
                    $namespace;
                $self->declare_prefix($conventional_prefix, $namespace);
                return $conventional_prefix;
            }
        }

        # Otherwise, we need to select a new one, and
        # put out the namespace definition.
        $namespace_index++;
        $self->declare_prefix("ns$namespace_index", $namespace);
        $namespace_declarations->{"xmlns:ns$namespace_index"} = $namespace;
        return "ns$namespace_index";
    } else {
        return $prefixes->[-1];
    }
}

sub outputtable_atts {
    my $self = shift;
    my $atts = shift; # in "Clarke-style" format
    my $namespace_declarations = shift || {}; # in straight {$attname => $att}

    my %final_atts;

    while (my ($clarke_key, $value) = each %$atts) {
        # process {namespace}att_name
        my ($namespace, $name) = $clarke_key =~ /^\{([^}]*)\}(.*)$/;
        if ($namespace) {
            my $prefix = $self->prefix_for_namespace($namespace, $namespace_declarations);
            if ($prefix eq '') {
                $final_atts{$name} = $value;
            } else {
                $final_atts{$prefix . ':' . $name} = $value;
            }
        } else {
            # things in the default namespace go out without
            # xmlns labels; I don't like this aspect of XML,
            # but there you are.
            $clarke_key =~ s/^\{\}//;
            $final_atts{$clarke_key} = $value;
        }
    }

    while (my ($key, $value) = each %$namespace_declarations) {
        $final_atts{$key} = $value;
    }

    return \%final_atts;
}

sub declare_prefix {
    my $self = shift;
    my $prefix = shift;
    my $namespace = shift;

    push @{$self->{namespace_to_prefixes}->{$namespace}}, $prefix;
    push @{$self->{prefixes_to_namespace}->{$prefix}}, $namespace;
}

sub output_tag {
    my $self = shift;
    my $element_name = shift;
    my $atts = shift;
    my $immediately_close = shift;

    my @strings = ($element_name);
    # "Why sort this? XML doesn't care about attribute order?"
    # Stability for the test suite; with this, I can guarantee
    # a testable order.
    for my $name (sort keys %$atts) {
        my $value = $atts->{$name};
        if (!defined($value)) {
            confess "In outputing att $name, undefined value.";
        }
        push @strings, "$name='" . pcdata_process($value) . "'";
    }

    my $element = ('<' . (join ' ', @strings) . 
                   ($immediately_close ? '/' : '') . '>');
    $self->{output}->($element);
}

1;
