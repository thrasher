#!/usr/bin/perl

use strict;
use warnings;

# Test that the entity capabilities plugin works as expected.

use Test::More 'no_plan';
use Data::Dumper;

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher', qw(:all);
    use_ok 'Thrasher::Component';
    use_ok 'Thrasher::Plugin', qw(:all);
    use_ok 'Thrasher::Plugin::EntityCapabilities';
    use_ok 'Thrasher::Backend::Test';
}

my $comp = logged_in_comp;
$comp->send_presence_xml('test@test.com', '');
my $expected = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='test\@test.com'>
    <c hash='sha-1' 
       node='http://developer.berlios.de/projects/thrasher/' 
       ver='vEhfNTvdI3o4kgwbVxIGfxr9S2g' 
       xmlns='http://jabber.org/protocol/caps'/>
</presence>
EXPECTED

is(output, $expected, 'caps correctly adds the capability tag for the transport');

$comp->send_presence_xml('test@test.com', 'juliet@' . transport_jid);
$expected = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='test\@test.com' 
          type='juliet\@test.transport'>
    <c hash='sha-1' 
       node='http://developer.berlios.de/projects/thrasher/' 
       ver='vEhfNTvdI3o4kgwbVxIGfxr9S2g' 
       xmlns='http://jabber.org/protocol/caps'/>
</presence>
EXPECTED

is(output, $expected, 'caps correctly adds the capability tag for '
   .'the virtual users');

