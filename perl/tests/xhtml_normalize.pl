use Test::More 'no_plan';
use Test::Deep;

use strict;
use warnings;

BEGIN {
    use_ok 'Thrasher::XHTML_IM_Normalize', ':all';
    use_ok 'Thrasher::HTMLNormalize', ':all';
}

my $RANDOM_TEST_COUNT = 1000;

ESCAPE: {
    # Verify escape works
    my $x = 'hello <>& &amp;!';
    my $y = escape($x);

    isnt($x, $y, "escape doesn't modify existing strings");
    is($y, 'hello &lt;&gt;&amp; &amp;amp;!',
       'escaping basically works');
}

# We test two cases: full-permission normalization, and the
# normalization defined by $CONSERVATIVE_PERMISSIONS. This creates a
# series of test cases [$input => $result]. The first set are used 
# in both. %CONSERVATIVE_TESTS defines a series of tests used only for
# the permissions, and $FULL_TESTS defines tests for the
# full-permission normalization.
# something), but I decided to leave them in for readability.

my %BOTH_TESTS = (
    # Had trouble with this
    '<ul><li>Test<ul><li>nested</li><li>nested2</li></ul></li><li>moo</li></ul>' =>
     '<ul><li>Test<ul><li>nested</li><li>nested2</li></ul></li><li>moo</li></ul>',
    'a' => '<p>a</p>',
    't&a' => '<p>t&amp;a</p>',
    "t\n\na" => "<p>t</p>\n\n<p>a</p>",
    "This is a test.\nMore.\n\nSo there." =>
    "<p>This is a test.<br />\nMore.</p>\n\n<p>So there.</p>",
    "<p>This is a test.<br />\nMore.</p>" =>
    "<p>This is a test.<br />\nMore.</p>",
    "<P>This is a test.<BR />\nMore.</P>" =>
    "<p>This is a test.<br />\nMore.</p>",
    '<b><i>moo</b></i>' => '<p><b><i>moo</i></b></p>',
    '<b><i><abbr title="moo">argle</b></i></abbr>' =>
    '<p><b><i><abbr title=\'moo\'>argle</abbr></i></b></p>',
    '' => '',
    "\n\t   \n" => '',
    # Incomplete CDATA tags get escaped no matter whether we are
    # allowing CDATA or not
    '<pre><![CDATA[moo' =>
     '<pre>&lt;![CDATA[moo</pre>',
    '&trade;' => '<p>&#8482;</p>',
    "\x{2033}" => "<p>\x{2033}</p>",
    '\x{e2}' => '<p>\x{e2}</p>',

    # Real-life example:
    '<A href="http://www.wssource.com/cgi-bin/auth/dispatch.cgi?_code=307&amp;alert=123256652117586003&amp;noback=1">[edit]</A>' =>
    '<p><a href=\'http://www.wssource.com/cgi-bin/auth/dispatch.cgi?_code=307&amp;alert=123256652117586003&amp;noback=1\'>[edit]</a></p>',
    '<A HREF="http://www.wssource.com/cgi-bin/auth/dispatch.cgi?_code=307&amp;alert=123256652117586003&amp;noback=1">[edit]</A>' =>
    '<p><a href=\'http://www.wssource.com/cgi-bin/auth/dispatch.cgi?_code=307&amp;alert=123256652117586003&amp;noback=1\'>[edit]</a></p>'
 );

my %CONSERVATIVE_TESTS = (
    %BOTH_TESTS,
    '<p id="moo">test</p>' => '<p>test</p>',
    '<p><a href="http://www.jerf.org/">test</a></p>' =>
     "<p><a href='http://www.jerf.org/'>test</a></p>",
    '<p><a href="/moo">test</a></p>' =>
     "<p><a href='http:///moo'>test</a></p>",
    '<ul><li>A list element.</li>\n\n<li>Another list '.
     'element</li></ul>' =>
     '<ul><li>A list element.</li>\n\n<li>Another list '.
     'element</li></ul>',
    '<ul><li>A list element</li><li>another list element</li>' =>
     '<ul><li>A list element</li><li>another list element</li></ul>',
    '<ul><li>A list element</li>\n<li>another list element</li>' =>
     '<ul><li>A list element</li>\n<li>another list '.
     'element</li></ul>',
    '<ul><li>A list element<li>another' =>
     '<ul><li>A list element</li><li>another</li></ul>',
    "<p>Moo.\n\n<p>More moo.\n\nForgot my p." =>
    "<p>Moo.</p>\n\n<p>More moo.</p>\n\n<p>Forgot my p.</p>",
    '<scr<script>ipt>' => '<p>ipt&gt;</p>',
    '<a href="javascript:alert(\'moo\')">test</a>' =>
     '<p>test</p>',
    '<bgsound src="javascript:alert()">moo' =>
     '<p>moo</p>',
    '<abbr title="oh & my">OM</abbr>' =>
     "<p><abbr title='oh &amp; my'>OM</abbr></p>"
);                          

my %FULL_PERM_TESTS = (
    %BOTH_TESTS,
    "Moo\n\n<h3>Header</h3>\n\nMoo" =>
    "<p>Moo</p>\n\n<h3>Header</h3>\n\n<p>Moo</p>",
    "<div class=\"NewsItemTitle\">title</div>\n<div ".
    "class=\"NewsItemBody\">moo\n\nthere</div>" =>
    "<div class=\'NewsItemTitle\'>title</div>\n<div ".
    "class=\'NewsItemBody\'>moo\n\n<p>there</p></div>",
    '<p>a</p> <hr><p>hello</p>' =>
     '<p>a</p> <hr /><p>hello</p>',
    '<table><tr><td>a</td></tr></table>' =>
     '<table><tr><td>a</td></tr></table>',
    # Non-quoted attributes handled as expected
    '<p name=cheesy 444 poofs>meh</p>' => "<p name='cheesy' poofs='1'>meh</p>",
    '<p name=cheesy poofs 333>meh</p>' => "<p name='cheesy' poofs='1'>meh</p>",
);                      

my %CDATA_TESTS = 
    (
     '<pre><![CDATA[ & < > <script> &amp ]]></pre>' =>
     '<pre><![CDATA[ & < > <script> &amp ]]></pre>' );

my @RANDOM_HTML_FRAGMENTS = 
    ('<scr', '<i>', '</i>',
     '<p id="testing">ddd',
     '<script>alert ()</script>',
     '<<<', '>>>', "\x{0437}",
     '&amp', '&amp;',
     '&notanentity;', "  \n\r\r\n",
     '<!-- -->', 'rc', '<!--',

     # An excitingly Unicode-laden string
     join('', map { chr($_) } 32..1024),
     '<?xml wha?>',
     '<![CDATA[', ']]>');

sub test_data_with_allowed_elements {
    my $hash = shift;
    my $allowed_elements = shift;
    my $allow_cdata = shift;
    my $name = shift;

    while (my ($input, $desired_output) = each %$hash) {
        my $actual_output = normalize($input, $allowed_elements,
                                      $allow_cdata);
        is($actual_output, $desired_output, 'test with ' . $name);
    }
}

test_data_with_allowed_elements(\%FULL_PERM_TESTS, undef, undef, 
                                'full permissions');
test_data_with_allowed_elements(\%CONSERVATIVE_TESTS,
                                $CONSERVATIVE_PERMISSIONS, undef,
                                'conservative permissions');
test_data_with_allowed_elements(\%CDATA_TESTS,
                                undef, 1, 'cdata permissions');

# This is primarily a verification that there exists no input that 
# results in invalid XML. "Sadly", this code doesn't solve the GIGO
# problem.
RANDOM_FRAGMENTS: {
    last;
    for (my $i = 0; $i < $RANDOM_TEST_COUNT; $i++) {
        my @random_fragments;
        for (my $j = 0; $j < 10; $j++) {
            push(@random_fragments, 
                 $RANDOM_HTML_FRAGMENTS[int(rand(@RANDOM_HTML_FRAGMENTS))]);
        }
        my $random_input = join '', @random_fragments;
        # Allow both cdata options.
        normalize($random_input, $CONSERVATIVE_PERMISSIONS, 0);
        normalize($random_input, $CONSERVATIVE_PERMISSIONS, 1);
    }
}

ALLOW_TAGLIKE: {
    my %allow_taglike = 
        ('<ping>' => '<p>&lt;ping&gt;</p>',
         '<p>' => '<p></p>',
         '<html><body><p>hello</body></html>' => '<p>hello</p>',
         %CONSERVATIVE_TESTS);
    # behaves differently
    $allow_taglike{'<scr<script>ipt>'} = 
        '<p>&lt;scr&lt;script&gt;ipt&gt;</p>';

    while (my ($input, $desired_output) = each %allow_taglike) {
        is(normalize($input, $CONSERVATIVE_PERMISSIONS,
                     0, 1), $desired_output,
           'test with allow_taglike');
        is(normalize($input, $CONSERVATIVE_PERMISSIONS,
                     1, 1), $desired_output,
           'test with allow_taglike');
    }
}


# The preceding is actually the generic test script for 
# Thrasher::HTMLNormalize. This tests the XHTML modifications, at
# least to some degree.
ACTUAL_XHTML_TESTS: {
    # Make sure the automatic <p> suppression occurs for 
    # text mode
    my ($xhtml, $text) = xhtml_and_text("hello!\nhello!\n\nhello!");
    is($text, "hello!\nhello!\n\nhello!",
       'text only correctly suppresses all automatically-added tags');
    is($xhtml, "<p>hello!<br />\nhello!</p>\n\n<p>hello!</p>",
       'xhtml mode still correctly adds them');

    # Test the translations
    $xhtml = xhtml("<p>hello <b>there</b> buddy</p>");
    is($xhtml, "hello <strong>there</strong> buddy",
       'simple tag normalization works');

    $xhtml = xhtml("<a href='http://www.jerf.org/'>Test</a>");
    is($xhtml, "<a href='http://www.jerf.org/'>Test</a>",
       "can handle links correctly.");

    $xhtml = xhtml("<FONT FACE='ARIAL'><i>itali</FONT></i>c");
    is($xhtml, "<em>itali</em>c",
       'correctly handles upper-case crap in the tags');
}

