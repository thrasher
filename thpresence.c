/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include "debug.h"
#include "status.h"
#include "account.h"
#include "thpresence.h"



void thrasher_set_presence (PurpleAccount *pa, PurpleStatusPrimitive primitive, gchar *message)
{
    /* Get the prpl-specific status_id (e.g. UNAVAILABLE may be "dnd"
     * or "busy" or...). */
    PurpleStatusType *type
        = purple_account_get_status_type_with_primitive(pa,
                                                        primitive);

    /* Flip the status on if we have a status_id */
    if (type)
    {
        const char *status_id = purple_status_type_get_id(type);
        purple_debug_info("thrasher", "found status with ID: %s\n", status_id);
        PurplePresence *presence = purple_presence_new_for_account(pa);
        PurpleStatus *status = purple_status_new(type, presence);
        if (message && purple_status_get_attr_value(status, "message")) {
            /* Don't add message if not supported by this status. */
            purple_account_set_status(pa, status_id, TRUE, "message", message, NULL);
        }
        else
            purple_account_set_status(pa, status_id, TRUE, NULL);
    }
    else {
        purple_debug_info("thrasher", "No status type found\n");
    }
}

