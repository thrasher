package Thrasher::Plugin::Socks;

# This file is based on IO::Socket::Socks, but is modified
# to work with the interface of Thrasher::EventLoop. It should
# work with any conformant implementation of the EventLoop.
# 
# Only reading and writing a single proxied socket works. Serving
# stuff will be removed, eventually.
#
# This file is used by the ProxyFileTransfer plugin, but I just don't
# care to go down another layer of hierarchy in the package name.
# Besides, other plugins could use this, someday.
# 
# In theory, we should also convert this so that the process of
# connecting to the socket doesn't block, but uses the event loop
# callback system for everything. However, this is unlikely to be a
# problem until you have incredibly high load. In my experience, a
# file transfer is a rare thing. You want it to work when you want to
# move a file, but you don't move files anywhere near as often as you
# send messages.

##############################################################################
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public
#  License along with this library; if not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
#  Boston, MA  02111-1307, USA.
#
#  Copyright (C) 2003 Ryan Eatmon
#
##############################################################################

use strict;
use IO::Socket;
use Carp;
use vars qw(@ISA @EXPORT $VERSION %CODES );
require Exporter;
@ISA = qw(Exporter IO::Socket::INET);
@EXPORT = qw( $SOCKS_ERROR );

use POSIX qw(F_SETFL O_NONBLOCK);
use Thrasher::Log qw(log);

$VERSION = "0.1";
our $SOCKS_ERROR;

use constant SOCKS5_VER =>  5;

use constant ADDR_IPV4       => 1;
use constant ADDR_DOMAINNAME => 3;
use constant ADDR_IPV6       => 4;

use constant CMD_CONNECT  => 1;
#use constant CMD_BIND     => 2;
#use constant CMD_UDPASSOC => 3;

use constant AUTHMECH_ANON     => 0;
#use constant AUTHMECH_GSSAPI   => 1;
use constant AUTHMECH_USERPASS => 2;
use constant AUTHMECH_INVALID  => 255;

$CODES{AUTHMECH}->[AUTHMECH_INVALID] = "No valid auth mechanisms";

use constant AUTHREPLY_SUCCESS  => 0;
use constant AUTHREPLY_FAILURE  => 1;

$CODES{AUTHREPLY}->[AUTHREPLY_FAILURE] = "Failed to authenticate";

use constant REPLY_SUCCESS             => 0;
use constant REPLY_GENERAL_FAILURE     => 1;
use constant REPLY_CONN_NOT_ALLOWED    => 2;
use constant REPLY_NETWORK_UNREACHABLE => 3;
use constant REPLY_HOST_UNREACHABLE    => 4;
use constant REPLY_CONN_REFUSED        => 5;
use constant REPLY_TTL_EXPIRED         => 6;
use constant REPLY_CMD_NOT_SUPPORTED   => 7;
use constant REPLY_ADDR_NOT_SUPPORTED  => 8;

$CODES{REPLY}->[REPLY_SUCCESS] = "Success";
$CODES{REPLY}->[REPLY_GENERAL_FAILURE] = "General failure";
$CODES{REPLY}->[REPLY_CONN_NOT_ALLOWED] = "Not allowed";
$CODES{REPLY}->[REPLY_NETWORK_UNREACHABLE] = "Network unreachable";
$CODES{REPLY}->[REPLY_HOST_UNREACHABLE] = "Host unreachable";
$CODES{REPLY}->[REPLY_CONN_REFUSED] = "Connection refused";
$CODES{REPLY}->[REPLY_TTL_EXPIRED] = "TTL expired";
$CODES{REPLY}->[REPLY_CMD_NOT_SUPPORTED] = "Command not supported";
$CODES{REPLY}->[REPLY_ADDR_NOT_SUPPORTED] = "Address not supported";

# To retain basic compatibility with the previous interface, we
# explicitly set which arguments we care about and which will
# go to the raw socket.
my %SOCKS_ARGUMENTS = map { $_ => 1 } 
    qw(ProxyAddr ProxyPort ConnectAddr ConnectPort AuthType
       RequireAuth UserAuth Username Password Debug Listen
       LocalAddr LocalPort PeerAddr PeerPort EventLoop
       SuccessCallback FailureCallback);

my @REQUIRED_ARGS = qw(ProxyAddr ProxyPort EventLoop SuccessCallback
                       FailureCallback);

# Note this removes the automatic connect
# EventLoop is a reference to a Thrasher::EventLoop object of some
# sort
# SuccessCallback will be called with the next chunk of data, when
# we receive some, or undef if the socket is over normally
# FailureCallback will be called when something fails, including
# a failure to log in to the proxy or a lost connection.
sub new {
    my $class = shift;

    my $self = {};
    bless $self, $class;

    $self->{socket_args} = {Proto => 'tcp',
                            Type => SOCK_STREAM};
    $self->{args} = {};
    while (@_) {
        my $key = shift;
        my $value = shift;

        if ($SOCKS_ARGUMENTS{$key}) {
            $self->{$key} = $value;
        } else {
            $self->{socket_args}->{$key} = $value;
        }
    }

    $self->{AuthType} ||= 'none';
    if ($self->{AuthType} ne 'none' &&
        (!$self->{Username} || !$self->{Password})) {
        croak "An AuthTyype other than none requires a Username "
            ."and Passward.";
    }

    $self->{AuthMethods} = [!$self->{RequireAuth}, 
                            0, 
                            exists($self->{Listen}) ?
                                defined($self->{UserAuth}) :
                                $self->{AuthType} eq 'userpass'
                           ];
    
    $self->{COMMAND} = undef;

    if (exists($self->{Listen})) {
        $self->{socket_args}->{LocalAddr} = $self->{ProxyAddr};
        $self->{socket_args}->{LocalPort} = $self->{ProxyPort};
    } else {
        $self->{socket_args}->{PeerAddr} = $self->{ProxyAddr};
        $self->{socket_args}->{PeerPort} = $self->{ProxyPort};
    }

    for my $required (@REQUIRED_ARGS) {
        if (!$self->{$required}) {
            croak "You must provide $required to a Socks connection.";
        }
    }

    if (ref($self->{FailureCallback}) ne 'CODE' ||
        ref($self->{SuccessCallback}) ne 'CODE') {
        croak "FailureCallback and SuccessCallback both need to be "
            ."subrefs.";
    }

    $self->{socket} = new IO::Socket::INET(%{$self->{socket_args}});

    if (!$self->{socket}) {
        log("Failed to connect to XMPP proxy.");
        return undef;
    }

    # Setting Blocking => 0 doesn't seem to work as well as this does.
    fcntl($self->{socket}, F_SETFL, O_NONBLOCK);

    return $self;
}

sub connect
{
    my $self = shift;

    print "Socks: Connecting\n";

    croak("Undefined Socks object passed to connect.")
        unless defined($self);

    my $completed_connect = sub {
        print "Calling completed connect callback\n";

        if (defined($self->{ConnectAddr}) &&
            defined($self->{ConnectPort}))
        {
            print "Proceding to connect socket.\n";
            $self->connect_to_socket();
        }
    };

    my $auth_mech = $self->_socks5_connect($completed_connect);
}


###############################################################################
#
# _socks5_connect - Send the opening handsake, and process the reply.
#
###############################################################################
sub _socks5_connect
{
    my $self = shift;
    my $after_connect_closure = shift;

    #--------------------------------------------------------------------------
    # Send the auth mechanisms
    #--------------------------------------------------------------------------
    my %connect;
    $connect{version} = SOCKS5_VER;
    my @methods;
    foreach my $method (0..scalar(@{$self->{AuthMethods}}))
    {
        push(@methods,$method)
            if ($self->{AuthMethods}->[$method] == 1);
    }
    $connect{num_methods} = $#methods + 1;
    $connect{methods} = \@methods;
    
    $self->_debug_connect("Send",\%connect);

    $self->_socks_send($connect{version});
    $self->_socks_send($connect{num_methods});
    foreach my $method (@{$connect{methods}})
    {
        $self->_socks_send($method);
    }
    $self->_socks_flush();

    my $handle_reply = sub {
        print "Socks: Made it to handle_reply.\n";
        my %connect_reply;
        print "Socks: A\n";
        $connect_reply{version} = $self->_socks_read();
        print "Socks: B\n";
        $connect_reply{auth_method} = $self->_socks_read();
        print "Socks: C\n";

        $self->_debug_connect_reply("Recv",\%connect_reply);
        
        if ($connect_reply{auth_method} == AUTHMECH_INVALID)
        {
            log("Could not complete connection to proxy.");
            $self->{socket}->close;
            $self->{FailureCallback}->();
            return 0;
        }

        if ($connect_reply{auth_method} != AUTHMECH_ANON)
        {
            die "Authentication not converted yet.";
            return unless $self->_socks5_connect_auth();
        }

        print "About to run after_connect_closure\n";
        $after_connect_closure->();
        
        # We'll never run *this* callback again on this socket.
        return 0;
    };

    print "Adding handle reply watcher\n";
    $self->{EventLoop}->add_fd_watch
        ($self->{socket}->fileno, 
         $Thrasher::EventLoop::IN,
         $handle_reply);
}


###############################################################################
#
# _socks5_connect_auth - Send and receive a SOCKS5 auth handshake
#
###############################################################################
sub _socks5_connect_auth
{
    my $self = shift;
    
    #--------------------------------------------------------------------------
    # Send the auth
    #--------------------------------------------------------------------------
    my %auth;
    $auth{version} = 1;
    $auth{user_length} = length($self->{Username});
    $auth{user} = $self->{Username};
    $auth{pass_length} = length($self->{Password});
    $auth{pass} = $self->{Password};
    
    $self->_debug_auth("Send",\%auth);
        
    $self->_socks_send($auth{version});
    $self->_socks_send($auth{user_length});
    $self->_socks_send_raw($auth{user});
    $self->_socks_send($auth{pass_length});
    $self->_socks_send_raw($auth{pass});
    $self->_socks_flush;
    
    #--------------------------------------------------------------------------
    # Read the reply
    #--------------------------------------------------------------------------
    my %auth_reply;
    $auth_reply{version} = $self->_socks_read();
    $auth_reply{status} = $self->_socks_read();
    
    $self->_debug_auth_reply("Recv",\%auth_reply);
        
    if ($auth_reply{status} != AUTHREPLY_SUCCESS)
    {
        $SOCKS_ERROR = "Authentication failed with SOCKS5 proxy.";
        return;
    }

    return 1;
}


sub connect_to_socket
{
    my $self = shift;
    my $command = CMD_CONNECT;

    print "Socks: Connecting to socket.\n";

    #--------------------------------------------------------------------------
    # Send the command
    #--------------------------------------------------------------------------
    my %command;
    $command{version} = SOCKS5_VER;
    $command{command} = $command;
    $command{reserved} = 0;
    $command{atype} = ADDR_DOMAINNAME;
    $command{host_length} = length($self->{ConnectAddr});
    $command{host} = $self->{ConnectAddr};
    $command{port} = $self->{ConnectPort};

    $self->_debug_command("Send",\%command);
        
    $self->_socks_send($command{version});
    $self->_socks_send($command{command});
    $self->_socks_send($command{reserved});
    $self->_socks_send($command{atype});
    $self->_socks_send($command{host_length});
    $self->_socks_send_raw($command{host});
    $self->_socks_send_raw(pack("n",$command{port}));
    $self->_socks_flush();

    my $handle_connect_reply = sub {
        print "Socks: Handling connect reply.\n";
        my %command_reply;
        $command_reply{version} = $self->_socks_read();
        $command_reply{status} = $self->_socks_read();
        
        if ($command_reply{status} == REPLY_SUCCESS)
        {
            $command_reply{reserved} = $self->_socks_read();
            $command_reply{atype} = $self->_socks_read();
            
            if ($command_reply{atype} == ADDR_DOMAINNAME)
            {
                $command_reply{host_length} = $self->_socks_read();
                $command_reply{host} = $self->_socks_read_raw($command_reply{host_length});
            }
            elsif ($command_reply{atype} == ADDR_IPV4)
            {
                $command_reply{host} = unpack("N",$self->_socks_read_raw(4));
            }
            
            $command_reply{port} = unpack("n",$self->_socks_read_raw(2));
        }
        
        $self->_debug_command_reply("Recv",\%command_reply);
        
        if ($command_reply{status} != REPLY_SUCCESS)
        {
            $self->{FailureCallback}->();
            return 0;
        }

        print "Socks: Final handler installed.\n";
        # If we got here, must be successful.
        #
        # We need to make sure that one side of the file transfer
        # doesn't run way ahead of the other, and to do that,
        # ProxyFileTransfer.pm needs full control of when the stream
        # is actually read, so the OS correctly rate-limits the TCP
        # socket....
        my $data_reader = sub {
            my ($chunk_sz) = @_;
            my ($eof, $error, $data);
            my $result = sysread($self->{socket}, $data, $chunk_sz);
            if (! defined($result)) {
                $error = $!;
            }
            elsif ($result == 0) {
                $eof = 1;
            }
            return ($eof, $error, $data);
        };
        $self->{SuccessCallback}->($data_reader);

        return 0;
    };

    print "Socks: Preparing connection reply watch.\n";
    $self->{EventLoop}->add_fd_watch
        ($self->{socket}->fileno,
         $Thrasher::EventLoop::IN,
         $handle_connect_reply);
}

sub add_fd_watch {
    my ($self, $directions, $callback) = @_;

    my $callback_wrapped = sub {
        my $ret = $callback->();
        if (! $ret) {
            # Unset watch_id to reflect that this FD is (about to be) removed.
            $self->{'watch_id'} = 0;
        }
        return $ret;
    };

    $self->{'watch_id'} = $self->{EventLoop}->add_fd_watch(
        $self->{socket}->fileno(),
        $directions,
        $callback_wrapped,
    );
}

sub remove_fd_watch {
    my ($self) = @_;

    if ($self->{'watch_id'}) {
        $self->{EventLoop}->remove_fd_watch($self->{'watch_id'});
        $self->{'watch_id'} = undef;
    }
}

sub close {
    my ($self) = @_;

    # Remove FD watch if present and close underlying socket.
    $self->remove_fd_watch();
    if ($self->{'socket'}) {
        $self->{'socket'}->close();
    }
}

###############################################################################
#+-----------------------------------------------------------------------------
#| Accept Functions
#+-----------------------------------------------------------------------------
###############################################################################

###############################################################################
#
# accept - When we are accepting new connections, we need to do the SOCKS
#          handshaking before we return a usable socket.
#
###############################################################################
sub accept
{
    my $self = shift;

    croak("Undefined IO::Socket::Socks object passed to accept.")
        unless defined($self);

    my $client = $self->SUPER::accept(@_);

    if (!$self)
    {
        $SOCKS_ERROR = "Proxy accept new client failed.";
        return;
    }

    my $authmech = $self->_socks5_accept($client);
    return unless defined($authmech);

    if ($authmech == AUTHMECH_USERPASS)
    {
        return unless $self->_socks5_accept_auth($client);
    }

    return unless $self->_socks5_accept_command($client);

    return $client;
}


###############################################################################
#
# _socks5_accept - Wait for an opening handsake, and reply.
#
###############################################################################
sub _socks5_accept
{
    my $self = shift;
    my $client = shift;

    #--------------------------------------------------------------------------
    # Read the auth mechanisms
    #--------------------------------------------------------------------------
    my %accept;
    $accept{version} = $client->_socks_read();
    $accept{num_methods} = $client->_socks_read();
    $accept{methods} = [];
    foreach (0..($accept{num_methods}-1))
    {
        push(@{$accept{methods}},$client->_socks_read());
    }
    
    $self->_debug_connect("Recv",\%accept);

    if ($accept{num_methods} == 0)
    {
        $SOCKS_ERROR = "No auth methods sent.";
        return;
    }

    my $authmech;
    
    foreach my $method (@{$accept{methods}})
    {
        if ($self->{AuthMethods}->[$method] == 1)
        {
            $authmech = $method;
            last;
        }
    }

    if (!defined($authmech))
    {
        $authmech = AUTHMECH_INVALID;
    }

    #--------------------------------------------------------------------------
    # Send the reply
    #--------------------------------------------------------------------------
    my %accept_reply;
    $accept_reply{version} = SOCKS5_VER;
    $accept_reply{auth_method} = AUTHMECH_INVALID;
    $accept_reply{auth_method} = $authmech if defined($authmech);

    $client->_socks_send($accept_reply{version});
    $client->_socks_send($accept_reply{auth_method});
    $client->_socks_flush;
    
    $self->_debug_connect_reply("Send",\%accept_reply);

    if ($authmech == AUTHMECH_INVALID)
    {
        $SOCKS_ERROR = "No available auth methods.";
        return;
    }
    
    return $authmech;
}


###############################################################################
#
# _socks5_accept_auth - Send and receive a SOCKS5 auth handshake
#
###############################################################################
sub _socks5_accept_auth
{
    my $self = shift;
    my $client = shift;
    
    #--------------------------------------------------------------------------
    # Send the auth
    #--------------------------------------------------------------------------
    my %auth;
    $auth{version} = $client->_socks_read();
    $auth{user_length} = $client->_socks_read();
    $auth{user} = $client->_socks_read_raw($auth{user_length});
    $auth{pass_length} = $client->_socks_read();
    $auth{pass} = $client->_socks_read_raw($auth{pass_length});
    
    $self->_debug_auth("Recv",\%auth);
    
    my $status = 0;
    if (defined($self->{UserAuth}))
    {
        $status = &{$self->{UserAuth}}($auth{user},$auth{pass});
    }

    #--------------------------------------------------------------------------
    # Read the reply
    #--------------------------------------------------------------------------
    my %auth_reply;
    $auth_reply{version} = 1;
    $auth_reply{status} = AUTHREPLY_SUCCESS;
    $auth_reply{status} = AUTHREPLY_FAILURE if !$status;
    
    $client->_socks_send($auth_reply{version});
    $client->_socks_send($auth_reply{status});
    $client->_socks_flush;
    
    $self->_debug_auth_reply("Send",\%auth_reply);
        
    if ($auth_reply{status} != AUTHREPLY_SUCCESS)
    {
        $SOCKS_ERROR = "Authentication failed with SOCKS5 proxy.";
        return;
    }

    return 1;
}


###############################################################################
#
# _socks5_acccept_command - Process a SOCKS5 command request.  Since this is
#                           a library and not a server, we cannot process the
#                           command.  Let the parent program handle that.
#
###############################################################################
sub _socks5_accept_command
{
    my $self = shift;
    my $client = shift;

    #--------------------------------------------------------------------------
    # Read the command
    #--------------------------------------------------------------------------
    my %command;
    $command{version} = $client->_socks_read();
    $command{command} = $client->_socks_read();
    $command{reserved} = $client->_socks_read();
    $command{atype} = $client->_socks_read();

    if ($command{atype} == ADDR_DOMAINNAME)
    {
        $command{host_length} =  $client->_socks_read();
        $command{host} = $client->_socks_read_raw($command{host_length});
    }
    elsif ($command{atype} == ADDR_IPV4)
    {
        $command{host} = unpack("N",$client->_socks_read_raw(4));
    }
    else
    {
        $client->_socks_accept_command_reply(REPLY_ADDR_NOT_SUPPORTED);
        $SOCKS_ERROR = $CODES{REPLY}->[REPLY_ADDR_NOT_SUPPORTED];
        return;
    }
    
    $command{port} = unpack("n",$client->_socks_read_raw(2));

    $self->_debug_command("Recv",\%command);

    ${*$client}->{SOCKS}->{COMMAND} = [$command{command},$command{host},$command{port}];

    return 1;
}


###############################################################################
#
# _socks5_acccept_command_reply - Answer a SOCKS5 command request.  Since this
#                                 is a library and not a server, we cannot
#                                 process the command.  Let the parent program
#                                 handle that.
#
###############################################################################
sub _socks5_accept_command_reply
{
    my $self = shift;
    my $reply = shift;
    my $host = shift;
    my $port = shift;

    if (!defined($reply) || !defined($host) || !defined($port))
    {
        croak("You must provide a reply, host, and port on the command reply.");
    }

    #--------------------------------------------------------------------------
    # Send the reply
    #--------------------------------------------------------------------------
    my %command_reply;
    $command_reply{version} = SOCKS5_VER;
    $command_reply{status} = $reply;
    $command_reply{reserved} = 0;
    $command_reply{atype} = ADDR_DOMAINNAME;
    $command_reply{host_length} = length($host);
    $command_reply{host} = $host;
    $command_reply{port} = $port;
    
    $self->_debug_command_reply("Send",\%command_reply);

    $self->_socks_send($command_reply{version});
    $self->_socks_send($command_reply{status});
    $self->_socks_send($command_reply{reserved});
    $self->_socks_send($command_reply{atype});
    $self->_socks_send($command_reply{host_length});
    $self->_socks_send_raw($command_reply{host});
    $self->_socks_send_raw(pack("n",$command_reply{port}));
    $self->_socks_flush;
}


###############################################################################
#
# command_reply - public reply wrapper to the client.
#
###############################################################################
sub command_reply
{
    my $self = shift;
    $self->_socks5_accept_command_reply(@_);
}





###############################################################################
#+-----------------------------------------------------------------------------
#| Helper Functions
#+-----------------------------------------------------------------------------
###############################################################################

###############################################################################
#
# _socks_read - send over the socket after packing according to the rules.
#
###############################################################################
sub _socks_send
{
    my $self = shift;
    my $data = shift;
    
    $data = pack("C",$data);
    $self->_socks_send_raw($data);
} 


###############################################################################
#
# _socks_send_raw - send raw data across the socket.
#
###############################################################################
sub _socks_send_raw
{
    my $self = shift;
    my $data = shift;

    push @{$self->{data}}, $data;
}

sub _socks_flush {
    my $self = shift;
    my $data = $self->{data};
    undef $self->{data};

    $data = join '', @$data;

    $self->{socket}->syswrite($data,length($data));
}


###############################################################################
#
# _socks_read - read from the socket, and then unpack according to the rules.
#
###############################################################################
sub _socks_read
{
    my $self = shift;
    my $length = shift;
    $length = 1 unless defined($length);
    
    my $data = $self->_socks_read_raw($length);
    $data = unpack("C",$data);
    return $data;
}


###############################################################################
#
# _socks_read_raw - read raw bytes off of the socket
#
###############################################################################
sub _socks_read_raw
{
    my $self = shift;
    my $length = shift;
    $length = 1 unless defined($length);

    my $data;
    sysread($self->{socket}, $data, $length);
    return $data;
}




###############################################################################
#+-----------------------------------------------------------------------------
#| Debug Functions
#+-----------------------------------------------------------------------------
###############################################################################

sub _debug_connect
{
    my $self = shift;
    my $tag = shift;
    my $connect = shift;

    return unless $self->{Debug};

    print "$tag: +------+------+-","-"x(4*$connect->{num_methods}),"-+\n";
    print "$tag: | Vers | Auth |";
    if ($connect->{num_methods} > 0)
    {
        print " Meth "," "x(4*($connect->{num_methods}-1)),"|\n";
    }
    print "$tag: +------+------+-","-"x(4*$connect->{num_methods}),"-+\n";

    print "$tag: | ";
    printf("\\%02X",$connect->{version});
    print "  | ";
    printf("\\%02X",$connect->{num_methods});
    print "  | ";
    if ($connect->{num_methods} > 0)
    {
        foreach my $method (@{$connect->{methods}})
        {
            printf("\\%02X ",$method);
        }
        print " |";
    }
    
    print "\n";
    print "$tag: +------+------+-","-"x(4*$connect->{num_methods}),"-+\n";
    print "\n";
}


sub _debug_connect_reply
{
    my $self = shift;
    my $tag = shift;
    my $connect_reply = shift;
    
    return unless $self->{Debug};

    print "$tag: +------+------+\n";
    print "$tag: | Vers | Auth |\n";
    print "$tag: +------+------+\n";
    print "$tag: | ";
    
    printf("\\%02X",$connect_reply->{version});
    print "  | ";
    printf("\\%02X",$connect_reply->{auth_method});
    print "  |\n";

    print "$tag: +------+------+\n";
    print "\n";
}


sub _debug_auth
{
    my $self = shift;
    my $tag = shift;
    my $auth = shift;

    return unless $self->{Debug};

    print "$tag: +------+------+------","-"x($auth->{user_length}-4),"+------+-----","-"x($auth->{pass_length}-4),"-+\n";
    print "$tag: | Vers | UsrL | User "," "x($auth->{user_length}-4),"| PasL | Pass"," "x($auth->{pass_length}-4)," |\n";
    print "$tag: +------+------+------","-"x($auth->{user_length}-4),"+------+-----","-"x($auth->{pass_length}-4),"-+\n";
    print "$tag: | ";

    printf("\\%02X",$auth->{version});
    print "  | ";
    printf("\\%02d",$auth->{user_length});
    print "  | ";
    print $auth->{user}," "x(4-$auth->{user_length});
    print " | ";
    printf("\\%02d",$auth->{pass_length});
    print "  | ";
    print $auth->{pass}," "x(4-$auth->{pass_length});

    print " |\n";
    print "$tag: +------+------+------","-"x($auth->{user_length}-4),"+------+-----","-"x($auth->{pass_length}-4),"-+\n";
    print "\n";
}  


sub _debug_auth_reply
{
    my $self = shift;
    my $tag = shift;
    my $auth_reply = shift;
    
    return unless $self->{Debug};

    print "$tag: +------+------+\n";
    print "$tag: | Vers | Stat |\n";
    print "$tag: +------+------+\n";
    print "$tag: | ";
    
    printf("\\%02X",$auth_reply->{version});
    print "  | ";
    printf("\\%02X",$auth_reply->{status});
    print "  |\n";

    print "$tag: +------+------+\n";
    print "\n";
}


sub _debug_command
{
    my $self = shift;
    my $tag = shift;
    my $command = shift;

    return unless $self->{Debug};

    print "$tag: +------+------+------+------+-------","-"x$command->{host_length},"-+-------+\n";
    print "$tag: | Vers | Comm | Resv | ATyp | Host  "," "x$command->{host_length}," | Port  |\n";
    print "$tag: +------+------+------+------+-------","-"x$command->{host_length},"-+-------+\n";
    print "$tag: | "; 

    printf("\\%02X",$command->{version});
    print "  | ";
    printf("\\%02X",$command->{command});
    print "  | ";
    printf("\\%02X",$command->{reserved});
    print "  | ";
    printf("\\%02X",$command->{atype});
    print "  | ";
    printf("\\%02d",$command->{host_length});
    print " - ";
    print $command->{host};
    print " | ";
    printf("%-5d",$command->{port});

    print " |\n";
    print "$tag: +------+------+------+------+-------","-"x$command->{host_length},"-+-------+\n";
    print "\n";
}


sub _debug_command_reply
{
    my $self = shift;
    my $tag = shift;
    my $command_reply = shift;

    return unless $self->{Debug};

    print "$tag: +------+------+";
    print "------+------+-------","-"x$command_reply->{host_length},"-+-------+"
        if ($command_reply->{status} == 0);
    print "\n";

    print "$tag: | Vers | Stat |";
    print " Resv | ATyp | Host  "," "x$command_reply->{host_length}," | Port  |"
        if ($command_reply->{status} == 0);
    print "\n";

    print "$tag: +------+------+";
    print "------+------+-------","-"x$command_reply->{host_length},"-+-------+"
        if ($command_reply->{status} == 0);
    print "\n";
    
    print "$tag: | "; 

    printf("\\%02X",$command_reply->{version});
    print "  | ";
    printf("\\%02X",$command_reply->{status});
    if ($command_reply->{status} == 0)
    {
        print "  | ";
        printf("\\%02X",$command_reply->{reserved});
        print "  | ";
        printf("\\%02X",$command_reply->{atype});
        print "  | ";
        printf("\\%02d",$command_reply->{host_length});
        print " - ";
        print $command_reply->{host};
        print " | ";
        printf("%-5d",$command_reply->{port});
    }
    else
    {
        print " ";
    }
    print " |\n";
    
    print "$tag: +------+------+";
    print "------+------+-------","-"x$command_reply->{host_length},"-+-------+"
        if ($command_reply->{status} == 0);
    print "\n";
    print "\n";
}


1;
