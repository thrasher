package Thrasher::Protocol::Test;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Test - test protocol for Thrasher Bird

=head1 DESCRIPTION

This is a test protocol; it is specifically designed to be used in
the unit tests. The primary difference is that it simply records 
interactions with the protocol, so the interactions can be tested,
rather than actually performing any actions.

=cut

use base 'Thrasher::Protocol';

use Thrasher::Log qw(log debug);
use Thrasher::Session;
use Thrasher::XML qw(extract save save_sub);

use MIME::Base64 qw(decode_base64);

use Data::Dumper;
use Carp qw(confess);

my $avatar1 = decode_base64(<<AVATAR1);
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA
BGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VG
AAADBUlEQVR42mLctX/X/lt8dxkuvbvMoPRS/kJFTHkhAx4AUv+V77vDkWdHGWTeSDEABBDTfw4G
h4NGxxxmuSxyuC/20ICBAGDkY2I4bnSGoddnGsMT4WcMAAHEBJNg/v2fgfHvfwZiwHsgZIKqBwgg
FrCp/xgYWL7/Z2D6C1HwcnmGAJuogAGnjAgDIwvLA3aVggcoJkDVg/QBBBATA9BSpl//IQJ/oAp+
/VwvoCiwn12Ibz8zJ8v9z4cq9j+ZFuMA0//79w+4AQABBHYBM8iAbwgv/P707QMDhxDQZG4G5r/f
GHjE2RxYvrA73K9wOPDowxsB4d/8DKwgA4AuBggglp/v3kIMQPLC96fvJn579CaAS/ATA8OHFwwM
f/8wcHCxMyhYSzuw7ZrAwPToKwOLOcgF/xkAAojlwqIlDBwT9jOYuPxl4BWEuEC1Y/eBu0yMC+Rd
VBOYv31meHz25YNPD94uZPj3j4FVQSY/askRgS/rfzOwezMwAAQAQQC+/wG0vrP/xeOcACyQsQA7
MSMABSossPPj8MHz0t3gGC8YrxwvFADr3PAA4MXkITbA5OCvva6/yxm6QMeqmwAs1zcAAoiJiZGJ
4ZkpI0NDHTPDhue/GBjOHwlQ12Hr10uzWy9hpQGMs/cMonw/Gf5//FgPch0nByfDB+YvDPesGBj4
PCwZAAKIiYWVBRwL/xj/MzwO9TywZtfbwKPzD3z4v387A8uJvQwML54xMLx9zcAm8C8AZAArCysD
229meCwABBATUB8D9x92eKjmv2bYcPjmb8W52z5MuPj4H8NfRmZgqP5g4DVTFwCnGZAr/rLDDQAI
IJZ/wJBkArqc6SciFVa+ZfjAwPC/sHTPy4la1mr5ovJqAd9e/twAjuJfvxmYgemXGageZBhAALGw
MDMzfPz8ioERSyrufs/wgGHLLWDmugXPYMxMTAwfv7wC2w4CAAHExMrBwnD55UkGYgEbByvDmecH
wez/QEsBAojl/t0HDD4/s8Ac4f+ShDPSk88XEj53wPkAAQYAlD41GOhA6JcAAAAASUVORK5CYII=
AVATAR1

my $avatar2 = decode_base64(<<AVATAR2);
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAAA
BGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VG
AAABsUlEQVR42mL8//8/AyUAIIAYcUmsC2boB1IFUO6EoLUMhdjUAQQQLs0OQPz/w5WC/+9PGvwH
sUFi2NQCBBATDkPmawYnMLy5MIHh7Y0LDGq2EDFsCgECiAmL7Q1cYgoK/BIPGC6tZ2C4tImBQVCa
gYGTj0EBJIeuHiCAmNA0KwCpfL2oAAZWpgNwcTYOBgYdJzAzH6oGDgACCN0F/SLaDgKSahsYGP4h
iQLZMpoMDEJSDAIgNcgaAAKICTnggFSAcQLQgj8PGBiQY/c/xBBjdzAvADlAAQKICT3guLiBtv9l
wHABCPPwMzComwugBChAADEhB5yyA9Dmvx8gGrC4AIRVtD4wcAoIwAMUIIAYoYFy3jg9QUBOfwED
wy8g7zcE3z3EAHaNsiFCDIQfXAZqOMYAtInBECCAQC6oBwWcnMkBuC0g/O0dA8OdE0B8Csh+z4Ai
J68CDFARcIDWAwQQyIAETT8BSMChGfANaMe3jxAaWY4R6CVdfbDnEgACiAVE/v4uwPDnG6ozuYEB
Jq8P8YKAKND876jynz9AggcggBihgVFPZmZsBAggRkqzM0CAAQCgU5LNtkF1hAAAAABJRU5ErkJg
gg==
AVATAR2

my $avatar3 = decode_base64(<<AVATAR3);
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHESURBVDjL1ZNPSJNxHMY/v9/7823Lbcxp
y2bpAsEsWWBCW130YLeYEEYEYUUgWkSHEgm6BEGwQ9Qp6FzQ/6CIboHHEoPAWDcHzrFXF5iw6bu9
77dD9OdYeIgeeC7P4Tk8f5SIsBloNol/b8D5jEV3GOIG2pug3f7FFg2pNoup4c7e989Pv6nkx51r
EwfuNltW4qfBZFqTMNAMhH9jiO9ati92+PPMqcL68ojUS71SLyflUa5jNhIw+wDUhYwOlv2OIy6B
ECI+CkDwBWnytLpyee90+oROsTgHXxch7kFgC5fOyrPbj90xc2ho4Obxq+cuKkuBVPGrRUxjiUZl
CRoak1TwJQ++A0GP+/d04eN8vTh40GTuPKFHv5wtl+u1ItpaASljWjXLTgWz08b01IA8bCyA1MAC
27RILNLvKhPVQEQBZnRox3VjWQNof+1Ydnc6d2tuZn9fOGTbja0nR9b7M0erUSqACwRDEI4yNel8
yD1wz/zI0gY6ge02DAJ7gFZgW6rLjH16bValpERKiCwgD2+wEgnoaSCm/qTqZJvJjg6riV1xr+vd
vFV8+tZ7VfP8F0BB/cVkokAC2AAcYA1A/f9n+gbO0Zmbd9Cq1QAAAABJRU5ErkJggg==
AVATAR3

my @avatars = ($avatar1, $avatar2, $avatar3);

sub registration {
    my $self = shift;
    my $jid = shift;
    my $registration_info = shift;

    # As a special case, if the registration info's username is 
    # "fail", we return an error given by $registration_info->{password}.
    no warnings 'uninitialized';
    if ($registration_info->{username} eq 'fail') {
        return 0, $registration_info->{password};
    } else {
        return $self->SUPER::registration($jid, $registration_info);
    }
}

sub name { 'Test' }

sub identifier { 'aim' }

sub create_login_session {
    my $self = shift;
    my $continuation = shift;
    my $registration_info = shift;
    my $full_jid = shift;
    my $component = shift;

    # Magic: If the username is "fail", we'll give the 
    # error indicated by "password".
    if ($registration_info->{username} eq 'fail') {
        $continuation->($registration_info->{password});
    } else {
        # FIXME: Check for existing logins.
        my $session = new Thrasher::Session($full_jid, $component,
                                            $self,
                                            $registration_info->{username});

        $self->set_session_state($session, 'logging in');

        $continuation->($session);
    }
}

sub initial_login {
    my $self = shift;
    my $session = shift;

    push @{$self->{logged_in}}, $session;

    $self->SUPER::initial_login($session);
}

sub subscribe {
    my $self = shift;
    my $session = shift;
    my $target_name = shift;
    my $continuation = shift;

    # Per our usual magic, if the target name contains the string
    # "fail", we'll report failure. Otherwise, we report success.
    if ($target_name =~ /fail/) {
        $continuation->(0);
    } else {
        $session->{subscribed}->{$target_name} = 1;
        $continuation->(1);
    }
}

sub unsubscribe {
    my $self = shift;
    my $session = shift;
    my $target_name = shift;
    my $continuation = shift;
    
    if (!(delete $session->{subscribed}->{$target_name})) {
        print STDERR "Warning, removing nonexistant contact\n";
    }
    $continuation->();
}

sub logout {
    my $self = shift;
    my ($session, $continuation) = @_;

    $continuation->($session);
    return $self->SUPER::logout(@_);
}

# FIXME: Updating the avatar should not make a person go online.
sub send_message {
    my $self = shift;
    my $session = shift;
    my $to = shift;
    my $body_text = shift;
    my $type = shift;
    my $error_sub = shift;
    my $chatstate = shift;

    if (my ($error) = ($body_text =~ /^Error: ([a-z_]+)/)) {
        $error_sub->($error);
        return;
    }

    # Record a message was sent
    my $message = [$to, $body_text, $type];
    push(@{$session->{messages}}, $message);

    my $from = $session->{jid};

    debug("Message From: $from, To: $to, body: $body_text\n");

    if ($body_text =~ /^Tell me: (.*)$/) {
        my $target_jid = $self->{backend}->legacy_name_to_jid
            (Thrasher::Component::strip_resource($from),
             $to, $session->{component}->{component_name}, 'en');
        print "Sending message from $target_jid to $from\n";
        $session->{component}->send_message($target_jid, $from, $1);
    }

    if ($body_text =~ /^avatar (\d)/) {
        my $avatar = $avatars[$1];
        if (!$avatar) { return; }
        log("Changing avatar for $session->{jid}");

        my $to_jid = $self->{backend}->legacy_name_to_jid
            ($from, $to, $session->{component}->{component_name});
        
        Thrasher::Avatar::set_avatar
            ($session->{component}, $session->{jid}, $to_jid, $avatar);
    }

    if ($body_text =~ /^presence ([^ ]+) ([^ ]+) (.*)$/) {
        my $type = $1;
        $type = '' if $type eq 'online';
        my $show = $2;
        my $status = $3;
        $self->legacy_presence_update($session, $to, $type, $show, $status);
    }

    return undef;
}

sub outgoing_chatstate {
    my ($self, $session, $to, $chatstate) = @_;
    if ($session->{'chatstates'}) {
        push(@{$session->{'chatstates'}}, $chatstate);
    }
    return;
}

sub subscribed {
    my $self = shift;
    my $session = shift;
    my $component = shift;
    my $legacy_username = shift;

    print Dumper($session->{jid}, $legacy_username);

    $session->{subscribed}->{$legacy_username} = 'subscribed';
    
    $component->send_presence($session->{jid}, $legacy_username,
				      'subscribed');

    my $jid = $self->{backend}->legacy_name_to_jid
        ($session->{jid}, $legacy_username,
         $component->{component_name}, 'en');

    $component->{protocol}->{backend}->set_roster_user_state
        ($session->{jid}, $jid, 
         $component->{protocol}->{backend}->subscribed);

    # Lie that the user is currently online
    $component->send_presence($session->{jid},
                              $legacy_username,
                              undef, 'Online');
}

sub unsubscribed {
    my $self = shift;
    my $session = shift;
    my $component = shift;
    my $legacy_username = shift;

    if (!defined($legacy_username)) {
        confess "Unsubscribing an undef user; shouldn't be called.";
    }
    
    $component->{protocol}->{backend}->set_roster_user_state
        ($session->{jid}, $legacy_username, 
         $component->{protocol}->{backend}->unsubscribed);

    $session->{subscribed}->{$legacy_username} = 'unsubscribed';
}

sub gateway_prompt {
    my $self = shift;
    my $lang = shift;

    return "Prompt" . ($lang ? " $lang" : "");
}

sub gateway_desc {
    my $self = shift;
    # allows testing that returning undef is permitted
    return $self->{gateway_desc};
}

sub user_presence_update {
    my $self = shift;
    my $session = shift;
    my $type = shift || '';
    my $show = shift || '';
    my $status = shift || '';

    push(@{$self->{presence_update}}, 
         [$session->{jid}, $type, $show, $status]);

    log "User presence update: type: $type, show: $show, status: $status";
}

sub user_targeted_presence_update {
    my $self = shift;
    my $session = shift;
    my $type = shift || '';
    my $show = shift || '';
    my $status = shift || '';
    my $target_user = shift || '';

    push(@{$self->{targeted_presence_update}},
         [$session->{jid}, $type, $show, $status, $target_user]);

    log "User presence update to $target_user: type: $type, show: $show, status: $status";
}

1;
