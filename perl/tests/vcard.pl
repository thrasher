#!/usr/bin/perl

use strict;
use warnings;

# Test that the vcard implementation works as expected. Only 
# does avatars for now, but that's good.

use Test::More 'no_plan';
use Data::Dumper;

use Thrasher::Test qw(:all);

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher';
    use_ok 'Thrasher::Plugin', qw(:all);
    use_ok 'Thrasher::Plugin::Vcard', qw(:all);
    use_ok 'Thrasher::Callbacks', qw(:all);
    use_ok 'Thrasher::Avatar', qw(:all);
    use_ok 'Thrasher::Constants', qw(:all);
}

my $JID = transport_jid();

VCARD_TESTS: {
    my $comp = logged_in_comp();

    # Send a presence tag for the transport itself, which should 
    # not get modified by this plugin.
    $comp->send_presence_xml(undef, '', undef);
    is(output, "<presence from='$JID'/>",
       "correctly does not decorate the transport's presences");

    # Send a presence tag for a user with no avatar yet.
    $comp->send_presence_xml('romeo@montague.lit',
                             '', "juliet\@$JID");
    is(output, 
       "<presence from='juliet\@test.transport' to='romeo\@montague.lit'><x xmlns='vcard-temp:x:update'/></presence>",
       'correctly indicates support for vcard avatars, but that '
       .'the user does not yet have one');

    my $vcard_query = <<VCARD_QUERY;
<iq from='romeo\@montague.lit'
    to='juliet\@$JID'
    type='get'
    id='test'>
    <vCard xmlns='$NS_VCARD'/>
</iq>
VCARD_QUERY

    $comp->xml_in($vcard_query);
    my $expected = clean_xml(<<EXPECTED);
<iq from='juliet\@test.transport' 
    id='test' 
    to='romeo\@montague.lit' 
    type='result'>
    <vCard xmlns='vcard-temp'/>
</iq>
EXPECTED
    is(output, $expected, 'empty vCards correctly reply');

    set_avatar($comp, 'romeo@montague.lit', "juliet\@$JID",
               $small_png);
    $expected = clean_xml(<<EXPECTED);
<presence from='juliet\@test.transport' 
          to='romeo\@montague.lit'>
    <x xmlns='vcard-temp:x:update'>0765a59164829a0fe4eb243ffb14bdd64a3d8f5d</x>
</presence>
EXPECTED
    # FIXME: Send this out or not?
    output;
    # is(output, $expected, 'we get the proper avatar hash on avatar setting');
    # That sort of automatically tests the presence code path, too

    # Ask for the vcard again now that we have an avatar.
    $comp->xml_in($vcard_query);
    $expected = clean_xml(<<EXPECTED);
<iq from='juliet\@test.transport' 
    id='test'
    to='romeo\@montague.lit' 
    type='result'>
    <vCard xmlns='vcard-temp'>
        <PHOTO>
            <TYPE>image/png</TYPE>
            <BINVAL>$small_png_base64</BINVAL>
        </PHOTO>
    </vCard>
</iq>
EXPECTED
    is(clean_xml(output()), $expected,
       'get the expected vCard with the avatar in it.');
}

