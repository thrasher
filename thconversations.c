/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */

#include <glib.h>
#include "thrasher.h"
#include "thperl.h"

#include <purple.h>

#include "thconversations.h"

static void
thrasher_destroy_conversation(PurpleConversation *conv) {
    purple_debug_info("thrasher", "destroy conversation\n");
}

PurpleConversation*
thrasher_find_conversation_with_account(PurpleAccount* pa,
                                        const char* who) {
    PurpleConversation *conv;

    /* For testing and the initial build we're recreating a
     * conversation *every time*
     */
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM, who, pa);

    /* If not, create a new one */
    if (conv == NULL) {
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM, pa, who);
    }

    return conv;
}


static void
thrasher_write_conv(PurpleConversation *conv,
                    const char *who,
                    const char *alias,
                    const char *message,
                    PurpleMessageFlags flags,
                    time_t mtime) {
    PurpleAccount *account;
    account = purple_conversation_get_account(conv);

    /* libpurple throws write_conv on every recv and send */
    if (! (flags & PURPLE_MESSAGE_RECV))
        return;

    purple_debug_info("thrasher",
                      "Incoming message:\n");

    thrasher_wrapper_incoming_msg(thrasher_account_get_jid(account),
                                  who,
                                  alias,
                                  message,
                                  flags);

#ifdef TH_DEBUG
    purple_debug_info("thrasher",
                      "(%s) %s %s: %s\n",
                      purple_conversation_get_name(conv),
                      purple_utf8_strftime("(%H:%M:%S)", localtime(&mtime)),
                      who,
                      message);
#endif /* TH_DEBUG */
}

gboolean
thrasher_outgoing_chatstate(PurpleAccount* pa,
                            char* recipient,
                            PurpleTypingState chatstate) {
    g_return_val_if_fail(pa != NULL, FALSE);
    g_return_val_if_fail(recipient != NULL, FALSE);

    PurpleConversation *conv
        = thrasher_find_conversation_with_account(pa, recipient);
    g_return_val_if_fail(conv != NULL, FALSE);

    PurpleConnection *gc = purple_conversation_get_gc(conv);
    if (gc == NULL) {
        return FALSE;
    }

    unsigned int timeout = serv_send_typing(gc, recipient, chatstate);
    if (chatstate == PURPLE_TYPING) {
        /* TODO: if the prpl requests the typing notification be resent
         * after timeout, "normal" UIs can check get_type_again() each
         * keystroke. Should Thrasher add a event loop timeout? (and rm it
         * if the conv ended or the state was no longer typing...)
         */
        PurpleConvIm *im = PURPLE_CONV_IM(conv);
        purple_conv_im_set_type_again(im, timeout);
    }
    return TRUE;
}

/* incoming_chatstate_cb(pa, who):
 *
 * Account "pa" got a typing notification state change from the contact "who".
 */
static void
incoming_chatstate_cb(PurpleAccount *pa,
                      const char *who) {
    PurpleConversation *conv
        = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,
                                                who,
                                                pa);
    if (! conv
       || purple_conversation_get_type(conv) != PURPLE_CONV_TYPE_IM) {
        return;
    }

    PurpleConvIm *im = PURPLE_CONV_IM(conv);
    if (! im) {
        return;
    }

    thrasher_wrapper_incoming_chatstate(pa,
                                        who,
                                        purple_conv_im_get_typing_state(im));
}

static gpointer
thrasher_conversations_get_handle() {
    static int handle;
    return &handle;
}

static PurpleConversationUiOps thrasher_conv_uiops =
{
    NULL,                      /* create_conversation  */
    thrasher_destroy_conversation, /* destroy_conversation */
    NULL,                      /* write_chat           */
    NULL,                      /* write_im             */
    thrasher_write_conv,       /* write_conv           */
    NULL,                      /* chat_add_users       */
    NULL,                      /* chat_rename_user     */
    NULL,                      /* chat_remove_users    */
    NULL,                      /* chat_update_user     */
    NULL,                      /* present              */
    NULL,                      /* has_focus            */
    NULL,                      /* custom_smiley_add    */
    NULL,                      /* custom_smiley_write  */
    NULL,                      /* custom_smiley_close  */
    NULL,                      /* send_confirm         */
    NULL,
    NULL,
    NULL,
    NULL
};

void
thrasher_conversations_init(void) {
    purple_conversations_set_ui_ops(&thrasher_conv_uiops);

    purple_signal_connect(purple_conversations_get_handle(),
                          "buddy-typing",
                          thrasher_conversations_get_handle(),
                          PURPLE_CALLBACK(incoming_chatstate_cb),
                          NULL);
    purple_signal_connect(purple_conversations_get_handle(),
                          "buddy-typing-stopped",
                          thrasher_conversations_get_handle(),
                          PURPLE_CALLBACK(incoming_chatstate_cb),
                          NULL);
}
