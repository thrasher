#!/usr/bin/perl

use strict;
use warnings;

# This tests the migration functionality. It uses two test backends,
# then cmp_deeply's 'em to make sure everything made it over. What
# an amazing cheat. But it works...

use Test::More 'no_plan';
use Data::Dumper;
use Test::Deep;

BEGIN {
    print "This test is expected to print lots of extra "
        ."crap as a test of the dry run feature.\n";

    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::Backend::Test';
    use_ok 'Thrasher::Backend::Migrate', 'migrate';
}

my $source = Thrasher::Backend::Test->new;
my $dest = Thrasher::Backend::Test->new;
my $fresh = Thrasher::Backend::Test->new;

MIGRATE: {
    for my $test (['romeo@montague.lit', 'rom'],
                  ['test@bbb.com', 'bbb']) {
        my ($jid, $diff) = @$test;

        # register
        $source->register($jid,
                          {username => "user_$diff",
                           password => $diff});

        # mappings
        $source->store_username_mapping
            ($jid, "legacy_$diff", "legacy_$diff\@aim.transport");
        $source->store_username_mapping
            ($jid, "legacy2_$diff", "legacy2_$diff\@aim.transport");

        # roster
        $source->set_roster
            ($jid, {"legacy_$diff" => $source->subscribed,
                    "legacy2_$diff" => $source->want_subscribe});

        # misc
        $source->set_misc($jid, "key1", $diff);

        # avatars
        $source->set_avatar($jid, "legacy_$diff", "AVATAR");
    }

    migrate($source, $dest, dry_run => 1);
    cmp_deeply($dest, $fresh, 'dry run does nothing correctly');

    migrate($source, $dest);
    cmp_deeply($dest, $source, 'migration appears to work as expected');
}

