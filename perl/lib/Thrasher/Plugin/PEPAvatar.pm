package Thrasher::Plugin::PEPAvatar;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Plugin::PEPAvatar - implement PEP-based avatars, based on
whether or not the server supports PEP, with an easy interface for the
protocols to set avatars.

=head1 DESCRIPTION

This module uses ::PEP to advertise avatars in accordance with
XEP-0084. The intent is to expose a single function for a protocol
to support avatar setting, and to allow this module to do the rest.

So far, I've not actually figured out if I'm missing something, or
if none of the clients I use actually supports this. Might be
missing some features?

=cut

use Thrasher::Log qw(log);
use Thrasher::Constants qw(:all);
use Thrasher::Callbacks qw(:all);
use Thrasher::Plugin qw(:all);
use Thrasher::Plugin::PEP qw(pep_publish);
use Carp qw(longmess);

use Digest::SHA1 qw(sha1_hex);

# Regardless of the server's support for PEP, we support PEP avatars
register_plugin({features => [$NS_PEP_AVATAR,
                              $NS_PEP_AVATAR_METADATA],
                 callbacks => {avatar_changed => {PEPAvatar => \&avatar_update}}});

sub avatar_update {
    my $component = shift;
    my $user_jid = shift;
    my $legacy_jid = shift;
    my $raw_binary_data = shift;
    my $base64_data = shift;
    my $image = shift;

    my $avatar_atts = image_atts_except_id($image);
    $avatar_atts->{id} = sha1_hex($raw_binary_data);

    pep_publish($component, $legacy_jid, $NS_PEP_AVATAR_METADATA,
                [[[$NS_PEP_AVATAR_METADATA, 'info'],
                  {},
                  [
                   [[$NS_PEP_AVATAR_METADATA, 'item'],
                    {id => $avatar_atts->{id}},
                    [
                     [[$NS_PEP_AVATAR_METADATA, 'metadata'], {},
                      [[[$NS_PEP_AVATAR_METADATA, 'info'],
                       $avatar_atts, []]]
                      ]
                     ]]
                   ]]]);
    
    pep_publish($component, $legacy_jid, $NS_PEP_AVATAR,
                [[[$NS_PEP_AVATAR, 'items'], {}, 
                  [[[$NS_PEP_AVATAR, 'item'], 
                   {id => $avatar_atts->{id}},
                   [[[$NS_PEP_AVATAR, 'data'], {},
                     [$base64_data]]]]]
                  ]]);
}

sub image_atts_except_id {
    my $image = shift;

    my $atts = 
    {
        bytes => $image->Get('filesize'),
        height => $image->Get('base-rows'),
        width => $image->Get('base-columns'),
        type => 'image/png',
    };

    return $atts;
}

1;
