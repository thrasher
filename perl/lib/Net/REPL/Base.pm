package Net::REPL::Base;
use strict;
use warnings;

=pod

=head1 NAME

Net::REPL::Base - Utilities for REPL clients and servers.

=head1 DESCRIPTION

This contains the protocol implementation and various other simple
utilities.

The REPL protocol uses length/value strings. A value is encoded to one
of these strings by concatenating its 32-bit unsigned big-endian
length with the value as a bytestring.

=head2 Methods

=cut

use Data::Dumper;
use IO::Socket;

=head3 lv_receive()

Read and return one length/value encoded string from the file handle.

Dies if an error (like the socket closing unexpectedly) occurs during
the read call. Returns a partial string or undef if the read is merely
incomplete.

=cut

sub lv_receive {
    my ($self) = @_;

    my $total_length = unpack('N', $self->_read_fh(4));
    if (not defined($total_length)) {
        return;
    }
    # $self->debug('Incoming message:', $total_length);
    my $message = $self->_read_fh($total_length);
    return unpack('a*', $message);
}

sub _read_fh {
    my ($self, $length) = @_;

    my $data;
    my $result = sysread($self->{'fh'}, $data, $length);
    if (not defined($result)) {
        $self->debug("REPL PID $$ reading: $!")
    }
    return $data;

}

=head3 lv_send($message)

Encode $message as a length/value string and write it to the file handle.

=cut

sub lv_send {
    my ($self, $message) = @_;

    my $length = length($message);
    my $initial = pack('N', $length);
    $self->{'fh'}->send($initial . $message);
    $self->{'fh'}->flush();
}

=head3 debug(@words)

Write words to the debug log according to the current debug level.

The default implementation writes to STDERR and uses the debug level
as a boolean (i.e., 0 or other false values disable it).

=cut

sub debug {
    my ($self, @words) = @_;

    if ($self->{'debug'}) {
        print STDERR join(' ', 'REPL:', @words) . "\n";
    }
}

=head3 formatted_eval($code)

Eval the Perl $code and return the Dumper'd or serialized result.

=cut

# TODO: integrate with Devel::REPL
sub formatted_eval {
    my ($self, $input) = @_;

    my @output;
    do {
        local $SIG{'__DIE__'} = 'DEFAULT';
        @output = eval('sub { ' . $input . '}->();');
    };
    # TODO: should this also pass through warnings?
    if ($@) {
        return $@;
    }
    else {
        my $dumper = Data::Dumper->new(\@output);
        $dumper->Deparse(1);
        return $dumper->Dump();
    }
}

### Internal methods

sub _create_socket {
    my ($self, %socket_args) = @_;

    my $domain = $socket_args{'Domain'};
    if (not defined($domain)) {
        die('Socket "Domain" argument is mandatory');
    }

    my $pkg = $IO::Socket::domain2pkg[$domain];
    if (defined($pkg)) {
        eval("use $pkg;");
    }
    return IO::Socket->new(
        %socket_args,
    );
}

1;
