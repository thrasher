package Thrasher::Backend::Migrate;

use strict;
use warnings;

use base 'Exporter';

our @EXPORT_OK = qw(migrate migrate_from_spec);

use Data::Dumper;
use Thrasher;

sub migrate_from_spec {
    my $source_backend = shift;
    my $source_backend_specs = shift;
    my $dest_backend = shift;
    my $dest_backend_specs = shift;
    my @args = @_;


    $source_backend = "Thrasher::Backend::$source_backend";
    $dest_backend = "Thrasher::Backend::$dest_backend";
    
    {
        local $@;
        eval "use $source_backend;";
        die if $@;

        eval "use $dest_backend;";
        die if $@;
    }
    
    my $source = $source_backend->new($source_backend_specs);
    my $dest = $dest_backend->new($dest_backend_specs);

    migrate($source, $dest, @args);
}

sub migrate {
    my $source = shift;
    my $dest = shift;
    my %args = @_;

    my $dry_run = $args{dry_run};

    print "Preparing to migrate backend data...\n";

    if (! $args{'protocol_module'}) {
        die('No protocol_module to register; cannot get registration_items!');
    }
    eval("require $args{'protocol_module'};")
      or die $@;
    my $protocol = $args{'protocol_module'}->new({}, $dest);
    $dest->register_protocol($protocol);

    my $source_jids = $source->all_jids;

    for my $jid (@$source_jids) {
        print "Migrating $jid... ";
        local $@;
        eval {
            # Migrate registration
            my $registration = $source->registered($jid);
            if ($registration) {
                if ($dry_run) {
                    print "Would register $jid with: "
                        . Dumper($registration);
                } else {
                    $dest->register($jid, $registration);
                }
            }

            # Migrate the mappings
            my $mappings = $source->all_mappings($jid);
            if ($dry_run) {
                print "Got mappings to migrate: " . Dumper($mappings);
            } else {
                while (my ($legacy, $mapped_jid) = each %$mappings) {
                    # The source transport may have a different
                    # canonical form for public IM usernames. This may
                    # cause Thrasher to think $legacy was not already
                    # mapped and create a duplicate mapping (e.g.
                    # optional spaces for AIM).
                    $legacy = $protocol->process_remote_username($legacy);

                    $dest->store_username_mapping($jid, $legacy, $mapped_jid);
                }
            }

            # Migrate roster
            my $roster = $source->get_roster($jid);
            if ($dry_run) {
                print "Got roster to migrate: " . Dumper($roster);
            } else {
                $dest->set_roster($jid, $roster);
            }

            # Migrate misc, if any
            {
                local $@;
                eval {
                    my $all_misc = $source->all_misc($jid);
                    if ($dry_run) {
                        print "Got misc to migrate: "
                            . Dumper($all_misc);
                    } else {
                        while (my ($key, $value) = each %$all_misc) {
                            $dest->set_misc($jid, $key, $value);
                        }
                    }
                };
                # "If this errored for some reason other than
                # one end or the other not supporting misc,
                # propogate the error, otherwise eat it."
                if ($@ && $@ !~ /not implemented/) {
                    die;
                }
            }

            # Migrate the avatars.
            my $avatars = $source->all_avatars($jid);
            if ($dry_run) {
                print "Got avatars to migrate: " . Dumper(sort keys
                                                          %$avatars);
            } else {
                while (my ($legacy, $avatar) = each %$avatars) {
                    $dest->set_avatar($jid, $legacy, $avatar);
                }
            }
        };

        if ($@) {
            warn "While trying to migrate $jid, got error: $@";
        } else {
            print "done.\n";
        }
    }
}

1;
