#!/usr/bin/perl

use strict;
use warnings;

# Test the connection manager.

# When testing things that are based in time, it is necessary to be
# able to manipulate the time for testing purposes.
sub now {
    my $now = shift;
    $Thrasher::ConnectionManager::NOW = $now;
}

use Test::More 'no_plan';
use Test::Deep;
use Data::Dumper;

use Time::HiRes qw(time);

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::ConnectionManager', qw(:all);
    use_ok 'Thrasher::Component', qw(:all);
}

DECAYABLES: {
    # Test that decayables work the way I think they should
    # Get the "base time" from time to make sure we can handle
    # it as we expect
    my $base_time = time;

    now($base_time);

    my $halfer = decayable(16, .5);

    is($halfer->value, 16, "no decay means no decay");
    now($base_time + 60);
    is($halfer->value, 8, "decay after one minute as expected");

    $halfer->add(24);
    is($halfer->value, 32, "can add as expected");
    now($base_time + 120);
    is($halfer->value, 16, "additions stick around and still decay.");
}

my @scheduled;
my $execution_count = 0;

BEHAVIOR: {
    $Thrasher::ConnectionManager::scheduler = sub {
        push @scheduled, \@_;
    };

    my $closure = sub { $execution_count++; };

    my $base_time = time;
    now($base_time);

    my $previous_execution_count = 0;
    my $incremented = sub {
        my $desired = shift;
        my $message = shift;

        is($execution_count - $previous_execution_count,
           $desired, $message);

        $previous_execution_count = $execution_count;
    };

    my $scheduler_advance = sub {
        my $advance = shift;
        now($base_time + $advance);
        $base_time = $base_time + $advance;

        Thrasher::ConnectionManager::connection_executor();
    };

    # First connection immediately executed
    request_connect($closure);
    $incremented->(1, 'immediately connected the first time');
    is($Thrasher::ConnectionManager::hammering->value,
       1, 'correctly recorded the hammering value');
    
    for my $i (0..14) {
        request_connect($closure);
        $incremented->(1, 'still immediately connecting on attempt '
                       . ($i+2));
    }

    # I'm saving this so I can eyeball the results of 
    # running this, but it is not part of the test suite.
    EYEBALL_IT: {
        last EYEBALL_IT;
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        request_connect($closure);
        $incremented->(0, "finally stopped immediately connecting");
        
        is(scalar(@scheduled), 1,
           'only one scheduling call executed');
        
        $scheduler_advance->(10);
        $incremented->(6, "we connected several times before 10 seconds went by.");
        is(scalar(@scheduled), 2, 'scheduled again');
        
        request_connect($closure);
        request_connect($closure);
        is(scalar(@scheduled), 2, 'did not cause fresh schedule');
        
        # This isn't a realistic test of the timing, but it tests
        # that connection_executor correctly does as much as it
        # can. This can arise in some circumstances.
        $scheduler_advance->(30);
        $incremented->(5, "we connected a lot since time went by");
        
        $scheduler_advance->(100);
        $incremented->(0, "correctly flushed out the queue as expected");
        ok($Thrasher::ConnectionManager::hammering->value < .1,
           'sanity check on the hammering passes');
        
        # Let's check the behavior of the connection success and failure.
        cmp_deeply(schedule_request, [0, 0],
                   'right now, we get immediate execution');
        
        # Uh oh! Failure!
        connection_failure;
        my ($delay, $hammering) = @{schedule_request()};
        is($hammering, 0, 'not hammering');
        ok($delay < 15, 'not much delay');
        
        $scheduler_advance->(1);
        
        # Uh oh! A pattern of failure!
        connection_failure;
        ($delay, undef) = @{schedule_request()};
        ok($delay > 1, 'ready to panic');
        
        $scheduler_advance->(1);
        
        # And somebody made it through, strong evidence that everything
        # is working.
        connection_success;
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        
        $scheduler_advance->(1);
        
        connection_success;
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        
        print "Connection cut\n\n\n";
        connection_failure;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_failure;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_failure;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_failure;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_failure;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        
        print "Connection restored\n\n\n";
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
        connection_success;
        $scheduler_advance->(5);
        ($delay, undef) = @{schedule_request()};
        print "Delay: $delay\n";
    }
}

my $JID = transport_jid;

# Test the interaction with the component, in particular to
# verify that the component outputs the expected presence tags.
TEST_COMPONENT_INTERACTION: {
    my $comp = get_registered_comp;
    my $direct_connect =
        \$Thrasher::Component::WILL_BE_DIRECTLY_CONNECTED;

    $$direct_connect = 0;

    # Log in and verify we get a message about being delayed.
    $comp->xml_in(<<LOGIN);
<presence from='romeo\@montague.lit/orchard'
          to='$JID'/>
LOGIN
    
    my $expected = clean_xml(<<EXPECTED);
<presence from='test.transport' 
          to='romeo\@montague.lit' 
          type='unavailable'>
    <status>connection queued</status>
    <thrasher:connection-queued xmlns:thrasher='xmpp:x:thrasher:presence'/>
</presence>
EXPECTED

    is($expected, output, 
       "Connection correctly notifies the user that it is delayed");
}
