#!/usr/bin/perl

# Test that Thrasher::XMPPStreams works as expected.
use Test::More 'no_plan';
use Test::Deep;

use strict;
use warnings;

use Data::Dumper;
use XML::SAX;

BEGIN {
    use_ok 'Thrasher::XMPPStreamIn', qw(:all);
    use_ok 'Thrasher::Constants', qw(:all);
    use_ok 'Thrasher::Log', qw(:all);
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::XML', qw(:all);
}

# Verify the support code is working correctly
SUPPORT_CODE: {
    cmp_deeply(normalize_name("x"), 
               [$NS_CLIENT, 'x'],
               'normalizing works as expected');
    cmp_deeply(normalize_name(['', 'x']),
               [$NS_CLIENT, 'x']);
    cmp_deeply(normalize_name(['stream', 'x']),
               [$NS_STREAM, 'x']);
    cmp_deeply(normalize_name([$NS_STREAM, 'x']),
               [$NS_STREAM, 'x']);

    my $tag1 = normalize_name('x');
    my $tag2 = 'x';
    my $tag3 = ['stream', 'x'];
    my $tag4 = [$NS_STREAM, 'x'];
    is(same_name($tag1, $tag2), 1, 'same names correctly compares names');
    is(same_name($tag1, $tag1), 1);
    is(same_name($tag2, $tag2), 1);
    is(same_name($tag3, $tag3), 1);
    is(same_name($tag3, $tag4), 1);
    is(same_name($tag4, $tag4), 1);
    is(same_name($tag1, $tag3), '');
    is(same_name($tag2, $tag3), '');
}

# Verify that XML is handled in the expected manner
XML_TESTING: {
    my $s = get_parser();
    is(ref($s), 'Thrasher::XMPPStreamIn', 'obj constructed');

    my $messages = $s->parse(<<STREAM);
<stream:stream 
  to='example.com'
  xmlns='jabber:client'
  xmlns:stream='http://etherx.jabber.org/streams'
  stream:moo="test"
  moo="test2"
  version='1.0'><stream:stream xmlns:stream="test" /></stream:stream>

STREAM

    cmp_deeply($messages, 
               [
                [
                 [$NS_STREAM, 'stream'],
                 {'{}version' => '1.0',
                  "{$NS_STREAM}moo" => 'test',
                  '{}to' => 'example.com',
                  '{}moo' => 'test2'},
                 []
                 ],

                [
                 ['test', 'stream'],
                 {},
                 []
                 ]
             ]);

    $s = get_parser();
    $messages = $s->parse(<<'STREAM');
<stream:stream
  to='example.com'
  xmlns='jabber:client'
  xmlns:stream='http://etherx.jabber.org/streams'>

<iq type="set" to="moo">
  <forbidden xmlns="stream-not-totally-awesome">
    <yo>queiro taco bell?</yo>
  </forbidden>
</iq>

<message to="moo@moo.com/Moo" from="baa@baa.com/Baa">
  <body>You &gt; make me smooth.</body>
  <xhtml xmlns="oh.some.xhtml.ns.thingy">You &gt; m<i>a</i>ke me smooth.</xhtml>
  <stream:special xmlns:stream="specially.for.you">
    <stream:termination />
  </stream:special>
</message>

STREAM

    cmp_deeply($messages, 
               [[[$NS_STREAM, 'stream'],
                 {'{}to' => 'example.com'},
                 []],
                [[$NS_CLIENT, 'iq'],
                 {'{}type' => 'set',
                  '{}to' => 'moo'},
            ['
  ',
             [['stream-not-totally-awesome',
               'forbidden'],
              {},
              ['
    ',
               [['stream-not-totally-awesome', 'yo'],
                {},
                ['queiro taco bell?']
                ],
               '
  ']],
              '
']],
          [[$NS_CLIENT, 'message'],
            {'{}from' => 'baa@baa.com/Baa',
             '{}to' => 'moo@moo.com/Moo'},
            ['
  ',
             [[$NS_CLIENT, 'body'],
              {},
              ['You > make me smooth.']],
             '
  ', 
             [['oh.some.xhtml.ns.thingy', 'xhtml'],
              {},
              ['You > m',
               [['oh.some.xhtml.ns.thingy', 'i'],
                {},
                ['a']
                ],
               'ke me smooth.'
               ]],
              '
  ',
              [['specially.for.you', 'special'],
               {},
               [
                  '
    ',
                [['specially.for.you', 'termination'],
                 {},
                 []
                 ],
                '
  '
                ]],
              '
'
            ]]], 
               'xml message parsing works as designed.');
    
}

EXTRACTING: {
    my $test_element = [$NS_CLIENT, 'message'];
    
    # degenerate case
    cmp_deeply(extract(undef, []), [],
               'degenerate extraction case works');

    # element match fail
    dies {
        extract([[$NS_CLIENT, 'message'], undef, undef],
                [[$NS_STREAM, 'message'], undef, undef]);
    } 'namespace mismatch detected';
    dies {
        extract([[$NS_CLIENT, 'message'], undef, undef],
                [[$NS_CLIENT, 'iq'], undef, undef]);
    } 'element name mismatch detected';

    # attribute match fail
    dies {
        extract([$test_element, { a => 'b' }, undef],
                [[$NS_CLIENT, 'test'], {'{}a' => 'c'}, undef]);
    } 'attribute mismatch detected.';

    # Attribute matching works when the pattern just gives
    # a straight attribute name with no preceding {} when
    # the attribute is in the default namespace.
    ok(extract([$test_element, { a => 'b' }, undef],
               [$test_element, { '{}a' => 'b' }, undef]),
       "don't have to provide default namespace specs for atts");

    # children mismatch fail
    dies {
        extract([$test_element, undef, ['x']],
                [$test_element, undef, []]);
    };
    # children match succeed
    cmp_deeply(extract([$test_element, undef, 'x'],
                       [$test_element, undef, ['y', 'x', []]]),
               {}, 'simple children match works');
    
    # test placeholder
    cmp_deeply(extract([save("element"), undef, undef],
                       [[$NS_CLIENT, 'message'],
                        {},
                        []]),
               {element => [$NS_CLIENT, 'message']},
               'non-recursive placeholder works');
    
    # test sub placeholder
    cmp_deeply(extract([save_sub('element',
                                 sub { my $x = shift; $x->[0] .=
                                           'eee'; return $x; }),
                        undef, undef],
                       [[$NS_CLIENT, 'message'], {}, []]),
               {element => [$NS_CLIENT.'eee', 'message']},
               'extracting with sub processing works.');

    # test match placeholder
    cmp_deeply(extract([[$NS_CLIENT, 'message'], 
                        undef, save_match([[$NS_CLIENT, 'body'], 
                                           undef, save('body')])],
                       [[$NS_CLIENT, 'message'],
                        {}, ['   ',
                             [[$NS_CLIENT, 'body'], {},
                               ['hizzle']],
                             'schmoo']
                        ]),
               {body => 'hizzle'},
               'extracting submatches works as expected');

    # test take
    # simply doesn't die
    extract([take('element'), undef, undef],
            [[$NS_CLIENT, 'message'], {}, []],
            {element => [$NS_CLIENT, 'message']});

    extract([[$NS_COMPONENT, 'handshake'], {}, []],
            [[$NS_COMPONENT, 'handshake'], {}, []]);

    dies {
        extract([take('element'), undef, undef],
                [[$NS_CLIENT, 'notamessage'], {}, []],
                {element => [$NS_CLIENT, 'message']});
    } 'Can pass in stuff to fill in placeholders.';

    extract([undef, {test => save('x', 1)}, undef],
	    [[$NS_COMPONENT, 'test'], {}, []]);
    pass("Saving can be optional.");

    extract([undef, undef, save_match('x', [undef, {x => 1}, []], 1)],
	    [[$NS_COMPONENT, 'test'], {}, []]);
    pass("save_matching can be optional");

    # Recursive extracting: test that recursive extraction works as
    # intended
    my $XML = [[$NS_COMPONENT, 'top'], {},
               [[[$NS_COMPONENT, 'middle'], {'{}x' => 1},
                 [[[$NS_COMPONENT, 'bottom'], {}, []]]]]];
    my $results = recursive_extract
        ($XML, 
         [[undef, 'top'], undef, save_match('rec', [[undef, 'middle']])],
         [undef, {x => save('moo')}, save_match('rec', [[undef, 'bottom']])]
        );
    is($results->{moo}, 1, 'found the attribute');
    is($results->{rec}->[0]->[1], 'bottom',
       'recurses as expected');
}

# These are packets that we encountered during testing that
# weren't handled correctly
REAL_WORLD_TESTS: {
    # This is a message with typing notifications before the body,
    # where the body is the word "menu", along with the XHTML.
    my $message_with_typing = [
          ['jabber:component:accept', 'message'],
          {
            '{}type' => 'chat',
            '{}id' => 'purplea511de5f',
            '{}from' => 'jbowers@barracudanetworks.com/Kopete',
            '{}to' => 'gossipingabby@aim.transport'
          },
          [
            [['jabber:x:event', 'x'],
              {},
              [
                [['jabber:x:event', 'composing'],
                  {},
                  []
                ]
              ]
            ],
            [
              ['jabber:component:accept', 'body'],
              {},
              ['menu']
            ],
            [
              ['http://jabber.org/protocol/xhtml-im', 'html'],
              {},
              [
                [['http://www.w3.org/1999/xhtml', 'body'],
                  {},
                  ['menu']
                ]
              ]
            ]
          ]
        ];

    my $match = [[$NS_COMPONENT, 'message'],
                 {to => save("to"),
                  from => save("from"),
                  type => save("type")},
                 save_match('body',
                            [[undef, 'body'], undef, undef])];

    my $result = extract($match, $message_with_typing);
    cmp_deeply($result->{body}, 
               [['jabber:component:accept', 'body'],
                {}, ['menu']],
               "body can be extracted even when not first");

    my $file_tag = 
        [
         [
          'http://jabber.org/protocol/si/profile/file-transfer',
          'file'
         ],
         {
             '{}size' => '3684376',
             '{}name' => 'aim.capture.pkts'
         },
         [
          ' ',
          [
           [
            'http://jabber.org/protocol/si/profile/file-transfer',
            'desc'
           ],
           {},
           [
            'oeioei'
           ]
          ],
          ' ',
          [
           [
            'http://jabber.org/protocol/si/profile/file-transfer',
            'range'
           ],
           {},
           []
          ],
          ' '
         ]
        ];

    $result = recursive_extract
        ($file_tag,
         [undef, undef, 
          save_match('rec', [[undef, 'desc'], undef, undef])],

         [undef, undef, save_sub('desc', \&text_extractor)]);

    cmp_deeply($result->{desc}, ['oeioei']);
}
