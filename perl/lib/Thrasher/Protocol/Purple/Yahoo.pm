package Thrasher::Protocol::Purple::Yahoo;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Purple::Yahoo - protocol for connection to Yahoo
Instant Messenger.

=cut

use base "Thrasher::Protocol::Purple";

use Thrasher::Log qw(log debug);

sub prpl {
    return "prpl-yahoo";
}

sub registration {
    my ($self, $jid, $registration_info) = @_;

    my $username = $registration_info->{'username'} || '';
    if ($username =~ /\@yahoo.com$/) {
        # FIXME: Some sources suggest this won't work for all IDs with
        # @yahoo.com emails, but our users seem to like it this way.
        # We should really do this only as a fallback if Yahoo gives
        # us a 1013 with the @yahoo.com username.
        debug("Suspiciously emailish Yahoo ID $username during registration.");
        $registration_info->{'username'} =~ s/\@yahoo.com$//;
    }

    # If the username has trailing whitespace, the yahoo prpl starts
    # an asynchronous connection and never comes back with either
    # _connection or _connection_error! (reproducible in pidgin.)
    #
    # Note: Registering with a usernames with internal whitespace
    # already returns an invalid username error.
    $registration_info->{'username'} =~ s/^\s+|\s+$//g;

    return $self->SUPER::registration($jid, $registration_info);
}

sub name { "Yahoo" }
sub identifier { "yahoo" }
sub gateway_prompt { "Enter Yahoo Username:" }
sub gateway_desc { "Enter Yahoo Username:" }

sub valid_id {
    my $self = shift;
    my $id = shift;

    # Valid id information based on the registration screen's messages.
    return $id =~ /^[a-z][a-z_0-9]*(?:\.[a-z_0-9]*)?/i;
}



1;
