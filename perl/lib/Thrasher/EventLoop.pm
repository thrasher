package Thrasher::EventLoop;
use strict;
use warnings;

use Thrasher::Log qw(log debug);

=pod

=head1 NAME

Thrasher::EventLoop - the 'default' event loop for Thrasher Bird

=head1 DESCRIPTION

The trickiest part of Thrasher is that it needs to manage both
connections to the protocols, and talking to the XMPP server
itself. You can't use blocking connections on both.

This both specifies an interface, and a default event loop
based on ignoring the transport entirely, which actually works
if your "transport" is a pure perl module that never has long-
running components.

The Event Loop is created by the main Thrasher routine while it is
configuring all the pieces. Event loops are tied to protocols 
pretty intimately, so the Protocol you are using has responsibility 
for controlling what event loop you are using, so the Protocol
can freely refer directly to advanced aspects of the 

=cut

use Thrasher::Log qw(log);

use base 'Exporter';

our @EXPORT_OK = qw(IN OUT);
our $IN = 1;
our $OUT = 2;

sub new {
    my $class = shift;
    my $self = {};
    
    $self->{protocol} = shift;
    $self->{socket} = shift;
    $self->{component} = shift;

    bless $self, $class;
    return $self;
}

sub go {
    my $self = shift;

    # This test event loop just assumes the protocol is a 
    # 'test' protocol and can be ignored, so despite the documentation
    # in this case we DO have exactly one socket to read, preventing
    # the need for fancy processing.
    while (defined(my $val = $self->{socket}->read)) {
        debug("IN: $val");
        $self->{component}->xml_in($val);
    }

    $self->{socket}->close;
}

# Returns some token that can be passed to 'cancel_schedule'
# Timeout is in milliseconds
# If the closure returns a true value, it should be rescheduled 
# again in the given timeout, otherwise it should stop.
sub schedule {
    my $self = shift;
    my $closure = shift;
    my $timeout = shift;

    log("Thrasher::EventLoop laughs in the face of your attempt to "
        ."schedule an event!");
    local $@;
    eval { $closure->(); };
    log("Error in scheduled callback: $@") if $@;
}

# fd must be an actual file descriptor
# Directions is a bit mask of $IN and $OUT
# Return true to continue the watch, false to cancel it.
sub add_fd_watch {
    my $self = shift;
    my $fd = shift;
    my $directions = shift;
    my $closure = shift;

    die "Thrasher::EventLoop laughs in the face of your attempt "
        ."to watch a file descriptor! You need a real event loop.";
}

sub remove_fd_watch {
    my $self = shift;
    my $opaque_token = shift;

    die "Thrasher::EventLoop laughs in the face of your attempt "
        ."to cancel a file descriptor watch! Hey, you shouldn't "
        ."even be able to get here...";
}

# Idle handler means "do a task after all other stuff in this 
# event loop run is done". The only thing Thrasher uses this for 
# is to properly shut the system down after a SIGINT or something.
# Simpler event loops can implement this as simply immediately
# executing the passed-in closure, but many event loops will become
# offended if you do that.
sub execute_on_idle {
    my $self = shift;
    my $closure = shift;

    die "Thrasher::EventLoop laughs in the face of your attempt to "
        ."schedule something for idle execution!";
}

# This should quit your event loop, preferably more politely than 
# with "exit()", since we may want to do more stuff later.
sub quit {
    my $self = shift;
    
    die "Thrasher::EventLoop laughs in the face of your quit!";
}

1;
