/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include "thrasher.h"
#include "thperl.h"

#include <ft.h>
#include <server.h>

#include "thft.h"

/* GENERAL NOTES:
 *
 * The Thrasher FT ID indexes file transfer structs by an integer,
 * which can be easily passed back to Perl as a token it can use to
 * manipulate the file transfer as needed. A value <= 0 indicates an
 * error. There isn't much value to having Perl manipulate the structs
 * directly, so the Perl-side FT object is entirely separate from the
 * libpurple xfer struct.
 *
 * SENDING:
 *
 * 1. Perl-side accepts the file and negotiates a SOCKS connection.
 *    Stash parameters by *stream* ID for this step only; everything
 *    else uses the Thrasher-generated ID from step 2.1.
 *
 * 2. Once Perl-side completes negotiation, it starts libpurple-side
 *    setup with thrasher_send_file().
 *
 * 2.1. Stash xfer parameters in the account until xfer is constructed.
 *
 * 2.2. Call server.c:serv_send_file() should end up calling
 *
 * 2.3. prpl:send_file, which calls
 *
 * 2.4. purple_xfer_new(), which calls
 *
 * 2.4. thrasher_new_xfer callback in ui_ops to copy parameters out of
 *      stash and store the xfer in Thrasher's C-side ID lookup table.
 *
 * 2.5. thrasher_send_file() returns the ID to Perl for building the
 *      lookup table for Perl-side FT data.
 *
 * 3. Perl-side connects to SOCKS and sets a watch on the FD to
 *    trigger ui_read.
 *
 * 4. Loop of ft.c:do_transfer() calling ui_read/data_not_sent
 *    callbacks until canceled or complete.
 *
 * RECEIVING:
 * 1. thrasher_new_xfer() stores the xfer by ID.
 *
 * 2. recv_request_cb() hands off to Perl-side to do negotiation.
 *    Eventually, a Perl-side event accepts or denies the FT with
 *    thrasher_xfer_recv_request_responder().
 *
 * 3. Loop of ft.cdo_transfer() calling ui_write. Perl-side stashes
 *    data if it is unable to keep up forwarding data to the Thrasher
 *    user. ui_write hides such problems (even removing the FD watch
 *    and taking over the loop if necessary) from its caller because
 *    libpurple 2.6 will cancel the FT instead of slowing down.
 */


/*
 * Set up the mapping and associated functions
 */

size_t next_file_transfer_id = 1;
/* guint id_ptr => PurpleXfer* xfer */
GHashTable *id_to_xfer = NULL;

void free_id (gpointer id)
{
    free(id);
}

size_t get_next_file_transfer_id () {
    return next_file_transfer_id++;
}

int store_xfer(PurpleXfer* xfer) {
    guint id = get_id_by_xfer(xfer);
    g_hash_table_insert(id_to_xfer, (void*)id, xfer);
    purple_debug_info("thrasher ft",
                      "%d: stored\n",
                      id);
    return id;
}

// Both of these functions assume the transfer is present and
// will segfault if it is not, so FIXME: Soften this. This
// is cool for debugging but production ought to be more graceful.
PurpleXfer* get_xfer_by_id(guint id)
{
    return g_hash_table_lookup(id_to_xfer, (void*)id);
}

guint get_id_by_xfer(PurpleXfer* xfer)
{
    Thrasher_Xfer_UI_Data *ui_data = xfer->ui_data;
    g_return_val_if_fail(ui_data, 0);
    g_return_val_if_fail(ui_data->id >= 1, 0);
    return ui_data->id;
}

// Correctly remove the transfer from our hash tables.
void thrasher_remove_xfer(PurpleXfer* xfer)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_remove_xfer\n",
                      id);
    g_hash_table_remove(id_to_xfer, (void*)id);
}

/*
 * Callbacks for transfers
 */

/* thrasher_xfer_recv_request_cb tells Perl side to send an offer IQ
 * for xfer to the transport user...
 */
void
thrasher_xfer_recv_request_cb(PurpleXfer *xfer,
                              gpointer data) {
    guint id = get_id_by_xfer(xfer);
    const char* remote_filename = purple_xfer_get_filename(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_request_cb with filename %s\n",
                      id,
                      remote_filename);

    /* Can accept/deny this xfer if this signal CB either sets
     * local_filename and status or somehow eventually results in
     * purple_xfer_request_* being called.
     *
     * Otherwise, would have to implement a request_action in
     * thrasher_request_uiops that could tell whether the request was
     * for an FT, cert, etc. Based on matching the human-readable
     * message? Ugh.
     */
    thrasher_wrapper_ft_recv_request(xfer, remote_filename);
}

/* ...and Perl-side calls thrasher_xfer_recv_request_responder when
 * the response to the offer arrives (first setting up the FT proxy if
 * accepting).
 */
void
thrasher_xfer_recv_request_responder(guint id,
                                     guint accept) {
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_request_responder(%d)\n",
                      id,
                      accept);
    PurpleXfer *xfer = get_xfer_by_id(id);
    g_return_if_fail(xfer);

    const char* remote_filename = purple_xfer_get_filename(xfer);
    if (accept) {
        /* local_filename doesn't matter--Thrasher user's XMPP client
         * maintains its own destination path--but libpurple checks it
         * before proceeding.
         */
        purple_xfer_request_accepted(xfer, remote_filename);
    }
    else {
        purple_xfer_request_denied(xfer);
    }
}

void thrasher_xfer_recv_accept_cb(PurpleXfer *xfer,
                                         gpointer data)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_accept_cb\n",
                      id);
}

void thrasher_xfer_recv_start_cb(PurpleXfer *xfer,
                                        gpointer data)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_start_cb\n",
                      id);
    purple_xfer_ui_ready(xfer);
}

void thrasher_xfer_recv_cancel_cb(PurpleXfer *xfer,
                                         gpointer data)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_cancel_cb\n",
                      id);
    thrasher_wrapper_ft_recv_cancel(id);
}

void
thrasher_xfer_recv_complete_cb(PurpleXfer *xfer,
                               gpointer data) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_recv_complete_cb\n",
                      id);
    thrasher_wrapper_ft_recv_complete(id);
}

void thrasher_xfer_send_accept_cb(PurpleXfer *xfer,
                                         gpointer data)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_send_accept_cb\n",
                      id);
}

void
thrasher_xfer_send_start_cb(PurpleXfer *xfer,
                            gpointer data) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_send_start_cb\n",
                      id);

    thrasher_wrapper_ft_send_start(id);
}

void thrasher_xfer_send_cancel_cb(PurpleXfer *xfer,
                                         gpointer data)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_send_cancel_cb\n",
                      id);
    thrasher_wrapper_ft_send_cancel(id);
}

void
thrasher_xfer_send_complete_cb(PurpleXfer *xfer,
                               gpointer data) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_send_complete_cb\n",
                      id);
    thrasher_wrapper_ft_send_complete(id);
}

/*
 * UI Ops for transfers
 */

void thrasher_new_xfer(PurpleXfer *xfer) {
    purple_debug_info("thrasher ft",
                      "new xfer!\n");

    if (purple_xfer_get_type(xfer) == PURPLE_XFER_RECEIVE) {
        Thrasher_Xfer_UI_Data* ui_data = g_new(Thrasher_Xfer_UI_Data, 1);
        ui_data->id = get_next_file_transfer_id();
        xfer->ui_data = ui_data;
    }
    else if (purple_xfer_get_type(xfer) == PURPLE_XFER_SEND) {
        PurpleAccount* account = purple_xfer_get_account(xfer);
        GHashTable* pending_send_file
            = thrasher_account_get_pending_send_file(account);
        const char* who = purple_xfer_get_remote_user(xfer);
        Thrasher_Xfer_UI_Data* ui_data = g_hash_table_lookup(pending_send_file,
                                                             who);
        g_return_if_fail(ui_data);
        g_hash_table_remove(pending_send_file, who);
        xfer->ui_data = ui_data;

        purple_xfer_set_message(xfer, ui_data->desc);
        g_free(ui_data->desc);
        ui_data->desc = NULL;

        purple_xfer_set_local_filename(xfer, ui_data->filename);
        purple_xfer_set_filename(xfer, ui_data->filename);
        g_free(ui_data->filename);
        ui_data->filename = NULL;
        purple_xfer_set_size(xfer, ui_data->size);
        purple_debug_info("thrasher ft",
                          "%d: xfer filename now %s and size now %d\n",
                          ui_data->id,
                          xfer->local_filename,
                          xfer->size);
    }

    store_xfer(xfer);
}

void thrasher_destroy_xfer(PurpleXfer *xfer)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_destroy_xfer\n", id);

    thrasher_remove_xfer(xfer);

    free(xfer->ui_data);
    xfer->ui_data = NULL;
}

// Called to add a transfer to a UI we don't have
void thrasher_add_xfer(PurpleXfer *xfer)
{
    int id = get_id_by_xfer(xfer);
    if (id == -1) {
        id = store_xfer(xfer);
    }
    purple_debug_info("thrasher ft",
                      "%d: thrasher_add_xfer\n",
                      id);
    return;
}

// Called to update a UI we don't have
void thrasher_update_xfer_progress(PurpleXfer *xfer, double percent)
{
    guint id = get_id_by_xfer(xfer);
    percent *= 100;
    purple_debug_info("thrasher ft",
                      "%d: %0.2f%% complete\n",
                      id,
                      percent);
    return;
}

void thrasher_xfer_cancel_local(PurpleXfer *xfer)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_cancel_local\n", id);
    return;
}

void thrasher_xfer_cancel_remote(PurpleXfer *xfer)
{
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: thrasher_xfer_cancel_remote\n",
                      id);
}

gssize
thrasher_ui_write(PurpleXfer *xfer,
                  const guchar *buffer,
                  gssize size) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: asked to write %d bytes.\n",
                      id,
                      size);
    gssize written_sz = thrasher_wrapper_ft_write(id,
                                                  (const char*) buffer,
                                                  size);
    purple_debug_info("thrasher ft",
                      "%d: wrote %d bytes.\n",
                      id,
                      written_sz);

    /* HACK: For some reason, libpurple 2.6 doesn't scale the window
     * if write is incomplete like when reading--it cancels the
     * transfer entirely! So:
     * 1. Have Perl-side write stash the leftover data.
     * 2. Do our own window scaling here.
     * 3. Lie to libpurple and say write completed.
     * 4. Have Perl-side slip stashed data into next write.
     */
    if (written_sz < size) {
        xfer->current_buffer_size = MAX(xfer->current_buffer_size * 0.5,
                                        /* Very low because of above hackery. */
                                        128);
        if (written_sz == 0) {
            /* Extreme case: wasn't ready to write anything at all.
             * Perl-side will call ui_ready when caught up.
             */
            purple_input_remove(xfer->watcher);
            xfer->watcher = 0;
        }
    }
    return size;
}

gssize
thrasher_ui_read(PurpleXfer *xfer,
                 guchar **buffer,
                 gssize size) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: asked to read %d bytes.\n",
                      id,
                      size);
    gssize read_sz = thrasher_wrapper_ft_read(id, buffer, size);
    purple_debug_info("thrasher ft",
                      "%d: read %d bytes.\n",
                      id,
                      read_sz);
    return read_sz;
}

void
thrasher_data_not_sent(PurpleXfer *xfer,
                       const guchar *buffer,
                       gsize size) {
    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: asked to unread %d bytes.\n",
                      id, size);
    thrasher_wrapper_ft_data_not_sent(id,
                                      (const char*) buffer,
                                      size);
}

/*
 * Initialization for libpurple file transfers
 */

static gpointer
thrasher_xfer_get_handle()
{
    static int handle;
    return &handle;
}

/* thrasher_request_file() exists only to skip over
 * purple_xfer_choose_file_ok_cb() to purple_xfer_request_accepted().
 * ok_cb verifies the file exists on disk, but with Thrasher it never
 * does. Obviously, Thrasher need not prompt for a filename here.
 */
void *thrasher_request_file(const char *title, const char *filename,
                            gboolean savedialog, GCallback ok_cb,
                            GCallback cancel_cb,
                            PurpleAccount *account,
                            const char *who,
                            PurpleConversation *conv,
                            void *user_data) {
    PurpleXfer* xfer = user_data;
    purple_debug_info("thrasher ft",
                      "thrasher_request_file (%s -> %s)\n",
                      filename, who);

    if (purple_xfer_get_type(xfer) == PURPLE_XFER_RECEIVE) {
        purple_debug_info("thrasher ft",
                          "Should skip ask_accept/request_file"
                          " if receiving. Refer to"
                          " thrasher_xfer_recv_request_responder().\n");
        return NULL;
    }

    guint id = get_id_by_xfer(xfer);
    purple_debug_info("thrasher ft",
                      "%d: about to purple_xfer_request_accepted\n",
                      id);
    /* local_filename already set, but do want xfer_add and ops.init called. */
    purple_xfer_request_accepted(xfer,
                                 xfer->filename);

    /* no meaningful ui handle */
    return NULL;
}

static PurpleXferUiOps thrasher_xfer_uiops = {
    thrasher_new_xfer,
    thrasher_destroy_xfer,
    thrasher_add_xfer,
    thrasher_update_xfer_progress,
    thrasher_xfer_cancel_local,
    thrasher_xfer_cancel_remote,
    thrasher_ui_write,
    thrasher_ui_read,
    thrasher_data_not_sent,

    // 1 reserved
    NULL
};

void thrasher_xfer_init()
{
    /* thrasher_xfer_destroy() would release id_to_xfer... but that is
     * assumed to always coincide with process exit.
     */
    id_to_xfer = g_hash_table_new(g_direct_hash, g_direct_equal);

    purple_xfers_init();
    purple_xfers_set_ui_ops(&thrasher_xfer_uiops);

    void *ft_handle = purple_xfers_get_handle();
    gpointer thrasher_xfer_handle = thrasher_xfer_get_handle();

    purple_signal_connect(ft_handle,
                          "file-recv-request",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_recv_request_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-recv-accept",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_recv_accept_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-recv-start",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_recv_start_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-recv-cancel",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_recv_cancel_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-recv-complete",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_recv_complete_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-send-accept",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_send_accept_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-send-start",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_send_start_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-send-cancel",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_send_cancel_cb),
                          NULL);
    purple_signal_connect(ft_handle,
                          "file-send-complete",
                          thrasher_xfer_handle,
                          PURPLE_CALLBACK(thrasher_xfer_send_complete_cb),
                          NULL);
}


/*
 * Actually initiate a file transfer
 */
size_t
thrasher_send_file(PurpleAccount *account,
                   const char *who,
                   char* filename,
                   size_t size,
                   char* desc) {
    g_return_val_if_fail(account, 0);
    g_return_val_if_fail(who, 0);
    g_return_val_if_fail(filename, 0);
    g_return_val_if_fail(size, 0);
    g_return_val_if_fail(desc, 0);

    PurpleConnection *connection = thrasher_connection_for_account(account);
    if (!connection) {
        purple_debug_info("thrasher ft",
                          "Connection could not be found for account!?\n");
        return 0;
    }

    PurplePlugin *prpl = purple_connection_get_prpl(connection);
    if (! prpl) {
        purple_debug_info("thrasher ft",
                          "prpl could not be found for connection!?\n");
        return 0;
    }

    PurplePluginProtocolInfo *prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(prpl);
    if (! prpl_info) {
        purple_debug_info("thrasher ft",
                          "prpl_info could not be found for prpl!?\n");
        return 0;
    }

    if (prpl_info->can_receive_file && ! prpl_info->can_receive_file(connection,
                                                                     who)) {
        purple_debug_info("thrasher ft",
                          "prpl says %s can't receive a file.\n",
                          who);
        return 0;
    }

    /* From now on, assume the FT will either succeed or be canceled.
     *
     * This is the last chance to decide how to proceed until/if
     * libpurple calls us back. If we're wrong and the libpurple side
     * ignores (neither uses nor cancels) the xfer, the XMPP user will
     * be connected to a proxy that goes nowhere until the proxied
     * transfer "succeeds"....
     *
     * Because serv_send_file() returns void and thus can't be
     * checked, add checks as necessary above.
     */
    size_t id = get_next_file_transfer_id();

    /* TODO: separate structures for this backchannel and ui_data.
     * local_filename and size are redundant once there's an xfer for
     * this to be a ui_data of.
     */
    Thrasher_Xfer_UI_Data* ui_data = g_new(Thrasher_Xfer_UI_Data, 1);
    ui_data->filename = g_strdup(filename);
    ui_data->size = size;
    ui_data->id = id;
    ui_data->desc = g_strdup(desc);

    /* FIXME: better not have more than one FT to the same who at the
     * same time.... */
    GHashTable* pending_send_file
        = thrasher_account_get_pending_send_file(account);
    g_hash_table_insert(pending_send_file,
                        (void*)who,
                        ui_data);

    purple_debug_info("thrasher ft",
                      "%d: calling serv_send for file %s size %d to %s\n",
                      id,
                      filename,
                      size,
                      who);

    /* Passing NULL filename causes the prpl to not call
     * purple_xfer_request_accepted() which is mostly a noop because
     * Thrasher has a ui_read callback. Would be nice if
     *
     * 1. request_accepted generated a file-send-accept signal and
     *
     * 2. it was only called after the remote side had in fact accepted
     *
     * but neither is currently true in most prpls. Instead, Perl-side
     * always accepts but then cancel if no remote side appears.
     */
    serv_send_file(connection, who, NULL);

    return id;
}

