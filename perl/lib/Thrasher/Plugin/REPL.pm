package Thrasher::Plugin::REPL;
use strict;
use warnings;

use Thrasher::EventLoop;
use Thrasher::Log;
use Thrasher::Plugin qw(register_plugin);

### Plugin init
#
# Subclass that integrates itself into a Thrasher::EventLoop.
#
# Warning 1: Anyone who can connect to the REPL port can inject
# arbitrary Perl code into the Thrasher process. The only security
# measure currently used is binding only to the IPv4 loopback
# interface.
#
# Warning 2: A REPL iteration blocks the main loop, so it would be
# best to avoid long-lived computations!
use base qw(Net::REPL::Server);

our $repl_server;
sub integrate_repl {
    my ($component) = @_;

    $repl_server ||= Thrasher::Plugin::REPL->new(
        'event_loop' => $component->{'event_loop'},
        'debug' => 1,
        'socket_args' => {
            # path in abstract namespace instead of FS
            'Local' => "\x00"
              . '/Thrasher/REPL/' . $component->{'component_name'},
        },
    );
}

register_plugin({
    'callbacks' => {
        'main_loop' => { 'Integrate REPL' => \&integrate_repl },
    },
});

### REPL::Server callbacks

sub debug {
    my ($self, @words) = @_;

    if ($self->{'debug'}) {
        my $line = join(' ', @words);
        Thrasher::Log::debug($line, $self->{'debug'});
    }
}

sub cb_listen {
    my ($self) = @_;

    $self->SUPER::cb_listen();

    $self->{'event_loop'}->add_fd_watch(
        $self->{'server'}->fileno(),
        $Thrasher::EventLoop::IN,
        sub {
            $self->interact();
            return 1;
        }
    );
}

sub cb_connect {
    my ($self) = @_;

    $self->SUPER::cb_connect();

    $self->{'fh_watch_id'} =
      $self->{'event_loop'}->add_fd_watch(
          $self->{'fh'}->fileno(),
          $Thrasher::EventLoop::IN,
          sub {
              return $self->interact();
          }
    );
}

sub cb_disconnect {
    my ($self) = @_;

    $self->SUPER::cb_disconnect();

    if ($self->{'fh_watch_id'}) {
        $self->{'event_loop'}->remove_fd_watch($self->{'fh_watch_id'});
        $self->{'fh_watch_id'} = undef;
    }
}

1;
