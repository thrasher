package Net::REPL::Server;
use strict;
use warnings;

=pod

=head1 NAME

Net::REPL::Server - Eval + Socket = interactive development

=head1 DESCRIPTION

Perl processes running Server objects accept and run code
interactively from remote clients. Perl code is received over a
socket, eval'd, and the Dumper'd result(s) are sent back.

This class can be used as-is or superclassed to plug into an event
loop or provide additional capabilities like Devel::REPL integration
or serializing results so that usuable Perl data structures can be
exchanged bidirectionally.

=head2 Methods

=cut

use base qw(Net::REPL::Base);

use Data::Dumper;
use Socket qw(AF_UNIX);

=head3 new(argument => value...)

Arguments:

=over

=item C<socket_args>

Hashref of arguments for L<IO::Socket> configuration. Use C<Domain> to
select a socket class.

=item C<debug>

Debug level (refer to L<Net::REPL::Base>).

=back

=cut

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {
        'socket_args' => {},
        @_,
    };
    bless($self, $class);

    $self->debug('Server starting:', Dumper($self->{'socket_args'}));

    my $server = $self->_create_socket(
        'Domain' => AF_UNIX,
        'Proto' => 'SOCK_STREAM',
        'Reuse' => 1,
        'Listen' => 1,
        %{$self->{'socket_args'}},
    );
    if (not $server) {
        die("Couldn't start server: $!\n");
    }
    $self->{'server'} = $server;
    $self->cb_listen();

    $self->{'fh'} = undef;

    return $self;
}

sub DESTROY {
    my ($self) = @_;

    if ($self->{'server'}) {
        $self->{'server'}->close();
        $self->{'server'} = undef;
    }
    $self->close_fh();
}

sub close_fh {
    my ($self) = @_;

    if ($self->{'fh'}) {
        $self->{'fh'}->close();
        $self->{'fh'} = undef;
        $self->cb_disconnect();
    }
}

=head3 interact()

Run one iteration of the REPL. Returns true if the socket remains open
for further iterations.

Blocks until a line of input can be read from the client socket and
the result flushed out to it.

=cut

sub interact {
    my ($self) = @_;

    if (not $self->{'fh'}) {
        $self->{'fh'} = $self->{'server'}->accept();
        if ($self->{'fh'}) {
            $self->cb_connect();
        }
        else {
            # Accept didn't.
            $self->{'fh'} = undef;
            return;
        }
    }

    my $input = $self->lv_receive();
    if (not $input) {
        $self->close_fh();
        return 0;
    }
    my @output = $self->formatted_eval($input);
    my $output_s = "@output";
    $self->lv_send($output_s);
    return 1;
}

=head2 Callback Methods

=over

=item C<cb_listen()>

Called when the Server begins to listen for client connections.

=item C<cb_connect()>

Called whenever a client connection is accepted.

=item C<cb_disconnect()>

Called whenever a client disconnects.

=back

=cut

sub cb_listen {
    my ($self) = @_;
    $self->debug("Server PID $$ listening.");
}

sub cb_connect {
    my ($self) = @_;

    if (not $self->{'fh'}) {
        return;
    }
    my $client_details = '';
    if ($self->{'fh'}->can('peerhost')) {
        $client_details = ' '
          . $self->{'fh'}->peerhost()
          . ':'
          . $self->{'fh'}->peerport();

    }
    $self->debug("Server PID $$ connected" . $client_details);
}

sub cb_disconnect {
    my ($self) = @_;
}

1;
