package Thrasher::Backend::Test;
use strict;
use warnings;

use base 'Thrasher::Backend';
use Thrasher::Callbacks qw(callbacks);

=head1 NAME

Thrasher::Backend::Test - a test backed

=head1 DESCRIPTION

This module implements a test backend for Thrasher, suitable for
unit testing without reference to a real storage backend.

This module accepts a simple hashref mapping from JIDs to the
hashrefs returned by C<registered>, and stores them if you
register. Very simple.

=cut

sub new {
    my $class = shift;
    my $self = $class->SUPER::new();

    my $args = shift || {};

    my $registered = $args->{registered} || {};
    $self->{registered} = $registered;

    $self->{legacy_to_jid} = {};
    $self->{jid_to_legacy} = {};
    $self->{roster} = {};

    return $self;
}

sub registered { 
    my $self = shift;
    my $jid = shift;

    return $self->{registered}->{$jid};
}

sub register {
    my $self = shift;
    my $jid = shift;
    my $registration_info = shift;

    $self->{registered}->{$jid} = $registration_info;
}

sub remove {
    my $self = shift;
    my $jid = shift;
    
    delete $self->{registered}->{$jid};
    delete $self->{legacy_to_jid}->{$jid};
    delete $self->{jid_to_legacy}->{$jid};
}

sub retrieve_legacy_name_to_jid {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_name = shift;
    return $self->{legacy_to_jid}->{$user_jid}->{$legacy_name};
}

sub jid_to_legacy_name {
    my $self = shift;
    my $user_jid = shift;
    my $jid = shift;
    my $stored_name = $self->{jid_to_legacy}->{$user_jid}->{$jid};
    return $stored_name if defined($stored_name);

    # Otherwise, our best guess is "chop off everything after @".
    return $self->fake_up_a_legacy_name($user_jid, $jid);
}

sub jid_has_legacy_name {
    my $self = shift;
    my $user_jid = shift;
    my $jid = shift;
    return $self->{jid_to_legacy}->{$user_jid}->{$jid};
}

sub store_username_mapping {
    my $self = shift;
    my $user_jid = shift;
    my $legacy = shift;
    my $mapped_jid = shift;

    $self->{jid_to_legacy}->{$user_jid}->{$mapped_jid} = $legacy;
    $self->{legacy_to_jid}->{$user_jid}->{$legacy} = $mapped_jid;
}

sub all_mappings {
    my $self = shift;
    my $user_jid = shift;
    return $self->{legacy_to_jid}->{$user_jid};
}

sub set_avatar {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    $self->{avatars}->{$user_jid}->{$legacy_username} = shift;
}

sub get_avatar {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    return $self->{avatars}->{$user_jid}->{$legacy_username};
}

sub all_avatars {
    my $self = shift;
    my $user_jid = shift;
    return $self->{avatars}->{$user_jid};
}

sub all_jids {
    my $self = shift;
    my @jids = keys %{$self->{registered}};
    return \@jids;
}

sub get_roster {
    my $self = shift;
    my $user_jid = shift;
    return $self->{roster}->{$user_jid} || {};
}

sub set_roster {
    my $self = shift;
    my $user_jid = shift;
    my $new_roster = shift;

    $self->{roster}->{$user_jid} = $new_roster;
}

sub set_roster_user_state {
    my $self = shift;
    my $user_jid = shift;
    my $legacy_username = shift;
    my $state = shift;

    if ($state == $self->unsubscribed) {
        delete $self->{roster}->{$user_jid}->{$legacy_username};
    } else {
        $self->{roster}->{$user_jid}->{$legacy_username} = $state;
    }
}

sub set_misc {
    my $self = shift;
    my $user_jid = shift;
    my $key = shift;
    my $value = shift;

    $self->{misc}->{$user_jid}->{$key} = $value;
}

sub get_misc {
    my $self = shift;
    my $user_jid = shift;
    my $key = shift;

    return $self->{misc}->{$user_jid}->{$key};
}

sub all_misc {
    my $self = shift;
    my $user_jid = shift;

    return $self->{misc}->{$user_jid};
}

1;

