/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <stdlib.h>
#include "blist.h"
#include "thperl.h"
#include "thrasher.h"
#include "thblist.h"
#include "status.h"

#include <server.h>
#include <account.h>

void thrasher_buddy_status_changed (PurpleBuddy *buddy, PurpleStatus *old, 
                                    PurpleStatus *new);
void thrasher_buddy_signed_on (PurpleBuddy *buddy);
void thrasher_buddy_signed_off (PurpleBuddy *buddy);
void thrasher_buddy_added (PurpleBuddy *buddy);
void thrasher_buddy_add_request (PurpleAccount *account, const char *remote_user,
                           const char *id, const char *alias,
                           const char *message);
static void * thrasher_buddy_request_authorize
    (PurpleAccount *account,
     const char *remote_user,
     const char *id,
     const char *alias,
     const char *message,
     gboolean on_list,
     PurpleAccountRequestAuthorizationCb authorize_cb,
     PurpleAccountRequestAuthorizationCb deny_cb,
     void *user_data);
void thrasher_buddy_removed (PurpleBuddy *buddy);
static gpointer thrasher_blist_get_handle (void);
static char* status_text_for_buddy(PurpleBuddy *buddy, PurpleStatus *status);

static gboolean thrasher_buddy_request_authorize_cb1(gpointer data_in);
static gboolean thrasher_buddy_request_authorize_cb2(gpointer data_in);
static void no_dot_purple_no_op(void*);

static PurpleAccountUiOps thrasher_account_uiops =
{
    // notify added
    thrasher_buddy_add_request,

    // status changed
    NULL,

    // request add
    thrasher_buddy_add_request,

    // request_authorize
    thrasher_buddy_request_authorize,

    // close_account_request
    NULL,
    // save_node
    NULL,
    // remove_node
    NULL,
    // save_account
    NULL,
    // Reserved
    NULL
};

static PurpleBlistUiOps thrasher_blist_uiops = {
    /* new_list */
    NULL,
    /* new_node */
    NULL,
    /* show */
    NULL,
    /* update */
    NULL,
    /* remove */
    NULL,
    /* destroy */
    NULL,
    /* set_visible */
    NULL,
    /* request_add_buddy */
    NULL,
    /* request_add_chat */
    NULL,
    /* request_add_group */
    NULL,
    /* save_node */
    (void(*)(PurpleBlistNode*)) no_dot_purple_no_op,
    /* remove_node */
    (void(*)(PurpleBlistNode*)) no_dot_purple_no_op,
    /* save_account */
    (void(*)(PurpleAccount*)) no_dot_purple_no_op,
    /* reserved */
    NULL,
};

struct _PurpleStatusType
{
    PurpleStatusPrimitive primitive;

    char *id;
    char *name;
    char *primary_attr_id;

    gboolean saveable;
    gboolean user_settable;
    gboolean independent;

    GList *attrs;
};


struct _PurpleStatus
{
    struct _PurpleStatusType *type;
    PurplePresence *presence;

    const char *title;

    gboolean active;

    GHashTable *attr_values;
};

/**
 * @brief This is the callback for the buddy-status-changed signal
 * @param buddy PurpleBuddy struct
 * @param old (unused)
 * @param new @buddy new PurpleStatus
 */
void thrasher_buddy_status_changed (PurpleBuddy *buddy, 
                                    PurpleStatus *old,
                                    PurpleStatus *new)
{
    PurpleGroup *group;
    const char *group_name = NULL;
    const char *alias      = NULL;

    g_return_if_fail(buddy != NULL);
    g_return_if_fail(new != NULL);

    group = purple_buddy_get_group(buddy);

    if (group)
        group_name = purple_group_get_name(group);

    alias = purple_buddy_get_alias_only(buddy);

    char* message = status_text_for_buddy(buddy, new);
    if (! message) {
        message = g_strdup(purple_status_get_attr_string(new, "message"));
    }

    thrasher_wrapper_presence_in(thrasher_account_get_jid(buddy->account),
                                 purple_buddy_get_name(buddy),
                                 alias,
                                 group_name,
                                 purple_status_type_get_primitive(new->type),
                                 message);
    g_free(message);
}

/* Returns a string that becomes owned by the caller or NULL. */
static char*
status_text_for_buddy(PurpleBuddy *buddy,
                      PurpleStatus *status) {
    if (! buddy) {
        return NULL;
    }
    if (! buddy->account->gc) {
        return NULL;
    }

    const char *prpl_id = purple_account_get_protocol_id(buddy->account);
    if (prpl_id) {
        PurplePlugin *prpl = purple_find_prpl(prpl_id);
        if (prpl) {
            PurplePluginProtocolInfo *prpl_info
                = PURPLE_PLUGIN_PROTOCOL_INFO(prpl);
            if (prpl_info && prpl_info->status_text) {
                char* status_text = prpl_info->status_text(buddy);
                if (status && status_text
                           && purple_status_is_available(status)
                           && 0 == strcmp(prpl_id, "prpl-icq")
                           && 0 == strcmp(status_text,
                                          purple_status_get_name(status))) {
                    /* The oscar prpl's status_text tries to suppress
                     * a redundant "Available" message, but won't if
                     * it's in the message attribute instead of the
                     * status name. libpurple's initial log on does
                     * not set this in the message, but subsequent
                     * status changes will.
                     */
                    status_text = g_strdup("");
                }
                return status_text;
            }
        }
    }
    return NULL;
}
void thrasher_buddy_authorize (const char *jid,
                               const char *legacy_username) {
    g_return_if_fail(jid);
    g_return_if_fail(legacy_username);

    PurpleAccount *account = thrasher_get_account_by_jid(jid);
    g_return_if_fail(account);

    PurpleConnection *gc =
        thrasher_get_connection_by_account(account);
    g_return_if_fail(gc);

    purple_debug_info("thrasher", "Authorizing %s for jid %s (%s)\n",
                      legacy_username, jid,
                      purple_account_get_username(account));

    // defensive; probably only add_permit is necesary
    serv_rem_deny(gc, legacy_username);

    serv_add_permit(gc, legacy_username);
}

void thrasher_buddy_deauthorize (const char *jid,
                                 const char *legacy_username) {
    g_return_if_fail(jid);
    g_return_if_fail(legacy_username);

    PurpleAccount *account = thrasher_get_account_by_jid(jid);
    g_return_if_fail(account);

    PurpleConnection *gc =
        thrasher_get_connection_by_account(account);
    g_return_if_fail(gc);

    purple_debug_info("thrasher", "Deauthorizing %s for jid %s\n",
                      legacy_username, jid);

    // no corresponding deny, we don't have a reperesentation for that
    serv_rem_permit(gc, legacy_username);
}

void thrasher_buddy_signed_on (PurpleBuddy *buddy)
{
    PurplePresence *presence;

    presence = purple_buddy_get_presence(buddy);

    if (!presence)
        return;

    // libpurple uses this to populate some stuff about the
    // buddy, particularly useful for XMPP file transfers but
    // probably the cause of random other errors if we don't fill this
    // out.
    PurpleAccount* account = buddy->account;
    g_return_if_fail(account);
    PurpleConnection* connection = thrasher_connection_for_account(account);
    g_return_if_fail(connection);

    purple_debug_info("thblist",
                      "getting server info for %s\n\n---------\n\n",
                      buddy->name);
    serv_get_info(connection, buddy->name);

    /* Currently active status may be PURPLE_STATUS_UNAVAILABLE
     * (translates XMPP dnd) instead of just "available".
     */
    PurpleStatus *status = purple_presence_get_active_status(presence);

    thrasher_buddy_status_changed(buddy,
                                  NULL,
                                  status);
}

void thrasher_buddy_signed_off (PurpleBuddy *buddy)
{
    PurplePresence *presence;

    presence = purple_buddy_get_presence(buddy);
    
    if (!presence)
        return;

    thrasher_buddy_status_changed(buddy,
                                  NULL,
                                  purple_presence_get_status(presence, "offline"));
}

struct auth_request_cb_data {
    PurpleAccount *account;
    char *remote_user;
    char *alias;
    char *message;
    PurpleAccountRequestAuthorizationCb authorize_cb;
    void *user_data;
};

static void * thrasher_buddy_request_authorize
    (PurpleAccount *account,
     const char *remote_user,
     const char *id,
     const char *alias,
     const char *message,
     gboolean on_list,
     PurpleAccountRequestAuthorizationCb authorize_cb,
     PurpleAccountRequestAuthorizationCb deny_cb,
     void *user_data) {
    purple_debug_info("thrasher", "%s request_authorize from %s reached cb0\n",
                      remote_user,
                      thrasher_account_get_jid(account));

    struct auth_request_cb_data *data
        = g_malloc(sizeof(struct auth_request_cb_data));
    data->account = account;
    data->remote_user = g_strdup(remote_user);
    data->alias = g_strdup(alias);
    data->message = g_strdup(message);
    data->authorize_cb = authorize_cb;
    data->user_data = user_data;
    purple_timeout_add_seconds(2,
                               thrasher_buddy_request_authorize_cb1,
                               data);

    void *uihandle = NULL;
    return uihandle;
}

static gboolean
thrasher_buddy_request_authorize_cb1(gpointer data_in) {
    struct auth_request_cb_data *data = data_in;
    purple_debug_info("thrasher", "%s request_authorize from %s reached cb1\n",
                      data->remote_user,
                      thrasher_account_get_jid(data->account));
    if (data->authorize_cb) {
        data->authorize_cb(data->user_data);
    }
    else {
        /* FIXME: I don't think this actually happens? Any more? */
        purple_debug_info("thrasher", "authorize_cb sometimes NULL?!?\n");
    }
    purple_timeout_add_seconds(2,
                               thrasher_buddy_request_authorize_cb2,
                               data);
    return FALSE;
}

static gboolean
thrasher_buddy_request_authorize_cb2(gpointer data_in) {
    struct auth_request_cb_data *data = data_in;
    purple_debug_info("thrasher", "%s request_authorize from %s reached cb2\n",
                      data->remote_user,
                      thrasher_account_get_jid(data->account));
    /* FIXME: call through purple_blist_request_add_buddy() and blist ui_ops. */
    thrasher_buddy_add_request(data->account,
                               data->remote_user,
                               "",
                               data->alias,
                               data->message);
    g_free(data->remote_user);
    g_free(data->alias);
    g_free(data->message);
    g_free(data);
    return FALSE;
}

/* thrasher_buddy_add_request
 *
 * Triggered on a buddy-added signal, this allows us to push new subscriptions.
 *
 */
void thrasher_buddy_added (PurpleBuddy *buddy) {
    PurplePresence *presence = purple_buddy_get_presence(buddy);
    PurpleStatus *status     = purple_presence_get_active_status(presence);
    PurpleStatusType *type   = purple_status_get_type(status);
    
    const char *jid = thrasher_account_get_jid(buddy->account);
    const char *sender = purple_buddy_get_name(buddy);
    const guint status_int = purple_status_type_get_primitive(type);

    purple_debug_info("thrasher",
                      "buddy added to %s: %s\n",
                      jid, sender);

    thrasher_wrapper_subscription_add(jid, sender, status_int);
}


void thrasher_buddy_add_request (PurpleAccount *account, const char *remote_user,
                           const char *id, const char *alias,
                           const char *message)
{
    const char *jid = thrasher_account_get_jid(account);
    if (! alias) {
        alias = "(unknown)";
    }
    purple_debug_info("thrasher",
                      "legacy user %s aka %s added %s to roster\n",
                      remote_user, alias, jid);
    thrasher_wrapper_legacy_user_add_user(jid, remote_user);
}


void thrasher_buddy_removed (PurpleBuddy *buddy)
{
/*    printf("Removed buddy:\t[%s]\t[%s]\n", purple_buddy_get_name(buddy),
 *        purple_status_get_name( purple_presence_get_active_status( purple_buddy_get_presence(buddy) ) )
 *   );
 *
    PurplePresence *presence = purple_buddy_get_presence(buddy);
    PurpleStatus *status     = purple_presence_get_active_status(presence);
    PurpleStatusType *type   = purple_status_get_type(status);
    PurpleGroup *group       = purple_buddy_get_group(buddy);

    * Need to fire jid, sender, group_name, alias, and status back to perl */

    // jid = thrasher_account_get_jid(buddy->account)
    // sender = purple_buddy_get_name(buddy)
    // group_name = purple_group_get_name(group)
    // alias = purple_buddy_get_alias_only(buddy)
    // status = purple_status_type_get_primitive(type)
}


static gpointer
thrasher_blist_get_handle ()
{
    static int handle;
    return &handle;
}

static void
no_dot_purple_no_op(void *nope_not_here) {}

void thrasher_blist_init()
{
    purple_accounts_set_ui_ops(&thrasher_account_uiops);

    purple_blist_set_ui_ops(&thrasher_blist_uiops);

    purple_signal_connect(purple_blist_get_handle(),
                          "buddy-status-changed",
                          thrasher_blist_get_handle(),
                          PURPLE_CALLBACK(thrasher_buddy_status_changed),
                          NULL);

    purple_signal_connect(purple_blist_get_handle(),
                          "buddy-signed-on",
                          thrasher_blist_get_handle(),
                          PURPLE_CALLBACK(thrasher_buddy_signed_on),
                          NULL);

    purple_signal_connect(purple_blist_get_handle(),
                          "buddy-signed-off",
                          thrasher_blist_get_handle(),
                          PURPLE_CALLBACK(thrasher_buddy_signed_off),
                          NULL);

    // It looks like this only comes in for things already on our roster.
    purple_signal_connect(purple_blist_get_handle(),
                          "buddy-added",
                          thrasher_blist_get_handle(),
                          PURPLE_CALLBACK(thrasher_buddy_added),
                          NULL);

    /*
    purple_signal_connect(purple_blist_get_handle(),
                          "buddy-removed",
                          thrasher_blist_get_handle(),
                          PURPLE_CALLBACK(thrasher_buddy_removed),
                          NULL);
    */
}


