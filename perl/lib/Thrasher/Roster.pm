package Thrasher::Roster;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Roster - code for dealing with the roster

=head1 DESCRIPTION

Gateways need to recall the previous state of the rosters, in order
to tell upon the next connection whether the user has added people
to their roster or removed them by means other than the gateway. 
Ideally, we'd cheat and extract the roster directly from the user,
but we can't do that with a standard XEP, so we have to track it 
separately.

Rosters are represented in Thrasher as simple hashes. The keys
are the legacy names on the roster, and the values are the state
of the user on the roster, using the constants
Thrasher::Roster::subscribed, Thrasher::Roster::unsubscribed,
and Thrasher::Roster::want_subscribe (that is, the legacy user wants
to subscribe to the gateway user). These are subs that return
numbers representing those states. However, legacy users who
are unsubscribed should not come back as having the value 
"unsubscribed", but with no entry in the roster at all. (API
users can use "unsubscribed" to indicate that a user is being
unsubscribed.)

In addition, this module provides a "diff" function that takes 
two rosters, and describes the differences between them, in the
form of a hash reference that gives the before and after states.
Components can use this to determine what additional presence tags
need to be sent out to bring the user's roster up-to-date.

=cut

use base 'Exporter';
our @EXPORT_OK = qw(subscribed unsubscribed want_subscribe roster_diff);
our %EXPORT_TAGS = (all => \@EXPORT_OK,
                    constants => [qw(subscribed unsubscribed want_subscribe)]);

sub subscribed { 1 }
sub unsubscribed { 2 }
sub want_subscribe { 3 }

# Returns a hash containing the changes, where the key is the legacy
# name and the value is an array ref containing [old, new], where
# these are each one of 'subscribed', 'unsubscribed', or 'want_subscribe'.
sub roster_diff {
    my $roster1 = shift;
    my $roster2 = shift;

    my $result = {};

    while (my ($legacy_user, $value) = each %$roster1) {
        my $other_value = $roster2->{$legacy_user} || unsubscribed;

        if ($value != $other_value) {
            $result->{$legacy_user} = [$value, $other_value];
        }
    }

    while (my ($legacy_user, $value) = each %$roster2) {
        if (!exists($result->{$legacy_user})) {
            my $other_value = $roster1->{$legacy_user} || unsubscribed;
            
            if ($value != $other_value) {
                $result->{$legacy_user} = [$other_value, $value];
            }
        }
    }

    return $result;
}

1;
