package Thrasher::Plugin::EntityCapabilities;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Plugin::EntityCapabilities - add XEP-0115 "Entity
Capabilities" support.

=head1 DESCRIPTION

This plugin adds support for XEP-0115, "Entity Capabilities". This
automatically implements support for the capabilities as described
by the data in Thrasher::Plugin, and should need no further
interaction beyond "use"ing this module if needed. It automatically
annotates outgoing presence packets with the appropriate <c> tag.

=cut

use Thrasher::Plugin qw(:all);
use Thrasher::Constants qw(:all);
use Thrasher::Log qw(log);
use Thrasher::XML qw(:all);
use Thrasher::Callbacks qw(:all);

use Digest::SHA1 qw(sha1_base64);

use base 'Exporter';

our @EXPORT_OK = qw(has_feature has_identity_category);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

my $CURRENT_CAP_HASH;

# set-like hash of features
our %SERVER_FEATURES;
our @SERVER_IDENTITIES;

sub has_feature { $SERVER_FEATURES{$_[0]}; }

sub has_identity_category { grep { $_->[0] eq $_[0] } @SERVER_IDENTITIES }

# At the time that the plugins are updated, we don't have a component.
# Mark that updates are needed, and on the next presence callback
# (which will have a component), we'll compute it.
my $UPDATE_NEEDED = 1;

register_plugin({features => [$NS_CAPS],
                 callbacks => 
                   {plugins_changed => 
                     {entity_caps => \&update_needed},
                    presence_out =>
                     {entity_caps => \&presence_out},
                    connected => 
                     {entity_caps => \&query_server_capabilities}}});

# This currently has no support for extended service discovery forms
# since we have none.
sub hash_for_caps {
    my $identity_info = shift;
    my $supported_features = shift;

    my $s = join('/', @$identity_info) . '<';

    for my $service_discovery_identity (@$supported_features) {
        $s .= $service_discovery_identity . '<';
    }

    return sha1_base64($s);
}

sub update_caps {
    my $component = shift;
    my @identity_info = 
        Thrasher::Plugin::Basic::component_identity_info($component);
    my @supported_features = supported_features;
    $CURRENT_CAP_HASH = hash_for_caps
        (\@identity_info, \@supported_features);
}

sub update_needed { 
    $UPDATE_NEEDED = 1;
}

sub run_update {
    my $component = shift;
    update_caps($component);
    $UPDATE_NEEDED = 0;
}

# Add the caps tag to the presence
sub presence_out {
    my $component = shift;
    my $presence_tag = shift;

    if ($UPDATE_NEEDED) {
        run_update($component);
    }

    my $from = $presence_tag->[1]->{from};

    my $c = [[$NS_CAPS, 'c'], 
             {hash => 'sha-1',
              node =>
                  'http://developer.berlios.de/projects/thrasher/',
              ver => $CURRENT_CAP_HASH},
             []];
    push @{$presence_tag->[2]}, $c;

    return 1;
}

# When we connect, query the server for its capabilities, which will
# be necessary for other plugins
sub query_server_capabilities {
    my $component = shift;

    # Query the server for its capabilities.
    my $iq_packet = [[$NS_COMPONENT, 'iq'],
                     {type => "get", 
                      from => $component->{component_name},
                      to => $Thrasher::SERVER_NAME || ''}, # blank if testing
                     [[[$NS_DISCO_INFO, 'query'], {}, []]]];

    $component->iq_query($iq_packet, \&extract_server_caps);
}

sub extract_server_caps {
    my $component = shift;
    my $iq_params = shift;
    my $iq_packet = shift;

    if ($iq_params->{'type'} eq 'error') {
        log("extract_server_caps: error from $iq_params->{from}");
        failed('server_capabilities_detected');
        return;
    }

    my $query = $iq_params->{query};

    my ($identities, $features) = extract_disco_info($query);

    @SERVER_IDENTITIES = @$identities;
    %SERVER_FEATURES = map { $_ => 1 } @$features;

    succeeded('server_capabilities_detected');
}

1;
