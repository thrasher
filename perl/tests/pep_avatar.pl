#!/usr/bin/perl

use strict;
use warnings;

# Test that avatar publishing works as expected, especially as it
# tends to have a lot of corner cases w.r.t. image integrity.

use Test::More 'no_plan';
use Data::Dumper;

use MIME::Base64 qw(encode_base64 decode_base64);

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher', qw(:all);
    use_ok 'Thrasher::Component';
    use_ok 'Thrasher::Plugin', qw(:all);
    use_ok 'Thrasher::Plugin::PEPAvatar';
    use_ok 'Thrasher::Backend::Test';
    use_ok 'Thrasher::Callbacks', qw(:all);
    use_ok 'Thrasher::Avatar', qw(:all);
}

my $JID = transport_jid;

# Lie that we detected PEP, so this stuff actually comes out in XML
succeeded('pep_detected');

# The user wishes to set a good PNG avatar; ensure the correct XML
# goes out, and that when the server replies with the expected
# packets, the component correctly ignores it.
GOOD_PNG_AVATAR: {
    my $comp = logged_in_comp;

    set_avatar($comp, 'romeo@montague.lit', 'juliet@' . $JID, $small_png);

    my $expected_xml = clean_xml(<<EXPECTED);
<iq from='juliet\@test.transport' 
    id='id3' 
    type='set'>
  <pubsub xmlns='http://jabber.org/protocol/pubsub'>
    <publish xmlns='urn:xmpp:avatar:metadata'>
      <info>
        <item id='0765a59164829a0fe4eb243ffb14bdd64a3d8f5d'>
          <metadata>
            <info bytes='741' 
                  height='16' 
                  id='0765a59164829a0fe4eb243ffb14bdd64a3d8f5d' 
                  type='image/png' 
                  width='16'/>
          </metadata>
        </item>
      </info>
    </publish>
  </pubsub>
</iq>
<iq from='juliet\@test.transport' 
    id='id4' 
    type='set'>
  <pubsub xmlns='http://jabber.org/protocol/pubsub'>
    <publish xmlns='urn:xmpp:avatar:data'>
      <items>
        <item id='0765a59164829a0fe4eb243ffb14bdd64a3d8f5d'>
          <data>$small_png_base64</data>
        </item>
      </items>
    </publish>
  </pubsub>
</iq>
EXPECTED

    is(clean_xml(output), $expected_xml, 
       'expected pub_sub output on setting a PNG avatar');
}

GOOD_GIF_AVATAR: {
    my $comp = logged_in_comp;

    set_avatar($comp, 'romeo@montague.lit', "juliet\@$JID", $small_gif);

    my $expected = clean_xml(<<EXPECTED);
<iq from='juliet\@test.transport' 
    id='id7' 
    type='set'>
  <pubsub xmlns='http://jabber.org/protocol/pubsub'>
    <publish xmlns='urn:xmpp:avatar:metadata'>
      <info>
        <item id='$small_gif_png_sha1'>
          <metadata>
            <info bytes='$small_gif_png_len' 
                  height='55' 
                  id='$small_gif_png_sha1' 
                  type='image/png' 
                  width='44'/>
          </metadata>
        </item>
      </info>
    </publish>
  </pubsub>
</iq>
<iq from='juliet\@test.transport'
    id='id8' 
    type='set'>
  <pubsub xmlns='http://jabber.org/protocol/pubsub'>
    <publish xmlns='urn:xmpp:avatar:data'>
      <items>
        <item id='$small_gif_png_sha1'>
          <data>$small_gif_png_base64</data>
        </item>
      </items>
    </publish>
  </pubsub>
</iq>
EXPECTED

    is(clean_xml(output), $expected,
       'proper handling of the GIF case; converted to PNG');
}

BAD_DATA: {
    my $comp = logged_in_comp;

    set_avatar($comp, 'romeo@montague.lit', "juliet\@$JID", $binary_garbage);

    is(output, '', 'no XML output');
    logged("failed to determine a type for the image",
           'correct error message output by the PEP plugin');
}
