package Thrasher::Callbacks;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Callbacks - abstract out recurring callback patterns

=head1 DESCRIPTION

Some recurring callback patterns keep arising; this centralizes them
so everything can freely use them.

Two recurring patterns arise in the Jabber client code:

=over 

=item * 

A need to perform a given task only after some criteria has been met,
which may succeed or fail, which may need different actions depending
on whether there was success or failure, and which, after the 
condition has been passed, should be immediately acted upon.

For example, a directed presence at a legacy user should be held until
the user has finished logging in to the legacy service. If the log in
fails, the presence should be forgotten.

=item *

A need to call a callback with a certain name, which may be aborted
if a callback requests for the rest of the chain to be aborted.

=back

This centralizes these actions into a single central interface.

=cut

use base 'Exporter';

our @EXPORT_OK = qw(do_when succeeded failed event_superceded

                    callbacks register_callback unregister_callback);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

use Thrasher::Log qw(log);

use Data::Dumper;
use Carp qw(confess);

# $DO_WHEN{x} => [] means stacked up actions
# $DO_WHEN{x} => 1 means the action has succeeded
# $DO_WHEN{x} => 0 means the action failed.
# $DO_WHEN{x} => undef means no callbacks registered
my %DO_WHEN;

sub do_when {
    my $action_name = shift;
    my $success_action = shift;
    my $failure_action = shift;

    my $do_when_data = $DO_WHEN{$action_name};
    if (!defined($do_when_data)) {
        $DO_WHEN{$action_name} = [];
        return do_when($action_name, $success_action, $failure_action);
    } elsif (ref($do_when_data) eq 'ARRAY') {
        my $spec = {success => $success_action};
        if ($failure_action) {
            $spec->{failure} = $failure_action;
        }
        push @$do_when_data, $spec;
        return;
    } elsif ($do_when_data == 1) {
        local $@;
        eval {
            $success_action->();
        };
        log "Failure in success callback: $@" if $@;
        return;
    }

    die "Bad call to do_when: " . 
        Dumper([$action_name, $success_action, $failure_action, @_]);
}

sub succeeded {
    process_actions($_[0], 'success');
}

sub failed {
    process_actions($_[0], 'failure');
}    

# Call this when you were using an event, but now it will never
# be used again.
sub event_superceded {
    delete $DO_WHEN{$_[0]};
}

# Note: Not exported; use succeeded or failed
sub process_actions {
    my $action_name = shift;
    my $type = shift;

    my $deferred_actions = $DO_WHEN{$action_name};

    if (defined($deferred_actions)) {
        if (!ref($deferred_actions)) {
            return;
        }

        for my $deferred (@$deferred_actions) {
            my $success_action = $deferred->{$type};
            if (ref($success_action) eq 'CODE') {
                local $@;
                eval {
                    $success_action->();
                };
                log("For action $action_name ($type), failure: $@") if $@;
            }
        }
    }

    $DO_WHEN{$action_name} = $type eq 'success';
}

my %CALLBACKS;

sub register_callback {
    my $type = shift;
    my $name = shift;
    my $callback = shift;

    $CALLBACKS{$type}->{$name} = $callback;
}

sub unregister_callback {
    my $type = shift;
    my $name = shift;
    delete $CALLBACKS{$type}->{$name};
}

# Returning undef cancels the rest of the callbacks and of the
# processing. Returning anything defined continues the chain.
# It is expected that if you need to make changes that you
# make them directly, since everything is passed by ref.
sub callbacks {
    my $callback_type = shift;
    my $extra = shift;
    my $final_action = shift;
    my @data = @_;

    my $callbacks = $CALLBACKS{$callback_type};
    if (!defined($callbacks)) {
        if (defined($final_action) && ref($final_action) ne 'CODE') {
            confess "Not a code ref passed to callbacks.";
        }

        if ($final_action) {
            return $final_action->(@data);
        } else {
            return;
        }
    }
    
    for my $name (keys %$callbacks) {
        my $callback = $callbacks->{$name};
        local $@;
        my $result;
        eval {
            $result = $callback->($extra, @data);
        };
        if ($@) {
            my $message = "Failure in presence callback $name: $@";
            if ($Thrasher::TESTING) {
                print $message;
            } else {
                log($message);
            }
        }
        if (!defined($result)) {
            return;
        }
    }

    # Passed the callback chain
    if ($final_action) {
        return $final_action->(@data);
    }
    return;
}

1;
