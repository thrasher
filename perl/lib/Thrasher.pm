package Thrasher;

use strict;
use warnings;

our $VERSION = '0.1';

=pod

=head1 NAME

Thrasher - top-level module for Thrasher Bird

=head1 SYNOPSIS

In the thrasherbird.pl file:

 use Thrasher;
 Thrasher::start($backend, $protocol, $server_ip, $server_port,
                 $server_secret);

=head1 DESCRIPTION

The Thrasher module stores a few misc. functions that don't belong 
anywhere else, and also has the C<start> function, which ties
together all of the pieces and actually starts the transport
functioning.

To learn how to write a new Protocol for Thrasher, see
L<Thrasher::Protocol>.

To learn how to write a new Backend for Thrasher, see
L<Thrasher::Backend>.

=cut

use Thrasher::Socket;
use Thrasher::Callbacks qw(callbacks);
use Thrasher::Component;
use Thrasher::Log qw(logger debug log);

our $SERVER_NAME;
# Item -> discovery info for each item as [$identities, $features],
# created by the component after it finishes connecting
our $SERVER_INFO = {};

our $event_loop;

sub start {
    my $backend = shift() || 'Test';
    my $backend_module = "Thrasher::Backend::$backend";
    my $protocol = (shift() || 'Test');
    my $protocol_module = "Thrasher::Protocol::$protocol";
    my $server_ip = shift;
    my $server_port = shift;
    my $server_secret = shift;
    my $backend_configuration = shift || {};
    my $protocol_configuration = shift || {};
    my $component_name_base = shift;
    $SERVER_NAME = shift;
    my $plugins = shift || [];

    my $event_loop_module;

    VALIDATE_PARAMETERS: {
        local $@;
        eval "use $backend_module;";
        if ($@) {
            print "While trying to load the $backend backend, an error\n"
                ."was encountered. The developers will want to know about\n"
                ."the following:\n\n$@\n";
            exit;
        }
        
        eval "use $protocol_module;";
        if ($@) {
            print "While trying to load the $protocol protocol, an error\n"
                ."was encountered. The developers will want to know about\n"
                ."the following:\n\n$@\n";
            exit;
        }

        if (!defined($server_ip)) {
            print "Missing server specification (\$server_ip in the "
                ."configuration file).\n";
            exit;
        }

        if (!defined($server_port)) {
            print "Missing server port specification (\$server_port "
                ."in the configuration file).\n";
            exit;
        }
        if ($server_port == 0 ||
            $server_port < 0 ||
            $server_port > 65536 ||
            int($server_port) ne $server_port) {
            print "Invalid port specification: $server_port\n";
            exit;
        }

        $component_name_base =~ s/^\.//;
        if (!$component_name_base) {
            print "Component name base (\$component_name_base) must not be empty.\n";
            exit;
        }

        $event_loop_module = $protocol_module->event_loop;
        eval "use $event_loop_module;";
        if ($@) {
            print "While trying to load $event_loop_module, an error\n"
                ."was encountered. The developers will want to know\n"
                ."the following:\n\n$@";
            exit;
        }

        for my $plugin_module (@{$plugins}) {
            eval("use ${plugin_module};");
            if ($@) {
                print
                  "While trying to load the $plugin_module plugin, an error\n"
                  . " was encountered. The developers will want to know about\n"
                  . " the following:\n\n$@\n";
                exit(1);
            }
        }
    }

    my $retry = 0;
    my $previous_error;
    my $sequential_errors = 0;
    logger("Start connection process for PID $$");
    while (1) {
        local $@ = '';

        # This sets up all the plumbing and starts the component
        # running.
        eval {
            debug("About to call $backend_module->new");
            my $backend =
                $backend_module->new($backend_configuration);
            debug("Got a backend back: $backend");
            debug("About to call $protocol_module->new");
            my $protocol =
                $protocol_module->new($protocol_configuration,
                                      $backend);
            debug("Got a protocol: $protocol");
            $backend->register_protocol($protocol);
            my $component_name = $protocol->identifier;
            if (!$component_name) {
                die "Protocol $protocol_module does not give "
                    ."us an identifier to use with the component.";
            }
            $component_name .= '.' . $component_name_base;

            $event_loop = $event_loop_module->new();
          
            my $socket = new Thrasher::Socket($server_ip,
                                              $server_port,
                                              $event_loop);
            $socket->connect;
            my $write_function = $socket->write_function;

            my $component = new Thrasher::Component
                ($protocol, $write_function, $server_secret,
                 $component_name);
            $component->{event_loop} = $event_loop;
            $component->{thrasher_socket} = $socket;
            $socket->{read_closure} =
                $component->socket_in_closure($socket);
            $socket->establish_fd_watch;

            $protocol->{component} = $component;
            $backend->{component} = $component;

            $component->output_initial_stream_tag;

            my $kill_thrasher = sub {
                logger("SIGINT received, shutting down.");
                $component->terminate;
                exit;
            };
            my $signal_handler = sub {
                $event_loop->execute_on_idle($kill_thrasher);
            };
            # If the user hits CTRL-C, nicely log people out,
            # and allow nice log outs with a signal.
            local $SIG{INT} = $signal_handler;
            local $SIG{TERM} = $signal_handler;

            callbacks('main_loop', $component);

            logger("Beginning main event loop.");
            $event_loop->go;

            $previous_error = '';
            $retry = 0;
            $sequential_errors = 0;

            logger("Main loop terminated.");

            # Paranoia, really shouldn't fail
            eval { $socket->close(); };
        };

        if ($@) {
            my $error = $@;
            $previous_error = $@;

            # Suppress repeating log messages: suppress everything but
            # the first five errors, and only print every ten minutes
            # or so after that, to prove it's still trying
            if ($previous_error eq $error) {
                $sequential_errors++;
                my $error_message = "Failed to start transport, retry $retry: $@";
                if ($sequential_errors == 5) {
                    logger($error_message);
                    # This will be true someday; right now we still
                    # want the debugging info.
                    logger("Suppressing most errors now; attempts "
                           ."will continue until successful.");
                } elsif ($sequential_errors % 60 == 0) {
                    logger($error_message);
                } elsif ($sequential_errors < 5) {
                    logger($error_message);
                }
            } else {
                logger("Failed to connect to server, retry $retry\n");
                logger("Error was: $@\n\n");
                $sequential_errors = 0;
            }
            sleep 5;
            $retry++;
            next;
        }
    }
}

# This currently has nowhere to live...
# If an error makes it back into the SWIG or libpurple layer,
# it is basically destroyed. This catches the error, outputs
# it in the log, and eats it (since the higher layer can't do 
# anything useful with it).
sub error_wrap {
    my $callback_name = shift;
    my $sub_to_wrap = shift;

    my $new_sub = sub {
        my @args = @_;

        undef $@;

        # We know there's never any useful return
        if (wantarray) {
            my @results;
            eval { @results = $sub_to_wrap->(@args); };

            if ($@) {
                Thrasher::Log::log("Error in $callback_name: $@");
            }

            return @results;
        } else {
            my $result;
            eval { $result = $sub_to_wrap->(@args); };

            if ($@) {
                Thrasher::Log::log("Error in $callback_name: $@");
            }

            return $result;
        }
    };

    return $new_sub;
}

1;
