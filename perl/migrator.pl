#!/usr/bin/perl

use strict;
use warnings;

# This is a Migrator script. It converts from one backend to another.
# It is primarily intended to convert from a Python-transport backend
# to the DBI backend, but in theory, this works with any combination.

# If you are the only user of your transport, or you have a very
# small number, my advice is not to bother with this. Your roster
# will still be downloaded from the legacy service. This is for
# large installations that may have hundreds of users.

# Some backends may only have partial support for the interface 
# Thrasher requires, so a migration is necessary.

# The interface here is that you need to set up the Source backend
# and the Destination backend up here at the top, then the code 
# does the rest.

###
### SOURCE BACKEND
###

my $source_backend = 'Test';
my $source_backend_configuration = 
{

};

###
### DESTINATION BACKEND
###

my $destination_backend = 'Test';
my $destination_backend_configuration = 
{

};


###
### PERL CODE - don't touch this
###

use Thrasher::Backend::Migrate;
Thrasher::Backend::Migrate::migrate($source_backend,
                                    $source_backend_configuration,
                                    $destination_backend,
                                    $destination_backend_configuration,
                                    dry_run => 1);

