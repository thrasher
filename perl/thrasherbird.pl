#!/usr/bin/env perl
# (leave that line in there)

# This is the Thrasher Bird configuration file. It is also the Perl
# script that actually starts up Thrasher Bird. If that intimidates
# you, just treat this as any other configuration file: Follow the
# comments (after the # symbols), and ignore everything after the 
# #### THRASHER BIRD PROGRAM comment.

# To configure Thrasher Bird, you need to specify three things:
# * The "Backend", which is what Thrasher Bird will use to store
#   the data it generates.
# * The "Protocol", which tells it which supported protocol will
#   be exposed by the component.
# * Connection information about the XMPP Server to connect to.
#   You will need to consult your server's documentation about
#   how to set up the server to receive connections.

########
## BACKEND
########

# (Perl gurus: Anything in the Thrasher::Backend::* namespace may be
# used here; specify the module without Thrasher::Backend:: in it.)

# Uncomment the backend you wish to use, configure as appropriate.
# Backend names are case-sensitive.

## The "Test" backend; can be used to test the system, but won't
#  permanently store any data!
# $backend = 'Test';
# $backend_configuration = {};

# Example for the DBI backend:
# $backend = 'DBI';
# $backend_configuration = {
#     dbi_data_source => 'dbi:mysql:thrasher',
#     username => 'mysqluser',
#     password => 'mysqlpass',
#     db_driver => 'mysql_innodb',
#     database_name => 'thrasher',
#     transport_name => 'someprotocol.transport',
# };

#######
## PROTOCOL
#######

# (Perl gurus: This is like the Backend, except under the 
# Thrasher::Protocol:: namespace.)

# Uncomment the protocol you wish to use, configure as appropriate.
# Backend names are case-sensitive.

## The "Test" protocol does very little for you.
#
# $protocol = Test;
# my $protocol_configuration = {};

#######
## CONNECTION INFO
#######

# You need to tell Thrasher Bird how to connect to your server.

# Leave the apostrophes around the ip or domain here.
# This is the "local server".
$server_ip = '127.0.0.1'; 

# Server port: The port the server is expecting the transport to use.
$server_port = 5556;

# Secret: The shared secret that the server is expecting. If you're
# not familiar with Perl string rules, stick to letters and numbers
# only.
$server_secret = 'secret';

# Component name base; this will be composed together with the name
# of the protocol to determine the component name. If you don't know
# what this means, leave it be. If you want to share the transport
# with people off your server, though, it needs to be routable,
# which means you should set this as the domain name of your server.
$component_name_base = 'localhost';

# Set $server_target_for_service_discovery to a target entity for
# XEP-0030 service discovery if Thrasher should find and use other
# XMPP items like a XEP-0065 file transfer proxy. Otherwise, leave it
# undefined.
my $server_target_for_service_discovery;

#######
## PLUGINS
#######

my $plugins = [];

# ProxyFileTransfer (currently must be used with a Purple protocol)
# enables file transfers to and from contacts on the transport.
# push(@{$plugins}, 'Thrasher::Plugin::ProxyFileTransfer');

#######
## INSTALLATION LOCATION
#######

# Thrasher can use libraries built in the same directory as this
# script. If you'd like to install Thrasher to a different path,
# either configure the below or add the path to PERL5LIB:

my $thrasher_perl_dir;

# Default: figure out where the script is and use libraries from that path:
use Cwd qw(abs_path);
use File::Basename qw(dirname);
if (not $thrasher_perl_dir) {
    $thrasher_perl_dir = dirname(abs_path($0));
    require lib;
    lib->import($thrasher_perl_dir . '/lib');
}

#### THRASHER BIRD PROGRAM
# If you are just configuring the program, ignore everything after
# this.

require Thrasher;
# Perl Gurus: This function call is all you need to start Thrasher
# Bird. This configuration file is unneccessary if you want
# to obtain these values some other way. (We do at Barracuda
# Networks.)
Thrasher::start($backend, $protocol, $server_ip, $server_port,
                $server_secret, $backend_configuration,
                $protocol_configuration, $component_name_base,
                $server_target_for_service_discovery,
                $plugins,
               );
