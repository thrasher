package Thrasher::Socket;
use strict;
use warnings;

=pod 

=head1 NAME

Thrasher::Socket - manage the socket connection to the XMPP server.

=head1 DESCRIPTION

Thrasher::Socket manages the connection to the XMPP server, and
once the connection is successfully established, feeds the socket 
into the Component.

=cut

use POSIX;
use Socket;
use IO::Socket;
use Thrasher::Log qw(log debug);

sub new {
    my $class = shift;
    my $self = {};

    $self->{host} = shift;
    $self->{port} = shift;
    $self->{event_loop} = shift;

    bless $self, $class;
    return $self;
}

# Returns the function to feed to the component to write to
# this socket.
sub write_function {
    my $self = shift;
    my $writer = sub {
        debug("OUT: " . join('', @_));

        $self->write(@_);
    };
    return $writer;
}

sub write {
    my $self = shift;

    my $socket = $self->{socket};
    for my $data (@_) {
        print $socket $data;
    }
    flush $socket;
}

# dies on failure
sub connect {
    my $self = shift;
    if ($self->{connected}) {
        return;
    }

    my $proto = getprotobyname('tcp');
    my $iaddr = inet_aton($self->{host});
    my $paddr = sockaddr_in($self->{port}, $iaddr);

    my $socket;

    log("Beginning socket connection to $self->{host}:$self->{port}...");
    socket($socket, PF_INET, SOCK_STREAM, $proto) 
        or die "SOCK_CREATE: socket failed to create";
    connect($socket, $paddr) 
        or die "SOCK_CONNECT: socket failed to connect";
    log("connected.");

    binmode($socket, ':encoding(utf8)');
 
    # No blocking here
    fcntl($socket, F_SETFL, O_NONBLOCK);

    $self->{socket}    = $socket;
    $self->{connected} = 1;
}

sub establish_fd_watch {
    my $self = shift;

    $self->{watch_handle} = $self->{event_loop}->add_fd_watch
        ($self->fileno, $Thrasher::EventLoop::IN,
         $self->{read_closure});
}

sub terminate_fd_watch {
    my $self = shift;
    if (!exists($self->{watch_handle})) {
        return;
    }

    $self->{event_loop}->remove_fd_watch($self->{watch_handle});
    delete($self->{watch_handle});
}    

sub get_socket {
    my $self = shift;
    if (!$self->{connected}) {
        die "No socket to get.";
    }

    return $self->{socket};
}

sub read {
    my $self = shift;

    if (!$self->{connected}) {
        die('Trying to read from an unconnected socket.');
    }

    my $value;
    my $return = sysread($self->{socket}, $value, 65535);
    if ($return) {
        return $value;
    }
    # For now restart on all read errors--dunno what state changes we
    # missed reading. Might eventually be able to reconnect only the
    # component w/o reconnecting everyone to the public IM network.
    elsif (! defined($return)) {
        if ($!{'EAGAIN'}) {
            # Not ready yet. That's OK--return undef and try again later.
            return;
        }
        else {
            die("Error reading from component socket: $!");
        }
    }
    else {
        die('EOF when reading from component socket!');
    }
}

sub close {
    my $self = shift;
    $self->{socket}->close;
    $self->terminate_fd_watch;
    delete $self->{connected};
    delete $self->{socket};
}

sub fileno {
    my $self = shift;
    return $self->{socket}->fileno;
}

sub eof {
    my $self = shift;
    return $self->{socket}->eof;
}

1;
