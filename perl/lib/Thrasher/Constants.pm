package Thrasher::Constants;

# I don't believe in "use constant"; see criticism in CPAN's
# "Readonly" module. And we don't use Readonly since that's just
# not worth requiring an additional module for. Just don't write
# over these.

use strict;
use warnings;

use base 'Exporter';

our @EXPORT_OK = qw($NS_XML $NS_XMLNS

                    %IQ_ERRORS

                    $NS_STREAM $NS_CLIENT $NS_COMPONENT $NS_REGISTER 
                    $NS_DISCO $NS_TIME $NS_VERSION $NS_ERROR
                    $NS_GATEWAY $NS_TIME $NS_CAPS
                    $NS_DISCO_ITEMS $NS_DISCO_INFO
                    $NS_PUBSUB $NS_TUNE $NS_PEP_AVATAR 
                    $NS_PEP_AVATAR_METADATA
                    $NS_LAST $NS_VCARD $NS_VCARD_UPDATE
                    $NS_ROSTER_EXCHANGE $NS_XHTML_IM
                    $NS_XHTML $NS_NICK $NS_BYTESTREAMS
                    $NS_FILE_TRANSFER $NS_FEATURE_NEG
                    $NS_DATA $NS_STREAM_INITIATION
                    $NS_CHATSTATES

                    $NS_THRASHER_PRESENCE
                    );
our %EXPORT_TAGS = (all => \@EXPORT_OK);

# Hash of 'friendly error name' to 
# [error code, error type, tag name]
our %IQ_ERRORS = 
    (
     bad_request => [400, 'modify', 'bad-request'],
     conflict => [409, 'cancel', 'conflict'],
     feature_not_implemented => [501, 'cancel', 'feature-not-implemented'],
     forbidden => [403, 'auth', 'forbidden'],
     gone => [302, 'modify', 'gone'],
     item_not_found => [404, 'cancel', 'item-not-found'],
     service_unavailable => [503, 'cancel', 'service-unavailable'],
     not_acceptable => [406, 'modify', 'not-acceptable'],
     remote_server_timeout => [504, 'wait', 'remote-server-timeout'],
     registration_required => [407, 'auth', 'registration-required'],
     not_authorized => [401, 'auth', 'not-authorized'],
     internal_server_error => [500, 'wait', 'internal-server-error']
     );

our $NS_XMLNS = 'http://www.w3.org/2000/xmlns';
our $NS_XML = 'http://www.w3.org/XML/1998/namespace';

our $NS_STREAM = 'http://etherx.jabber.org/streams';
our $NS_CLIENT = 'jabber:client';
our $NS_COMPONENT = 'jabber:component:accept';
our $NS_DISCO = 'http://jabber.org/protocol/disco';
our $NS_DISCO_ITEMS = $NS_DISCO . '#items';
our $NS_DISCO_INFO = $NS_DISCO . '#info';
our $NS_AGENT = 'jabber:iq:agents';
our $NS_REGISTER = 'jabber:iq:register';
our $NS_TIME = 'jabber:iq:time';
our $NS_VERSION = 'jabber:iq:version';
our $NS_ERROR = 'urn:ietf:params:xml:ns:xmpp-stanzas';
our $NS_GATEWAY = 'jabber:iq:gateway';
our $NS_CAPS = 'http://jabber.org/protocol/caps';
our $NS_PUBSUB = 'http://jabber.org/protocol/pubsub';
our $NS_TUNE = 'http://jabber.org/protocol/tune';
our $NS_PEP_AVATAR = 'urn:xmpp:avatar:data';
our $NS_PEP_AVATAR_METADATA = 'urn:xmpp:avatar:metadata';
our $NS_LAST = 'jabber:iq:last';
our $NS_VCARD = 'vcard-temp';
our $NS_VCARD_UPDATE = 'vcard-temp:x:update';
our $NS_ROSTER_EXCHANGE = 'http://jabber.org/protocol/rosterx';
our $NS_XHTML_IM = 'http://jabber.org/protocol/xhtml-im';
our $NS_XHTML = 'http://www.w3.org/1999/xhtml';
our $NS_NICK = 'http://jabber.org/protocol/nick';
our $NS_BYTESTREAMS = 'http://jabber.org/protocol/bytestreams';
our $NS_FILE_TRANSFER = 'http://jabber.org/protocol/si/profile/file-transfer';
our $NS_FEATURE_NEG  = 'http://jabber.org/protocol/feature-neg';
our $NS_DATA = 'jabber:x:data';
our $NS_STREAM_INITIATION = 'http://jabber.org/protocol/si';
our $NS_CHATSTATES = 'http://jabber.org/protocol/chatstates';

our $NS_THRASHER_PRESENCE = 'xmpp:x:thrasher:presence';

1;
