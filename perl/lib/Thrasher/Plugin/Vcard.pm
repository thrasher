package Thrasher::Plugin::Vcard;
# The Perl module standard and the capitalization of vCard are in
# conflict. I like Perl better. Perl wins!

use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Plugin::Vcard - implements vCard support for transport
users, including avatar support.

=head1 DESCRIPTION

This module replies when a user's vcard is requested, filling in their
avatar, and possibly other things. This module is intended to be
filled out, as more things that can be stuck in vCards are 
implemented.

In the meantime, you get conventional avatars this way, including 
full XEP-0153 support for avatar hash advertising.

=cut

use Thrasher::Log qw(log);
use Thrasher::Plugin qw(:all);
use Thrasher::XML qw(strip_resource);
use Thrasher::Constants qw(:all);

use Digest::SHA1 qw(sha1_hex);
use Carp qw(confess);
use Thrasher::Component qw(strip_resource);

# $user_jid -> $legacy_jid -> avatar sha1_hex
my %AVATARS;

register_plugin({client_iq_handlers => 
                     {$NS_VCARD => { get => \&return_vcard }},
                 component_iq_handlers =>
                     {$NS_VCARD => { get => \&return_component_vcard}},
                 features => [$NS_VCARD, $NS_VCARD_UPDATE],
                 callbacks => {
                     presence_out => { vcard => \&presence_hook },
                     avatar_changed => { vcard => \&avatar_update }
                 }
                });
                     

sub avatar_update {
    my $component = shift;
    my $user_jid = shift;
    my $legacy_jid = shift;
    my $raw_binary_data = shift;
    my $base64_data = shift;
    my $image = shift;

    my $hash = sha1_hex($raw_binary_data);
    my $old_hash = $AVATARS{$user_jid}->{$legacy_jid};
    $AVATARS{$user_jid}->{$legacy_jid} = $hash;

    if (!$old_hash || $hash ne $old_hash) {
        my $session = $component->session_for($user_jid);
        my $presence_info = 
            $session->{component}->{presence}->{strip_resource($user_jid)}->{strip_resource($legacy_jid)};
        if (ref($presence_info) eq 'ARRAY') {
            my ($type, $show, $status) = @$presence_info;
            $component->send_presence_xml
                ($user_jid, $type, $legacy_jid,
                 $show, $status);
        }
    }
}

sub presence_hook {
    my $component = shift;
    my $presence_tag = shift;

    my $component_name = $component->{component_name};

    my $user_jid = $presence_tag->[1]->{to};
    my $legacy_jid = $presence_tag->[1]->{from};

    if ($legacy_jid && $user_jid &&
        $legacy_jid =~ /$component_name$/) {
        my @children;

        my $avatar_hash = $AVATARS{$user_jid}->{$legacy_jid};
        if ($avatar_hash) {
            push(@children, [[$NS_VCARD_UPDATE, 'photo'], {}, $avatar_hash]);
        }

        if ($legacy_jid =~ /\@/) {
            my $legacy_name
              = $component->xmpp_name_to_legacy($user_jid, $legacy_jid);
            my $displayname
              = $component->{protocol}->get_displayname($user_jid,
                                                        $legacy_name);
            if ($displayname) {
                push(@children, [[$NS_VCARD_UPDATE, 'fn'],
                                 {},
                                 [ $displayname ]]);
            }
        }

        push(@{$presence_tag->[2]},
             [[$NS_VCARD_UPDATE, 'x'], {}, \@children]);
    }

    return 1;
}

sub return_component_vcard {
    my $component = shift;
    my $iq_params = shift;
    my $iq_tag = shift;

    my $vcard = [[$NS_VCARD, 'vCard'], {},
                 [
                  [[$NS_VCARD, 'EMAIL'], {}, 
                   [[[$NS_VCARD, 'USERID'], {},
                     [$component->{component_name}]]]]]];
    $component->iq_reply($iq_params, $vcard);
}

sub return_vcard {
    my $component = shift;
    my $iq_params = shift;
    my $iq_tag = shift;

    my $vcard_target = strip_resource($iq_params->{to});
    my $request_from = strip_resource($iq_params->{from});
    my @vcard_elts;

    my $avatar =
        $component->{protocol}->{backend}->get_avatar($request_from,
                                                      $vcard_target);
    if ($avatar) {
        push(@vcard_elts, [[$NS_VCARD, 'PHOTO'], {}, [
            [[$NS_VCARD, 'TYPE'], {}, ['image/png']],
            [[$NS_VCARD, 'BINVAL'], {}, [$avatar]],
        ]]);
    }

    my $legacy_name = $component->xmpp_name_to_legacy($request_from,
                                                      $vcard_target);
    my $displayname = $component->{protocol}->get_displayname($request_from,
                                                              $legacy_name);
    if ($displayname) {
        push(@vcard_elts, [[$NS_VCARD, 'FN'], {}, [ $displayname ]]);
    }

    my $vcard_xml = [[$NS_VCARD, 'vCard'], {}, \@vcard_elts];
    $component->iq_reply($iq_params, $vcard_xml);
}

1;
