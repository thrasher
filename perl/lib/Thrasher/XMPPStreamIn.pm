package Thrasher::XMPPStreamIn;

=head1 NAME

Thrasher::XMPPStream - wrap XML::Parser for XMPP stream parsing

=head1 SYNOPSIS

 my $stream = new Thrasher::XMPPStream;

 # messages as described below
 my $messages = $stream->parse($some_text);
 my $more_messages = $stream->parse($more_text_in_stream);

 # dies
 my $yet_more_messages = $stream->parse($invalid_xml);

=head1 DESCRIPTION

Thrasher::XMPPStream converts from the Jabber XML stream into 
perl objects representing the events from the XML stream.

This abstracts out managing the XML::Parser correctly.

=head2 Output format

Events are output as array refs with three elements, containing the
following:

=over 4

=item * 

B<Tag name reference>: The full name of the tag. See "Tags" section
below.

=item *

B<Attributes hash>: A hash of full attribute names to their
values. See "Tags" section below.

=item *

B<Children>: The children of this tag. The children are each one of
two things: A string, representing character data, or another tag
arrayref as described here.

=back

=head2 Tags

Thrasher::XMPPStream correctly handles namespaces; therefore, all
tags and attribute names actually have two parts, the namespace
and the tag name itself. 

Tags are typically represented as an array ref, with the first part
representing the full name of the namespace (NOT the prefix,
like "stream", the whole URI, like
"http://etherx.jabber.org/streams"), and the second part representing 
the tag name. 

Attributes are represented in "Clark-style" attribute names, as
XML::SAX's documentation calls it, which is the namespace in 
{}, followed by the tag/attribute name. Note that this means
that an attribute with no explicit namespace will be
C<{}attname>, not C<attname>.

=cut

use strict;
use warnings;

use XML::SAX;
$XML::SAX::ParserPackage = "XML::SAX::Expat::Incremental";
use XML::SAX::ParserFactory;

use Data::Dumper;
use Carp qw(confess);

use Thrasher::XML qw(:all);

use base 'XML::SAX::Base', 'Exporter';

use Encode;

# Just export the non-methods.
our @EXPORT_OK = qw(get_parser);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

sub get_parser {
    my $self = Thrasher::XMPPStreamIn->new();
    my $parser = XML::SAX::ParserFactory->parser
        (Handler => $self);
    $self->{parser} = $parser;
    return $self;
}

sub new {
    my $self = {_debug => 1};
    bless $self, 'Thrasher::XMPPStreamIn';

    my $handlers = {Start => sub { $self->tag_start(@_) },
                    End => sub { $self->tag_end(@_) },
                    Char => sub { $self->char(@_) },
                    XMLDecl => sub { $self->xml_decl(@_) }};

    # Consists of a list of prefixes->array ref of strings,
    # where the last string is the current namespace for that
    # prefix.
    $self->{namespaces} = {};

    # this stores up the messages as they are generated, so the
    # end of the "parse" call can return them.
    $self->{messages} = [];

    return $self;
}

sub start_element {
    my ($self, $el) = @_;

    my $atts = $el->{Attributes};
    while (my ($name, $att_data) = each %$atts) {
        # Filter out namespace declarations
        if ($name =~ m|http://www.w3.org/2000/xmlns| ||
            $name eq '{}xmlns') {
            delete $atts->{$name};
            next;
        }

        # If it's not a namespace declaration, cut the info
        # down to the value
        $atts->{$name} = $atts->{$name}->{Value};
    }

    my $element = [$el->{NamespaceURI}, $el->{LocalName}];

    # As a special case, if this is the initial stream tag,
    # it constitutes a message
    if (same_name(['stream', 'stream'], $element)) {
        # pretend the children are empty
        push @{$self->{messages}}, [$element, $atts, []];
    } else {
        # Otherwise, we need to remember it
        if (!exists($self->{current_message})) {
            $self->{children} = [[]];
            $self->{current_message} = [$element, $atts, 
                                        $self->{children}->[0]];
        } else {
            my $new_children = [];
            my $current_children = $self->{children}->[-1];
            push @{$self->{children}}, $new_children;
            my $new_element = [$element, $atts,
                               $new_children];
            push @$current_children, $new_element;
        }
    }
}

sub end_element {
    my ($self, $el) = @_;

    if (!scalar@{$self->{children}}) {
        return; # got a final </stream:stream>
    }

    # Return to the previous tag's children.
    pop @{$self->{children}};

    # If we've received a complete message, remember it
    # so we can return it.
    if (!scalar(@{$self->{children}})) {
        my $message = $self->{current_message};
        push @{$self->{messages}}, $message;
        delete $self->{current_message};
    }    
}

sub parse {
    my ($self, $xml) = @_;

    $self->{parser}->parse_string($xml);

    my $messages = $self->{messages};
    $self->{messages} = [];
    return $messages;
}

sub dump {
    my $self = shift;
    if ($self->{_debug}) {
        print Dumper(@_);
        print "\n";
    }
}

sub att_name {
    my $self = shift;
    my $att_name = shift;
    my $name = $self->convert_name($att_name);
    return '{' . $name->[0] . '}' . $name->[1];
}

sub convert_name {
    my $self = shift;
    my $name = shift;

    my ($left, $right) = split /:/, $name, 2;
    if (defined($right)) {
        return [$self->namespace_for_prefix($left), $right];
    } else {
        return [$self->namespace_for_prefix(''), $left];
    }
}

sub namespace_for_prefix {
    my $self = shift;
    my $prefix = shift;

    my $namespaces = $self->{namespaces}->{$prefix};
    if (defined($namespaces) && scalar(@$namespaces) > 0) {
        return $namespaces->[-1];
    }
    
    return '';
}

sub characters {
    my ($self, $el_data) = @_;
    
    my $chars = $el_data->{Data};
    my $current_children = $self->{children}->[-1];
    if (!defined($current_children)) {
        # We're still not to the first message yet.
        return;
    }

    if (scalar(@$current_children) && ref($current_children->[-1]) eq '') {
        $current_children->[-1] = $current_children->[-1] . $chars;
    } else {
        push @$current_children, $chars;
    }

}

1;
