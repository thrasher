/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */

#ifdef SWIG

%module "THPPW"

%{
#include "purple.h"
%}
%include libpurple/conversation.h

%{
#include "thperl.h"
#include "thrasher.h"
#include "thblist.h"
#include "thconnection.h"
#include "glib.h"

%}

  /* 
   Define a typemap that goes from perl hash to a
   string -> string g_hash_table (that C subsequently owns) 

   Note that contrary to what I'd expect, this is actually keyed
   off of the C variable name in the incoming function.  
  */

%typemap(in) GHashTable* hash_args 
{
    if (!SvROK($input)) 
    {
        croak("$input is not a reference.");
    }

    if (SvTYPE(SvRV($input)) != SVt_PVHV)
    {
        croak("$input is not a hashref.");
    }

    $1 = g_hash_table_new_full(g_str_hash, g_str_equal,
                               g_free, g_free);

    HV *hv = (HV*)SvRV($input);
    I32 len;
    SV *elt;
    char *key;
    char *new_value;
    STRLEN strlen;

    hv_iterinit(hv);
    while (elt = hv_iternextsv(hv, &key, &len) )
    {
        new_value = SvPV(elt, strlen);
        char *new_key = g_strdup(key);
        new_value = g_strdup(SvPV(elt, strlen));
        
        g_hash_table_insert($1, new_key, new_value);
    }
}

/* Map GList of char* to a Perl string list. */
%typemap(out) GList_String* {
    /* Push new Perl strings onto the Perl argument stack. */
    GList *c_list = $1;
    while (c_list) {
        if (argvi >= items) {
            EXTEND(sp, 1);
        }
        ST(argvi) = sv_newmortal();
        sv_setpv(ST(argvi),
                 (char *) c_list->data);
        c_list = g_list_next(c_list);
        argvi++;
    }
    g_list_free(c_list);
}

#endif /* SWIG */

#ifndef THPERL_H
#define THPERL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <error.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <glib.h>

#include "blist.h"

/* Perl */
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

/* libpurple */
#include "eventloop.h"

/* We can't use the Glib macros because they're int-based */
#define LONG_TO_PTR(a)  (void*) a
#define PTR_TO_LONG(a)  (long) a
#define TTRUE  1
#define TFALSE 0
#define TNULL  0

typedef struct _perl_callbacks {
    SV *timeout_add;
    SV *input_add;
    SV *source_remove;
    SV *incoming_msg;
    SV *presence_in;
    SV *subscription_add;
    SV *legacy_user_add_user;
    SV *connection_error;
    SV *connection;
    SV *incoming_chatstate;
} perl_callbacks;

typedef struct _perl_ft_callbacks {
    SV *ft_recv_request_cb;
    SV *ft_recv_accept_cb;
    SV *ft_recv_start_cb;
    SV *ft_recv_cancel_cb;
    SV *ft_recv_complete_cb;
    SV *ft_send_accept_cb;
    SV *ft_send_start_cb;
    SV *ft_send_cancel_cb;
    SV *ft_send_complete_cb;
    SV *ft_read_cb;
    SV *ft_write_cb;
    SV *ft_data_not_sent_cb;
} perl_ft_callbacks;

typedef enum {
    TIMEOUT_ADD = 1,
    INPUT_ADD   = 2
} record_type;

typedef struct _hash_record {
    record_type type;
    void *function;
    void *data;
    /* These values pertain only to type == INPUT_ADD */
    PurpleInputCondition cond;
    guint fd;
} hash_record;

typedef GList GList_String;

/* Push the internal init through */
extern void thrasher_init();

void thrasher_blist_init();
void thrasher_connection_init();

/* Actions */
int thrasher_action_login (GHashTable *hash_args);
int thrasher_action_logout (char *jid);
void thrasher_action_debug_logged_in(void);
int thrasher_action_outgoing_msg (char *jid, char *recipient, char *message);
void thrasher_action_outgoing_chatstate(char *jid, char *recipient, PurpleTypingState chatstate);
int thrasher_action_presence (char *jid, int status, char *message);
int thrasher_action_buddy_add (char *jid, char *buddy_name);
int thrasher_action_buddy_remove (char *jid, char *buddy_name);
void thrasher_action_buddy_authorize(char *jid, char *legacy_username);
void thrasher_action_buddy_deauthorize(char *jid, char *legacy_username);
GList_String* thrasher_action_get_logged_in_jids();

void thrasher_perl_init(void);
void thrasher_purple_debug (int tf);

/* FT Actions */
void thrasher_action_ft_ui_ready(size_t id);
void thrasher_action_ft_recv_request_respond(size_t id, unsigned int accept);
void thrasher_action_ft_cancel_local(size_t id);

/* Perl-end Wrappers */
#ifdef SWIG
%varargs(1, SV *cb_arg = NULL) thrasher_wrapper_init;
#endif /* SWIG */
void thrasher_wrapper_init (SV *timeout_add_cb,
                            SV *input_add_cb,
                            SV *source_remove_cb,
                            SV *incoming_msg_cb,
                            SV *presence_in_cb,
                            SV *subscription_add_cb,
                            SV *legacy_user_add_user_cb,
                            SV *connection_error_cb,
                            SV *connection_cb,
                            ...);

void thrasher_wrapper_ft_init(SV *ft_recv_request_cb,
                              SV *ft_recv_accept_cb,
                              SV *ft_recv_start_cb,
                              SV *ft_recv_cancel_cb,
                              SV *ft_recv_complete_cb,
                              SV *ft_send_accept_cb,
                              SV *ft_send_start_cb,
                              SV *ft_send_cancel_cb,
                              SV *ft_send_complete_cb,
                              SV *ft_read_cb,
                              SV *ft_write_cb,
                              SV *ft_data_not_sent_cb);

int thrasher_wrapper_legacy_user_add_user(const char *jid,
                                          const char *sender);

int thrasher_wrapper_subscription_add(const char *jid,
                                      const char *sender,
                                      const guint status);

int thrasher_wrapper_incoming_msg (const char *jid,
                                   const char *sender,
                                   const char *alias,
                                   const char *message,
                                   PurpleMessageFlags flags);

int thrasher_wrapper_connection (const char *jid);

/* Would have to swig PurpleStatusPrimitive if Perl were to ever call this. */
int thrasher_wrapper_presence_in(const char *jid,
                                 const char *sender,
                                 const char *alias,
                                 const char *group,
                                 const PurpleStatusPrimitive status,
                                 const char * message);

int thrasher_wrapper_connection_error(const char *jid,
                                      const guint error_code,
                                      const char *message);

int thrasher_wrapper_trigger_timeout_func(long key);
int thrasher_wrapper_trigger_input_func(long fd, SV *cond, long key);
void thrasher_wrapper_destructor ();

void thrasher_wrapper_incoming_chatstate(PurpleAccount* pa, const char* who, PurpleTypingState state);

/* Core wrapper */
int thrasher_wrapper_call_source_remove (long key);
long thrasher_wrapper_set_timeout_add (guint interval, GSourceFunc function, gpointer data);
long thrasher_wrapper_set_input_add (guint fd, PurpleInputCondition cond, PurpleInputFunction func, gpointer user_data);

/* File transfer wrappers */
size_t thrasher_wrapper_send_file(const char *jid, const char *who, char *filename, size_t size, char *desc);
int thrasher_wrapper_ft_send_start(guint id);
void thrasher_wrapper_ft_send_cancel(guint id);
void thrasher_wrapper_ft_send_complete(guint id);
gssize thrasher_wrapper_ft_read(guint id, guchar **buffer, gssize size);
void thrasher_wrapper_ft_data_not_sent(guint id, const char *buffer, gsize size);
void thrasher_wrapper_ft_recv_request(PurpleXfer *xfer, const char* remote_filename);
gssize thrasher_wrapper_ft_write(guint id, const char *buffer, gssize size);
void thrasher_wrapper_ft_recv_cancel(guint id);
void thrasher_wrapper_ft_recv_complete(guint id);

#ifdef TH_DEBUG
long thrasher_wrapper_trigger_timeout_add ();
long thrasher_wrapper_trigger_timeout_add2 ();
long thrasher_wrapper_trigger_timeout_add3 ();
int thrasher_wrapper_trigger_timeout_remove (long key);
#endif /* TH_DEBUG */

#endif /* THPERL_H */
