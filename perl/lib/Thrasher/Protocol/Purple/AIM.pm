package Thrasher::Protocol::Purple::AIM;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Purple::AIM - protocol for connection to AOL
Instant Messenger using libpurple

=cut

use base "Thrasher::Protocol::Purple";

use Thrasher::Log qw(log debug);
use CGI qw(escapeHTML);

sub prpl {
    return "prpl-aim";
}

# This receives the message we're about to send out, and adds any
# further processing that may be necessary.
sub process_message {
    my $self = shift;
    my $message = shift;

    return escapeHTML($message);
}

sub process_remote_username {
    my $self = shift;
    my $s = shift;
    $s =~ tr/[A-Z]/[a-z]/;
    $s =~ s/ //g;
    return $s;
}

sub name { "AIM" }
sub identifier { "aim" }
sub gateway_prompt { "Enter AIM Username:" }
sub gateway_desc { "Enter AIM Username:" }
sub purple_forces_kill { return 1; }

1;
