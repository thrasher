/*
 * Thrasher Bird - XMPP transport via libpurple
 * Copyright (C) 2008 Barracuda Networks, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrasher Bird; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include <request.h>

#include "threquest.h"
#include "thft.h"
#include "thperl.h"
#include "thrasher.h"

static void *
thrasher_request_input(const char *title,
                       const char *primary,
                       const char *secondary,
                       const char *default_value,
                       gboolean multiline,
                       gboolean masked, gchar *hint,
                       const char *ok_text,
                       GCallback ok_cb,
                       const char *cancel_text,
                       GCallback cancel_cb,
                       PurpleAccount *account,
                       const char *who,
                       PurpleConversation *conv,
                       void *user_data) {
    /* Non-matching input requests fire no callback, which is the
     * default if the UI does not provide this request uiop.
     */

    /* Ensure callback to actually send the authorization request
     * fires. For now, always use the default message.
     */
    if (0 == g_strcmp0(primary, _("Authorization Request Message:"))) {
        gchar* jid = thrasher_account_get_jid(account);
        purple_debug_info("thrasher request",
                          "%s: Requesting authorization from %s: %s\n",
                          jid,
                          who,
                          default_value);
        ((PurpleRequestInputCb)ok_cb)(user_data, default_value);
    }

    else {
        purple_debug_error("thrasher_request",
                           "Unhandled thrasher_request_input(%s, %s, %s)\n",
                           title,
                           primary,
                           secondary);
    }

    /* No meaningful UI handle. */
    return NULL;
}

static void *
thrasher_request_action(const char *title,
                        const char *primary,
                        const char *secondary,
                        int default_action,
                        PurpleAccount *account,
                        const char *who,
                        PurpleConversation *conv,
                        void *user_data,
                        size_t action_count,
                        va_list actions) {

    PurpleRequestActionCb default_cb = NULL;
    int i;
    for (i = 0; i < action_count; ) {
        char* label = va_arg(actions, char*);    /* throw away label */
        PurpleRequestActionCb cb = va_arg(actions, PurpleRequestActionCb);
        if (i == default_action) {
            default_cb = cb;
            break;
        }
        i++;
    }
    if (! default_cb) {
        purple_debug_error("thrasher request",
                           "Default action not found?"
                           " thrasher_request_action(%s, %s, %s)\n",
                           title,
                           primary,
                           secondary);
        /* No meaningful UI handle. */
        return NULL;
    }

    if (0 == g_strcmp0(title, _("SSL Certificate Verification"))) {
        purple_debug_info("thrasher request",
                          "%s %s\n",
                          /* Accept certificate for ...? */
                          primary,
                          /* text of errors */
                          secondary);
        default_cb(user_data, default_action);
    }

    else {
        purple_debug_error("thrasher_request",
                           "Unhandled thrasher_request_action(%s, %s, %s)\n",
                           title,
                           primary,
                           secondary);
    }

    /* No meaningful UI handle. */
    return NULL;
}

static PurpleRequestUiOps thrasher_request_uiops =
{
    // request_input
    thrasher_request_input,

    // request_choice
    NULL,

    // request_action
    thrasher_request_action,

    // request_fields
    NULL,

    // request_file
    thrasher_request_file,      /* from thft */

    // close_request
    NULL,

    // request_folder
    NULL,

    // 4 reserved
    NULL,
    NULL,
    NULL,
    NULL
};

void thrasher_request_init() {
    purple_request_set_ui_ops(&thrasher_request_uiops);
}
