package Thrasher::Plugin::PEP;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Plugin::PEP - adds PEP support to the virtual accounts,
allowing them to publish things in accordance with the PEP spec

=head1 DESCRIPTION

Thrasher::Plugin::PEP adds PEP support to your Component. The primary
manifestation is a function that you can use to publish things with
PEP.

At the moment, I'm not sure how to tell if the server supports PEP.
The standard says you're supposed to query 

=cut

use Thrasher::Callbacks qw(:all);
use Thrasher::Constants qw(:all);
use Thrasher::Plugin::EntityCapabilities qw(:all);

use base 'Exporter';

our @EXPORT_OK = qw(pep_publish);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

# use "do_when('pep_found')" for all major actions

sub detect_pep {
    if (has_identity_category('pep')) {
        succeeded('pep_detected');
    } else {
        failed('pep_detected');
    }
}

do_when('server_capabilities_detected', \&detect_pep);

# Call this to publish something via PEP; nicely blocks on PEP being
# found
#
# If you get errors about "Can't use string ("SOME_NAMESPACE") as an
# ARRAY ref", your problem is that you need to add one more list
# around your $publish_xml; remember, you're providing the full
# children, not just one node.
sub pep_publish {
    my $component = shift;
    my $from_jid = shift;
    my $publish_node_ns = shift;
    my $publish_xml = shift;

    do_when('pep_detected',
            sub {
                $component->iq_query
                    ([[$NS_COMPONENT, 'iq'],
                      {from => $from_jid, 
                       type => 'set'},
                      [
                       [[$NS_PUBSUB, 'pubsub'], {}, 
                        [
                         [[$publish_node_ns, 'publish'], {},
                          $publish_xml
                         ]]
                        ]
                       ]]);
            });
}

1;
