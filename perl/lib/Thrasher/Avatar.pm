package Thrasher::Avatar;
use strict;
use warnings;

=pod

=head1 NAME

Thrasher::Avatar - common avatar manipulation code, including setting
them

=head1 DESCRIPTION

Thrasher::Avatar manages the common bits of avatar code, including
setting them. It takes care of converting them into PNG avatars,
and firing a callback for plugins to catch.

The callback 'avatar_changed' is called, with: ($component, $user_jid, 
$legacy_username, $raw_binary_data, $base64_data, $image_magick_image).

=cut

use Thrasher::Callbacks qw(callbacks);
use Thrasher::Log qw(:all);

use MIME::Base64 qw(encode_base64);
use Image::Magick;
use Carp qw(confess);

use base 'Exporter';

our @EXPORT_OK = qw(set_avatar);
our %EXPORT_TAGS = (all => \@EXPORT_OK);

# I realize later versions of Image::Magick can do this with the 
# 'mime' attribute, but I don't want to require that recent of
# a version.
my %MAGICK_TO_MIME = 
    (
     'PNG' => 'image/png',
     'GIF' => 'image/gif'
     );

# After $component, you give it a $binary_avatar_data
sub set_avatar {
    my $component = shift;
    my $jid = shift;
    my $legacy_jid = shift;
    my $avatar_binary = shift;

    if (!defined($avatar_binary)) {
        confess "Undefined avatar being passed to set_avatar.";
    }

    my $image = Image::Magick->new;
    if (my $res = $image->BlobToImage($avatar_binary)) {
        log("While trying to load avatar for $jid, " .
            "Image::Magick complained: $res. Skipped because "
            ."we failed to determine a type for the image.");
        return;
    }

    if (!$image->Get('magick')) {
        log("While trying to load avatar for $jid, "
            ."Image::Magick failed to determine a type for "
            ."the image. Skipped.");
        return;
    }

    if ($image->Get('magick') ne 'PNG') {
        $image->Set(magick => 'PNG');
        $avatar_binary = $image->ImageToBlob();
    }

    my $base64_binary = encode_base64($avatar_binary);

    $component->{protocol}->{backend}->set_avatar($jid, $legacy_jid,
                                      $base64_binary);

    callbacks('avatar_changed', $component, undef,
              $jid, $legacy_jid,
              $avatar_binary, $base64_binary, $image);
}

1;
