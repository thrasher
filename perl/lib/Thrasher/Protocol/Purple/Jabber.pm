package Thrasher::Protocol::Purple::Jabber;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Purple::Jabber - Protocol for connecting to Jabber
servers via a gateway.

=head1 WTF?

Well, my proximate reason for developing this was so I could debug
file transfer support using a local XMPP server instead of hammering
on one of the "real" services with lots of half-assed file transfers 
that would look an awful lot like a hack attack.

But... we actually get alot of requests for this at Barracuda,
specifically, people asking to connect to GTalk as a transport.
While a much, much better transport for such a thing could be
written, this has the virtue of being I<easy>, once all the other
hard work is done.

=cut

use base "Thrasher::Protocol::Purple";

use Thrasher::Log qw(log debug);
use Thrasher::Constants qw($NS_REGISTER);
use Thrasher::XML qw(strip_resource);

sub prpl {
    return "prpl-jabber";
}

sub registration_items {
    return qw(username password connect_server bool_auth_plain_in_clear);
}

sub registration_defaults {
    return { connect_server => "",
             bool_auth_plain_in_clear => "1"};
}

sub registration_xml {
    my $self = shift;
    my $jid = shift;

    my $registration = $self->{backend}->registered($jid);
    
    # Add an optional connect server to override the usual server.
    # We still need to work out how to pass number for ports, but 
    # I don't need that now.
    return [[[$NS_REGISTER, 'instructions'], {},
             ['Please provide your username and password for '
              .$self->name]],
            ($registration ? ([[$NS_REGISTER, 'registered'], {}, []])
             : ()),
            [[$NS_REGISTER, 'username'], {},
             [$registration->{username} ? ($registration->{username}) : ()]],
            [[$NS_REGISTER, 'password'], {}, 
             [$registration->{password} ? ($registration->{password}) : ()]],
            [[$NS_REGISTER, 'connect_server'], {},
             [$registration->{connect_server} ?
              $registration->{connect_server} : ()]],
            [[$NS_REGISTER, 'bool_auth_plain_in_clear'], {},
             [defined($registration->{bool_auth_plain_in_clear}) ?
              ($registration->{bool_auth_plain_in_clear}) : ()]]
        ];
    
}

sub process_remote_username {
    my $self = shift;
    my $username = shift;

    return strip_resource($username);
}
    

sub name { "XMPP" }
sub identifier { "xmpp" }
sub gateway_prompt { "Enter XMPP Username:" }
sub gateway_desc { "Enter XMPP Username:" }

1;
