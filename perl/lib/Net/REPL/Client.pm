package Net::REPL::Client;
use strict;
use warnings;

=pod

=head1 NAME

Net::REPL::Client - ReadLine + Socket to Eval Server = interactive development

=head1 DESCRIPTION

The client object connects L<Term::ReadLine> to a socket that speaks
the L<Net::REPL::Server> protocol. Perl code is sent over the socket
to be eval'd in the server process, which sends back a result for the
client to print.

Like the Server, this class can be used as-is or superclassed to
provide additional capabilities.

=head2 Methods

=cut

use base qw(Net::REPL::Base);

use Socket qw(AF_UNIX);
use Term::ReadLine;

=head3 new(argument => value...)

Arguments:

=over

=item C<socket_args>

Hashref of arguments for L<IO::Socket> configuration. Use C<Domain> to
select a socket class.

=item C<debug>

Debug level (refer to L<Net::REPL::Base>).

=back

=cut

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {
        'socket_args' => {},
        'prompt' => '',
        @_,
    };
    bless($self, $class);

    my $client = $self->_create_socket(
        'Domain' => AF_UNIX,
        'Proto' => 'SOCK_STREAM',
        %{$self->{'socket_args'}},
    );
    if (not $client) {
        die("Can't connect client: $!\n");
    }
    $self->debug("Client PID $$ connected");

    $self->{'fh'} = $client;

    $self->setup_io();

    return $self;
}

sub DESTROY {
    my ($self) = @_;

    if ($self->{'fh'}) {
        $self->{'fh'}->close();
    }
}

=head3 interact()

Run one iteration of the REPL. Returns true if the socket remains open
for further iterations.

Blocks until a line of input can be read from the terminal, sent to
the server, and the result received and printed.

=cut

sub interact {
    my ($self) = @_;

    my $input = $self->read();
    if (not defined($input)) {
        return 0;
    }
    elsif ($input eq '') {
        return 1;
    }

    my $output;
    if ($input =~ m{^/(.*)$}) {
        # $input is a local command
        my ($cmd, $input) = split(/\s+/, $1);
        if ($cmd eq 'local_eval') {
            my @output = $self->formatted_eval($input);
            $output = "@output";
        }
        elsif ($cmd eq 'exit') {
            return 0;
        }
    }
    else {
        # $input is for remote eval
        $self->lv_send($input);
        $output = $self->lv_receive();
        if (not defined($output)) {
            # Server has gone away?
            return 0;
        }
    }

    $self->print($output, "\n");
    return 1;
}

=head3 setup_io()

Create the input source for L<read>() and output for L<print>().

=cut

sub setup_io {
    my ($self) = @_;

    $self->{'term'} ||= Term::ReadLine->new($self->{'prompt'});
    $self->{'out_fh'} = $self->{'term'}->OUT();
}

=head3 read()

Read and return one line of user input.

=cut

sub read {
    my ($self) = @_;

    my $prompt = $self->{'prompt'} . '> ';
    return $self->{'term'}->readline($prompt);
}

=head3 print()

Output the result string(s) from one L<interact>() iteration.

=cut

sub print {
    my ($self, @output) = @_;

    for my $s (@output) {
        print { $self->{'out_fh'} } $s;
    }
}



1;
