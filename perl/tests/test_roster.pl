#!/usr/bin/perl

use strict;
use warnings;

# Test the roster implementation in the Test backend works. Note
# that it is the Test in Thrasher::Backend::Test that is referred
# to in the name of this script. (That is, it's not just a
# redundant statement that this is a test; you may test other
# backends' rosters.)

use Test::More 'no_plan';
use Test::Deep;
use Data::Dumper;

use Thrasher::Test qw(:all);

BEGIN {
    use_ok 'Thrasher::Test', qw(:all);
    use_ok 'Thrasher::Backend';
    use_ok 'Thrasher::Backend::Test';
    use_ok 'Thrasher::Component';
    use_ok 'Thrasher::Roster', qw(:all);
    use_ok 'Thrasher::Protocol::Test';
}

my $backend = new Thrasher::Backend::Test;

my $jid = 'romeo@montague.lit';
my $legacy = 'juliet';

cmp_deeply($backend->get_roster($jid), {},
   'initially we have an empty roster');
$backend->set_roster($jid, {$legacy => $backend->subscribed});
cmp_deeply($backend->get_roster($jid), {$legacy => $backend->subscribed},
   'roster can be set');
$backend->set_roster_user_state($jid, $legacy, 
                                $backend->unsubscribed);
cmp_deeply($backend->get_roster($jid), {},
   'unsubscribing correctly manifests as a removal');
$backend->set_roster_user_state($jid, $legacy,
                                $backend->subscribed);
cmp_deeply($backend->get_roster($jid), {$legacy => $backend->subscribed},
   'correctly remembers that the legacy user is subscribed.');

cmp_deeply(roster_diff({}, {}), {},
           'roster diff compares empties correctly');
cmp_deeply(roster_diff({$legacy => subscribed}, {}),
           {$legacy => [subscribed, unsubscribed]},
           'roster diff handles people no longer being subscribed');
cmp_deeply(roster_diff({}, {$legacy => subscribed}),
           {$legacy => [unsubscribed, subscribed]},
           'roster diff handles people now subscribed');
cmp_deeply(roster_diff({$legacy => want_subscribe},
                       {$legacy => subscribed}),
           {$legacy => [want_subscribe, subscribed]},
           'roster diff handles people with changed status');

# This tests the set_current_legacy_roster by forging an old
# roster, then forging a new one, to ensure that we get the
# behavior we expect.
TEST_ROSTER_SYNC: {
    # Six cases, used in user names for identification:
    # 1. subscribed -> unsubscribed
    # 2. subscribed -> want_subscribe
    # 3. unsubscribed -> subscribed
    # 4. unsubscribed -> want_subscribe
    # 5. want_subscribe -> unsubscribed
    # 6. want_subscribe -> subscribed

    my $component = logged_in_comp;

    my $old_roster = 
    { j1 => subscribed,
      j2 => subscribed,
      # unsubscribed is "not present"
      j5 => want_subscribe,
      j6 => want_subscribe };

    my $new_roster =
    { j2 => want_subscribe,
      j3 => subscribed,
      j4 => want_subscribe,
      j6 => subscribed };

    $component->{protocol}->{backend}->set_roster('romeo@montague.lit',
                                                  $old_roster);

    $component->{protocol}->set_current_legacy_roster
        ($component->session_for('romeo@montague.lit'), $new_roster);

    my $expected = clean_xml(<<'EXPECTED');
<presence from='j1@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribed'/>
<presence from='j1@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribe'/>
<presence from='j2@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribe'/>
<presence from='j2@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribed'/>
<presence from='j2@test.transport' 
          to='romeo@montague.lit' 
          type='subscribe'/>
<presence from='j3@test.transport' 
          to='romeo@montague.lit' 
          type='subscribe'/>
<presence from='j3@test.transport' 
          to='romeo@montague.lit' 
          type='subscribed'/>
<presence from='j4@test.transport' 
          to='romeo@montague.lit' 
          type='subscribe'/>
<presence from='j5@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribe'/>
<presence from='j5@test.transport' 
          to='romeo@montague.lit' 
          type='unsubscribed'/>
<presence from='j6@test.transport' 
          to='romeo@montague.lit' 
          type='subscribe'/>
<presence from='j6@test.transport' 
          to='romeo@montague.lit' 
          type='subscribed'/>
EXPECTED

     is(output, $expected, 
        'set_current_legacy_roster works as expected.');
}

SET_ROSTER_NAME: {
    my $component = logged_in_comp;
    $component->set_roster_name('horatio@denmark.lit',
                                'rosencrantz@denmark.lit',
                                'Rosencrantz', 1);

    my $expected = clean_xml(<<EXPECTED);
<iq from='test.transport' 
    id='id3' 
    to='horatio\@denmark.lit' 
    type='set'>
    <x xmlns='http://jabber.org/protocol/rosterx'>
        <item action='modify' 
              jid='rosencrantz\@denmark.lit' 
              name='Rosencrantz'/>
    </x>
</iq>
EXPECTED

    is($expected, output, 'set roster name appears to work.');
}
