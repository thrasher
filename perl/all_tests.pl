#!/usr/bin/perl 

# This file runs all the test suites. From experience I have learned
# that you want to make sure all files in the test directory are
# accounted for, or you end up forgetting to add tests here.
# Thus, this makes you state a USE or IGNORE flag for every file.
# It is not an error to IGNORE a file that doesn't exist, to
# keep things clean for committing changes to this file before 
# committing the test script itself.

use Test::Harness;
use Data::Dumper;

my @files = glob('tests/*.pl');

sub USE { 1 }
sub IGNORE { 0 }

# This is borrowed from another source, if it seems a bit overblown.
# This makes it mandatory to specify whether you are running a test,
# so you can't forget to add it to the suite, while still making it
# easy to switch tests in and out.
my %FILES = 
    (
     component => USE,
     xmpp_stream_in => USE,
     xmpp_stream_out => USE,
     entity_capabilities => USE,
     pep_avatar => USE,
     vcard => USE,
     test_roster => USE,
     connection_manager => USE,
     debug => USE,
     dbi_backend_mysql_innodb => USE,
     migrate => USE,
     xhtml_normalize => USE
     );

my @no_entries;

@files = map {
    my $filename = $_;
    $filename =~ s/tests\///;
    $filename =~ s/\.pl$//;
    if (!exists($FILES{$filename})) {
        push @no_entries, $filename;
    }
    $FILES{$filename} ? ($_) : ()
    } @files;

if (@no_entries) {
    die "No \%FILES entries found for: " . join(", ", @no_entries);
}

runtests(@files);
