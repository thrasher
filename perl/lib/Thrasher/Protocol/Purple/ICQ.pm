package Thrasher::Protocol::Purple::ICQ;
use strict;
use warnings;

=head1 NAME

Thrasher::Protocol::Purple::AIM - protocol for connection to AOL
Instant Messenger using libpurple

=cut

use base "Thrasher::Protocol::Purple";

use Thrasher::Log qw(log debug);
use CGI qw(escapeHTML);

sub prpl {
    return "prpl-icq";
}

sub process_message {
    my ($self, $body_text) = @_;

    return escapeHTML($body_text);
}

sub process_remote_username {
    my $self = shift;
    my $s = shift;
    $s =~ tr/[A-Z]/[a-z]/;
    $s =~ s/ //g;
    return $s;
}

sub name { "ICQ" }
sub identifier { "icq" }
sub gateway_prompt { "Enter ICQ identifier:" }
sub gateway_desc { "Enter ICQ identifier:" }

1;
